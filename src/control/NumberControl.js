(function () {

    var NumberControl = function (settings) {
        NumberControl.superclass.call(this, settings);
        /**
         * @event onKeyUp
         * Fired when a pressed key is released.
         * @param {Object} event The event object.
         */
        this.onKeyUp = null;
        this.mask = null;
        this.percentage = null;
        this.filterKey = null;
        this.maskList = [];
        this.expRegList = [];
        this.expRegMask = [];
        this.optionDecimal = null;
        this.decimals = null;
        this.valueInit = null;
        this.onlyNumber = null;

        this.onHandlerMaskKeyDown = null;
        this.onHandlerMaskKeyPress = null;
        NumberControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.TextControl', NumberControl);

    NumberControl.prototype.type = "TextControl";
    /**
     * Initializes the object.
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    NumberControl.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 524288,
            onKeyUp: null,
            optionDecimal: null,
            valueInit: "",
            mask: "###,###,###,###.###########"
        };

        $.extend(true, defaults, settings);
        this.setMaskDefault();
        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setOnKeyUpHandler(defaults.onKeyUp)
            .setOptionDecimal(defaults.optionDecimal)
            .setMask(defaults.mask);

        this.mask = defaults.mask;

    };

    /**
     * Sets the callback function to be called when the keyup event occurs.
     * @chainable
     */

    NumberControl.prototype.evalMask = function (textvalue) {
        var i;

        for (i = 0; i < this.expRegList.length; i += 1) {
            if (this.expRegList[i].test(textvalue))
                return true;
        }
        return false;
    };

    NumberControl.prototype.setOptionDecimal = function (option) {
        if (typeof option === "string")
            this.optionDecimal = option;
        return this;
    };


    NumberControl.prototype.evalMaskNumeric = function (textvalue) {
        var sw = true,
            i;

        if (this.expRegMask === undefined) {
            return true;
        }
        else {
            for (i = 0; i < this.expRegMask.length; i += 1) {
                if (this.expRegMask[i][2] != null) {
                    if (!this.expRegMask[i][2].test(textvalue))
                        sw = false;
                }

            }
            return sw;
        }
    };

    NumberControl.prototype.truncate = function (number, digits) {
        var multiplier = Math.pow(10, digits),
            adjustedNum = number * multiplier,
            truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);
        return truncatedNum / multiplier;
    };


    NumberControl.prototype.setValue = function (strControl) {
        var strAux;
        this.valueInit = strControl;
        if (this.isNumber(strControl)) {
            if (strControl != null && strControl != "") {
                if (this.evalMaskNumeric(strControl) || strControl == "") {
                    NumberControl.superclass.prototype.setValue.call(this, strControl);
                    //this.setValue(strControl);
                }
                else {
                    if (this.optionDecimal == "truncate") {

                    }

                    strControl = this.removeNumberCommas(strControl);

                    if (this.optionDecimal == "truncate")
                        strAux = this.truncate(parseFloat(strControl), this.decimals);
                    if (this.optionDecimal == "round")
                        strAux = Math.ceil(strControl * ('1e' + this.decimals)) / ('1e' + this.decimals);


                    strAux = this.setNumberCommas(strAux == undefined ? "" : strAux.toString());
                    if (this.evalMaskNumeric(strAux)) {
                        NumberControl.superclass.prototype.setValue.call(this, strAux);
                        // this.setValue(strAux);
                    }
                }
            }
        }
        return this;

    };

    NumberControl.prototype.getValue = function (control) {
        var value,
            array,
            result,
            i;

        if (this.valueInit != null) {
            if (control)
                return this.removeNumberCommas(this.valueInit);
            else {
                if (this.html)
                    return this.html.value;
                else
                    return "";
            }
        }
        if (control) {
            value = this.getValue();
            array = value.split(",");
            result = "";
            for (i = 0; i < array.length; i += 1) {
                result = result + array[i];
            }
            return result;
        }

        return NumberControl.superclass.prototype.getValue.call(this);
    };

    NumberControl.prototype.isNumber = function (strControl) {
        var validator = /^[\d\,\.]*$/;
        if (validator.test(strControl))
            return true;
        else
            return false;
    };

    NumberControl.prototype.removeNumberCommas = function (strControl) {
        var pint = this.getParseInt(strControl),
            pdec = this.getParseDec(strControl);
        if (pdec != undefined)
            strControl = pint + "." + pdec;
        else
            strControl = pint;
        return strControl;
    };

    NumberControl.prototype.setNumberCommas = function (strControl) {
        var pint = this.getParseInt(strControl),
            pdec = this.getParseDec(strControl),
            array,
            arraypos,
            a1,
            a2,
            strFinal = "",
            i;
        array = pint.split(",");
        for (i = 0; i <= array.length - 1; i += 1) {
            strFinal += array[i];
        }
        arraypos = strFinal.length;
        while (arraypos > 3) {
            a1 = strFinal.substr(0, arraypos - 3);
            a2 = strFinal.substr(arraypos - 3, strFinal.length);
            strFinal = a1 + "," + a2;
            arraypos++;
            arraypos = arraypos - 4;
        }
        if (pdec != undefined)
            return strFinal + "." + pdec;
        else
            return strFinal;
    };

    NumberControl.prototype.setMaskDefault = function () {
        this.expRegMask.push([/^\#{0,3}(\,\#{3})*\%{0,1}$/, '^\\d{0,3}(\\,\\d{3}){@n}\%{0,1}$', null, false]);   // captura de numeros sin decimal con comas
        this.expRegMask.push([/^[\#\,0]+(?:(\.)([\#0]*)){1}(\%){0,1}$/, '^[\\d\\,]+(\\.){0,1}(\\d{@n}){0,1}(\%){0,1}$', null, false]);   // decimales
        this.expRegMask.push([/^[\#0]{0,3}(\,[\#0]{3})*(?:(\.)([\#0]*))(\%){0,1}$/, '^\\d{@x}(\\,\\d{3}){@n}((\\.)(\\d*)){0,1}(\%){0,1}$', null, false]);   // parte entera
        this.expRegMask.push([/^[\#\,]*(0*)\.(0*)(\%){0,1}$/, '^[\\d\\,]*(0{@x})\\.([\\d]{@y})(\%){0,1}$', null, false]);   // zeros fijos
        this.expRegMask.push([/^[\#\,\.]*\%{1}$/, '^[\\d\\,\\.]*\\%{1}$', null, false]);   // percentage
        this.expRegMask.push([/^\#*\%{0,1}$/, '^\\d{@n}\%{0,1}$', null, false]);   // captura de numeros sin decimal con comas

    };

    NumberControl.prototype.setMaskNumeric = function (mask) {
        var array,
            ngroups,
            ini,
            nreg,
            nx,
            ny,
            a;

        if (this.expRegMask[0][0].test(mask)) {
            array = this.expRegMask[0][0].exec(mask);
            ngroups = array[0].split(",").length - 1;
            this.expRegMask[0][2] = new RegExp(this.expRegMask[0][1].replace(/@n/g, '0,' + ngroups.toString()));
            this.expRegMask[0][3] = true;
        }
        if (this.expRegMask[1][0].test(mask)) {
            array = this.expRegMask[1][0].exec(mask);
            ngroups = array[2].length;
            this.decimals = ngroups;
            this.expRegMask[1][2] = new RegExp(this.expRegMask[1][1].replace(/@n/g, '0,' + ngroups.toString()));
            this.expRegMask[1][3] = true;
        }
        if (this.expRegMask[2][0].test(mask)) {
            array = this.expRegMask[2][0].exec(mask);
            ngroups = array[0].split(",").length - 1;
            ini = this.expRegMask[2][0].exec("##,###.##")[0].split(",")[0].length
            nreg = this.expRegMask[2][1].replace(/@n/g, '0,' + ngroups.toString());
            this.expRegMask[2][2] = new RegExp(nreg.replace(/@x/g, '0,3'));
            this.expRegMask[2][3] = true;
        }
        if (this.expRegMask[3][0].test(mask)) {
            array = this.expRegMask[3][0].exec(mask);
            nx = array[1].length;
            ny = array[2].length;
            a = this.expRegMask[3][1].replace(/@x/g, '0,' + nx.toString());
            a = a.replace(/@y/g, '0,' + ny.toString());
            this.expRegMask[3][2] = new RegExp(a);
            this.expRegMask[3][3] = true;
        }
        //percentage
        if (this.expRegMask[4][0].test(mask)) {
            this.expRegMask[4][2] = new RegExp(this.expRegMask[4][1]);
            this.expRegMask[4][3] = true;
            this.percentage = true;
        }
        //number not comas
        if (this.expRegMask[5][0].test(mask)) {
            array = this.expRegMask[5][0].exec(mask);
            nx;
            if (this.percentage)
                nx = array[0].length - 1;
            else
                nx = array[0].length;


            this.expRegMask[5][2] = new RegExp(this.expRegMask[5][1].replace(/@n/g, '0,' + nx.toString()));
            this.expRegMask[5][3] = true;
            this.onlyNumber = true;
        }


    };

    NumberControl.prototype.setMaskHandlerNumeric = function (mask) {
        //handler numeric
        var that = this,
            thatMask = mask;
        handlerKeyDown = function (e) {
            var pressKey,
                textValue,
                regResult,
                selStart,
                selEnd,
                strStart,
                strEnd,
                strControl = "",
                textLength,
                strFinal,
                pressKey = (window.event) ? window.event.keyCode : e.which,
                onlynumbers,
                dash,
                flagequal,
                flagZeros,
                pint,
                pdec;

            textValue = that.html.value;
            selStart = that.html.selectionStart;
            selEnd = that.html.selectionEnd;
            textLength = that.html.value.length;
            onlynumbers = that.expRegMask[0][3];
            dash = that.expRegMask[1][3];
            flagequal = false;
            flagZeros = that.expRegMask[3][3];
            if (selStart == selEnd) {
                flagequal = true;
            }

            if ((pressKey > 46 && pressKey <= 57) || (pressKey >= 96 && pressKey <= 105) || pressKey == 8 || pressKey == 190 || pressKey == 37 || pressKey == 39) {
                this.valueInit = null;
                //quitar porcentaje
                if (this.percentage)
                    textValue = textValue.substr(0, textLength - 1);


                if (pressKey >= 96 && pressKey <= 105)
                    pressKey = pressKey - 48;
                if (pressKey > 45 && pressKey <= 57) {
                    if (selStart == textLength) {
                        strControl += textValue + String.fromCharCode(pressKey);
                        selStart = selStart + 1;
                    }
                    else {
                        strStart = textValue.substr(0, selStart);
                        strEnd = textValue.substr(selStart, textLength);
                        strControl = strStart + String.fromCharCode(pressKey) + strEnd;
                        selStart++;
                    }
                }
                if (pressKey == 8) {
                    if (flagequal && selStart == textLength) {
                        strControl = textValue.substr(0, selStart - 1);
                    }
                    else {
                        strStart = textValue.substr(0, selStart - 1);
                        strEnd = textValue.substr(selStart, textLength);
                        strControl = strStart + strEnd;
                        selStart--;
                    }
                }
                if (onlynumbers && pressKey == 190) {
                    strControl = textValue;
                }
                if (pressKey == 37) {
                    selStart = selStart - 1;
                    strControl = textValue;

                }
                if (pressKey == 39) {
                    if (selStart == selEnd) {
                        selEnd++;
                    }
                    selStart = selStart + 1;
                    strControl = textValue;
                }
                if (flagZeros) {
                    strControl = that.setZeros(strControl, thatMask);
                }

                //convierto con comas
                if (this.onlyNumber) {
                    if (pressKey == 190) {
                        strFinal = textValue;
                    }
                    if (pressKey != 190) {
                        strFinal = strControl;
                    }
                }
                else {
                    pint = that.getParseInt(strControl);
                    pdec = that.getParseDec(strControl);
                    selStart = selStart - (strControl.split(",").length - 1);
                    strFinal = that.setComas(pint);
                    selStart = selStart + (strFinal.split(",").length - 1);
                    if (pdec != undefined) {
                        strFinal = strFinal + "." + pdec;
                        if (flagequal && selStart > selEnd)
                            selEnd = selStart;
                        else {
                            selEnd = selStart;
                        }
                    }
                }
                if (dash && pressKey == 190) {
                    strControl += textValue + ".";
                    strFinal = strControl;
                    selStart = selStart + 1;
                }
            }
            else {
                strFinal = this.html.value;
            }

            //agregar signo de porcentaje
            if (this.percentage) {
                strFinal += "%";
            }

            if (this.evalMaskNumeric(strFinal) || strFinal == "") {
                that.html.value = strFinal;
                that.html.selectionStart = selStart;
                if (flagequal) {
                    that.html.selectionEnd = selStart;
                }
            }
            e.preventDefault();
        }
        this.onHandlerMaskKeyDown = handlerKeyDown;
    };


    NumberControl.prototype.setComas = function (strControl) {
        var array,
            arraypos,
            a1,
            a2,
            i,
            strFinal = "";

        array = strControl.split(",");
        for (i = 0; i <= array.length - 1; i += 1) {
            strFinal += array[i];
        }
        arraypos = strFinal.length;
        while (arraypos > 3) {
            a1 = strFinal.substr(0, arraypos - 3);
            a2 = strFinal.substr(arraypos - 3, strFinal.length);
            strFinal = a1 + "," + a2;
            arraypos++;
            arraypos = arraypos - 4;
        }
        return strFinal;
    };

    NumberControl.prototype.getParseInt = function (strControl) {
        var aux1,
            aux2,
            array,
            strFinal = "",
            i;

        aux1 = strControl.split(".")[0];
        array = aux1.split(",");
        for (i = 0; i <= array.length - 1; i += 1) {
            strFinal += array[i];
        }
        return strFinal;
    };

    NumberControl.prototype.getParseDec = function (strControl) {
        var aux1,
            aux2,
            array,
            strFinal = "";
        aux1 = strControl.split(".")[1];
        return aux1;
    }

    NumberControl.prototype.setZeros = function (strControl, mask) {
        var i,
            aux1,
            aux2,
            array,
            strFinal = "",
            nZerosLeft,
            nZerosRight,
            nRightStr;
        array = this.expRegMask[3][0].exec(mask);
        nZerosLeft = array[1].length;
        nZerosRight = array[2].length;

        if (strControl.split(".").length != 1) {
            nRightStr = strControl.split(".")[1].length;
            if (nRightStr < nZerosRight) {
                for (i = 0; i < nZerosRight - nRightStr; i += 1) {
                    strControl += "0";
                }
                return strControl;
            }
            if (nRightStr > nZerosRight) {
                strControl = strControl.substr(0, strControl.length - 1);
            }
            return strControl;
        }
        else {
            if (strControl == "")
                return array[1] + "." + array[2];
            else {
                return strControl + "." + array[2];
            }
        }
        return aux1;
    }

    NumberControl.prototype.setMask = function (mask) {
        var expMaskPercentage = /^(\#+)(?:(\.)(\#*))?\%$/i,
            expMaskNumber1 = /^(\#+)(?:(\.)(\#*))?$/i,
            expMaskNumber2 = /^(\#{0,3})(?:(\,)(\#{3}))*(?:(\.)(\#*))?$/i;
        this.onHandlerMaskKeyDown = null;
        this.onHandlerMaskKeyPress = null;
        this.maskList = [];
        this.expRegMask[0][3] = false;
        this.expRegMask[1][3] = false;
        this.expRegMask[2][3] = false;
        this.expRegMask[3][3] = false;
        this.expRegMask[4][3] = false;
        this.expRegMask[0][2] = null;
        this.expRegMask[1][2] = null;
        this.expRegMask[2][2] = null;
        this.expRegMask[3][2] = null;
        this.expRegMask[4][2] = null;
        // Save the list of mask
        if (typeof mask === "string") {
            this.percentage = null;
            this.onlyNumber = null;
            this.maskList.push(mask);
            this.setMaskNumeric(mask);
            this.setMaskHandlerNumeric(mask);
        }
    };
    /**
     * Set the events for the object.
     * @chainable
     */
    NumberControl.prototype.defineEvents = function () {
        var that = this;

        if (this.html && !this.eventsDefined) {
            NumberControl.superclass.prototype.defineEvents.call(this);
            this.addEvent('keyup').listen(this.html, function (e) {
                if (typeof that.onKeyUp === 'function') {
                    that.onKeyUp(e);
                }
            });
            this.addEvent('keydown').listen(this.html, function (e) {
                if (e.which === PMUI.keyCodeF5) {
                    this.blur();
                    e.preventDefault();
                    window.location.reload(true);
                }
                if (typeof that.onHandlerMaskKeyDown === 'function') {
                    that.onHandlerMaskKeyDown(e);
                }
            });
            this.addEvent('keypress').listen(this.html, function (e) {
                if (typeof that.onHandlerMaskKeyPress === 'function') {
                    that.onHandlerMaskKeyPress(e);
                }
            });
            this.eventsDefined = true;
        }
        return this;
    };
    /**
     * Creates the HTML element for the object
     * @return {HTMLElement}
     */
    NumberControl.prototype.createHTML = function () {
        NumberControl.superclass.prototype.createHTML.call(this);
        this.html.type = "text";
        this.setPlaceholder(this.placeholder)
            .setMaxLength(this.maxLength)
            .setReadOnly(this.readonly);

        //this.defineEvents();
        this.setMask(this.maskList[0]);

        return this.html;
    };
    /**
     * Sets if the control will be enabled for read only.
     * @param {Boolean}
     * @chainable
     */

    PMUI.extendNamespace('PMUI.control.NumberControl', NumberControl);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = NumberControl;
    }
}());