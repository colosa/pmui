//***********************other class******************************************************//
describe('PMUI.ui.MessageWindow', function () {
	var a;
	beforeEach(function () {
		a = new PMUI.ui.MessageWindow(
											{
												message: "this is a window message!"
											}
				);
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('method "setMessage"', function () {
		it('should set the message for the message window', function () {
			expect(a.message).toContain('this is a window message!');
			expect($(a.dom.messageContainer).text() === 'this is a window message!').toBeTruthy();
			expect(function () {a.setMessage(434)}).toThrow();
			expect(function () {a.setMessage(new PMUI.core.Base())}).toThrow();
			expect(function () {a.setMessage({data1:"data1"})}).toThrow();
			expect(function () {a.setMessage("new message!");}).not.toThrow();
			expect(a.message).toContain('new message!');
			expect($(a.dom.messageContainer).text() === 'new message!').toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});