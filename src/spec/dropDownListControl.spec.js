//***********************other class******************************************************//
describe('PMUI.control.DropDownListControl', function() {
    var control, settings = {
        options: [
            {
                label: "one",
                value: 1
            },
            {
                label: "two",
                value: 2
            },
            {
                label: "three",
                value: 3,
                selected: true
            },
            {
                label: "aGroup",
                options: [
                    {
                        value: "four"
                    }, 
                    {
                        value: "five"
                    }
                ]
            }
        ],
        value: 2
    }, newOption = {
        label: "newLabel",
        value: "newValue",
        selected: true
    };

    beforeEach(function() {
        control = new PMUI.control.DropDownListControl(settings);
    });

    afterEach(function() {
        jQuery(control.getHTML()).remove();
        control = null;
    });

    describe("class behavior", function() {
        it('[US-24, a] should create a new instance with all the default options', function() {
            expect(control instanceof PMUI.control.DropDownListControl).toBeTruthy();
        });

        it('[US-24, b] should create a new instance with all the default options', function() {
            expect(control.getHTML().tagName.toLowerCase()).toEqual("select");
        });

        it('[US-24, c] should add/remove options', function() {
            var options = control.getOptions().length,
                directOptions;
            control.addOption({ value: "newOne"});
            expect(control.getOptions().length).toBe(options + 1);
            control.removeOption("newOne");
            expect(control.getOptions().length).toBe(options);
            //add/remove option groups
            directOptions = control.getOptions(true).length;
            control.addOptionGroup({
                label: "myGroup"
            });
            expect(control.getOptions(true).length).toBe(directOptions + 1);
            control.removeOption("myGroup");
            expect(control.getOptions().length).toBe(options);
        });
    });

    describe("constructor", function() {
        it("Should create a new instance of the class applying all the specified settings", function() {
            var options = control.getOptions(true),
                initialOptions = settings.options;
            expect(control instanceof PMUI.control.DropDownListControl).toBeTruthy();
            expect(options.length).toBe(settings.options.length);
            expect(options[0].label).toEqual(initialOptions[0].label.toString());
            expect(options[0].value).toEqual(initialOptions[0].value.toString());
            expect(options[1].label).toEqual(initialOptions[1].label.toString());
            expect(options[1].value).toEqual(initialOptions[1].value.toString());
            expect(options[2].label).toEqual(initialOptions[2].label.toString());
            expect(options[2].value).toEqual(initialOptions[2].value.toString());
            expect(control.getValue()).toEqual(settings.value.toString());
        });
    });

    describe('method "clearOptions"', function() {
        it("should clear all the options from the control", function(){
            control.clearOptions();
            expect(control.getOptions().length).toEqual(0);
            expect(jQuery(control.getHTML()).find('option').length).toEqual(0);
        });
    });

    describe('method "getValue"', function() {
        it("should return the control's current value", function() {
            expect(control.getValue()).toEqual(settings.value.toString());
        });
    });

    describe('method "addOption"', function() {
        it("should add a new option to the control", function() {
            control.clearOptions();
            control.addOption(newOption);
            expect(control.getOptions().length).toEqual(1);
            expect(jQuery(control.getHTML()).find('option').length).toEqual(1);
            expect(control.getValue()).toEqual(newOption.value);
        });
        it("should add a new option into a option group", function() {
            var i = control.getOptions(true)[3].options.length;
            control.addOption({value: "uhmm"}, "aGroup");
            expect(control.getOptions(true)[3].options.length).toBe(i + 1);
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').find('>option[value="uhmm"]').length).toBe(1);
        });
    });
    describe('method "enableDisableOption"', function() {
        it("should enable options and option groups using an index", function() {
            control.enableDisableOption(false, 0);
            expect(control.options[0].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('>*').eq(0).is(':disabled')).toBeFalsy(); 
        });
        it("should enable an option using a value.", function() {
            control.enableDisableOption(false, "2");
            expect(control.options[1].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('>option[value="2"]').is(':disabled')).toBeFalsy();
        });
        it("should enable an option using a label.", function() {
            control.enableDisableOption(false, "aGroup");
            expect(control.options[3].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').get(0).disabled).toBeFalsy();
        });
        it("should enable an option from a option group using an index", function() {
            control.enableDisableOption(false, 1, "aGroup");
            expect(control.options[3].options[1].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').find('option').eq(1).get(0).disabled).toBeFalsy(); 
        });
        it("should enable an option from a option group using a value", function() {
            control.enableDisableOption(false, "four", "aGroup");
            expect(control.options[3].options[0].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').find('option[value="four"]').get(0).disabled).toBeFalsy(); 
        });
        it("should enable an option searching only in direct child options", function(){
            control.enableDisableOption(false, {criteria: "1", applyTo: "options"});
            expect(control.options[0].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('option[value="1"]').is(':disabled')).toBeFalsy();
        });
        it("should enable an option group using an object", function(){
            control.enableDisableOption(false, {criteria: "aGroup", applyTo: "groups"});
            expect(control.options[3].disabled).toBeFalsy();
            expect(jQuery(control.getHTML()).find('optgroup[label="aGroup"]').get(0).disabled).toBeFalsy();
        });
        it("should disable an option using an index.", function() {
            control.enableDisableOption(true, 0);
            expect(control.options[0].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('>*').eq(0).is(':disabled')).toBeTruthy();
        });
        it("should disable an option using a value.", function() {
            control.enableDisableOption(true, "2");
            expect(control.options[1].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('>option[value="2"]').is(':disabled')).toBeTruthy();
        });
        it("should disable an option using a label.", function() {
            control.enableDisableOption(true, "aGroup");
            expect(control.options[3].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').get(0).disabled).toBeTruthy();
        });
        it("should disable an option from a option group using an index", function() {
            control.enableDisableOption(true, 1, "aGroup");
            expect(control.options[3].options[1].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').find('option').eq(1).get(0).disabled).toBeTruthy(); 
        });
        it("should disable an option from a option group using a value", function() {
            control.enableDisableOption(true, "four", "aGroup");
            expect(control.options[3].options[0].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('>optgroup[label="aGroup"]').find('option[value="four"]').get(0).disabled).toBeTruthy(); 
        });
        it("should disable an option searching only in direct child options", function(){
            control.enableDisableOption(true, {criteria: "1", applyTo: "options"});
            expect(control.options[0].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('option[value="1"]').is(':disabled')).toBeTruthy();
        });
        it("should disable an option group using an object", function(){
            control.enableDisableOption(true, {criteria: "aGroup", applyTo: "groups"});
            expect(control.options[3].disabled).toBeTruthy();
            expect(jQuery(control.getHTML()).find('optgroup[label="aGroup"]').get(0).disabled).toBeTruthy();
        });
    });

    describe('method "disableOption"', function() {
        beforeEach(function() {
            spyOn(control, 'enableDisableOption');    
        });
        
        it("should disable an option using an index.", function() {
            control.disableOption(0);
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, 0, undefined);
        });
        it("should disable an option using a value.", function() {
            control.disableOption("2");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, "2", undefined);
        });
        it("should disable an option using a label.", function() {
            control.disableOption("aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, "aGroup", undefined);
        });
        it("should disable an option from a option group using an index", function() {
            control.disableOption(0, "aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, 0, "aGroup");
        });
        it("should disable an option from a option group using a value", function() {
            control.disableOption("five", "aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, "five", "aGroup");
        });
        it("should enable an option searching only in direct child options", function(){
            control.disableOption({criteria: "1", applyTo: "options"});
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, {criteria: "1", applyTo: "options"}, undefined);
        });
        it("should enable an option group using an object", function(){
            control.disableOption({criteria: "aGroup", applyTo: "groups"});
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(true, {criteria: "aGroup", applyTo: "groups"}, undefined);
        });
    });

    describe('method "enableOption"', function() {
        beforeEach(function() {
            spyOn(control, 'enableDisableOption');    
        });
        it("should enable an option using an index.", function() {
            control.enableOption(0);
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, 0, undefined);
        });
        it("should enable an option using a value.", function() {
            control.enableOption("2");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, "2", undefined);
        });
        it("should enable an option using a label.", function() {
            control.enableOption("aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, "aGroup", undefined);
        });
        it("should enable an option from a option group using an index", function() {
            control.enableOption(0, "aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, 0, "aGroup");
        });
        it("should enable an option from a option group using a value", function() {
            control.enableOption("five", "aGroup");
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, "five", "aGroup");
        });
        it("should enable an option searching only in direct child options", function(){
            control.enableOption({criteria: "1", applyTo: "options"});
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, {criteria: "1", applyTo: "options"}, undefined);
        });
        it("should enable an option group using an object", function(){
            control.enableOption({criteria: "aGroup", applyTo: "groups"});
            expect(control.enableDisableOption).toHaveBeenCalled();
            expect(control.enableDisableOption).toHaveBeenCalledWith(false, {criteria: "aGroup", applyTo: "groups"}, undefined);
        });
    });
    
    describe('method "removeOption"', function() {
        it("should remove an option using an index.", function() {
            var aux = control.getOptions(true)[0],
                value = control.getOptions(true)[0].value;
            control.removeOption(0);
            expect(control.getOptions(true) === aux).toBeFalsy();
            expect(jQuery(control.getHTML()).find('option[value="' + value + '"]').length).toBe(0);
        });
        it("should remove an option using a value (for option)", function() {
            var value = "two", i, j, flag = true;
            control.removeOption(value);
            for(i = 0; i < control.options.length; i++) {
                if(!control.options[i].isGroup && control.options[i].value === value) {
                    flag = false;
                    break;
                }
            }
            expect(flag).toBeTruthy();
            expect(jQuery(control.getHTML()).find('option[value="' + value + '"]').length).toBe(0);
        });
        it("should remove an option using a label (for option group)", function() {
            var value = "aGroup", i, j, flag = true;
            control.removeOption(value);
            for(i = 0; i < control.options.length; i++) {
                if(control.options[i].isGroup && control.options[i].label === value) {
                    flag = false;
                    break;
                }
            }
            expect(flag).toBeTruthy();
            expect(jQuery(control.getHTML()).find('optgroup[label="' + value + '"]').length).toBe(0);
        });
    });
    describe('method "addOptionGroup"', function() {
        it("It should add an option group.", function() {
            var newGroup = {label: "new group"};
            control.addOptionGroup(newGroup);
            expect(control.options[control.options.length - 1].label === newGroup.label && control.options[control.options.length - 1].isGroup).toBeTruthy();
            expect(jQuery(control.getHTML()).find('optgroup[label="' + newGroup.label + '"]').length).toBeGreaterThan(0);
        });
    });
    describe('method "getSelectedLabel"', function() {
        it("it should return the label of the selected option", function() {
            expect(control.getSelectedLabel()).toBe("two");
            expect(control.getSelectedLabel()).toBe(jQuery(control.getHTML()).find('option[value="2"]:selected').attr("label"));
            control.setValue("1");
            expect(control.getSelectedLabel()).toBe("one");
            expect(control.getSelectedLabel()).toBe(jQuery(control.getHTML()).find('option[value="1"]:selected').attr("label"));
        });
    });
    describe('method "setOptions"', function() {
        it("it should set the options properly", function() {
            var flag = true, i, options = [
                {
                    label: "letter B",
                    value: "B"
                },
                {
                    label: "letter C",
                    value: "C"
                },
                {
                    label: "Vowels",
                    options: [
                        {
                            value: "A"
                        }
                    ]
                }
            ], html, children;
            control.setOptions(options);
            expect(control.options[0].value).toBe("B");
            expect(control.options[0].label).toBe("letter B");
            expect(control.options[0].isGroup).toBeFalsy();
            expect(control.options[1].value).toBe("C");
            expect(control.options[1].label).toBe("letter C");
            expect(control.options[1].isGroup).toBeFalsy();
            expect(control.options[2].label).toBe("Vowels");
            expect(control.options[2].isGroup).toBeTruthy();
            expect(control.options[2].options[0].value).toBe("A");
            expect(control.options[2].options[0].label).toBe("A");
            expect(control.options[2].options[0].isGroup).toBeFalsy();
            html = jQuery(control.getHTML());
            children = html.find('>*');
            expect(children[0].value).toBe("B");
            expect(children[0].label).toBe("letter B");
            expect(children[1].value).toBe("C");
            expect(children[1].label).toBe("letter C");
            expect(children[2].label).toBe("Vowels");
            html = jQuery(children[2]);
            children = html.find('>*');
            expect(children[0].value).toBe("A");
            expect(children[0].label).toBe("A");
        });
    });
    describe('method "getOptions"', function() {
        it("should return all the options from the control.", function() {
            var options = control.getOptions();
            expect(options.length).toBe(5);
            expect(options[0].value).toBe("1");
            expect(options[0].label).toBe("one");
            expect(options[0].isGroup).toBeFalsy();
            expect(options[1].value).toBe("2");
            expect(options[1].label).toBe("two");
            expect(options[0].isGroup).toBeFalsy();
            expect(options[2].value).toBe("3");
            expect(options[2].label).toBe("three");
            expect(options[0].isGroup).toBeFalsy();
            expect(options[3].value).toBe("four");
            expect(options[3].label).toBe("four");
            expect(options[0].isGroup).toBeFalsy();
            expect(options[4].value).toBe("five");
            expect(options[4].label).toBe("five");
            expect(options[0].isGroup).toBeFalsy();
        });
        it('should return only the direct children from the control', function() {
            var options = control.getOptions(true);
            expect(options.length).toBe(settings.options.length);
            expect(options[0].value).toBe("1");
            expect(options[0].label).toBe("one");
            expect(options[0].isGroup).toBeFalsy();
            expect(options[1].value).toBe("2");
            expect(options[1].label).toBe("two");
            expect(options[1].isGroup).toBeFalsy();
            expect(options[2].value).toBe("3");
            expect(options[2].label).toBe("three");
            expect(options[2].isGroup).toBeFalsy();
            expect(options[3].label).toBe("aGroup");
            expect(options[3].isGroup).toBeTruthy();
        });
    });
    describe('method "defineEvents"', function() {
        it("should add the 'change' event", function() {
            var key, eventName;
            control.defineEvents();
            for(key in control.events) {
                eventName = control.events[key].eventName;
                break;
            }
            expect(eventName).toBe("change");
        });
    });
    describe('method "createHTML"', function() {
        it("should create the html for the object", function() {
            var children, i, options, groups, j;
            control.getHTML();
            expect(control.html.tagName.toLowerCase()).toBe("select");
            children = jQuery(control.html).find('>*');
            expect(children.length).toBe(4);
            expect(children.filter('option').length).toBe(3);
            expect(children.filter('optgroup').length).toBe(1);
            for(i = 0; i < children.length; i++) {
                expect(jQuery(children[i]).attr("label")).toEqual(settings.options[i].label);
            }
            options = children.filter('option');
            for(i = 0; i < options.length; i++) {
                expect(jQuery(options[i]).attr("value")).toEqual(settings.options[i].value.toString());
                expect(jQuery(options[i]).attr("label")).toEqual(settings.options[i].label || settings.options[i].value.toString());
            }
            for(i = 0; i < children.length; i++) {
                if(children[i].tagName.toLowerCase() === 'optgroup') {
                    options = jQuery(children[i]).find('option');
                    for(j = 0; j < options.length; j++) {
                        expect(jQuery(options[j]).attr("label")).toEqual(settings.options[i].options[j].label || settings.options[i].options[j].value);
                        expect(jQuery(options[j]).attr("value")).toEqual(settings.options[i].options[j].value);
                    }
                }
            }
        });
    });

});