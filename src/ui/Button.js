(function () {
    /**
     * @class PMUI.ui.Button
     * @extend PMUI.core.Element
     *   * Usage example (only for subclasses since this is an abstract class):
     *
     *          @example
     *          //Remember, this is an abstract class so it shouldn't be instantiate,
     *          //anyway we are instantiating it just for this example
     *
     *          var a;
     *
     *           a = new PMUI.ui.Button({
     *               handler : function () {
     *                   alert("hi!");     
     *               },
     *               text : 'closed',
     *               iconClass:"pmui-gridpanel-pager_previous",
     *               iconPosition : "left",
     *               messageTooltip : 'this button to close the window'
     *           });
     *
     *           document.body.appendChild(a.getHTML());
     *           a.defineEvents();
     *
     * @constructor
     * Create a new instace of the Window class
     * @param {Object} settigs Constructor object
     */
    var Button = function (settings, parent) {
        Button.superclass.call(this, jQuery.extend(settings, {elementTag: "a"}));
        /**
         * @property icon for the Button, it can a string
         * @type {String}
         */
        this.icon = null;
        /**
         * @property {PMUI.event.Action} Defines the Action Object to handle he action windows
         * @type {Action}
         */
        this.action = null;
        /**
         * @property parent
         * @type {String}
         */
        this.parent = null;
        /**
         * @property text of the Button
         * @type {String}
         */
        this.text = null;
        /**
         * @property alias Button for idetified to alias
         * @type {String}
         */
        this.aliasButton = null;
        /**
         * [iconPosition description]
         * @type {[type]}
         */
        this.iconPosition = null;
        /**
         * [messageTooltip description]
         * @type {[type]}
         */
        this.messageTooltip = null;

        //this.buttonClass = null;

        //this.foreignClassName = null;

        this.linkStyle = null;

        this.labelVisible = null;
        this.iconVisible = null;
        this.disabled = null;
        this.dom = null;
        this.buttonType = null;

        Button.prototype.init.call(this, settings, parent);
    };

    PMUI.inheritFrom('PMUI.core.Element', Button);

    Button.prototype.type = 'Button';
    Button.prototype.family = 'Button';

    Button.prototype.init = function (settings, parent) {
        var defaults;

        defaults = {
            iconClass: '',
            aliasButton: null,
            parent: parent || null,
            height: "auto",
            width: "auto",
            handler: function () {
            },
            text: '[button]',
            iconPosition: "right",
            tooltip: false,
            messageTooltip: "",
            //buttonClass : '',
            iconVisible: true,
            labelVisible: true,
            disabled: false,
            //cls: 'pmui-button',
            //linkStyle: false,
            //foreignClassName : "",
            //useForeignClass : false
            buttonType: 'default'
        };
        this.dom = {};

        jQuery.extend(true, defaults, settings);

        this.setAliasButton(defaults.aliasButton)
            .setParent(defaults.parent)
            .setText(defaults.text)
            .setIcon(defaults.iconClass)
            .setWidth(defaults.width)
            .setHeight(defaults.height)
            .setIconPosition(defaults.iconPosition)
            .setHandler(defaults.handler)
            .setTooltipMessage(defaults.messageTooltip)
            .setDisabled(defaults.disabled)
            .setButtonType(defaults.buttonType);

    };

    Button.prototype.setButtonType = function (buttonType) {
        var classButton = "";

        if (typeof buttonType !== "string") {
            throw  new Error("setButtonType(): the type value is not valid");
        }
        this.buttonType = buttonType;
        this.style.removeClasses(["pmui-error", "pmui-warning", "pmui-success", "pmui-info", "pmui-link"]);
        classButton = 'pmui-' + this.buttonType;
        switch (this.buttonType) {
            case 'error':
                this.style.addClasses([classButton]);
                break;
            case 'warning':
                this.style.addClasses([classButton]);
                break;
            case 'success':
                this.style.addClasses([classButton]);
                break;
            case 'info':
                this.style.addClasses([classButton]);
                break;
            case 'link':
                this.style.addClasses([classButton]);
                break;
        }
        return this;
    };

    Button.prototype.showLabel = function () {
        this.labelVisible = true;
        if (this.html) {
            this.label.style.display = '';
        }
        return this;
    };

    Button.prototype.setDisabled = function (value) {
        if (typeof value !== 'boolean') {
            throw new Error('setDisabled(): the parameter is not valid, should be type boolean');
        }
        this.disabled = value;
        if (this.html) {
            if (this.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        }
        return this;
    }

    Button.prototype.disable = function () {
        this.disabled = true;
        this.style.addClasses(['pmui-disabled']);
        if (this.eventsDefined) {
            this.defineEvents();
        }
        return this;
    };
    /**
     * @method enable
     * button can be enabled if it was disabled before
     * @chainable
     */
    Button.prototype.enable = function () {
        this.disabled = false;
        this.style.removeClasses(['pmui-disabled']);
        if (this.eventsDefined) {
            this.defineEvents();
        }
        return this;
    };

    /**
     * Performs the click action on the button.
     * @chainable
     */
    Button.prototype.click = function () {
        jQuery(this.html).trigger('click');
        return this;
    };

    /**
     * @method setHandler
     * Sets the handler for the button
     * @param {Function} handler is a Function.
     * @chainable
     */
    Button.prototype.setHandler = function (handler) {
        if (typeof handler !== "function" && handler !== null) {
            throw new Error("setHandler(): the parameter should be a function or null;");
        }
        this.handler = handler;
        this.action = new PMUI.event.Action({
            actionText: this.aliasButton,
            handler: this.handler
        });
        return this;
    };

    /**
     * @method removeEvent
     * Remove Event for the button
     * @param {Function} handler is a Function.
     * @chainable
     */
    Button.prototype.removeEvent = function (type) {
        jQuery(this.html).unbind(type);
        return this;
    };

    /**
     * @method removeEvents
     * Remove Events for the button
     * @param {Function} handler is a Function.
     * @chainable
     */
    Button.prototype.removeEvents = function () {
        var x;
        for (x in  this.events) {
            this.removeEvent(this.events[x].eventName);
        }
        this.events = {};
        return this;
    };

    /**
     * Sets the text for the label Button.
     * @param {String} is a String.
     * @chainable
     */
    Button.prototype.setText = function (text) {
        this.text = text;
        if (this.html && this.dom.spanText) {
            if (jQuery.trim(text) !== "") {
                this.dom.spanText.textContent = this.text;
            } else {
                this.dom.spanText.inerHTML = '&nbsp;';
            }
        }
        return this;
    };

    /**
     * Sets the icon for the Button.
     * @param {String} is a String.
     * @chainable
     */
    Button.prototype.setIcon = function (iconClass) {
        var icon;

        if (!(typeof iconClass == "string")) {
            throw new Error("setIcon(): the property should bo string");
        }
        this.icon = iconClass;
        if (this.html && this.dom.icon) {
            this.dom.icon.className = this.dom.icon.className + " " + this.icon;
        }
        return this;
    };

    /**
     * @method setAliasButton
     * Sets the alias for the label Button
     * @param {String} is a String. lue
     * @chainable
     */
    Button.prototype.setAliasButton = function (alias) {
        this.aliasButton = alias;
        return this;
    };

    /**
     * @method setParent
     * Sets the parent for the Button.
     * @param {Object} is a HTML Element.
     * @chainable
     */
    Button.prototype.setParent = function (parent) {
        this.parent = parent;
        return this;
    };
    /**
     * @method createHTML
     * Create HTML for the element
     * @return {HTMLElement} HTML Generated
     */

    Button.prototype.createHTML = function () {
        var spanText,
            icon;

        if (this.html) {
            return this.html;
        }
        Button.superclass.prototype.createHTML.call(this);
        this.html.href = '#';
        //***conatainer for text
        spanText = PMUI.createHTMLElement('span');
        spanText.className = "button-label";
        this.dom.spanText = spanText;
        this.html.appendChild(spanText);

        // container for icon
        icon = PMUI.createHTMLElement('span');
        icon.className = 'button-icon';
        this.dom.icon = icon;
        this.html.appendChild(icon);

        this.setIconPosition(this.iconPosition);
        this.setTooltipMessage(this.messageTooltip);

        this.setDisabled(this.disabled);
        this.setText(this.text);
        this.setIcon(this.icon);
        this.applyStyle();

        return this.html;
    };
    /**
     * @method defineEvents
     * Define events click for the Button, listening the function action
     */

    Button.prototype.defineEvents = function () {
        var that = this;

        Button.superclass.prototype.defineEvents.call(this);
        if (this.html) {
            if (this.disabled) {
                this.addEvent('click').listen(this.html, function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
            } else {
                this.addEvent('click').listen(this.html, function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (typeof that.handler === 'function') {
                        that.handler(that);
                    }
                });
            }
        }
        return this;
    };

    /**
     * @method setHeight
     * Sets the height for the button.
     * @param {String, number} height
     * This parameter can take the following types
     * - number
     * - string
     */
    Button.prototype.setHeight = function (height) {
        if (typeof height === 'number') {
            this.height = height + "px";
        } else if (/^\d+(\.\d+)?px$/.test(height)) {
            this.height = height;
        } else if (/^\d+(\.\d+)?%$/.test(height)) {
            this.height = height;
        } else if (/^\d+(\.\d+)?em$/.test(height)) {
            this.height = height;
        } else if (height === 'auto' || height === 'inherit') {
            this.height = height;
        } else {
            throw new Error('setHeight: height param is not a number');
        }
        if (this.height !== 'auto') {
            this.style.addProperties({"line-height": parseInt(this.height, 10) + "px"});
        } else {
            this.style.addProperties({"line-height": 'normal'});
        }
        this.applyStyle();
        return this;
    };
    /**
     * @method setIconPosition
     * Creates the HTML element for the control.
     * @param {String} position
     * This parameter can take the following values: left, right
     */
    Button.prototype.setIconPosition = function (position) {
        if (position == 'right' || position == "left" || position == "top" || position == "bottom") {
            this.iconPosition = position;
            if (this.html) {
                if (this.iconPosition == 'left' || this.iconPosition == 'top') {
                    $(this.html).prepend(this.dom.icon);
                    if (this.iconPosition == 'left') {
                        this.dom.icon.style.display = 'inline-block';
                        this.dom.icon.style.marginLeft = '';

                    } else {
                        this.dom.icon.style.display = 'block';
                        this.dom.icon.style.marginLeft = '50%';
                    }
                } else {
                    $(this.html).append(this.dom.icon);
                    if (this.iconPosition == 'right') {
                        this.dom.icon.style.display = 'inline-block';
                        this.dom.icon.style.marginLeft = '';
                    } else {
                        this.dom.icon.style.display = 'block';
                        this.dom.icon.style.marginLeft = '50%';
                    }
                }
            }

        } else {
            throw new Error("setIconPosition(): the parameter is not valid, should be 'right','left', 'top' or 'bottom'");
        }
        return this;
    };
    /**
     * @method setTooltipMessage
     * Sets the tooltip message
     * @param {String} messageTooltip
     */
    Button.prototype.setTooltipMessage = function (messageTooltip) {
        if (typeof messageTooltip == "string") {
            this.messageTooltip = messageTooltip;
            if (this.html) {
                this.html.setAttribute('title', messageTooltip);
            }
        }
        return this;
    };

    PMUI.extendNamespace("PMUI.ui.Button", Button);

    if (typeof exports !== "undefined") {
        module.exports = Button;
    }
}());

