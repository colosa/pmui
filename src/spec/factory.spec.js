//***********************other class******************************************************//
describe('PMUI.util.Factory', function () {
    var a, b, c;
    describe('class behavior', function () {
    });
    describe('method "setDefaultProduct"', function () {
        it('should set a product default property', function () {
            a = new PMUI.util.Factory();
            expect(a.defaultProduct).toBeDefined();
            expect(a.defaultProduct === 'element').toBeTruthy();
        });
    });
    describe('method "setProducts"', function () {
        it('should set the products for a factory object', function () {
            a = new PMUI.util.Factory();
            expect(a.products).toBeDefined();
            var obj = {"element": PMUI.core.Element};
            a.setProducts(obj);
            expect(a.products === obj).toBeTruthy();
        });
    });
    describe('method "register"', function () {
        it('should register the product name type and the product class', function () {
            a = new PMUI.util.Factory();
            var obj={"element": PMUI.core.element};
            a.register("element", obj);
            expect(a.defaultProduct === "element").toBeTruthy();

        });
    });
    describe('method "build"', function () {
        it('should build a new product into the products object', function () {
            a = new PMUI.util.Factory();
            var obj={"element": PMUI.core.element};
            a.build("element", obj);

        });
    });
    describe('method "isValidName"', function () {
        it('should return a value: true if the type is valid or false if it is invalid', function () {
            a = new PMUI.util.Factory();
            var obj={"element": PMUI.core.element};
            a.register("element", obj);
            expect(a.isValidName("element")).toBeDefined();
        });
    });
});