(function () {
    /**
     * @class PMUI.field.PasswordField
     * @extends PMUI.form.Field
     * Class to handle a {@link PMUI.control.PasswordControl} field.
     *
     * Usage example:
     *
     *      @example
     *          var a;
     *          $(function() {
     *              a = new PMUI.field.PasswordField({
     *                  label: "Some Text",
     *                  required: true,
     *                  helper: "Introduce a text (6 chars. max.)",
     *                  validators: [
     *                      {
     *                          pmType: 'textLength',
     *                          criteria: {
     *                              maxLength: 6
     *                          }
     *                      }
     *                  ]
     *              });
     *              document.body.appendChild(a.getHTML());
     *              a.defineEvents();
     *          });
     *
     *
     * The code above will generate a required PasswordField with "Some Text" as label,
     * the control will have a placeholder with the text "a text here".
     * Also the field will contain a helper with the text "Introduce a text (6 chars. max.)"
     * and a validator that will control the maximum length for the text to be entered.
     *
     * @cfg {Number} [maxLength=0] An integer that specifies the maximum character length for the text to be entered.
     * 0 means no max length.
     *
     * #Note: This setting doesn't act as a validator, it simply set the max character length
     * for the field's control.
     * @cfg {Boolean} [trimOnBlur=true] A boolean that specifies if the value entered will be trimmed
     * when the field loses focus.
     */
    var PasswordField = function (settings) {
        PasswordField.superclass.call(this, settings);
        /**
         * @property {Boolean} [trimOnBlur=true] If the field's value must be trimmed every time it loses focus.
         */
        this.trimOnBlur = null;
        PasswordField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', PasswordField);

    PasswordField.prototype.type = "PasswordField";

    PasswordField.prototype.init = function (settings) {
        var defaults = {
            maxLength: 0,
            trimOnBlur: true
        };

        $.extend(true, defaults, settings);

        this.setMaxLength(defaults.maxLength)
            .setTrimOnBlur(defaults.trimOnBlur);
    };
    /**
     * Sets the value for the field.
     * @param {String} value
     * @chainable
     */
    PasswordField.prototype.setValue = function (value) {
        if (this.trimOnBlur) {
            value = jQuery.trim(value);
        }
        PasswordField.superclass.prototype.setValue.call(this, value);

        return this;
    };

    /**
     * Set MaxLength
     * @param maxLength
     * @returns {PMUI.field.PasswordField}
     */
    PasswordField.prototype.setMaxLength = function (maxLength) {
        this.controls[0].setMaxLength(maxLength);
        return this;
    };

    /**
     * Get Max Length
     * @returns {*}
     */
    PasswordField.prototype.getMaxLength = function () {
        return this.controls[0].getMaxLength();
    };

    /**
     * Switches on/off the value trimming for the field's value when it loses focus.
     * @param {Boolean} trimOnBlur Send true for switch on or false to switch off.
     * @chainable
     */
    PasswordField.prototype.setTrimOnBlur = function (trimOnBlur) {
        this.trimOnBlur = !!trimOnBlur;
        return this;
    };
    /**
     * Returns a boolean value that indicates if the trimming function is enabled/disabled
     * @return {Boolean}
     */
    PasswordField.prototype.getTrimOnBlur = function () {
        return this.trimOnBlur;
    };
    /**
     * Sets the control for the PasswordField
     * @chainable
     * @private
     */
    PasswordField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }

        this.controls.push(new PMUI.control.PasswordControl());

        return this;
    };
    /**
     * Establish the handler function for the internal onChange event
     * @return {Function}
     * @private
     */
    PasswordField.prototype.onChangeHandler = function () {
        var that = this;

        return function () {
            var previousValue = that.value,
                value;

            if (that.trimOnBlur) {
                value = that.controls[0].getValue();
                value = jQuery.trim(value);
                that.controls[0].setValue(value);
            }
            that.updateValueFromControls();
            if (that.validAtChange) {
                that.isValid();
            }
            if (typeof that.onChange === 'function') {
                that.onChange(that.getValue(), previousValue);
            }
            if (that.form) {
                (that.form.onChangeHandler())(that, that.getValue(), previousValue);
            }
        };
    };

    PMUI.extendNamespace('PMUI.field.PasswordField', PasswordField);

    if (typeof exports !== "undefined") {
        module.exports = PasswordField;
    }
}());