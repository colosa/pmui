//***********************other class******************************************************//
describe('PMUI.field.OptionsSelectorField', function () {
	var a, b, c, i1, i2, i3;
	beforeEach(function () {
		a = new PMUI.field.OptionsSelectorField({
                        label: "Options radiogroup",
                        items: [
                            {
                                text: "First option"
                            },
                            {
                                text: "Second option",
                                selected: true
                            },
                            {
                                text: "Third option"
                            }
                        ],
                        listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }
                        }
        });
        document.body.appendChild(a.getHTML());
        a.defineEvents();
        b = new PMUI.field.OptionsSelectorField({
                        label: "Options radiogroup",
                        multipleSelection: true,
                        items: [
                            {
                                text: "one"
                            },
                            {
                                text: "two",
                                selected: true
                            },
                            {
                                text: "three"
                            }
                        ],
                        listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }
                        }
            });
        document.body.appendChild(b.getHTML());
        b.defineEvents();
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
        $(b.getHTML()).remove();
        b = null;
	});
	describe('method "setOptions"', function () {
		it('should set the options removing the current ones', function () {
            expect(a.getOptions()[0].text === "First option").toBeTruthy();
            expect(a.getOptions()[0].value === "First option").toBeTruthy();
            expect($(a.html).find('a').first().text() === "First option").toBeTruthy();
            expect(a.getOptions()[1].text === "Second option").toBeTruthy();
            expect(a.getOptions()[1].value === "Second option").toBeTruthy();
            expect(a.getOptions()[2].text === "Third option").toBeTruthy();
            expect(a.getOptions()[2].value === "Third option").toBeTruthy();
            expect($(a.html).find('a').last().text() === "Third option").toBeTruthy();
            c = [
                {
                    text: "one",
                    value: "1"
                },
                {
                    text: "two",
                    value: "2"
                },
                {
                    text: "three",
                    value: "3",
                    selected: true
                }
            ];
            expect(function () {a.setOptions(c)}).not.toThrow();
            expect(a.getOptions()[0].text === "one").toBeTruthy();
            expect(a.getOptions()[0].value === "1").toBeTruthy();
            expect($(a.html).find('a').first().text() === "one").toBeTruthy();
            expect(a.getOptions()[1].text === "two").toBeTruthy();
            expect(a.getOptions()[1].value === "2").toBeTruthy();
            expect(a.getOptions()[2].text === "three").toBeTruthy();
            expect(a.getOptions()[2].value === "3").toBeTruthy();
            expect($(a.html).find('a').last().text() === "three").toBeTruthy();
		});
        it('should not accept any other parameter but an array', function () {
            expect(function () {a.setOptions({text: "two"})}).toThrow();
            expect(function () {a.setOptions("two")}).toThrow();
            expect(function () {a.setOptions(234)}).toThrow();
            expect(function () {a.setOptions(new PMUI.field.OptionsSelectorField())}).toThrow();
        });
	});
    describe('method "setValue"', function () {
        it('should set the value for an single selection, it must be a string', function () {
            expect($(a.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "Second option").toBeTruthy();
            a.setValue("First option");
            expect(a.value === "Second option").toBeFalsy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "Second option").toBeFalsy();
            expect(a.value === "First option").toBeTruthy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "First option").toBeTruthy();
            a.setValue("Third option");
            expect(a.value === "First option").toBeFalsy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "First option").toBeFalsy();
            expect(a.value === "Third option").toBeTruthy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "Third option").toBeTruthy();
        });
        it('should set the value for a multiple selection, it must be an array', function () {
            expect($(b.html).find('a[class="pmui-switch-buttonitem-selected"]').text() === "two").toBeTruthy();
            b.setValue(["one", "two", "three"])
            expect($(b.html).find('a[class="pmui-switch-buttonitem-selected"]').length === 3).toBeTruthy();
            expect(b.value === "one,two,three").toBeTruthy();                       
        });
    });
    describe('method "getValue"', function () {
        it('should return the value of the OptionsSelectorField, only for single selection', function () {
            expect(a.getValue() === "Second option").toBeTruthy();
            a.setValue("First option");
            expect(a.getValue() === "First option").toBeTruthy();
        });
        it('should return the value of the OptionsSelectorField, only for multiple selection', function () {
            expect(b.getValue() === "two").toBeTruthy();
            b.setValue(["one", "two", "three"])
            expect(b.getValue() === "one,three,two").toBeTruthy(); ;
        });
    });
    describe('method "addOption"', function () {
        it('should add a new option and set it at the end of the control', function () {
            expect(function () {a.addOption({ text: "Fourth option", value: "4"})}).not.toThrow();
            expect(a.getOptions()[3].text === "Fourth option").toBeTruthy();
            expect(a.getOptions()[3].value === "4").toBeTruthy();
            expect($(a.html).find('a').last().text() === "Fourth option").toBeTruthy();
        });
    });
    describe('method "getOptions"', function () {
        it('should return all the options as a array', function () {
            expect($.isArray(a.getOptions())).toBeTruthy();
            expect(a.getOptions()[0].text === "First option").toBeTruthy();
            expect(a.getOptions()[1].text === "Second option").toBeTruthy();
            expect(a.getOptions()[2].text === "Third option").toBeTruthy();
        });
    });
    describe('method "clearOptions"', function () {
        it('should clear and remove all the options and change to an empty field', function () {
            expect(a.getOptions().length === 3).toBeTruthy();
            expect($(a.html).find('a').first().text() === "First option").toBeTruthy();
            expect($(a.html).find('a').last().text() === "Third option").toBeTruthy();
            a.clearOptions();
            expect(a.getOptions().length === 0).toBeTruthy();
            expect($(a.html).find('a').length === 0).toBeTruthy();
        });
    });
    describe('method "disableOption"', function () {
        it('should disable an option, the parameter must be a number which is its index', function () {
            expect(function () {a.disableOption(1)}).not.toThrow();
            expect(a.getOptions()[1].disabled).toBeTruthy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').text() === "Second option").toBeTruthy();
        });
    });
    describe('method "enableOption"', function () {
        it('should enable an option, the parameter must be a number which is its index', function () {
            a.disableOption(0);
            a.disableOption(1);
            a.disableOption(2);
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').length === 3).toBeTruthy();
            a.enableOption(1);
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').length === 2).toBeTruthy();
            expect(a.getOptions()[1].disabled).toBeFalsy();
            a.enableOption(2);
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').length === 1).toBeTruthy();
            expect(a.getOptions()[2].disabled).toBeFalsy();
        });
    });
    describe('method "removeOption"', function () {
        it('should remove an option, the option to be removed must be its index', function () {
            expect(a.getOptions().length === 3).toBeTruthy();
            expect(a.getOptions()[1].text === "Second option").toBeTruthy();
            expect($(a.html).find('a')[1].textContent === "Second option").toBeTruthy();
            a.removeOption(1);
            expect(a.getOptions().length === 2).toBeTruthy();
            expect(a.getOptions()[1].text === "Third option").toBeTruthy();
            expect($(a.html).find('a')[1].textContent === "Third option").toBeTruthy();
            expect(a.getOptions()[2]).not.toBeDefined();
        });
    });
    describe('method "setSeparator"', function () {
        it('should set the separator, that is showed when it return the value of multiple selection', function () {
            b.setValue(["one", "two", "three"])
            b.setSeparator("-");
            expect(b.separator === "-").toBeTruthy();
            expect(b.getValue() === "one-three-two");
            b.setSeparator("#");
            expect(b.separator === "#").toBeTruthy();
            expect(b.getValue() === "one#three#two");
        });
    });
    describe('method "disable"', function () {
        it('should disable the OptionsSelectorField', function () {
            a.disable();
            expect(a.disabled).toBeTruthy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').length === 3).toBeTruthy();         
        });
    });
    describe('method "enable"', function () {
        it('should enable the OptionsSelectorField', function () {
            a.disable();
            expect(a.disabled).toBeTruthy();
            a.enable();
            expect(a.disabled).toBeFalsy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem-disabled"]').length === 0).toBeTruthy();
            expect($(a.html).find('a[class="pmui-switch-buttonitem"]').length === 3).toBeTruthy();
        });
    });
    describe('method "getItemsSelected"', function () {
        it('should return the items selected as PMUI.control.OptionsSelectorItemControl objects', function () {
            i1 = b.getOptions()[0];
            i2 = b.getOptions()[1];
            i3 = b.getOptions()[2];
            b.setValue(["one", "two"]);
            expect(b.getItemsSelected()[0] === i1 || b.getItemsSelected()[0] === i2).toBeTruthy();
            expect(b.getItemsSelected()[1] === i1 || b.getItemsSelected()[1] === i2).toBeTruthy();
        });
    });
    describe('method "setOrientation"', function () {
        it('should set the Orientation(vertical, horizontal) for the OptionsSelectorField', function () {
            a.setOrientation("vertical");
            expect($(a.html).find('span[style="display:inline-table;"]').length === 1).toBeTruthy();
            a.setOrientation("horizontal");
            expect($(a.html).find('span[style="display:inline-table;"]').length === 1).toBeFalsy();
        });
    });
    describe('method "setLabel"', function () {
        it('should set the label for the field', function () {
            a.hideColon();
            expect(a.label === "Options radiogroup").toBeTruthy();
            expect($(a.html).find('span[class="pmui-field-label"]').text() === "Options radiogroup").toBeTruthy();
            a.setLabel("new label");
            expect(a.label === "new label").toBeTruthy();
            expect($(a.html).find('span[class="pmui-field-label"]').text() === "new label").toBeTruthy();
        });
    });
});