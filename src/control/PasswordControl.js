(function () {
    /**
     * @class PMUI.control.PasswordControl
     * Class for handle the HTML native password input control.
     * @extends PMUI.control.HTMLControl
     *
     * Quick usage example:
     *
     *      @example
     *      myTextbox = new PMUI.control.PasswordControl({
     *          name: "my_name",
     *          value: "John Doe",
     *          maxLength: 12,
     *          disabled: false,
     *          onChange: function(currentValue, previousValue) {
     *              if(previousValue !== "") {
     *                  alert("Your name is not \"" + previousValue + "\" anymore.\nNow it's \"" + currentValue + "\"");
     *              } else {
     *                  alert("Now your name is " + currentValue);
     *              }
     *          }
     *      });
     *
     *      document.body.appendChild(myTextbox.getHTML());
     *
     * @constructor
     * Creates a new instance of the PasswordControl object.
     * @param {Object} [settings=null] A JSON object with the config options.
     *
     * @cfg {Number} [maxLength=524288] A number which specifies the maximum character length the control can accept.
     * @cfg {Function} [onKeyUp=null] A callback function to be called every time a pressed key is released on the
     * control. For info about the callback parameters please read the
     * {@link PMUI.control.PasswordControl#event-onKeyUp onKeyUp event} section.
     */
    var PasswordControl = function (settings) {
        PasswordControl.superclass.call(this, settings);
//        /**
//         * @property {String} [placeholder=""]
//         * The control's placeholder (the text to be shown inside the control when there is not any text in it).
//         * @readonly
//         */
//        this.placeholder = null;
        /**
         * @property {Number} [maxLength=524288] The maximum character length the control accepts.
         * @readonly
         */
        this.maxLength = null;
        /**
         * @event onKeyUp
         * Fired when a pressed key is released.
         * @param {Object} event The event object.
         */
        this.onKeyUp = null;
        PasswordControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', PasswordControl);

    PasswordControl.prototype.type = "PasswordControl";
    /**
     * Initializes the object.
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    PasswordControl.prototype.init = function (settings) {
        var defaults = {
            maxLength: 524288,
            onKeyUp: null,
            height: 30
        };

        $.extend(true, defaults, settings);

        this.setMaxLength(defaults.maxLength)
            .setOnKeyUpHandler(defaults.onKeyUp)
            .setHeight(defaults.height);
    };
    /**
     * Sets the callback function to be called when the keyup event occurs.
     * @chainable
     */
    PasswordControl.prototype.setOnKeyUpHandler = function (handler) {
        if (typeof handler === 'function') {
            this.onKeyUp = handler;
        }

        return this;
    };
    /**
     * Sets the maximun character number to be accepted in the control.
     * @param {Number} maxLength The number must be an integer.
     * If the value is minor or equal to 0 then the maxLength property is set to the default (524288).
     * @chainable>
     */
    PasswordControl.prototype.setMaxLength = function (maxLength) {
        if (typeof maxLength === 'number' && maxLength % 1 === 0) {
            this.maxLength = maxLength;
            if (this.html) {
                this.html.maxLength = maxLength > 0 ? maxLength : 524288;
            }
        } else {
            throw new Error("method setMaxLength() only accepts integer values.");
        }

        return this;
    };
    /**
     * Set the events for the object.
     * @chainable
     */
    PasswordControl.prototype.defineEvents = function () {
        var that = this;

        PasswordControl.superclass.prototype.defineEvents.call(this);
        this.addEvent('keyup').listen(this.html, function (e) {
            if (typeof that.onKeyUp === 'function') {
                that.onKeyUp(e);
            }
        });
        return this;
    };
    /**
     * Creates the HTML element for the object
     * @return {HTMLElement}
     */
    PasswordControl.prototype.createHTML = function () {
        PasswordControl.superclass.prototype.createHTML.call(this);
        this.html.type = "password";
        this.setMaxLength(this.maxLength);
        return this.html;
    };

    PMUI.extendNamespace('PMUI.control.PasswordControl', PasswordControl);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = PasswordControl;
    }
}());