//***********************other class******************************************************//
describe('PMUI.control.TextAreaControl', function () {
	var a, b;
	beforeEach(function () {
		a = new PMUI.control.TextAreaControl();
	});
	describe('class behavior', function () {
		it('[US-29,a]should set the disabled property to the TextAreaControl', function () {
			expect(a.disabled).toBeFalsy();
			a.disable(true);
			expect(a.disabled).toBeTruthy();
		});
		it('[US-29,b]should set a maximal text length for the TextAreaControl', function () {
			expect(a.maxLength === 524288).toBeTruthy();
			a.setMaxLength(10);
			expect(a.maxLength === 10).toBeTruthy();
		});
		it('[US-29,c]should set if the TextAreaControl is for ReadOnly', function () {
			expect(a.readonly).toBeFalsy();
			a.setReadOnly(true);
			expect(a.readonly).toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});