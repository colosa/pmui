//***********************other class******************************************************//
describe('PMUI.field.CheckBoxGroupField', function () {
	var a, a1, a2, a3;
	beforeEach(function () {
		a = new PMUI.field.CheckBoxGroupField();
		a1 = new PMUI.control.SelectableControl({mode: 'checkbox', label: "op1", value: "1"});
		a2 = new PMUI.control.SelectableControl({mode: 'checkbox', label: "op2", value: "2"});
		a3 = new PMUI.control.SelectableControl({mode: 'checkbox', label: "op3", value: "3"});
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-27,a]should add two or more options and remove them', function () {
			a.addOption(a1);
			a.addOption(a2);
			a.addOption(a3);
			expect(a.controls.length === 3).toBeTruthy();
			a.removeOption(1);
			expect(a.controls.length === 2).toBeTruthy();
			a.removeOption(0);
			expect(a.controls.length === 1).toBeTruthy();
		});
		it('[US-27,a]should add and edit two or more options', function () {
			a.addOption(a1);
			a.addOption(a2);
			a.addOption(a3);
			a.controls[0].setLabel("box1");
			a.controls[1].setLabel("box2");
			a.controls[2].setLabel("box3");
			expect(a.controls[0].mode === "checkbox").toBeTruthy();
			expect(a.controls[0].label === "box1").toBeTruthy();
			expect(a.controls[1].label === "box2").toBeTruthy();
			expect(a.controls[2].label === "box3").toBeTruthy();
			a.controls[0].setName("name1");
			a.controls[1].setName("name2");
			expect(a.controls[0].name === "name1").toBeTruthy();
			expect(a.controls[1].name === "name2").toBeTruthy();
		});
		it('[US-27,b]should set, edit or hide a Label for the CheckBoxGroupField', function () {
			a.setLabel("OPTIONS");
			expect(a.label === "OPTIONS").toBeTruthy();
			a.setLabel("NEW_OPTIONS");
			expect(a.label === "NEW_OPTIONS").toBeTruthy();
			expect(a.labelVisible).toBeTruthy();
			a.hideLabel();
			expect(a.labelVisible).toBeFalsy();
		});
		it('[US-28,c]should set, edit or hide a Helper for the CheckBoxGroupField', function () {
			a.setHelper("new help");
			expect(a.helper.message === "new help").toBeTruthy();
			expect(a.helperIsVisible).toBeFalsy();
			a.showHelper();
			expect(a.helperIsVisible).toBeTruthy();
			a.setHelper("other help");
			expect(a.helper.message === "other help").toBeTruthy();
			a.hideHelper();
			expect(a.helperIsVisible).toBeFalsy();
		});
	});
	describe('method "clearOptions"', function () {
		it('should clear and remove all options previously added', function () {
			a.addOption(a1);
			a.addOption(a2);
			a.addOption(a3);
			expect(a.controls.length === 3).toBeTruthy();
			a.clearOptions();
			expect(a.controls.length === 0).toBeTruthy();
		});
	});
	describe('method "setOptions"', function () {
		it('should add one or more options as array', function () {
			a.setOptions([a1, a2, a3]);
			expect(a.controls.length === 3).toBeTruthy();
		});
	});
	describe('method "setValueToControls"', function () {
		it('should set the value to the controls', function () {
			a.setOptions([a1, a2, a3]);
			a.setValueToControls("1");
			a.setValueToControls("2");
			a.setValueToControls("3");
		});
	});
	describe('method "updateValueFromControls"', function () {
		it('should update the value property from the controls', function () {
			a.setOptions([a1, a2, a3]);
			a.setValueToControls("1");
			a.updateValueFromControls();
			expect(a.value === '["1"]').toBeTruthy();
			a.setValueToControls("2");
			a.updateValueFromControls();
			expect(a.value === '["1","2"]').toBeTruthy();
		});
	});
});