(function () {
    /**
     * @class PMUI.behavior.ContainerItemBehavior
     * Abstract class that encapsulates common properties ans functionality for the specific behaviors that can be
     * attached to the Container's subclasses.
     * @extends {PMUI.core.Base}
     * @abstract
     *
     * @cfg {PMUI.core.Container} targetObject The object the behavior will be applied to.
     * @cfg {String} [scope="pmui-containeritem-behavior"] A string that identifies group sets of items that can
     * interact with this target object's behavior.
     * @cfg {String} [handle=null] If specified, restricts dragging from starting unless the mousedown occurs on the
     * specified element(s). Only elements that descend from the draggable element are permitted.
     */
    var ContainerItemBehavior = function (settings) {
        ContainerItemBehavior.superclass.call(this, settings);
        /**
         * The object the behavior will be applied to.
         * @type {PMUI.core.Container}
         */
        this.targetObject = null;
        /**
         * The string that identifies group sets of items that can interact with this target object's behavior.
         * @type {String}
         */
        this.scope = null;
        /**
         * If the behavior has been attached to its
         * {@link PMUI.behavior.ContainerItemContainer#property-targetObject targetObject}.
         * @type {Boolean}
         * @readonly
         */
        this.behaviorAttached = null;
        /**
         * The JQuery Object the JQuery UI behavior is currently applied to.
         * @type {Object}
         * @private
         */
        this.targetHTML = null;
        /**
         * The selector for the sortable items in case a related behavior is applied.
         * @type {String}
         * @private
         */
        this.sortableItems = null;
        /**
         * If the behavior is disabled or not.
         * @type {Boolean}
         * @readonly
         */
        this.disabled = null;
        /**
         * The current object that currently is being dragged.
         * @type {PMUI.core.Element}
         * @readonly
         */
        this.draggedObject = null;
        /**
         * The class for the placeholder used in some behaviors.
         * @type {String}
         * @readonly
         */
        this.placeholderClass = null;
        /**
         * The class to be applied to the element when it is droppable and has a draggable element being dragged over it.
         * @type {String}
         * @readonly
         */
        this.hoverClass = null;

        this.sortItem = null;

        this.sortIObject = null;

        /**
         * Restricts dragging/sorting from starting unless the mousedown occurs on the specified element(s).
         * Only elements that descend from the draggable element are permitted.
         * @type {String}
         */
        this.handle = null;

        ContainerItemBehavior.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Base', ContainerItemBehavior);

    ContainerItemBehavior.prototype.init = function (settings) {
        var defaults = {
            scope: 'pmui-containeritem-behavior',
            disabled: false,
            placeholderClass: 'pmui-containeritembehavior-placeholder',
            hoverClass: 'pmui-containeritembehavior-hover',
            handle: null
        };

        $.extend(true, defaults, settings);

        this.scope = defaults.scope;
        this.placeholderClass = defaults.placeholderClass;
        this.hoverClass = defaults.hoverClass;
        this.setTargetObject(defaults.targetObject);
        this.handle = defaults.handle;

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };
    /**
     * Updates the behavior status (enabled/disabled).
     * @chainable
     * @abstract
     * @private
     */
    ContainerItemBehavior.prototype.updateBehaviorAvailability = function () {
    };
    /**
     * Enables the behavior.
     * @chainable
     */
    ContainerItemBehavior.prototype.enable = function () {
        this.disabled = false;
        if (this.targetHTML) {
            this.updateBehaviorAvailability();
        }
        return this;
    };
    /**
     * Disables the behavior.
     * @chainable
     */
    ContainerItemBehavior.prototype.disable = function () {
        this.disabled = true;
        if (this.targetHTML) {
            this.updateBehaviorAvailability();
        }
        return this;
    };
    /**
     * Sets the object the behavior will be applied to.
     * @param {PMUI.core.Container} targetObject
     * @private
     */
    ContainerItemBehavior.prototype.setTargetObject = function (targetObject) {
        if (targetObject instanceof PMUI.core.Container) {
            this.targetObject = targetObject;
            this.sortableItems = this.targetObject.sortableItems;
        } else {
            throw new Error("setTargetObject(): the parameter must be an instance of PMUI.core.Container.");
        }

        return this;
    };
    /**
     * Returns the target object the behavior will be applied to.
     * @return {PMUI.core.Container}
     */
    ContainerItemBehavior.prototype.getTargetObject = function () {
        return this.targetObject;
    };
    /**
     * Returns true if the behavior has been attached to its
     * {@link PMUI.behavior.ContainerItemContainer#property-targetObject targetObject}.
     * @return {Boolean}
     */
    ContainerItemBehavior.prototype.isBehaviorAttached = function () {
        return this.behaviorAttached;
    };
    /**
     * Defines the handler to be executed when the
     * {@link #property-targetObject targetObject}'s item dragging starts.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemBehavior.prototype.onStart = function () {
        var that = this;
        return function (e, ui) {
            var targetObject = that.targetObject;
            ui.helper.get(0).style.width = $(e.target).width() + "px";
            $(this).hide();
            that.draggedObject = PMUI.getPMUIObject(this);
            if (typeof targetObject.onDragStart === 'function') {
                targetObject.onDragStart(targetObject, that.draggedObject);
            }
        };
    };
    /**
     * Defines the handler to be executed while a {@link #property-targetObject targetObject}'s item is being dragged.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemBehavior.prototype.onDrag = function () {
        var that = this;
        return function (e, ui) {
        };
    };
    /**
     * Defines the handler to be executed when the
     * {@link #property-targetObject targetObject}'s item dragging stops.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemBehavior.prototype.onStop = function () {
        var that = this;
        return function (e, ui) {
            var targetObject = that.targetObject;
            that.draggedObject = PMUI.getPMUIObject(this);
            if (typeof targetObject.onStopDrag === 'function') {
                targetObject.onStopDrag(targetObject, that.draggedObject);
            }
            targetObject = $(this);
            if (targetObject.parent().length) {
                targetObject.show();
            } else {
                targetObject.remove();
            }
            that.draggedObject = null;
        };
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dropped on the
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemBehavior.prototype.onDrop = function () {
        var that = this;
        return function (e, ui) {
            var item = PMUI.getPMUIObject(ui.draggable.get(0)),
                data,
                placeholder = ui.draggable.data().sortableItem && ui.draggable.data().sortableItem.placeholder[0],
                targetObject = that.targetObject, performDrop;

            jQuery(ui.helper).remove();

            if (!item || targetObject.isDirectParentOf(item)) {
                return;
            }

            if (typeof targetObject.onBeforeDrop === 'function') {
                targetObject.onBeforeDrop(targetObject, item, targetObject.items.getSize());
            }

            if (typeof targetObject.onDrop === 'function') {
                performDrop = targetObject.onDrop(targetObject, item, targetObject.items.getSize() - 1);
            }
            if (performDrop || performDrop === undefined) {
                item.getParent().behaviorObject.onDropOut(item, targetObject);
            }
            //The next lines avoid error when the dropped element is a sortable element
            if (placeholder) {
                placeholder.style.display = 'none';
                targetObject.getContainmentArea().appendChild(placeholder);
            }
        };
    };
    /**
     * The function to be perform when a draggable item is drop out from the container. Only applicable the behavior id
     * of the drag type.
     * @param {PMUI.core.Element} item The draggable item involved in the action.
     * @param {PMUI.core.Container} destiny The container the draggable item was dropped on.
     * @chainable
     * @private
     */
    ContainerItemBehavior.prototype.onDropOut = function (item, destiny, index) {
        var targetObject = this.targetObject;
        targetObject.removeItem(item);
        if (destiny.canContain(item)) {
            destiny.addItem(item, index);
            item.setVisible(true);
        } else {
            data = item.getData();
            destiny.addDataItem(data, index);
        }
        if (typeof targetObject.onDropOut === 'function') {
            targetObject.onDropOut(item, targetObject, destiny);
        }
        return this;
    };
    /**
     * Attaches event handlers for the mouse events triggered by the draggable/sortableItems.
     * @chainable
     * @private
     */
    ContainerItemBehavior.prototype.attachOnDraggableMouseEvents = function () {
        var that = this;
        this.detachOnDraggableMouseEvents();
        this.targetHTML.on('mouseover.pmui-containeritemdrag', function () {
            var pmuiObject = PMUI.getPMUIObject(this);
            that.targetObject.onDraggableMouseOver(pmuiObject);
            return true;
        }).on('mouseout.pmui-containeritemdrag', function () {
            var pmuiObject = PMUI.getPMUIObject(this);
            that.targetObject.onDraggableMouseOut(pmuiObject);
            return true;
        });
        return this;
    };
    /**
     * Attaches event handlers for the mouse events triggered by the draggable/sortableItems.
     * @chainable
     * @private
     */
    ContainerItemBehavior.prototype.detachOnDraggableMouseEvents = function () {
        this.targetHTML.off('mouseover.pmui-containeritemdrag')
            .off('mouseout.pmui-containeritemdrag');
        return this;
    };
    /**
     * Attaches the behavior to its
     * {@link PMUI.behavior.ContainerItemContainer#property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemBehavior.prototype.attachBehavior = function () {
        this.behaviorAttached = true;
        this.updateBehaviorAvailability();
        return this;
    };
    /**
     * Detaches the behavior from  its
     * {@link PMUI.behavior.ContainerItemContainer#property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemBehavior.prototype.detachBehavior = function () {
        this.behaviorAttached = false;
        return this;
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemBehavior', ContainerItemBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemBehavior;
    }
}());