(function() {
    /**
     * @class PMUI.behavior.ContainerItemDropSortBehavior
     * @extends PMUI.behavior.ContainerItemDropBehavior
     * Class that encapsulates the drop behavior for a {@link PMUI.core.Container Container} object and the drag 
     * behavior for its items. The object that applies this behavior, will be able to:
     *
     * - Accept draggable items from other Containers which ones are applying dragging behaviors 
     * ({@link PMUI.behavior.ContainerItemDragBehavior drag}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDragSortBehavior dragsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}). 
     * 
     * - The object items will be sortable.
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "dropsort" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "sortable item #1"
     *              }, {
     *                  label: "sortable item #2"
     *              }, {
     *                  label: "sortable item #3"
     *              }, {
     *                  label: "sortable item #4"
     *              }, {
     *                  label: "sortable item #5"
     *              }, {
     *                  label: "sortable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("dropsort");
     *      tree2.setBehavior("drag");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDropSortBehavior = function(settings) {
        ContainerItemDropSortBehavior.superclass.call(this, settings);
        this.dropped = null;
        this.cancelledDrop = null;
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemDropBehavior', ContainerItemDropSortBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDropSortBehavior.prototype.updateBehaviorAvailability = function() {
        this.targetHTML.sortable(this.disabled ? 'disable' : 'enable');
        return this;
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dropped on the 
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDropSortBehavior.prototype.onDrop = function() {
        var that = this;
        return function(e, ui) {
            var droppable = $(this),
                targetObject = that.targetObject,
                item = PMUI.getPMUIObject(ui.item.get(0)),
                index = !targetObject.getItems().length ? 0 : droppable.find('>*').index(item.getHTML()),
                data,
                performDrop;

            if(index < 0) {
                index = droppable.find('>*').index(droppable.find('.ui-draggable'));
            }
            if(typeof targetObject.onBeforeDrop === 'function') {
                targetObject.onBeforeDrop(targetObject, item, index);
            }
            if(typeof targetObject.onDrop === 'function') {
                performDrop = targetObject.onDrop(targetObject, item, index);
            }
            if(performDrop || performDrop === undefined) {
                item.getParent().behaviorObject.onDropOut(item, targetObject, index);
                that.dropped = true;
                if(!targetObject.canContain(item)) {
                    $(e.target).find('.ui-draggable').detach();
                }
                that.cancelledDrop = false;
            } else {
                //if the dropped object came from a container with draggable or sortable behavior
                if(ui.helper) {
                    ui.sender.draggable('cancel');
                } else {
                    ui.sender.sortable('cancel');
                }
                that.cancelledDrop = true;
            }
        };
    };
    /**
     * Defines the handler to be executed when the user stopped sorting and the 
     * {@link #property-targetObject targetObject}'s items positions have 
     * changed.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDropSortBehavior.prototype.onUpdate = function() {
        var that = this;
        return function(e, ui) {
            var parentHTML = that.targetObject.getHTML(),
                item = PMUI.getPMUIObject(ui.item.get(0));

            if(that.dropped) {
                that.dropped = false;
                return;
            }

            if(item && jQuery(parentHTML).has(item.getHTML()).length) {
                (that.targetObject.onSortingChange()(e, ui));
            }
        };
    };
    /**
     * @inheritdoc
     */
    ContainerItemDropSortBehavior.prototype.onStop = function() {
        var that = this;
        return function(e, ui) {
            if(that.cancelledDrop) {
                ui.item.remove();
            }
            that.cancelledDrop = null;
        };
    };
    /**
     * @inherit
     */
    ContainerItemDropSortBehavior.prototype.onStart = function() {
        var that = this;
        return function(e, ui) {
            that.cancelledDrop = null;
        };
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDropSortBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            this.targetHTML = jQuery(targetObject.getContainmentArea());
            this.targetHTML.addClass('pmui-containeritembehavior-sort').sortable({
                containment: targetObject.getContainmentArea(),
                receive: this.onDrop(),
                start: this.onStart(),
                update: this.onUpdate(),
                items: this.sortableItems,
                stop: this.onStop(),
                tolerance: 'pointer',
                handle: this.handle || false,
                placeholder: this.placeholderClass
            });
        }
        return ContainerItemDropSortBehavior.superclass.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDropSortBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                this.targetHTML.removeClass('pmui-containeritembehavior-sort').sortable('destroy');
            } catch(e) {}
        }
        return ContainerItemDropSortBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDropSortBehavior', ContainerItemDropSortBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDropSortBehavior;
    }
}());