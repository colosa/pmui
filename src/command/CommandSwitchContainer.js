(function () {
    /**
     * @class PMUI.command.CommandSwitchContainer
     * Class that encapsulates the action of switching containers
     *
     *              //e.g.
     *              var command = new PMUI.command.CommandSwitchContainer(arrayOfShapes);
     * @extends PMUI.command.Command
     *
     * @constructor
     * Creates an instance of this command
     * @param {Array} shapesAdded array of shapes that are going to switch container
     */
    var CommandSwitchContainer = function (shapesAdded) {
        CommandSwitchContainer.superclass.call(this, shapesAdded[0].shape);
        /**
         * Properties of the object before the command is executed
         * @property {Object}
         */
        this.before = null;
        /**
         * Properties of the object after the command is executed
         * @property {Object}
         */
        this.after = null;
        /**
         * Reference to all objects involved in this command
         * @type {Array}
         */
        this.relatedShapes = [];
        CommandSwitchContainer.prototype.initObject.call(this, shapesAdded);
    };

    PMUI.inheritFrom('PMUI.command.Command', CommandSwitchContainer);

    /**
     * Type of the instances of this command
     * @property {String}
     */
    CommandSwitchContainer.prototype.type = "CommandSwitchContainer";

    /**
     * Initializer of the command
     * @param {Array} shapesAdded array of shapes that are going to switch container
     */
    CommandSwitchContainer.prototype.initObject = function (shapesAdded) {
        var i,
            shape,
            beforeShapes = [],
            afterShapes = [];

        for (i = 0; i < shapesAdded.length; i += 1) {
            shape = shapesAdded[i];
            this.relatedShapes.push(shape.shape);
            beforeShapes.push({
                parent: shape.shape.parent,
                x: shape.shape.getOldX() * shape.shape.canvas.getZoomFactor(),
                y: shape.shape.getOldY() * shape.shape.canvas.getZoomFactor(),
                topLeft: true
            });
            afterShapes.push({
                parent: shape.container,
                x: shape.x,
                y: shape.y,
                topLeft: shape.topLeft
            });
        }

        this.before = {
            shapes: beforeShapes
        };

        this.after = {
            shapes: afterShapes
        };
        this.containerUndo = false;
    };

    /**
     * The command execution implementation, updates the parents, and if necessary,
     * updates the children positions and connections.
     */
    CommandSwitchContainer.prototype.execute = function () {
        var i,
            j,
            max,
            connection,
            shape,
            delta;

        for (i = 0; i < this.relatedShapes.length; i += 1) {
            shape = this.relatedShapes[i];
            delta = {
                dx: this.before.shapes[i].x - this.after.shapes[i].x,
                dy: this.before.shapes[i].y - this.after.shapes[i].y
            };
            this.before.shapes[i].parent.swapElementContainer(
                shape,
                this.after.shapes[i].parent,
                this.after.shapes[i].x,
                this.after.shapes[i].y,
                this.after.shapes[i].topLeft
            );
            shape.refreshChildrenPositions(true, delta);
            shape.refreshConnections(false, this.relatedShapes, delta);
        }
        this.canvas.triggerParentChangeEvent(this.relatedShapes,
            this.before.shapes, this.after.shapes);
        if (this.containerUndo) {
            for (j = 0; j < PMUI.getActiveCanvas().refreshArray.getSize(); j += 1) {
                connection = PMUI.getActiveCanvas().refreshArray.get(j);
                connection.reconnectManhattah(false);
                connection.setSegmentMoveHandlers()
                    .checkAndCreateIntersectionsWithAll();
                PMUI.getActiveCanvas()
                    .triggerConnectionStateChangeEvent(connection);
            }
            this.containerUndo = false;
        }

    };

    /**
     * Returns to the state before this command was executed
     */
    CommandSwitchContainer.prototype.undo = function () {
        var i,
            delta,
            shape,
            connection,
            j,
            max;
        for (i = 0; i < this.relatedShapes.length; i += 1) {
            shape = this.relatedShapes[i];
            delta = {
                dx: this.before.shapes[i].x - this.after.shapes[i].x,
                dy: this.before.shapes[i].y - this.after.shapes[i].y
            };
            this.before.shapes[i].parent.swapElementContainer(
                shape,
                this.before.shapes[i].parent,
                this.before.shapes[i].x,
                this.before.shapes[i].y,
                this.before.shapes[i].topLeft
            );
            shape.refreshChildrenPositions(true, delta);
            shape.refreshConnections(false, this.relatedShapes, delta);
        }

        this.canvas.triggerParentChangeEvent(this.relatedShapes,
            this.after.shapes, this.before.shapes);


        for (j = 0; j < PMUI.getActiveCanvas().refreshArray.getSize(); j += 1) {
            connection = PMUI.getActiveCanvas().refreshArray.get(j);
            connection.reconnectManhattah(false);
            connection.setSegmentMoveHandlers()
                .checkAndCreateIntersectionsWithAll();
            PMUI.getActiveCanvas()
                .triggerConnectionStateChangeEvent(connection);
        }
        this.containerUndo = true;

    };


    /**
     *  Executes the command again after an undo action has been done
     */
    CommandSwitchContainer.prototype.redo = function () {
        this.execute();
    };

    PMUI.extendNamespace('PMUI.command.CommandSwitchContainer', CommandSwitchContainer);
}());
