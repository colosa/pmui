(function () {
    /**
     * @class PMUI.item.TreeNode
     * Each node in a TreePanel. It is able to apply data binding, that means that it can use its
     * {@link PMUI.item.TreeNode#property-data data property} for set another object's property.
     * @extends PMUI.core.Item
     *
     * The following example shows a TreeNode object whose child nodes are created using the
     * {@link PMUI.item.TreeNode#cfg-items items config option}:
     *
     *      @example
     *      var t;
     *      $(function() {
     *          t = new PMUI.item.TreeNode({
     *              label: 'America',
     *              childrenDefaultSettings: {
     *                  label: '[untitled node]'
     *              },
     *              items: [
     *                  {
     *                      label: "North America",
     *                      items: [
     *                          {
     *                              label: 'Canada',
     *                              data: {
     *                                  label: 'Canada',
     *                                  value: 'cnd'
     *                              }
     *                          },
     *                          {
     *                              label: 'Mexico'
     *                          }
     *                      ]
     *                  },
     *                  {
     *                      label: "Central America and Caribbean",
     *                      items: [
     *                          {
     *                              label: 'Cuba'
     *                          },
     *                          {
     *                              label: 'Guatemala'
     *                          } 
     *                      ]
     *                  },
     *                  {
     *                      label: "South America",
     *                      items: [
     *                          {
     *                              label: 'Argentina'
     *                          }, 
     *                          {
     *                              label: 'Bolivia'
     *                          }
     *                      ]
     *                  }
     *              ]
     *          });
     *          document.body.appendChild(t.getHTML());
     *          t.defineEvents();
     *      });
     *
     * The following example shows a TreeNode object whose child nodes are created using the
     * {@link PMUI.item.TreeNode#cfg-dataItems dataItems config option}:
     *
     *      @example
     *      var t2;
     *      $(function() {
     *          t2 = new PMUI.item.TreeNode({
     *              data: {
     *                  name: 'America'
     *              },
     *              labelDataBind: 'name',
     *              autoBind: true,
     *              childrenDefaultSettings: {
     *                  labelDataBind: 'name',
     *                  autoBind: true
     *              },
     *              dataItems: [
     *                  {
     *                      name: "Argentina"
     *                  },
     *                  {
     *                      name: "Bolivia"
     *                  },
     *                  {
     *                      name: "Brasil"
     *                  }
     *              ]
     *          });
     *          document.body.appendChild(t2.getHTML());
     *          t2.defineEvents();
     *      });
     *
     * @cfg {Boolean} [sterile=false] If the node will be able to have children nodes.
     * @cfg {String} [itemsDataBind=null] The key from the object's data to be used as the array for create the child
     * nodes of the current node when the data binding be applied. If the data binding is applied then the
     * {@link PMUI.item.TreeNode#cfg-items items config option} will be ignored.
     * @cfg {String} [labelDataBind=null] The key from the object's data to be used as the label for the current node
     * when the data binding be applied. If the data binding is applied then the
     * {@link PMUI.item.TreeNode#cfg-label label config option} will be ignored.
     * @cfg {String} [nodeClassDataBind=null] The key from the object's data to be used as the class for the node's
     * html when the data binding be applied. If the data binding is applied then the
     * {@link PMUI.item.TreeNode#cfg-nodeClass nodeClass config option} will be ignored.
     * @cfg {Object} [data={}] The data for the node.
     * @cfg {Boolean} [collapsed=true] If the node will be collapsed or not from the beginning.
     * @cfg {String} [label='[item]'] The label text for the node.
     * @cfg {String} [nodeClass=''] The css class for the node's html.
     * @cfg {Function} [onCollapse=null] The callback function to be executed everytime the current node is collpased.
     * Please read the {@link PMUI.item.TreeNode#event-onCollapse onCollapse event documentation}.
     * @cfg {Function} [onExpand=null] The callback function yto be executed everytime the current node is expanded.
     * Please read the {@link PMUI.item.TreeNode#event-onExpanded onExpanded event documentation}.
     * @cfg {Object} [childrenDefaultSettings={}] The default config options to be applied to the new child nodes for
     * the current node. This only will be applied when the appended item is an object literal.
     * @cfg {Boolean} [autobind] If the data binding will be applied automatically on instantiation and everytime the
     * data is set. This will cause that some others config options be ignored (like
     * {@link PMUI.item.TreeNode#cfg-label label}, {@link PMUI.item.TreeNode#cfg-nodeClass nodeClass} and/or
     * {@link PMUI.item.TreeNode#cfg-items items}).
     * @cfg {Boolean} [recursiveChildrenDefaultSettings=false] If the current
     * {@link PMUI.item.TreeNode#cfg-childrenDefaultSettings childrenDefaultSettings config option} will be inherited
     * to the same config option of its child nodes.
     * @cfg {Function|null} [onBeforeAppend=null] A callback function to be called everytime the
     * {@link #event-onBeforeAppend onBeforeAppend} event is fired. For info about the parameters used for this
     * callback please read the documentation for this event.
     * @cfg {Function|null} [onAppend=null] A callback function to be called everytime the
     * {@link #event-onAppend onAppend} event is fired. For info about the parameters used for this callback please
     * read the documentation for this event.
     */
    var TreeNode = function (settings) {
        TreeNode.superclass.call(this, settings);
        /**
         * If the node can have child nodes.
         * @type {Boolean}
         */
        this.sterile = null;
        /**
         * If the node is collapsed.
         * @type {Boolean}
         * @readonly
         */
        this.collapsed = null;
        /**
         * The current label being used by the node.
         * @type {String}
         * @readonly
         */
        this.label = null;
        /**
         * A literal object that will contain all the dom elements that compose the node's html structure.
         * @type {Object}
         * @readonly
         */
        this.dom = {};
        /**
         * The css class for the node's html.
         * @type {String}
         * @readonly
         */
        this.nodeClass = null;
        /**
         * @event onClick
         * Fired when the current node is clicked
         */
        this.onClick = null;
        /**
         * @event onCollapse
         * Fired when the current node is collapsed.
         */
        this.onCollapse = null;
        /**
         * @event onExpand
         * Fired when the current node is expanded.
         */
        this.onExpand = null;
        /**
         * The key of the object's data that will be used as the array to build the child nodes for the current one
         * when the data binding is applied.
         * @type {String}
         * @readonly
         */
        this.itemsDataBind = null;
        /**
         * The key of the object's data that will be used as the label for the current node when the data binding is
         * applied.
         * @type {String}
         * @readonly
         */
        this.labelDataBind = null;
        /**
         * The key of the object's data that will be used as the css class for the current node's html when the data
         * binding is applied.
         * @type {String}
         * @readonly
         */
        this.nodeClassDataBind = null;
        /**
         * If the data binding will be applied on initialization and everytime the data is set.
         * @type {Boolean}
         */
        this.autoBind = null;
        /**
         * The default config options to be applied to the child nodes to be add to the current node, but only when an
         * object literal is used.
         * @type {Object}
         */
        this.childrenDefaultSettings = {};
        /**
         * The data for the current node. This data can be used to modify another node's properties through data
         * binding.
         * @type {PMUI.data.DataField}
         * @private
         */
        this.data = null;
        /**
         * If the settings defined in the {@link #property-childrenDefaultSettings childrenDefaultSettings} property will
         * be inherited to all the child nodes of the current one.
         * @type {Boolean}
         */
        this.recursiveChildrenDefaultSettings = null;
        /**
         * @event onBeforeAppend
         * Fired before a child node is appended to the current node or to any of its children nodes.
         * @param {PMUI.item.TreeNode} node The node in which the event is being fired.
         * @param {PMUI.item.TreeNode} newNode The new node to be appended.
         */
        this.onBeforeAppend = null;
        /**
         * @event onAppend
         * Fired everytime a child node is appended to the current node or to any of its children nodes.
         * @param {PMUI.item.TreeNode} node The node in which the event is being fired.
         * @param {PMUI.item.TreeNode} newNode The appended node.
         */
        this.onAppend = null;
        TreeNode.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Item', TreeNode);
    /**
     * @property {String} type=TreeNode The type of the class.
     * @readonly
     */
    TreeNode.prototype.type = 'TreeNode';
    /**
     * @property {String} family=Tree The class family.
     * @readonly
     */
    TreeNode.prototype.family = 'Tree';
    /**
     * Initializes the object.
     * @param  {Object} settings The object literal with the config options.
     * @private
     */
    TreeNode.prototype.init = function (settings) {
        var defaults = {
            itemsDataBind: null,
            labelDataBind: null,
            nodeClassDataBind: null,
            data: {},
            sterile: false,
            collapsed: true,
            label: '[item]',
            nodeClass: "",
            onCollapse: null,
            onExpand: null,
            onClick: null,
            childrenDefaultSettings: {},
            autoBind: false,
            recursiveChildrenDefaultSettings: false,
            onAppend: null,
            onBeforeAppend: null
        };

        jQuery.extend(true, defaults, settings);

        this.data = new PMUI.data.DataField();

        this.setNodeClass(defaults.nodeClass)
        if (defaults.autoBind) {
            this.enableAutoBind();
        } else {
            this.disableAutoBind();
        }
        this.setRecursiveChildrenDefaultSettings(defaults.recursiveChildrenDefaultSettings)
            .setChildrenDefaultSettings(defaults.childrenDefaultSettings)
            .setSterility(defaults.sterile)
            .setLabel(defaults.label)
            .setParent(defaults.parent)
            .setOnCollapseHandler(defaults.onCollapse)
            .setOnExpandHandler(defaults.onExpand)
            .setOnClickHandler(defaults.onClick)
            .setItemsDataBind(defaults.itemsDataBind)
            .setLabelDataBind(defaults.labelDataBind)
            .setNodeClassDataBind(defaults.nodeClassDataBind)
            .setOnAppendHandler(defaults.onAppend)
            .setOnBeforeAppendHandler(defaults.onBeforeAppend);

        if (defaults.dataItems && jQuery.isArray(defaults.dataItems)) {
            this.setDataItems(defaults.dataItems);
        } else {
            this.setItems(defaults.items);
        }
        this.setData(defaults.data);

        if (defaults.collapsed) {
            this.collapse();
        } else {
            this.expand();
        }
        this.elementTag = 'li';
    };
    /**
     * Sets the callback function to be called everytime the {@link #event-onBeforeAppend onBeforeAppend} event is
     * fired. For info about the parameters used for this callback please read the documentation for this event.
     * @param {Function|null} handler
     */
    TreeNode.prototype.setOnBeforeAppendHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnBeforeAppendHandler(): The parameter must be a function or null.");
        }
        this.onBeforeAppend = handler;
        return this;
    };
    /**
     * Sets the callback function to be called everytime the
     * {@link #event-onAppend onAppend} event is fired. For info about the parameters used for this callback please
     * read the documentation for this event.
     * @param {Function|null} handler
     */
    TreeNode.prototype.setOnAppendHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnAppendHandler(): The parameter must be a function or null.");
        }
        this.onAppend = handler;
        return this;
    };
    /**
     * Returns the level of the node relative to its root node.
     * @return {Number}
     */
    TreeNode.prototype.getDepth = function () {
        if (!this.parent || !(this.parent instanceof PMUI.item.TreeNode)) {
            return 0;
        } else {
            return this.parent.getDepth() + 1;
        }
    };
    /**
     * Returns the root node of the current one.
     * @return {PMUI.item.TreeNode}
     */
    TreeNode.prototype.getRoot = function () {
        if (this.getDepth() === 0) {
            return this;
        } else {
            return this.parent.getRoot();
        }
    };
    /**
     * Turns on the auto binding functionality.
     * @chainable
     */
    TreeNode.prototype.enableAutoBind = function () {
        this.autoBind = true;
        return this;
    };
    /**
     * Turns off the auto binding functionality.
     * @chainable
     */
    TreeNode.prototype.disableAutoBind = function () {
        this.autoBind = false;
        return this;
    };
    /**
     * Returns true if the current {@link PMUI.item.TreeNode#property-childrenDfaultSettings childrenDfaultSettings}
     * will be inherited to the next child nodes that will be added.
     * @return {Boolean} [description]
     */
    TreeNode.prototype.isRecursiveChildrenDefaultSettings = function () {
        return this.recursiveChildrenDefaultSettings;
    };
    /**
     * Establish if the current object's childrenDefaultSettings property values will be set it to the same property
     * of its future child nodes.
     * @param {Boolean} recursive
     */
    TreeNode.prototype.setRecursiveChildrenDefaultSettings = function (recursive) {
        this.recursiveChildrenDefaultSettings = !!recursive;
        return this;
    };
    /**
     * Returns the default config options for the node's child nodes.
     * @return {Object}
     */
    TreeNode.prototype.getChildrenDefaultSettings = function () {
        return this.childrenDefaultSettings;
    };
    /**
     * Sets the default config options for the child nodes to be added. Note that if you set this, the effect will
     * take effect only in new nodes added from the moment of the new set. The default settings will have effect only
     * when you add an item using anything but an instance of {@link PMUI.core.Element} (or ant subclass of it).
     * @param {Object} settings A object literal with some or all of the config options for a TreeNode instance.
     */
    TreeNode.prototype.setChildrenDefaultSettings = function (settings) {
        if (typeof settings !== 'object') {
            throw new Error('setChildrenDefaultSettings(): The parameter must be an object.');
        }
        this.childrenDefaultSettings = settings;

        return this;
    };
    /**
     * Sets the key of the current node's data to be used as the css class for the node's html.
     * @param {String} nodeClassDataBind
     */
    TreeNode.prototype.setNodeClassDataBind = function (nodeClassDataBind) {
        if (nodeClassDataBind !== null && typeof nodeClassDataBind !== 'string') {
            throw new Error("setNodeClassDataBind(): the parameter must be a string or null.");
        }
        this.nodeClassDataBind = nodeClassDataBind;
        return this;
    };
    /**
     * Sets the key of the current node's data to be used as the array of items to be add to the current node.
     * Note that every item in the array will be added as a data item
     * (see the {@link PMUI.core.Container#method-addDataItem addDataItem()} method).
     * @param {String} itemsDataBind
     * @chainable
     */
    TreeNode.prototype.setItemsDataBind = function (itemsDataBind) {
        if (itemsDataBind !== null && typeof itemsDataBind !== 'string') {
            throw new Error("setItemsDataBind(): the parameter must be a string or null.");
        }
        this.itemsDataBind = itemsDataBind;
        return this;
    };
    /**
     * Sets the key of the current node's data to be used as the label for the current node.
     * @param {String} labelDataBind
     * @chainable
     */
    TreeNode.prototype.setLabelDataBind = function (labelDataBind) {
        if (labelDataBind !== null && typeof labelDataBind !== 'string') {
            throw new Error("setLabelDataBind(): the parameter must be a string or null.");
        }
        this.labelDataBind = labelDataBind;
        return this;
    };
    /**
     * Bind the node's data to its properties using:
     *
     * - The {@link PMUI.item.TreeNode#property-labelDataBind labelDataBind property} to set the
     * {@link PMUI.item.TreeNode#property-label label property}.
     * - The {@link PMUI.item.TreeNode#property-itemsDataBind labelDataBind property} for set the node's child items.
     * - The {@link PMUI.item.TreeNode#property-nodeClassDataBind nodeClassDataBind property} for set the css class for
     * the node's html.
     * @chainable
     */
    TreeNode.prototype.bindData = function () {
        var data = this.data.getRecord();

        this.setLabel(data[this.labelDataBind] || this.label);
        if (this.itemsDataBind && jQuery.isArray(data[this.itemsDataBind])) {
            this.setDataItems(data[this.itemsDataBind]);
        }
        this.setNodeClass(data[this.nodeClassDataBind] || this.nodeClass);

        return this;
    };
    /**
     * Sets the data for the node.
     * @param {Object|PMUI.data.DataField} data
     */
    TreeNode.prototype.setData = function (data) {
        var key;

        if (data instanceof PMUI.data.DataField) {
            this.data = data;
        } else if (typeof data === 'object') {
            this.data.clear();
            for (key in data) {
                this.data.addAttribute(key, data[key]);
            }
        } else {
            throw new Error("setData(): it only accepts a JSON object o an instance of PMUI.data.DataField as parameter.");
        }

        if (this.autoBind) {
            this.bindData();
        }

        return this;
    };
    /**
     * Return the node's data.
     * @return {Object} A object literal containing the node's data.
     */
    TreeNode.prototype.getData = function () {
        return this.data.getRecord();
    };
    /**
     * Sets the callback function for the {@link PMUI.item.TreeNode#event-onClickHandler onClickHandler event}.
     * @param {Function} handler
     */
    TreeNode.prototype.setOnClickHandler = function (handler) {
        if (handler === null || typeof handler === 'function') {
            this.onClick = handler;
        }
        return this;
    };
    /**
     * Sets the callback function for the {@link PMUI.item.TreeNode#event-onCollapse onCollapse event}.
     * @param {Function} handler
     */
    TreeNode.prototype.setOnCollapseHandler = function (handler) {
        if (handler === null || typeof handler === 'function') {
            this.onCollapse = handler;
        }

        return this;
    };
    /**
     * Sets the callback function for the {@link PMUI.item.TreeNode#event-onExpand onExpand event}.
     * @param {Function} handler
     */
    TreeNode.prototype.setOnExpandHandler = function (handler) {
        if (handler === null || typeof handler === 'function') {
            this.onExpand = handler;
        }

        return this;
    };
    /**
     * Sets the factory for the curren node.
     * @private
     * @chainable
     */
    TreeNode.prototype.setFactory = function () {
        this.factory = new PMUI.util.Factory({
            products: {
                'treeNode': PMUI.item.TreeNode
            },
            defaultProduct: 'treeNode'
        });

        return this;
    };
    /**
     * Sets the parent node for the current one.
     * @param {null|PMUI.item.TreeNode|PMUI.panel.TreePanel} parent.
     * @chainable
     */
    TreeNode.prototype.setParent = function (parent) {
        if (parent === null || parent instanceof PMUI.item.TreeNode || parent instanceof PMUI.panel.TreePanel) {
            TreeNode.superclass.prototype.setParent.call(this, parent);
        }
        return this;
    };
    /**
     * Returns true if the node is sterile (it can have more child nodes).
     * @return {Boolean}
     */
    TreeNode.prototype.isSterile = function () {
        return this.sterile;
    };
    /**
     * Sets a custom css class for the node's html.
     * @param {String} nodeClass
     */
    TreeNode.prototype.setNodeClass = function (nodeClass) {
        if (typeof nodeClass !== 'string') {
            throw new Error('setNodeClass(): the parameter must be a string.');
        }
        nodeClass = jQuery.trim(nodeClass);
        if (this.html) {
            jQuery(this.dom.nodeElement).removeClass(this.nodeClass).addClass('pmui-treepanel-node').addClass(nodeClass);
        }
        this.nodeClass = nodeClass;
        return this;
    };
    /**
     * Returns the custom css class of the node's html.
     * @return {String}
     */
    TreeNode.prototype.getNodeClass = function () {
        return this.nodeClass;
    };
    /**
     * Sets if the node is sterile or not (if can have more child nodes).
     * @param {Boolean} sterile
     * @chainable
     */
    TreeNode.prototype.setSterility = function (sterile) {
        this.sterile = !!sterile;
        return this;
    };
    /**
     * Sets the label for the node.
     * @param {String} label
     */
    TreeNode.prototype.setLabel = function (label) {
        if (typeof label !== 'string') {
            throw new Error('setLabel(): This method only accepts parameter of type String.');
        }
        this.label = label;
        if (this.html) {
            this.dom.label.textContent = label;
        }

        return this;
    };
    /**
     * Applies the collapsing to its child nodes list.
     * @param  {Boolean} applyToChildren If the child nodes will apply the collapsing too.
     * @chainable
     */
    TreeNode.prototype.collapse = function (applyToChildren) {
        var i, childNodes;
        this.collapsed = true;
        if (applyToChildren) {
            childNodes = this.items.asArray();
            for (i = 0; i < childNodes.length; i += 1) {
                childNodes[i].collapse(true);
            }
        }
        if (this.html) {
            jQuery(this.containmentArea).hide();
            jQuery(this.dom.nodeElement).addClass('pmui-treepanel-node-collapsed');
            if (typeof this.onCollapse === 'function') {
                this.onCollapse();
            }
        }

        return this;
    };
    /**
     * Applies the expanding to its child nodes list.
     * @param  {Boolean} applyToChildren If the child nodes will apply the expanding too.
     * @chainable
     */
    TreeNode.prototype.expand = function (applyToChildren) {
        var i,
            childNodes;

        this.collapsed = false;
        if (applyToChildren) {
            childNodes = this.items.asArray();
            for (i = 0; i < childNodes.length; i += 1) {
                childNodes[i].expand(true);
            }
        }
        if (this.html) {
            jQuery(this.containmentArea).show();
            jQuery(this.dom.nodeElement).removeClass('pmui-treepanel-node-collapsed');
            if (typeof this.onExpand === 'function') {
                this.onExpand();
            }
        }

        return this;
    };
    /**
     * Returns true if the node's child list is collapsed, otherwise it returns false.
     * @return {Boolean}
     */
    TreeNode.prototype.isCollapsed = function () {
        return this.collapsed;
    };
    /**
     * Returns true if the node has no child nodes, otherwise it returns false.
     * @return {Boolean}
     */
    TreeNode.prototype.isLeaf = function () {
        return !this.items.getSize();
    };
    /**
     * Returns true id the node is root, otherwise it returns false.
     * @return {Boolean}
     */
    TreeNode.prototype.isRoot = function () {
        return this.parent instanceof PMUI.panel.TreePanel || !this.parent;
    };
    /**
     * Updates the node's css class that sepcifies if the node is leaf or not.
     * @private
     * @chainable
     */
    TreeNode.prototype.updateNodeTypeClass = function () {
        if (this.items.getSize() === 0) {
            jQuery(this.dom.nodeElement).removeClass('pmui-treepanel-node-father').addClass('pmui-treepanel-node-leaf');
        } else {
            jQuery(this.dom.nodeElement).removeClass('pmui-treepanel-node-leaf').addClass('pmui-treepanel-node-father');
        }
        return this;
    };
    /**
     * The private handler fot the {@link #event-onBeforeAppend onBeforeAppend} event.
     * @private
     * @chainable
     */
    TreeNode.prototype.onBeforeAppendHandler = function (treeNode) {
        if (typeof this.onBeforeAppend === 'function') {
            this.onBeforeAppend(this, treeNode);
        }
        this.parent && this.parent.onBeforeAppendHandler(treeNode);
        return this;
    };
    /**
     * The private handler for the {@link #event-onAppend} event.
     * @private
     * @chainable
     */
    TreeNode.prototype.onAppendHandler = function (treeNode) {
        if (typeof this.onAppend === 'function') {
            this.onAppend(this, treeNode);
        }
        this.parent && this.parent.onAppendHandler(treeNode);
        return this;
    };
    /**
     * Adds a new child node.
     * @param {Object|PMUI.item.TreeNode} item  The item to add, it can be:
     * - A object literal with the config options to create the {@link PMUI.item.TreeNode TreeNode} to be added. If
     * you have set the {@link PMUI.item.TreeNode#property-childDefaultSettings childDefaultSettings property} the
     * contents of the object literal will be merge with (and into) the contens of this property. The resulting object
     * literal will be used to create the {@link PMUI.item.TreeNode TreeNode} to be added.
     * - An instance of {@link PMUI.item.TreeNode TreeNode}.
     * @param {Number} index The index position for the node insertion.
     */
    TreeNode.prototype.addItem = function (item, index) {
        var settings = {},
            toAdd;

        if (this.isSterile()) {
            throw new Error('addItem(): The tree node is sterile, it can\'t have children.');
        }
        if (typeof item !== 'object' && !(item instanceof PMUI.item.TreeNode)) {
            throw new Error('addItem(): The parameter must be an instance of PMUI.item.TreeNode or an object.');
        }

        if (item instanceof PMUI.item.TreeNode) {
            item.setParent(this);
            toAdd = item;
        } else if (typeof item === 'object') {
            if (this.recursiveChildrenDefaultSettings) {
                item.childrenDefaultSettings = this.childrenDefaultSettings;
                item.recursiveChildrenDefaultSettings = true;
            }
            jQuery.extend(true, settings, this.childrenDefaultSettings, item);
            settings.parent = this;
            toAdd = this.factory.make(settings);
        }

        if (toAdd) {
            if (typeof this.onBeforeAppend === 'function') {
                this.onBeforeAppend(this, toAdd);
            }
            this.onBeforeAppendHandler(toAdd);
            TreeNode.superclass.prototype.addItem.call(this, toAdd, index);
            this.onAppendHandler(toAdd);
        }

        if (this.html) {
            this.updateNodeTypeClass();
        }

        return this;
    };
    /**
     * @inheritdoc
     */
    TreeNode.prototype.setItems = function (items) {
        if (this.onAppend !== undefined) {
            TreeNode.superclass.prototype.setItems.call(this, items);
        }
        return this;
    };
    /**
     * Removes a direct child node.
     * @param  {Number|PMUI.item.TreeNode|String} item It can be a string (id of the child to remove),
     * a number (index of the child to remove) or a TreeNode object.
     * @chainable
     */
    TreeNode.prototype.removeItem = function (item) {
        TreeNode.superclass.prototype.removeItem.call(this, item);
        this.updateNodeTypeClass();
        return this;
    };
    /**
     * Toggles the collapsing/expanding actions.
     * @chainable
     */
    TreeNode.prototype.toggleCollapsing = function () {
        if (this.isCollapsed()) {
            this.expand();
        } else {
            this.collapse();
        }
        return this;
    };
    /**
     * Clears the current filter criteria.
     * @chainable
     */
    TreeNode.prototype.clearFilter = function () {
        this.filter("");
        return this;
    };
    /**
     * Filter the current node and all its children.
     * @param  {String} filterCriteria The criteria for filtering, the filtering will be perform in the node's data.
     * @return {Boolean} True if any of its nodes meets the filtering criteria.
     */
    TreeNode.prototype.filter = function (filterCriteria) {
        var res = false,
            childNodes = this.items.asArray(),
            i,
            regExp,
            partialRes;

        for (i = 0; i < childNodes.length; i += 1) {
            childNodes[i].setVisible(partialRes = childNodes[i].filter(filterCriteria));
            res = res || partialRes;
        }
        if (!res) {
            regExp = new RegExp(filterCriteria, "i");
            if (regExp.test(this.label)) {
                return true;
            }
        }
        return res;
    };
    /**
     * Define then events for the object.
     * @chainable
     */
    TreeNode.prototype.defineEvents = function () {
        var that;

        TreeNode.superclass.prototype.defineEvents.call(this);
        if (this.dom.title) {
            that = this;
            this.addEvent('click').listen(this.dom.title, function (e) {
                var root = that.getRoot(),
                    panel = root.parent;

                e.preventDefault();
                if (typeof that.onClick === 'function') {
                    that.onClick();
                }
                if (panel instanceof PMUI.panel.TreePanel && typeof panel.onNodeClick === 'function') {
                    panel.onNodeClick(panel, that);
                }
                that.toggleCollapsing();
            });
        }

        return this;
    };
    /**
     * Creates the node's html.
     * @return {HTMLELement}
     */
    TreeNode.prototype.createHTML = function () {
        var nodeElement,
            title,
            label,
            icon,
            childrenList,
            elbow;

        if (this.html) {
            return this.html;
        }

        html = PMUI.createHTMLElement(this.elementTag || "div");
        html.id = this.id;
        PMUI.linkToPMUIObject(html, this);
        this.html = html;
        this.actionHTML = html;
        this.applyStyle();

        nodeElement = this.html;
        nodeElement.className = 'pmui-treepanel-node';
        nodeElement.id = this.id;
        title = PMUI.createHTMLElement('a');
        title.className = 'pmui-treepanel-node-title';
        title.href = '#';
        elbow = PMUI.createHTMLElement('b');
        elbow.className = 'pmui-treepanel-node-elbow';
        elbow.innerHTML = '&nbsp;';
        label = PMUI.createHTMLElement('span');
        label.className = 'pmui-treepanel-node-label';
        icon = PMUI.createHTMLElement('i');
        icon.className = 'pmui-treepanel-node-icon';
        icon.innerHTML = '&nbsp;';
        childrenList = PMUI.createHTMLElement('ul');
        childrenList.className = 'pmui-treepanel-list';

        this.actionHTML = title;

        title.appendChild(elbow);
        title.appendChild(icon);
        title.appendChild(label);
        nodeElement.appendChild(title);
        nodeElement.appendChild(childrenList);

        this.dom.nodeElement = nodeElement;
        this.dom.title = title;
        this.dom.label = label;
        this.dom.icon = icon;
        this.containmentArea = childrenList;

        this.html = nodeElement;

        this.updateNodeTypeClass();

        this.setParent(this.parent)
            .setSterility(this.sterile)
            .setLabel(this.label)
            .setNodeClass(this.nodeClass);
        if (this.items.getSize() > 0) {
            this.paintItems(this.items.asArray().slice(0));
        }
        if (this.collapsed) {
            this.collapse();
        } else {
            this.expand();
        }

        this.style.applyStyle();
        return this.html;
    };

    PMUI.extendNamespace('PMUI.item.TreeNode', TreeNode);
}());