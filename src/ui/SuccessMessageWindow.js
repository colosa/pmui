(function () {
    /**
     * @class PMUI.ui.SuccessMessageWindow
     * A window to show a success message.
     * @extends {PMUI.ui.MessageWindow}
     *
     * Usage example:
     *
     *      @example
     *      var message_window = new PMUI.ui.SuccessMessageWindow({
     *          message: 'This is a success message.'
     *      });
     *      message_window.open();
     *
     * @constructor
     * Creates a new instance of the class SuccessMessageWindow
     * @param {Object} [settings={}] An object with the config options.
     *
     * @cfg {String} [title='Success'] The message to show in the message window.
     */
    var SuccessMessageWindow = function (settings) {
        SuccessMessageWindow.superclass.call(this, settings);
        SuccessMessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.MessageWindow', SuccessMessageWindow);
    /**
     * Initializes the object.
     * @param  {Object} settings The object with the config options.
     * @private
     */
    SuccessMessageWindow.prototype.init = function (settings) {
        var defaults = {
            title: 'Success'
        };

        jQuery.extend(true, defaults, settings);

        this.setTitle(defaults.title);
    };
    /**
     * Creates the message window ĤTML.
     * @return {HTMLElement}
     */
    SuccessMessageWindow.prototype.createHTML = function () {
        SuccessMessageWindow.superclass.prototype.createHTML.call(this);

        jQuery(this.dom.icon).addClass('pmui-messagewindow-icon-success');

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.SuccessMessageWindow', SuccessMessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = SuccessMessageWindow;
    }
}());