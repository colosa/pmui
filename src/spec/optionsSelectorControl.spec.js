//***********************other class******************************************************//
describe('PMUI.control.OptionsSelectorControl', function () {
	var item1, item2, a, b, c;
	beforeEach(function () {
		item1 = new PMUI.control.OptionsSelectorItemControl();
		item2 = new PMUI.control.OptionsSelectorItemControl();
		a = new PMUI.control.OptionsSelectorControl({
							position: "hbox",
							items:[item1,item2],
							listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }                 
                        }});
			document.body.appendChild(a.getHTML());
			a.defineEvents();
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-52,a]should have two or more SwitchButtonItems to set a status', function () {
			var x = new PMUI.control.OptionsSelectorItemControl(),
			y = new PMUI.control.OptionsSelectorItemControl(),
			z = new PMUI.control.OptionsSelectorItemControl(),
			switchb = new PMUI.control.OptionsSelectorControl({
							position: "hbox",
							items:[x,y,z],
							listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }                 
                        }});
			document.body.appendChild(switchb.getHTML());
			switchb.defineEvents();
			expect(switchb.items.asArray().length === 3).toBeTruthy();
			expect(switchb.itemsSelected.asArray().length === 0).toBeTruthy();
			$(switchb.getHTML()).remove();
			switchb = null;
		});
		it('[US-52,b]should set a initial status of the items', function () {
			var x = new PMUI.control.OptionsSelectorItemControl(),
			y = new PMUI.control.OptionsSelectorItemControl({selected:true}),
			z = new PMUI.control.OptionsSelectorItemControl(),
			switchb = new PMUI.control.OptionsSelectorControl({
							position: "hbox",
							items:[x,y,z],
							listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }                 
                        }});
			document.body.appendChild(switchb.getHTML());
			switchb.defineEvents();
			expect(switchb.items.asArray().length === 3).toBeTruthy();
			expect(switchb.itemsSelected.asArray()).toBeDefined();
			expect(switchb.itemsSelected.asArray()[0].id === y.id).toBeDefined();
			$(switchb.getHTML()).remove();
			switchb = null;
		});
	});
	describe('method "setOrientation"', function () {
		it('should set the items position (vbox, hbox)', function () {
			expect(a.orientation).toBeDefined();
			a.setOrientation('vertical');
			expect(a.orientation === 'vertical').toBeTruthy();
			a.setOrientation('horizontal');
			expect(a.orientation === 'horizontal').toBeTruthy();
		});
	});
	describe('method "setChildren"', function () {
		it('should set the switchbutton children, it must be an array', function () {
			var x = new PMUI.control.OptionsSelectorItemControl(),
			y = new PMUI.control.OptionsSelectorItemControl(),
			switchb = new PMUI.control.OptionsSelectorControl({
							position: "hbox",
							listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }                 
                        }});
			document.body.appendChild(switchb.getHTML());
			switchb.defineEvents();
			switchb.setChildren([x,y]);
			xid = x.id;
			yid = y.id;
			expect(switchb.items.asArray().length === 2).toBeTruthy();
			expect(switchb.items.asArray()[0].id === xid).toBeTruthy();
			expect(switchb.items.asArray()[1].id === yid).toBeTruthy();
			$(switchb.getHTML()).remove();
			switchb = null;
		});
	});
	describe('method "setIconClass"', function () {
		it('should set the icon Class, it must be a string', function () {
			a.setIconClass(234);
			expect(a.iconClass === '').toBeTruthy();
			a.setIconClass(true);
			expect(a.iconClass === '').toBeTruthy();
			a.setIconClass("pmui-switch-buttonitem-icon2");
			expect(a.iconClass === "pmui-switch-buttonitem-icon2");
		});
	});
	describe('method "setListeners"', function (){
		it('should set the switchbutton listeners', function () {
			switchb = new PMUI.control.OptionsSelectorControl({
							position: "hbox"         
                        });
			document.body.appendChild(switchb.getHTML());
			switchb.defineEvents();
			expect(switchb.listeners.select.length === 0).toBeTruthy();
			switchb.setListeners({select: function (item, event) {
                                console.log("selected", item);
                            }});
			expect(switchb.listeners.select.length === 2).toBeTruthy();
			$(switchb.getHTML()).remove();
			switchb = null;
		});
	});
	describe('method "addItem"', function () {
		it('should add an item to the switchbutton', function () {
			var item3 = new PMUI.control.OptionsSelectorItemControl();
			expect(a.items.asArray().length === 2).toBeTruthy();
			a.addItem(item3);
			expect(a.items.asArray().length === 3).toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});