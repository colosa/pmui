(function () {
    /**
     * @class PMUI.grid.GridPanelRow
     * Class that defines a row to be used in a {@link PMUI.grid.GridPanel GridPanel}.
     * The single class is not very useful alone, it should be used with a {@link PMUI.grid.GridPanel GridPanel}.
     * @extends {PMUI.core.Element}
     *
     * Usage example:
     *
     *      @example
     *      var row = new PMUI.grid.GridPanelRow({
     *              data: {
     *                  name: "John",
     *                  lastname: "McAllister"
     *              }
     *          }),
     *          grid = new PMUI.grid.GridPanel({
     *             pageSize: 6,
     *             columns:[
     *                 {
     *                     title:'columna1',
     *                     dataType:'string',
     *                     columnData: "name"
     *                 },
     *                 {
     *                     title:'columna2',
     *                     dataType:'number',
     *                     width : 150,
     *                     columnData: "lastName",
     *                     sortable: true
     *                 }
     *             ],
     *             items: [
     *                 row
     *             ]
     *          });
     *
     *      document.body.appendChild(grid.getHTML());
     *
     * @constructor
     * Creates a new instance of the GridPanelRow class.
     * @param {Object} settings A JSON object with the config options.
     *
     * @cfg {PMUI.data.DataField|Object} [data={}] A {@link PMUI.data.DataField DataField} or a JSON object with the
     * data for the GridPanelRow.
     * @cfg {PMUI.grid.GridPanel} [parent=null] The {@link PMUI.grid.GridPanel GridPanel} the GridPanelRow belongs to.
     */
    var GridPanelRow = function (settings) {
        GridPanelRow.superclass.call(this, jQuery.extend(settings, {elementTag: "tr"}));
        /**
         * An ArrayList that contains the row's cells ({@link PMUI.grid.GridPanelCell GridPanelCell}).
         * @type {PMUI.util.ArrayList}
         * @private
         */
        this.cells = null;
        /**
         * The parent GridPanel of the GridPanelRow.
         * @type {PMUI.grid.GridPanel GridPanel}
         * @readonly
         */
        this.parent = null;
        /**
         * The data that belongs to the GridPanelRow.
         * @type {PMUI.data.DataField}
         * @private
         */
        this.data = null;

        this.onClick = null;

        this.selected = null;
        this.onSelect = null;
        GridPanelRow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', GridPanelRow);
    /**
     * The class family.
     * @type {String}
     */
    GridPanelRow.prototype.family = 'GridPanelRow';
    /**
     * The object's family.
     * @type {String}
     */
    GridPanelRow.prototype.type = 'GridPanelRow';
    /**
     * Initialize the object.
     * @param  {Object} settings A JSON object with the config options.
     * @private
     */
    GridPanelRow.prototype.init = function (settings) {
        var defaults = {
            data: {},
            parent: null,
            onClick: null,
            onSelect: function () {
            }
        };

        jQuery.extend(true, defaults, settings);

        this.cells = new PMUI.util.ArrayList();
        this.data = new PMUI.data.DataField();

        this.setData(defaults.data)
            .setParent(defaults.parent)
            .setOnClick(defaults.onClick)
            .setOnSelectCallback(defaults.onSelect);
    };
    /**
     * Set the data for the object.
     * @param {PMUI.data.DataField|Object} dataç
     * @chainable
     */
    GridPanelRow.prototype.setData = function (data) {
        var key;

        if (data instanceof PMUI.data.DataField) {
            this.data = data;
        } else if (typeof data === 'object') {
            this.data.clear();
            for (key in data) {
                this.data.addAttribute(key, data[key]);
            }
        } else {
            throw new Error("setData(): it only accepts a JSON object o an instance of PMUI.data.DataField as parameter.");
        }

        if (this.html) {
            this.setCells();
        }

        return this;
    };
    /**
     * Set the parent grid for the object.
     * @param {PMUI.grid.GridPanel} parent
     * @chainable
     */
    GridPanelRow.prototype.setParent = function (parent) {
        if (parent) {
            if (parent instanceof PMUI.grid.GridPanel) {
                this.parent = parent;
            } else {
                throw new Error("setParent() method only accepts a object [PMUI.panel.GridPanel] as parameter.");
            }
        }
        return this;
    };
    /**
     * Returns the object's parent grid.
     * @return {PMUI.grid.GridPanel}
     */
    GridPanelRow.prototype.getParent = function () {
        return this.parent;
    };
    /**
     * Updates the width for each row's cell using the respective column's width.
     * @chainable
     */
    GridPanelRow.prototype.updateCellsWidth = function () {
        var i,
            cells,
            cellsLength;

        cellsLength = (cells = this.cells.asArray()).length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setWidth(cells[i].column.width);
        }
        return this;
    };
    /**
     * Paints a cell in the row.
     * @param  {PMUI.grid.GridPanelCell} cell
     * @chainable
     * @private
     */
    GridPanelRow.prototype.paintCell = function (cell) {
        if (this.html) {
            this.html.appendChild(cell.getHTML());
        }
        return this;
    };
    /**
     * Paints all cells in the row.
     * @chainable
     * @private
     */
    GridPanelRow.prototype.paintCells = function () {
        var i,
            cells = this.cells.asArray();

        for (i = 0; i < cells.length; i += 1) {
            this.paintCell(cells[i]);
        }
        return this;
    };
    /**
     * Add a single cell into the row.
     * @param {PMUI.grid.GridPanelColumn|null} [column=null] It can be:
     *
     * - null, in this case an empty cell will be added at the end of the row.
     * - A {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the cell will be created using the settings
     * the parameter has for its cells. Also the cell will be associated to the column.
     */
    GridPanelRow.prototype.addCell = function (column) {
        var data,
            columnData,
            cell,
            cellIndex,
            buttonLabel,
            buttonTooltip,
            content,
            that = this;

        if (!(column === null || column instanceof PMUI.grid.GridPanelColumn)) {
            throw new Error("addCell(): The parameter must be an instance of PMUI.grid.GridPanel o null.");
        }
        data = this.data.getRecord();
        cellIndex = this.parent.getColumns().length;
        cell = new PMUI.grid.GridPanelCell({
            columnIndex: cellIndex,
            row: this,
            column: column || null,
            visible: (column && column.visible) && true,
            width: (column && column.width) || "auto"
        });
        if (column) {
            columnData = column.getColumnData();
            if (column.dataType === 'button') {
                buttonLabel = column.buttonLabel;
                buttonTooltip = column.buttonTooltip;
                content = new PMUI.ui.Button({
                    style: column.getUsableButtonStyle(),
                    text: typeof buttonLabel === 'function' ? buttonLabel(this, data) : (buttonLabel || ""),
                    messageTooltip: (typeof buttonTooltip === 'function' ? buttonTooltip.call(cell, cell, data)
                        : (buttonTooltip || "")),
                    handler: (function (handler) {
                        return function () {
                            handler(that, that.parent);
                        };
                    }(column.getOnButtonClick()))
                });
            } else if (column.dataType === 'index') {
                content = this.parent.getItemIndex(this) + 1;
            } else if (columnData) {
                if (typeof columnData === 'function') {
                    content = columnData.call(this, data);
                } else {
                    content = this.data.getAttribute(columnData);
                }
            }

            cell.style.addClasses(["pmui-gridpanelcell-" + column.dataType]);
        } else {
            content = null;
        }
        cell.setContent(content);
        this.cells.insert(cell);
        column.addCell(cell);
        this.paintCell(cell);
        return this;
    };
    /**
     * @method setCells
     * Build the cells for the row using its parent {@link PMUI.grid.GridPanel#property-columns GridPanel's columns} to
     * retrieve the data from the odject's {@link PMUI.grid.GridPanelRow#property-data data}.
     * @chainable
     */
    GridPanelRow.prototype.setCells = function () {
        var i,
            columns;

        this.clearCells();
        if (this.parent) {
            columns = this.parent.getColumns();
            for (i = 0; i < columns.length; i += 1) {
                this.addCell(columns[i]);
            }
        }

        return this;
    };
    /**
     * Returns the data that belongs to the object.
     * @return {Object} A JSON object with the data.
     */
    GridPanelRow.prototype.getData = function () {
        return this.data.getRecord();
    };
    /**
     * Returns the object's cells.
     * @return {Array} An array in which every item is a object's {@link PMUI.grid.GridPanelCell cell}.
     */
    GridPanelRow.prototype.getCells = function () {
        return this.cells.asArray().slice(0);
    };
    /**
     * Clear the cells from the object.
     * @chainable
     */
    GridPanelRow.prototype.clearCells = function () {
        var i;
        while (this.cells.getSize()) {
            this.removeCell(0);
        }
        return this;
    };
    /**
     * Remove a cell from the object.
     * @param  {PMUI.grid.GridPanelCell|Number|String} cell It can be:
     *
     * - A instance of {@link PMUI.grid.GridPanelCell GridPanelCell}, in this case it must be a cell that belongs to
     * the object.
     * - A Number, in this case the cell with that index will be removed.
     * - A String, in this case the cell with that id will be removed.
     * @chainable
     */
    GridPanelRow.prototype.removeCell = function (cell) {
        var cellToRemove,
            column;

        if (cell instanceof PMUI.grid.GridPanelCell) {
            cellToRemove = cell;
        } else if (typeof cell === 'number') {
            cellToRemove = this.cells.get(cell);
        } else if (typeof cell === 'string') {
            cellToRemove = this.cells.find("id", cell)
        } else {
            throw new Error("removeCell(): This method only accepts a number or a string or a instance of " +
                "PMUI.grid.GridPanelCell as parameter");
        }

        if (cellToRemove) {
            cellToRemove.setColumnIndex(null);
            cellToRemove.row = null;
            jQuery(cellToRemove.html).remove();
            column = cellToRemove.getColumn();
            if (column) {
                column.removeCell(cellToRemove);
            }
            cellToRemove.column = null;
            this.cells.remove(cellToRemove);
        }

        return this;
    };
    /**
     * Attach the listeners for the object and every of its children.
     * @chainable
     */
    GridPanelRow.prototype.defineEvents = function () {
        var that = this;

        this.removeEvents().eventsDefined = true;
        if (this.html) {
            this.addEvent('click').listen(this.html, function (e) {
                e.preventDefault();
                if (typeof that.onClick === 'function') {
                    that.onClick(that, that.getData());
                }
                if (that.parent.selectable) {
                    if (!that.selected) {
                        that.selectRow();
                    }
                }
                if (that.parent && typeof that.parent.onRowClick === 'function') {
                    that.parent.onRowClick(that, that.getData());
                }
            });
        }
        return this;
    };
    /**
     * Creates the object's HTML.
     * @return {HTMLElement}
     */
    GridPanelRow.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        GridPanelRow.superclass.prototype.createHTML.call(this);
        this.paintCells()
            .defineEvents();
        return this.html;
    };
    /**
     * [setOnClick description]
     * @param {[type]} onClick [description]
     */
    GridPanelRow.prototype.setOnClick = function (onClick) {
        if (typeof  onClick !== 'function' && onClick !== null) {
            throw new Error('setOnClick(): the parameter is no valid, should be a function');
        }
        this.onClick = onClick;
        return this;
    };
    /**
     * [selectRow description]
     * @return {[type]} [description]
     */
    GridPanelRow.prototype.selectRow = function () {
        this.selected = true;
        this.style.addClasses(['pmui-active-row']);
        if (typeof this.onSelect === 'function') {
            this.onSelect();
        }
        return this;
    };
    /**
     * [isSelectedRow description]
     * @return {Boolean} [description]
     */
    GridPanelRow.prototype.isSelectedRow = function () {
        return this.selected;
    };
    /**
     * [deselectRow description]
     * @return {[type]} [description]
     */
    GridPanelRow.prototype.deselectRow = function () {
        if (this.onSelect) {
            this.selected = false;
            this.style.removeClasses(['pmui-active-row']);
        }

        return this;
    };
    /**
     * [setOnSelectRowCallback description]
     * @param {Function} callback [description]
     */
    GridPanelRow.prototype.setOnSelectCallback = function (callback) {
        if (typeof callback === 'function' || callback == null) {
            this.onSelect = callback;
        } else {
            throw new Error("setOnSelectCallback(): The parameter is not a function.");
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.grid.GridPanelRow', GridPanelRow);

    if (typeof exports !== 'undefined') {
        module.exports = GridPanelRow;
    }
}());