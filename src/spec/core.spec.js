//***********************other class******************************************************//
describe('PMUI.draw.Core', function () {
	var a, b, c;
	beforeEach(function() {
    	a = new PMUI.draw.Core(); 
    });
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var h = a.createHTML();
            expect(h).toBeDefined();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
	describe('method "setPosition"', function () {
		it('should set a new x and y position', function () {
			a.setPosition(4,5);
			expect(a.x === 4).toBeTruthy();
			expect(a.y === 5).toBeTruthy();
		});
	});
	describe('method "setDimension"', function () {
		it('should set new dimensions', function () {
			a.setDimension(3,2);
			expect(a.width === 3).toBeTruthy();
			expect(a.height === 2).toBeTruthy();
		});
	});
	describe('method "setX"', function () {
		it('should set a new value for x position', function () {
			a.setX(8);
			expect(a.x === 8).toBeTruthy();
		});
	});
	describe('method "setY"', function () {
		it('should set a new value for y position', function () {
			a.setY(10);
			expect(a.y === 10).toBeTruthy();
		});
	});
	describe('method "setAbsoluteX"', function () {
		it('should set a new absolute value for x', function () {
			a.setX(2);
			a.setAbsoluteX();
			expect(a.absoluteX === 2).toBeTruthy();
		});
	});
	describe('method "setOldX"', function () {
		it('should set the value to an old X reference', function () {
			a = new PMUI.draw.Core();
			a.setOldX(9);
			expect(a.oldX === 9).toBeTruthy();
		});
	});
	describe('method "setOldY"', function () {
		it('should set the value to an old Y reference', function () {
			a.setOldY(7);
			expect(a.oldY === 7).toBeTruthy();
		});
	});
	describe('method "setAbsoluteY"', function () {
		it('should set a new absolute value for y', function () {
			a.setY(3);
			a.setAbsoluteY();
			expect(a.absoluteY === 3).toBeTruthy();
		});
	});
	describe('method "setWidth"', function () {
		it('should set a new width if the value is accepted successfully', function () {
			expect(a.width).toBeNull();
			a.setWidth(-10);
			expect(a.width).toBeNull();
			a.setWidth(10);
			expect(a.width).not.toBeNull();
			expect(a.width === 10).toBeTruthy();
		});
	});
	describe('method "setHeight"', function () {
		it('should set a new height if the value is accepted successfully', function () {
			expect(a.height).toBeNull();
			a.setHeight(-10);
			expect(a.height).toBeNull();
			a.setHeight(10);
			expect(a.height).not.toBeNull();
			expect(a.height === 10).toBeTruthy();
		});
	});
	describe('method "setZOrder"', function () {
		it('should set a new ZOrder if the value is accepted successfully', function () {
			expect(a.zOrder).toBeDefined();
			expect(a.zOrder === 1).toBeTruthy();
			a.setZOrder(-1);
			expect(a.zOrder === 1).toBeTruthy();
			a.setZOrder(5);
			expect(a.zOrder === 5).toBeTruthy();
		});
	});
	describe('method "setVisible"', function () {
		it('should set the visible value', function () {
			expect(a.visible).toBeTruthy();
			a.setVisible(false);
			expect(a.visible).toBeFalsy();
		});
	});
	describe('method "setID"', function () {
		it('should set the id of the element', function () {
			a.setID("new_id");
			expect(a.id).not.toBeNull();
			expect(a.id).toBeDefined();
			expect(a.id === "new_id").toBeTruthy();
		});
	});
});