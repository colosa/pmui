//***********************other class******************************************************//
describe('PMUI.ui.Menu', function () {
    var menuButton,
            menu,
            objMenu;
    beforeEach(function () {
             objMenu = {
                id: "menu",
                items: [
                    {
                        text:"New",
                        icon: "pmui-customized-icon",
                        listeners: {
                            click: function() {
                                console.log("New");
                            }
                        }
                    },
                    {
                        pmType: "menuseparator"
                    },
                    {
                        text: "Edit",
                        icon: "pmui-customized-icon",
                        listeners: {
                            click: function(){
                                console.log("Edit");
                            }
                        }
                    },
                    {
                        text: "Remove",
                        icon: "pmui-customized-icon",
                        listeners: {
                            click: function(){
                                console.log("Remove");
                            }
                        }
                    },
                    {
                        text: "Disabled",
                        icon: "pmui-customized-icon",
                        disabled: true
                    },
                    {
                        text: "Styles",
                        icon: "pmui-customized-icon",
                        style: {
                            cssClasses: [
                                "pmui-sprite",
                                "pmui-menu-button"
                            ]
                        },
                        listeners: {
                            click: function(){
                                console.log("Styles");
                            }
                        }
                    }
                ]
            };
            //Button 1
            menuButton = new PMUI.ui.Button({
                text: 'my Menu',
                enabledMenu: true
            });
            menu = new PMUI.ui.Menu();    
            document.body.appendChild(menu.getHTML());           

            /*
            document.body.appendChild(menuButton.getHTML());
            menuButton.showMenu(objMenu);
            menuButton.defineEvents();
            */

            //menuButton.menu.removeContextMenu();
            //menuButton.menu.menuFactory(objMenu);
            
            /*
            menu = new PMUI.ui.menu(objMenu);
            menuButton.menu.menu = factory.make(objMenu);
            */
            /*$(menuButton.html).trigger({
                type: 'contextmenu',
                pageX: 100, 
                pageY: 100
            });*/     
         
    });
    afterEach(function () {
        
    }); 
    describe('class behavior', function () {
      
    });
    describe('method "calculateDimension"', function () {
        it('should return a correct width and height of the menu', function () {
            var c = 0, 
            h = 10 + 10, 
            i, 
            len, 
            text,
            counter=0, 
            w;
            for (i = 0; i < objMenu.items.length; i += 1) {
                switch (objMenu.items[i].pmtype){
                case 'CheckboxItem':
                    h += 20;
                    counter++;
                    break;
                case 'SeparatorItem':
                    //h += 4;
                    h += 0;                    
                    break;
                }
                text = objMenu.items[i].text || "";
                if (text !== "") {
                    h += 21;
                    counter++;
                  }               
            }
            //w = 21 + 48 + 2 + c;
            var t=counter*21;            
            expect(h >= t).toBeTruthy();            
        });
    });

    describe('method "setDimension"', function () {
        it('should sets a new width and height of the menu', function () {
            menu.setDimension(100,100);
            expect(menu.width == 100).toBeTruthy();
            expect(menu.height == 100).toBeTruthy();           
        });
    });

    describe('method "removeItem"', function () {
        it('should remove this menu', function () {
            menu.removeItem();            
            expect(menu.visible == false).toBeTruthy();
            expect(PMUI.currentContextMenu == null).toBeTruthy();           
        });
    });

    describe('method "show"', function () {
        it('should remove this menu', function () {
            menu.show(100,101);            
            expect(menu.visible == true).toBeTruthy();
            expect(menu.x == 100).toBeTruthy();
            expect(menu.y == 101).toBeTruthy();                        
        });
    });

    describe('method "createHTML"', function () {
        it('should create HTML for this menu', function () {
            menu.createHTML();            
            expect(menu.visible !== null).toBeTruthy();
        });
    });

    describe('method "setParent"', function () {
        it('should sets parent for this menu', function () {
            menu.setParent(menuButton);            
            expect(menu.parent !== null &&  typeof menu.parent == "object").toBeTruthy();
        });
    });
    
    describe('method "setClassName"', function () {
        it('should sets class name for this menu', function () {
            menu.setClassName("class-test")            
            expect(menu.className == "class-test").toBeTruthy();
        });
    });

    describe('method "setListeners"', function () {
        it('should sets listeners for this menu', function () {
            menu.setListeners(function (){
                console.log("test print !!!");
            });            
            expect(menu.listeners != null).toBeTruthy();
            expect(menu.listeners != undefined).toBeTruthy();
        });
    });
});

