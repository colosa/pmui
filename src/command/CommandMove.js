(function () {
    /**
     * @class PMUI.command.CommandMove
     * Encapsulates the action of moving an element
     *
     *              //e.g.
     *              var command = new PMUI.command.CommandMove(shape);
     * @extends PMUI.command.Command
     *
     * @constructor
     * Creates an instance of CommandMove
     * @param {Object} receiver The object that will perform the action
     */
    var CommandMove = function (receiver) {
        CommandMove.superclass.call(this, receiver);
        this.before = null;
        this.after = null;
        this.relatedShapes = [];
        CommandMove.prototype.initObject.call(this, receiver);
    };

    PMUI.inheritFrom('PMUI.command.Command', CommandMove);
    /**
     * Type of the instances of this class
     * @property {String}
     */
    CommandMove.prototype.type = "CommandMove";

    /**
     * Initializes the command parameters
     * @param {PMUI.draw.Core} receiver The object that will perform the action
     */
    CommandMove.prototype.initObject = function (receiver) {
        var i,
            beforeShapes = [],
            afterShapes = [];
        for (i = 0; i < receiver.getSize(); i += 1) {
            this.relatedShapes.push(receiver.get(i));
            beforeShapes.push({
                x: receiver.get(i).getOldX(),
                y: receiver.get(i).getOldY()
            });
            afterShapes.push({
                x: receiver.get(i).getX(),
                y: receiver.get(i).getY()
            });
        }
        this.before = {
            shapes: beforeShapes
        };
        this.after = {
            shapes: afterShapes
        };
        this.moveUndo = false;
    };

    /**
     * Executes the command, changes the position of the element, and if necessary
     * updates the position of its children, and refreshes all connections
     */
    CommandMove.prototype.execute = function () {
        var i,
            j,
            max,
            connection,
            shape,
            delta;
        //clear used array
        PMUI.getActiveCanvas().refreshArray.clear();
        PMUI.getActiveCanvas().connToRefresh.clear();
        for (i = 0; i < this.relatedShapes.length; i += 1) {
            shape = this.relatedShapes[i];
            //delta to move user connections
            delta = {
                dx: this.after.shapes[i].x - this.before.shapes[i].x,
                dy: this.after.shapes[i].y - this.before.shapes[i].y
            };
            shape.setPosition(this.after.shapes[i].x, this.after.shapes[i].y)
                .refreshChildrenPositions(true, delta);

            for (j = 0, max = this.canvas.connToRefresh.getSize(); j < max; j += 1) {
                connection = this.canvas.connToRefresh.get(j);
                connection.reconectSwitcher(delta, false);
            }
            shape.refreshConnections(false, this.relatedShapes, delta);

        }
        this.canvas.triggerPositionChangeEvent(this.relatedShapes,
            this.before.shapes, this.after.shapes);
        if (this.moveUndo) {
            for (j = 0; j < PMUI.getActiveCanvas().refreshArray.getSize(); j += 1) {
                connection = PMUI.getActiveCanvas().refreshArray.get(j);
                connection.reconnectUser(delta, false);
                connection.setSegmentMoveHandlers()
                    .checkAndCreateIntersectionsWithAll();
                PMUI.getActiveCanvas().triggerConnectionStateChangeEvent(connection);
            }
            this.moveUndo = false;
        }

    };

    /**
     * Returns to the state before the command was executed
     */
    CommandMove.prototype.undo = function () {
        var i,
            delta,
            shape,
            connection,
            j, max;
        PMUI.getActiveCanvas().refreshArray.clear();
        PMUI.getActiveCanvas().connToRefresh.clear();
        for (i = 0; i < this.relatedShapes.length; i += 1) {
            shape = this.relatedShapes[i];
            delta = {
                dx: this.before.shapes[i].x - this.after.shapes[i].x,
                dy: this.before.shapes[i].y - this.after.shapes[i].y
            };
            shape.setPosition(this.before.shapes[i].x, this.before.shapes[i].y)
                .refreshChildrenPositions(true, delta);

            for (j = 0, max = this.canvas.connToRefresh.getSize(); j < max; j += 1) {
                connection = this.canvas.connToRefresh.get(j);
                connection.reconectSwitcher(delta, false);
            }
            shape.refreshConnections(false, this.relatedShapes, delta);

        }
        this.canvas.triggerPositionChangeEvent(this.relatedShapes,
            this.after.shapes, this.before.shapes);

        for (j = 0; j < PMUI.getActiveCanvas().refreshArray.getSize(); j += 1) {
            connection = PMUI.getActiveCanvas().refreshArray.get(j);
            connection.reconnectUser(delta, false);
            connection.setSegmentMoveHandlers()
                .checkAndCreateIntersectionsWithAll();
            PMUI.getActiveCanvas()
                .triggerConnectionStateChangeEvent(connection);
        }
        this.moveUndo = true;
    };

    /**
     *  Executes the command again after an undo action has been done
     */
    CommandMove.prototype.redo = function () {
        this.execute();
    };

    PMUI.extendNamespace('PMUI.command.CommandMove', CommandMove);
}());
