//***********************other class******************************************************//
describe('PMUI.control.OptionsSelectorItemControl', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.control.OptionsSelectorItemControl();
	});
	afterEach(function () {
		a = null;
	});
	describe('method "setSelected"', function () {
		it('should set the status of the item, it must be a boolean value', function () {
			var s = a.selected;
			expect(s).not.toBeNull();
			expect(typeof s === 'boolean').toBeTruthy();
			a.setSelected("hello");
			expect(a.selected === s).toBeTruthy();
			a.setSelected(23);
			expect(a.selected === s).toBeTruthy();
			a.setSelected(true);
			expect(a.selected).toBeTruthy();
			expect(a.selected === s).toBeFalsy();
		});
	});
	describe('method "setCls"', function () {
		it('should set the className, it must be a string', function () {
			var cls = a.cls;
			expect(cls).not.toBeNull();
			expect(typeof cls === "string").toBeTruthy();
			a.setCls(234);
			expect(a.cls === '').toBeTruthy();
			a.setCls(true);
			expect(a.cls === '').toBeTruthy();
			a.setCls("pmui-switch-buttonItem2");
			expect(a.cls === "pmui-switch-buttonItem2");
		});
	});
	describe('method "setClsSelected"', function () {
		it('should set the className, it must be a string', function () {
			var clsSelected = a.clsSelected;
			expect(clsSelected).not.toBeNull();
			expect(typeof clsSelected === "string").toBeTruthy();
			a.setClsSelected(234);
			expect(a.clsSelected === '').toBeTruthy();
			a.setClsSelected(true);
			expect(a.clsSelected === '').toBeTruthy();
			a.setClsSelected("pmui-switch-buttonItem2-selected");
			expect(a.setClsSelected === "pmui-switch-buttonItem2-selected");
		});
	});
	describe('method "setIconClass"', function () {
		it('should set the icon Class, it must be a string', function () {
			var icon = a.iconCls;
			expect(icon).not.toBeNull();
			expect(typeof icon === "string").toBeTruthy();
			a.setIconClass(234);
			expect(a.iconCls === '').toBeTruthy();
			a.setIconClass(true);
			expect(a.iconCls === '').toBeTruthy();
			a.setIconClass("pmui-switch-buttonitem-icon2");
			expect(a.iconCls === "pmui-switch-buttonitem-icon2");
		});
	});
	describe('method "setText"', function () {
		it('should set the text for the switch button', function () {
			expect(typeof a.text === 'string').toBeTruthy();
			a.setText(34);
			expect(a.text === '').toBeTruthy();
			a.setText(true);
			expect(a.text === '').toBeTruthy();
			a.setText("title1");
			expect(a.text === 'title1').toBeTruthy();
		});
	});
	describe('method "selectItem"', function () {
		it('should select the item', function () {
			var p = new PMUI.control.OptionsSelectorControl({items:[a],listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }
                        }});
			document.body.appendChild(p.getHTML());
			a.selectItem();
			expect(a.selected).toBeTruthy();
			$(p.getHTML()).remove();
			p = null;
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var p = new PMUI.control.OptionsSelectorControl({items:[a],listeners: {
                            select: function (item, event) {
                                console.log("selected", item);
                            }
                        }});
			
			var html = p.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
            $(p.getHTML()).remove();
			p = null;
		});
	});
});