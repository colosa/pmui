(function () {
    /**
     * @class PMUI.control.TextControl
     * Class for handle the HTML native text input control.
     * @extends PMUI.control.HTMLControl
     *
     * Quick usage example:
     *
     *      @example
     *      myTextbox = new PMUI.control.TextControl({
     *          name: "my_name",
     *          value: "John Doe",
     *          maxLength: 12,
     *          placeholder: "insert your name here",
     *          disabled: false,
     *          onChange: function(currentValue, previousValue) {
     *              if(previousValue !== "") {
     *                  alert("Your name is not \"" + previousValue + "\" anymore.\nNow it's \"" + currentValue + "\"");
     *              } else {
     *                  alert("Now your name is " + currentValue);
     *              }
     *          }
     *      });
     *
     *      document.body.appendChild(myTextbox.getHTML());
     *
     * @constructor
     * Creates a new instance of the TextControl object.
     * @param {Object} [settings=null] A JSON object with the config options.
     *
     * @cfg {String} [placeholder=""] A string to be used as the control's placeholder.
     * @cfg {Number} [maxLength=524288] A number which specifies the maximum character length the control can accept.
     * @cfg {Function} [onKeyUp=null] A callback function to be called every time a pressed key is released on the
     * control. For info about the callback parameters please read the
     * {@link PMUI.control.TextControl#event-onKeyUp onKeyUp event} section.
     */
    var TextControl = function (settings) {
        TextControl.superclass.call(this, settings);
        /**
         * @property {String} [placeholder=""]
         * The control's placeholder (the text to be shown inside the control when there is not any text in it).
         * @readonly
         */
        this.placeholder = null;
        /**
         * @property {Number} [maxLength=524288] The maximum character length the control accepts.
         * @readonly
         */
        this.maxLength = null;
        /**
         * @event onKeyUp
         * Fired when a pressed key is released.
         * @param {Object} event The event object.
         */
        this.onKeyUp = null;
        TextControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', TextControl);

    TextControl.prototype.type = "TextControl";
    /**
     * Initializes the object.
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    TextControl.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 524288,
            onKeyUp: null,
            height: 30
        };

        $.extend(true, defaults, settings);

        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setOnKeyUpHandler(defaults.onKeyUp)
            .setHeight(defaults.height);

    };
    /**
     * Sets the callback function to be called when the keyup event occurs.
     * @chainable
     */
    TextControl.prototype.setOnKeyUpHandler = function (handler) {
        if (typeof handler === 'function') {
            this.onKeyUp = handler;
        }

        return this;
    };
    /**
     * Sets the placeholder test to show in the control when there's not ant value in it.
     * @param {String} placeholder
     * @chainable
     */
    TextControl.prototype.setPlaceholder = function (placeholder) {
        if (typeof placeholder === 'string') {
            this.placeholder = placeholder;
            if (this.html) {
                this.html.placeholder = placeholder;
            }
        }

        return this;
    };
    /**
     * Sets the maximun character number to be accepted in the control.
     * @param {Number} maxLength The number must be an integer.
     * If the value is minor or equal to 0 then the maxLength property is set to the default (524288).
     * @chainable>
     */
    TextControl.prototype.setMaxLength = function (maxLength) {
        if (typeof maxLength === 'number' && maxLength % 1 === 0) {
            this.maxLength = maxLength;
            if (this.html) {
                this.html.maxLength = maxLength > 0 ? maxLength : 524288;
            }
        } else {
            throw new Error("method setMaxLength() only accepts integer values.");
        }

        return this;
    };
    /**
     * Set a value in the parameter disabled.
     * @param {Boolean} value
     */
     TextControl.prototype.setDisabled = function (value) {
        if (typeof value === 'boolean') {
            this.disabled = value;
            if (this.html) {
                this.html.disabled = value;
            }
        } else {
            throw new Error("method setDisabled() only accepts boolean values.");
        }
    };
    /**
     * Set the events for the object.
     * @chainable
     */
    TextControl.prototype.defineEvents = function () {
        var that = this;
        TextControl.superclass.prototype.defineEvents.call(this);
        if (this.html) {
            this.addEvent('keyup').listen(this.html, function (e) {
                if (typeof that.onKeyUp === 'function') {
                    that.onKeyUp(e);
                }
                //if(e.which == 13){
                //    that.onChangeHandler();
                //}
            });
        }
        return this;
    };
    /**
     * Creates the HTML element for the object
     * @return {HTMLElement}
     */
    TextControl.prototype.createHTML = function () {
        TextControl.superclass.prototype.createHTML.call(this);
        this.html.type = "text";
        this.setPlaceholder(this.placeholder)
            .setMaxLength(this.maxLength)
            .setReadOnly(this.readonly);

        return this.html;
    };
    /**
     * Sets if the control will be enabled for read only.
     * @param {Boolean}
     * @chainable
     */
    TextControl.prototype.setReadOnly = function (readonly) {
        if (typeof readonly !== 'undefined') {
            this.readonly = !!readonly;
            if (this.html) {
                this.html.readOnly = this.readonly;
            }
        }
        return this;
    };
    /**
     * Returns true if the control is enabled for read only.
     * @return {Boolean}
     */
    TextControl.prototype.isReadOnly = function () {
        return this.readonly;
    };

    TextControl.prototype.getMaxLength = function () {
        return this.maxLength;
    }

    PMUI.extendNamespace('PMUI.control.TextControl', TextControl);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = TextControl;
    }
}());