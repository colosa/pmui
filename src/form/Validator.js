(function () {
    /**
     * @class PMUI.form.Validator
     * Handles the validations of the fields
     * @extend PMUI.core.Base
     *
     * @constructor
     * Create a new instance of the class
     * @param {Object} settings
     * @param {PMUI.form.Field} parent
     *
     * @cfg {*} [criteria=null] This is the criteria info to execute the validation,
     * each validator needs a different data type criteria,
     * so please read about the criteria config option for every validator object.
     * @cfg {String} [errorMessage='the validation has failed'] This property defines
     * the error message to display if validation fails when filled fields
     * @cfg {PMUI.form.Field} [parent=null] Defines the field the validator belongs to.
     */
    var Validator = function (settings) {

        Validator.superclass.call(this, settings);
        /**
         * Defines the Field parent
         * @type {PMUI.form.Field}
         */
        this.parent = null;
        /**
         * Defines the criteria object
         * @type {Object}
         */
        this.criteria = null;
        /**
         * Defines if the object is validated
         * @type {Boolean}
         */
        this.validated = false;
        /**
         * Defines the validation state
         * @type {null/Boolean}
         */
        this.valid = null;
        /**
         * Defines the error message to show in case of the validation fails
         * @type {null/Boolean}
         */
        this.errorMessage = null;

        Validator.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Base', Validator);
    /**
     * Defines the object's type
     * @type {String}
     */
    Validator.prototype.type = 'Validator';
    /**
     * Defines the object's family
     * @type {String}
     */
    Validator.prototype.family = 'Validator';
    /**
     * Initializes the object with default values
     * @param {Object} settings
     * @param {PMUI.form.Field} parent
     */
    Validator.prototype.init = function (settings) {
        var defaults = {
            criteria: null,
            errorMessage: 'the validation has failed',
            parent: null
        };
        jQuery.extend(true, defaults, settings);
        this.setCriteria(defaults.criteria)
            .setParent(defaults.parent)
            .setErrorMessage(defaults.errorMessage);
    };
    /**
     * Sets the validation error message to show in case of the validation fails
     * @param {String} errorMessage
     * @chainable
     */
    Validator.prototype.setErrorMessage = function (errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    };
    /**
     * Sets the validation error message to show in case of the validation fails
     * @param {String} errorMessage
     * @chainable
     */
    Validator.prototype.getErrorMessage = function () {
        return this.errorMessage;
    };
    /**
     * Sets the validation criteria
     * @param {Object} criteria
     * @chainable
     */
    Validator.prototype.setCriteria = function (criteria) {
        this.criteria = criteria;
        return this;
    };
    /**
     * Sets the parent field
     * @param {PMUI.form.Field|GridControlCell} parent
     * @chainable
     */
    Validator.prototype.setParent = function (parent) {
        if (parent) {
            if (parent instanceof PMUI.form.Field || parent instanceof PMUI.grid.GridControlCell) {
                this.parent = parent;
            } else {
                throw new Error("setParent() method only accepts a Field object as parameter.");
            }
        }
        return this;
    };

    /**
     * Returns the parent field
     * @return {PMUI.form.Field}
     */
    Validator.prototype.getParent = function () {
        return this.parent;
    };
    /**
     * Evaluates the validator
     */
    Validator.prototype.validate = function () {
        this.valid = true;
    };
    /**
     * Returns the validation response
     * @chainable
     */
    Validator.prototype.isValid = function () {
        this.validate();
        this.updateTooltip();
        return this.valid;
    };
    /**
     * Updates de the tooltip message
     */
    Validator.prototype.updateTooltip = function () {
        if (this.parent && this.parent.message) {
            if (this.valid) {
                this.parent.hideMessage();
            } else {
                this.parent.showMessage(this.errorMessage, "error");
            }
        }
    };
    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Validator;
    }

    PMUI.extendNamespace('PMUI.form.Validator', Validator);

}());