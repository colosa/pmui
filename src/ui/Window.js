(function () {
    /**
     * @class PMUI.ui.Window
     * @extend PMUI.core.Panel
     *    Usage example:
     *
     *      @example
     *
     *        f = new PMUI.form.Form({
     *               items: [
     *                   {
     *                       pmType: "text",
     *                       label: "Name",
     *                       id: "123",
     *                       value: "",
     *                       placeholder: "insert your name",
     *                       name: "name",
     *                       helper: "Introduce your name",
     *                       required : true,
     *                       controlsWidth: 200,
     *                       valueType: 'string'
     *                   },
     *                   {   
     *                       pmType: "text",
     *                       label: "test Number",
     *                       id: "1234",
     *                       value: 10,
     *                       placeholder: "insert your number",
     *                       name: "numberTest",
     *                       helper: "Introduce your number",
     *                       required : true,
     *                       controlsWidth: 200,
     *                       valueType: 'number'
     *                   },
     *                   {
     *                       pmType: "datetime",
     *                       visible : true,
     *                       label: "birth date",
     *                       name: "birthdate",
     *                       valueType: 'date',
     *                       required: true
     *                   }
     *               ]
     *           });
     *
     *      w = new PMUI.ui.Window({
     *               title: "Window Example",
     *               width: 500,
     *               height: 230,
     *               modal: true,
     *               buttons: [
     *                   {
     *                       pmType: 'button',
     *                       text: 'Save',
     *                       handler: function() {
     *                           alert("Saved!");
     *                       }
     *                   },
     *                   {
     *                       pmType: 'label',
     *                       text: " or "
     *                   },
     *                   {
     *                       pmType: 'button',
     *                       text: "Close",
     *                       handler : function () {
     *                           w.close();
     *                       },
     *                       border: 'link'
     *                   }
     *               ],
     *               closable: true,
     *               footerItems: [
     *                   {
     *                       text: "Close"
     *                   }
     *               ],
     *               visibleFooter: true,
     *               buttonPanelPosition: 'top'
     *           });
     *           w.addItem(f);
     *           w.open();
     *           w.defineEvents();
     *
     * Create a new instace of the Window class
     *
     * @cfg {String}  [title = [Untitled window]] Title The title for the Window
     * @cfg {Boolean} [modal = true] Defines the  window property  is modal or not
     * @cfg {String|Number} [height = "auto"] window height can be a number or a string. height
     * is the div
     * Element which contains a header, body and footer. When using a string that you can only use
     * 'auto' or 'inherit' or # # # # px or% or # # # # em when is a number
     * @cfg {String|Number} [width = 400] The width of the main window which contains a html element
     * Element 'div' header, body and footer
     * @cfg {String|Number} [footerHeight = "auto"] the height of the Element div footer (px, auto,
     * number)which default 'auto'
     * @cfg {PMUI.util.ArrayList} [buttons = []], defines an array of objects that are buttons
     * settings may in the fotter Element div.
     * @deprecated This config optio will be removed soon, please use the {@link #cfg-footerItems footerItems} config
     * option instead.
     * @cfg {boolean} [visibleCloseButton = true], the button close are can define or not in the window.
     * @cfg {Array} [footerItems=[]] Sets the elements in the window footer, this elements can be instances of Button
     * and/or instances of Label. The value for this config option must be an array in which each element can be:
     *
     * - An object literal, in this case the object literal must have the property "pmType" with its value set to
     * "button" (if you want the element be a {@link PMUI.ui.Button Button}) or "label" (if you want the element be a
     * {@link PMUI.ui.TextLabel Label}). Optionally you can add the respective config options for each case.
     *
     * - A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of
     * {@link PMUI.ui.TextLabel Label}.
     *
     * @cfg {String} [footerAlign="center"] Sets the horizontal alignment for the items in the wondow footer, the
     * possible values are "center", "left", "right".
     * @cfg {Boolean} [visibleFooter="false"] indicating whether the footer of the Window is visible or was hidden
     * @cfg {String}  [buttonPanelPosition="bottom"] indicates that position does the panel of buttons, you can be
     * above or below body.
     * @cfg {Function|null} [onOpen=null] The callback function to be executed everytime the
     * {@link #event-onOpen onOpen} event.
     * @cfg {Function|null} [onClose=null] The callback function to be executed everytime the
     * {@link #event-onClose onClose} event.
     */
    var Window = function (settings) {
        Window.superclass.call(this, jQuery.extend(true, {
            positionMode: 'absolute'
        }, settings));
        /**
         * The window's header.
         * @type {HTMLElement}
         * @private
         */
        this.header = null;
        /**
         * The window's body.
         * @type {HTMLElement}
         * @private
         */
        this.body = null;
        /**
         * The window's footer.
         * @type {PMUI.panel.ButtonPanel}
         * @private
         */
        this.footer = null;
        /**
         * @property {String} title
         * The window's title. Set by the {@link #cfg-title title} config option and the
         * {@link #method-setTitle setTitle()} method.
         * @readonly
         */
        this.title = null;
        /**
         * The windo's close button.
         * @type {PMUI.ui.Button}
         * @private
         */
        this.closeButton = null;
        /**
         * If the window is opened or not.
         * @type {Boolean}
         * @readonly
         */
        this.isOpen = false;
        /**
         * is defined if button panes Object is visible
         * @type {[type]}
         */
        this.visibleFooter = null;
        /**
         * If the window's close button is visible or not. Set by the
         * {@link #cfg-visibleCloseButton visibleCloseButton} config option and the
         * {@link #method-showCloseButton showCloseButton()} and {@link #method-hideCloseButton hideCloseButton()}
         * methods.
         * @type {Boolean}
         * @readonly
         */
        this.visibleCloseButton = null;
        /**
         * If the window will be a modal window. Set by the {@link #cfg-modal modal} config option and the
         * {@link #method-setModal setModal()} method.
         * @type {Boolean}
         * @readonly
         */
        this.modal = null;
        /**
         * The window's footer height.
         * @type {Number|String}
         */
        this.footerHeight = null;
        /**
         * The window's footer alignment.
         * @type {String}
         */
        this.footerAlign = null;
        /**
         * defines the position of the buttons panel on the window
         * @type {String}
         */
        this.buttonPanelPosition = null;
        /**
         * @event onOpen
         * Fired everytime the window opens.
         * @param {PMUI.ui.Window} window The window that was opened.
         */
        this.onOpen = null;
        /**
         * @event onClose
         * Fired everytime the window closes.
         * @param {PMUI.ui.Window} window The window that was closed.
         */
        this.onClose = null;
        this.dom = {};
        this.onBeforeClose = null;
        Window.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Panel', Window);
    /**
     * Defines the object's type
     * @type {String}
     */
    Window.prototype.type = 'Window';
    /**
     * Defines the object's family
     * @type {String}
     */
    Window.prototype.family = 'ui';

    Window.prototype.init = function (settings) {
        var defaults = {
            height: 'auto',
            width: 'auto',
            title: '[Untitled window]',
            modal: true,
            footerHeight: 'auto',
            zOrder: 100,
            footerItems: [],
            footerAlign: 'center',
            visibleCloseButton: true,
            visibleFooter: false,
            buttons: [],
            buttonPanelPosition: 'bottom',
            onOpen: null,
            onClose: null,
            onBeforeClose: null
        };

        jQuery.extend(true, defaults, settings);

        this.footer = new PMUI.panel.ButtonPanel({
            style: {
                cssClasses: ['pmui-window-footer']
            }
        });

        this.setTitle(defaults.title)
            .setModal(defaults.modal)
            .setWidth(defaults.width)
            .setHeight(defaults.height)
            .setFooterHeight(defaults.footerHeight)
            .setFooterAlign(defaults.footerAlign)
            .setZOrder(defaults.zOrder)
            .setButtonPanelPosition(defaults.buttonPanelPosition)
            .setFooterItems(defaults.footerItems)
            .setOnOpenHandler(defaults.onOpen)
            .setOnCloseHandler(defaults.onClose);
        this.setOnBeforeCloseHandler(defaults.onBeforeClose);

        if (defaults.visibleCloseButton) {
            this.showCloseButton();
        } else {
            this.hideCloseButton();
        }
        if (defaults.visibleFooter) {
            this.showFooter();
        } else {
            this.hideFooter();
        }
        //TODO tell the guys to replace its 'buttons' config option by FooterItems
        if (defaults.footerItems && defaults.footerItems.length) {
            this.setFooterItems(defaults.footerItems);
        } else {
            this.setFooterItems(defaults.buttons);
        }
    };
    /**
     * Sets the callback function to be executed everytime the {@link #event-onClose onClose} event fires.
     * @param {Function|null} handler It can be a function or null.
     */
    Window.prototype.setOnCloseHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnCloseHandler(): The parameter must be a function or null.");
        }
        this.onClose = handler;
        return this;
    };

    Window.prototype.setOnBeforeCloseHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnCloseHandler(): The parameter must be a function or null.");
        }
        this.onBeforeClose = handler;
        return this;
    };

    /**
     * Sets the callback function to be executed everytime the {@link #event-onOpen onOpen} event fires.
     * @param {Function|null} handler It can be a function or null.
     */
    Window.prototype.setOnOpenHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnOpenHandler(): The parameter must be a function or null.");
        }
        this.onOpen = handler;
        return this;
    };
    /**
     * updates the dimensions of the height of the HTMLElement window, body and foot when
     it is modified by the user
     * @chainable
     */
    Window.prototype.updateDimensionsAndPosition = function () {
        var bodyHeight,
            footerHeight,
            headerHeight,
            windowHeight = this.height,
            windowWidth;

        if (!this.footer || !this.html || !this.isOpen) {
            return this;
        }
        if (this.footerHeight === 'auto') {
            this.footer.setHeight('auto');
        } else {
            this.footer.setHeight(this.footerHeight);
        }

        if (windowHeight === 'auto') {
            this.body.style.height = 'auto';
            this.body.style.minHeight = '150px';
        } else {
            if (/^\d+(\.\d+)?em$/.test(windowHeight)) {
                windowHeight = PMUI.emToPx(parseInt(windowHeight, 10), this.modalObject);
            } else if (/^\d+(\.\d+)?%$/.test(windowHeight)) {
                windowHeight = jQuery(this.html).outerHeight();
            }
            footerHeight = this.visibleFooter ? jQuery(this.footer.getHTML()).outerHeight() : 0;
            headerHeight = jQuery(this.header).outerHeight();
            bodyHeight = windowHeight - footerHeight - headerHeight;
            if (bodyHeight <= 0) {
                bodyHeight = 0;
            }
            this.body.style.minHeight = '';
            this.body.style.height = bodyHeight + "px";
        }

        windowWidth = jQuery(this.header).width();
        windowWidth = windowWidth - (this.visibleCloseButton ? jQuery(this.closeButton.getHTML()).outerWidth() : 0);
        this.dom.titleContainer.style.width = windowWidth < 0 ? 0 : windowWidth + 'px';

        windowWidth = jQuery(this.html).outerWidth();
        windowHeight = jQuery(this.html).outerHeight();

        this.addCSSProperties({
            left: '50%',
            "margin-left": (windowWidth / -2) + "px",
            top: '50%',
            "margin-top": (windowHeight / -2) + "px"
        });

        return this;
    };
    /**
     * shows the footer where the buttons are located
     * @chainable
     */
    Window.prototype.showFooter = function () {
        this.footer.setVisible(this.visibleFooter = true);
        return this.updateDimensionsAndPosition();
    };
    /**
     * hides the footer where the buttons are located
     * @chainable
     */
    Window.prototype.hideFooter = function () {
        this.footer.setVisible(this.visibleFooter = false);
        return this.updateDimensionsAndPosition();
    };
    /**
     * @method setTitle
     * Sets the window title in the header part of the window.
     * @param {String} title
     * @chainable
     */
    Window.prototype.setTitle = function (title) {
        if (typeof title === 'string') {
            this.title = title;
            if (this.dom.titleContainer) {
                this.dom.titleContainer.textContent = title;
                this.dom.titleContainer.title = title;
            }
        } else {
            throw new Error("The setTitle() method accepts only string values.");
        }
        return this;
    };
    /**
     * @method getTitle
     * its title of teh window
     * @return {String}
     */
    Window.prototype.getTitle = function () {
        return this.title;
    };
    /**
     * @method setModal
     * Set if the window is modal or not.
     * @param {Boolean} modal
     * @chainable
     */
    Window.prototype.setModal = function (modal) {
        if (typeof modal !== 'undefined') {
            this.modal = !!modal;
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    Window.prototype.setWidth = function (width) {
        Window.superclass.prototype.setWidth.call(this, width);
        return this.updateDimensionsAndPosition();
    };
    /**
     * @inheritdoc
     */
    Window.prototype.setHeight = function (height) {
        Window.superclass.prototype.setHeight.call(this, height);
        return this.updateDimensionsAndPosition();
    };
    /**
     * @method  setFooterHeight
     * high fixed size for the footer of the window
     * @param {Number|String} height it can be a number or a string.
     In case of using a String equals to 'auto','inherit','##px','##%','##em'
     * @chainable
     */
    Window.prototype.setFooterHeight = function (footerHeight) {
        if (typeof footerHeight === 'number') {
            this.footerHeight = footerHeight;
        } else if (/^\d+(\.\d+)?px$/.test(footerHeight)) {
            this.footerHeight = parseInt(footerHeight, 10);
        } else if (footerHeight === 'auto') {
            this.footerHeight = footerHeight;
        } else {
            throw new Error('setFooterHeight: footerHeight param is not valid.');
        }
        if (this.footer) {
            this.footer.style.height = this.footerHeight + 'px';
        }
        if (this.isOpen) {
            this.updateDimensionsAndPosition();
        }

        return this;
    };
    /**
     * Sets the alignment for the items on the footer.
     * @param {String} align Possible values: "center", "left", "right".
     * @chainable
     */
    Window.prototype.setFooterAlign = function (align) {
        this.footer.setAlignment(align);
        this.footerAlign = align;
        return this;
    };
    /**
     * buttonPanelPosition parameter sets that have the panel of buttons, within the window
     * @param {String} buttonPanelPosition Accepted values ​​are 'top' or 'bottom'
     */
    Window.prototype.setButtonPanelPosition = function (buttonPanelPosition) {
        if (!(buttonPanelPosition === 'top' || buttonPanelPosition === 'bottom')) {
            throw  new Error('setButtonPanelPosition(): the value is not valid, should be a "top" or "bottom"');
        }
        this.buttonPanelPosition = buttonPanelPosition;
        if (this.html) {
            if (buttonPanelPosition === 'top') {
                this.html.insertBefore(this.footer.html, this.body);
            } else {
                this.html.appendChild(this.footer.html);
            }
        }
        return this;
    };
    /**
     * Clear the items in the window footer.
     * @chainable
     */
    Window.prototype.clearFooterItems = function () {
        this.footer.clearItems();
        return this;
    };
    /**
     * Adds an item to the window footer.
     * @param {Object|PMUI.ui.Button|PMUI.ui.TextLabel} item This parameter can be:
     *
     * - An object literal, in this case the object literal must have the property "pmType" with its value set to
     * "button" (if you want the element be a {@link PMUI.ui.Button Button}) or "label" (if you want the element be a
     * {@link PMUI.ui.TextLabel Label}). Optionally you can add the respective config options for each case.
     *
     * - A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of
     * {@link PMUI.ui.TextLabel Label}.
     * @chainable
     */
    Window.prototype.addFooterItem = function (item) {
        this.footer.addItem(item);
        return this;
    };
    /**
     * Sets the items for the window's footer.
     * @param {Array} items This accepted value for this parameter is the same than the one for the
     * {@link #cfg-footerItems footerItems} config option.
     * @chainable
     */
    Window.prototype.setFooterItems = function (items) {
        var i;

        if (!jQuery.isArray(items)) {
            throw new Error("setFooterItems(): The parameter must be an array.");
        }
        this.clearFooterItems();
        for (i = 0; i < items.length; i += 1) {
            this.addFooterItem(items[i]);
        }
        return this;
    };
    /**
     * @method close
     * if the window is open, sets the status to closed
     * @chainable
     */
    Window.prototype.close = function () {
        jQuery(this.modalObject).detach();
        jQuery(this.html).detach();
        jQuery(this.closeButton).detach();
        if (typeof this.onClose === 'function') {
            this.onClose(this);
        }
        this.isOpen = false;
        if (document.body && this.modal) {
            document.body.style.overflow = "auto";
        }
        return this;
    };
    /**
     * Shows the window's close button.
     * @chainable
     */
    Window.prototype.showCloseButton = function () {
        this.visibleCloseButton = true;
        if (this.closeButton) {
            this.closeButton.setVisible(true);
        }
        return this;
    };
    /**
     * Adds an child item to the body HTMLElement
     * @param {PMUI.core.Window|Object} item It can be one of the following data types:
     * - {PMUI.core.Element} the object to add
     * - {Object} a JSON object with the settings for the Container to be added
     * @chainable
     */
    Window.prototype.addItem = function (item) {
        var itemToBeAdded;

        if (this.factory) {
            itemToBeAdded = this.factory.make(item);
        }
        if (itemToBeAdded && !this.isDirectParentOf(itemToBeAdded)) {
            itemToBeAdded.parent = this;
            this.items.insert(itemToBeAdded);
            if (this.body) {
                this.body.appendChild(itemToBeAdded.getHTML());
                if (this.eventsDefined) {
                    itemToBeAdded.defineEvents();
                }
            }
        }
        return this;
    };
    /**
     * @method open
     * If the window is closed, sets the status to open and displays the values ​​set in the properties window, header, body and footer
     * @chainable
     */
    Window.prototype.open = function () {
        var the_window;

        if (this.isOpen) {
            return this;
        }
        the_window = this.getHTML();
        if (this.modal) {
            this.modalObject.appendChild(the_window);
            document.body.appendChild(this.modalObject);
            jQuery(the_window).draggable({
                handle: $(this.header),
                containment: '#' + this.modalObject.id, 
                scroll: false
            });
        } else {
            document.body.appendChild(the_window);
            jQuery(this.getHTML()).draggable();
        }
        if (typeof this.onOpen === 'function') {
            this.onOpen(this);
        }
        this.isOpen = true;
        this.updateDimensionsAndPosition();
        this.setVisible(true);
        this.defineEvents();
        if (document.body && this.modal) {
            document.body.style.overflow = "hidden"
        }
        return this;
    };
    /**
     * Hides the window's close button.
     * @chainable
     */
    Window.prototype.hideCloseButton = function () {
        this.visibleCloseButton = false;
        if (this.closeButton) {
            this.closeButton.setVisible(false);
        }
        return this;
    };
    /**
     * Shows the window's close button.
     * @chainable
     */
    Window.prototype.showCloseButton = function () {
        this.visibleCloseButton = true;
        if (this.closeButton) {
            this.closeButton.setVisible(true);
        }
        return this;
    };
    /**
     * @method updateModalDimensions
     * Updates the status of modal HTMLElement and
     fixed all styles required to show
     */
    Window.prototype.updateModalDimensions = function () {
        if (document && this.modalObject) {
            this.modalObject.style.height = this.modalObject.style.width = "0px";
            this.modalObject.style.width = window.innerWidth + "px";
            this.modalObject.style.height = window.innerHeight + "px";
        }
        return this;
    };
    /**
     * Sets the padding for the windows body.
     * @param {Number|String} bodyPadding A number or a string with a pixel-unit formatted text.d
     */
    Window.prototype.setBodyPadding = function (bodyPadding) {
        if (typeof bodyPadding === 'number') {
            this.bodyPadding = bodyPadding
        } else if (/^\d+(\.\d+)?px$/.test(height)) {
            this.bodyPadding = parseInt(bodyPadding, 10);
        } else {
            throw new Error('setHeight: height param is not valid.');
        }

        this.bodyPadding = bodyPadding;
        if (this.html) {
            this.body.style.padding = bodyPadding + 'px';
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    Window.prototype.paintItems = function () {
        if (this.body) {
            Window.superclass.prototype.paintItems.call(this);
        }
        return this;
    };
    /**
     * @method defineEvents
     * Defines the events associated with the Window
     */
    Window.prototype.defineEvents = function () {
        var that = this,
            html = this.html,
            modal = this.modal,
            stopPropagation,
            updateDimensions,
            cancelWheeling = function (e) {
                var height = that.body.clientHeight,
                    scrollHeight = that.body.scrollHeight,
                    scrollTop = that.body.scrollTop;

                e.stopPropagation();
            };

        Window.superclass.prototype.defineEvents.call(this);
        stopPropagation = new PMUI.event.Action({
            handler: function (e) {
                e.stopPropagation();
            }
        }),
            updateDimensions = new PMUI.event.Action({
                handler: function () {
                    that.updateModalDimensions();
                }
            });
        this.addEvent('mousedown').listen(this.body, stopPropagation);
        that.addEvent('click').listen(this.html, stopPropagation);
        that.addEvent('mouseover').listen(this.html, stopPropagation);
        that.addEvent('mouseout').listen(this.html, stopPropagation);
        that.addEvent('mousedown').listen(this.html, stopPropagation);
        that.addEvent('resize').listen(window, updateDimensions);
        this.addEvent('keydown').listen(this.html, stopPropagation);
        this.addEvent('keypress').listen(this.html, stopPropagation);
        this.addEvent('keyup').listen(this.html, stopPropagation);

        this.modalObject.addEventListener('wheel', function (e) {
            e.preventDefault();
            e.stopPropagation();
        }, false);

        this.body.addEventListener('wheel', cancelWheeling, false);
        this.footer.defineEvents();
        that.closeButton.defineEvents();
        this.updateModalDimensions();
        return this;
    };
    /**
     * @method createHTML
     * Creates natural elements to the window html: head, body and foot
     * @return {HTMLElement} returns a HTML element
     */
    Window.prototype.createHTML = function () {
        var header,
            body,
            titleContainer,
            closeButton,
            modal,
            that = this;

        if (this.html) {
            return this.html;
        }

        Window.superclass.prototype.createHTML.call(this);

        header = PMUI.createHTMLElement('div');
        header.className = 'pmui-window-header';
        body = PMUI.createHTMLElement('div');
        body.className = 'pmui-window-body';
        modal = PMUI.createHTMLElement('div');
        modal.className = 'pmui-window-modal';
        modal.id = this.id + "-modal";

        titleContainer = PMUI.createHTMLElement('span');
        titleContainer.className = 'pmui-window-title';
        titleContainer.style.display = "inline-block";

        closeButton = new PMUI.ui.Button({
            style: {
                cssClasses: ['pmui-window-close']
            },
            text: "",
            width: '16px',
            handler: function (event) {
                if (typeof that.onBeforeClose === "function") {
                    that.onBeforeClose(that);
                } else {
                    that.close();
                }
            }
        });

        this.modalObject = modal;
        header.appendChild(titleContainer);
        header.appendChild(closeButton.getHTML());
        $(closeButton.html).find(".button-icon").css('opacity', 'inherit');

        this.html.appendChild(header);
        this.html.appendChild(body);
        this.html.appendChild(this.footer.getHTML());

        this.dom.titleContainer = titleContainer;
        this.header = header;
        this.body = this.containmentArea = body;
        this.closeButton = closeButton;

        this.setTitle(this.title)
            .setFooterHeight(this.footerHeight)
            .setItems(this.items.asArray().slice(0))
            .setButtonPanelPosition(this.buttonPanelPosition);

        if (this.visibleCloseButton) {
            this.showCloseButton();
        } else {
            this.hideCloseButton();
        }
        if (this.visibleFooter) {
            this.showFooter();
        } else {
            this.hideFooter();
        }

        if (this.eventsDefined) {
            this.defineEvents();
        }

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.Window', Window);

    if (typeof exports !== "undefined") {
        module.exports = Window;
    }
}());