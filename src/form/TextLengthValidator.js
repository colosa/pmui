(function () {
    /**
     * @class  PMUI.form.TextLengthValidator
     * @extend PMUI.form.Validator
     *
     * Create a new instace abstrac of the TextLengthValidator class
     * @param {Object} settings
     *
     * @cfg {String} [errorMessage='the validation has failed'] This property defines the error
     * message to display if the validation text size entered is not valid
     */
    var TextLengthValidator = function (settings) {

        TextLengthValidator.superclass.call(this, settings);
        TextLengthValidator.prototype.init.call(this, settings);

    };

    PMUI.inheritFrom('PMUI.form.Validator', TextLengthValidator);
    /**
     * Defines the object's type
     * @type {String}
     */
    TextLengthValidator.prototype.type = 'TextLengthValidator';

    TextLengthValidator.prototype.init = function (settings) {
        var maxMessage = settings.criteria.maxLength ? " at most " + settings.criteria.maxLength + " characters" : "",
            minMessage = settings.criteria.minLength ? "at least " + settings.criteria.minLength + " characters" : "",
            defaults = {
                errorMessage: "The text length must have " + minMessage + (maxMessage ? " and " : "") + maxMessage
            };

        jQuery.extend(true, defaults, settings);

        this.setErrorMessage(defaults.errorMessage);
    };
    /**
     * Evaluates the validator
     * this method is for validate the length of the string on control the value
     * input in text field, cutting spaces white in use trim of the jquery function
     * @chainable
     */
    TextLengthValidator.prototype.validate = function () {
        var res = false,
            value = this.criteria.trim ? jQuery.trim(this.parent.value) : this.parent.value;

        this.valid = true;

        if (this.criteria.maxLength) {
            this.valid = value.length <= parseInt(this.criteria.maxLength, 10);
        }
        if (this.criteria.minLength) {
            this.valid = (this.valid !== null ? this.valid : true) &&
                value.length >= parseInt(this.criteria.minLength, 10);
        }

        return this;
    };

    PMUI.extendNamespace('PMUI.form.TextLengthValidator', TextLengthValidator);

    if (typeof exports !== "undefined") {
        module.exports = TextLengthValidator;
    }

}());