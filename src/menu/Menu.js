(function () {
    var Menu = function (settings) {
        settings = jQuery.extend(true, settings, {
            elementTag: 'ul',
            positionMode: 'absolute'
        });
        Menu.superclass.call(this, settings);
        this.targetElement = null;
        this.parent = null;
        this.onShow = null;
        this.onHide = null;
        this.displayed = null;
        this.factory = new PMUI.menu.MenuItemFactory();
        this.items = new PMUI.util.ArrayList();
        this.onOutsideClickHandler = null;
        Menu.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', Menu);

    Menu.prototype.type = 'Menu';

    Menu.prototype.init = function (settings) {
        var defaults = {
            targetElement: null,
            parent: null,
            onShow: null,
            onHide: null,
            items: []
        };

        jQuery.extend(true, defaults, settings);

        this.onShow = defaults.onShow;
        this.onHide = defaults.onHide;
        this.setParent(defaults.parent)
            .setItems(defaults.items)
            .setTargetElement(defaults.targetElement)
            .setOutsideClickHandler();
    };

    Menu.prototype.setOutsideClickHandler = function () {
        var that = this;

        this.onOutsideClickHandler = function (e) {
            if (!jQuery(e.currentTarget).parents('#' + that.id).length) {
                that.hide();
            }
        };
        return this;
    };

    Menu.prototype.setTargetElement = function (target) {
        var rootMenu,
            targetElement = (this.targetElement && this.targetElement.html) || document.body;

        $(targetElement).removeClass('pmui-contextmenu-target');
        if (target instanceof PMUI.core.Element || null) {
            rootMenu = this.getRootMenu();
            if (rootMenu.targetElement) {
                if (rootMenu.targetElement.menu === rootMenu) {
                    rootMenu.targetElement.menu = null;
                }
            }
            rootMenu.targetElement = target;
        }
        return this;
    };

    Menu.prototype.getTargetElement = function () {
        var rootMenu = this.getRootMenu();
        return rootMenu.targetElement;
    };

    Menu.prototype.addItem = function (item) {
        var newItem = this.factory.make(item);

        if (newItem) {
            newItem.setParent(this);
            this.items.insert(newItem);
            if (this.html) {
                this.html.appendChild(newItem.getHTML());
            }
        }
        return this;
    };

    Menu.prototype.removeItem = function (item) {
        var itemToRemove;

        if (typeof item === 'string') {
            itemToRemove = this.items.find('id', item);
        } else if (typeof item === 'number') {
            itemToRemove = this.items.get(item);
        } else if (item instanceof PMUI.item.MenuItem && this.items.contains(item)) {
            itemToRemove = item;
        }
        if (itemToRemove) {
            this.items.remove(itemToRemove);
            jQuery(itemToRemove.html).detach();
        }
        return this;
    };

    Menu.prototype.clearItems = function () {
        var index = 0;
        while (this.items.getSize()) {
            this.removeItem(index);
        }
        return this;
    };

    Menu.prototype.setItems = function (items) {
        var i;

        if (!jQuery.isArray(items)) {
            throw new Error('setItems(): The parameter must be an array.');
        }
        this.clearItems();
        for (i = 0; i < items.length; i += 1) {
            this.addItem(items[i]);
        }
        return this;
    };

    Menu.prototype.getItems = function () {
        var index = 0;
        return this.items.asArray().slice(index);
    };

    Menu.prototype.setParent = function (parent) {
        if (!(parent === null || parent instanceof PMUI.menu.MenuItem)) {
            throw new Error('setParent(): The parameter must be an instance of PMUI.item.MenuItem or null.');
        }
        this.parent = parent;
        return this;
    };

    Menu.prototype.defineEventListeners = function () {
        this.removeEvents();
        this.addEvent('contextmenu').listen(this.html, function (e) {
            e.stopPropagation();
            e.preventDefault();
        });
        this.addEvent('click').listen(this.html, function (e) {
            e.stopPropagation();
        });
        this.addEvent('mousedown').listen(this.html, function (e) {
            e.stopPropagation();
        });

        return this;
    };

    Menu.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }

        Menu.superclass.prototype.createHTML.call(this);

        this.setItems(this.items.asArray().slice(0));

        this.defineEventListeners();

        return this.html;
    };

    Menu.prototype.isRoot = function () {
        return !this.parent;
    };

    Menu.prototype.getRootMenu = function () {
        if (this.isRoot()) {
            return this;
        } else {
            return this.parent.parent.getRootMenu();
        }
    };

    Menu.prototype.show = function (x, y) {
        var rootMenu = this.getRootMenu(),
            targetElement = (this.targetElement && this.targetElement.html) || document.body,
            zIndex = parseInt(targetElement.style.zIndex);
        x = x || 0;
        y = y || 0;
        rootMenu.setPosition({
            x: x,
            y: y
        });
        PMUI.removeCurrentMenu();
        rootMenu.setZOrder(zIndex + 1);
        $(targetElement).addClass('pmui-contextmenu-target');
        document.body.appendChild(rootMenu.getHTML());
        this.addEvent('mousedown', 'clickOutside').listen(document, this.onOutsideClickHandler);
        this.displayed = true;
        PMUI.currentContextMenu = this;
        if (typeof this.onShow === 'function') {
            this.onShow(this);
        }
        if (document.documentElement.clientHeight - y < jQuery(this.html).outerHeight()) {
            this.html.style.top = y - jQuery(this.html).outerHeight() + 'px'
        }
        return this;
    };

    Menu.prototype.hide = function () {
        var rootMenu = this.getRootMenu(),
            targetElement = (this.targetElement && this.targetElement.html) || document.body;

        this.removeEvent('clickOutside');
        $(targetElement).removeClass('pmui-contextmenu-target');
        jQuery(rootMenu.html).detach();
        rootMenu.displayed = false;
        PMUI.currentContextMenu = null;
        if (typeof rootMenu.onHide === 'function') {
            rootMenu.onHide(rootMenu);
        }
        return this;
    };

    Menu.prototype.setContextMenu = function () {
        return this;
    };

    PMUI.extendNamespace('PMUI.menu.Menu', Menu);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Menu;
    }
}());