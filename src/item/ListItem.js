(function () {
    /**
     * @class PMUI.item.ListItem
     */
    var ListItem = function (settings) {
        ListItem.superclass.call(this, settings);
        this.text = null;
        this.iconClass = null;
        this.toolbar = null;
        this.dom = {};
        this.actions = [];
        this.visibleIcon = null;
        this.data = null;
        this.textDataBind = null;
        this.onClick = null;
        ListItem.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Item', ListItem);

    ListItem.prototype.type = "ListItem";

    ListItem.prototype.init = function (settings) {
        var defaults = {
            elementTag: 'li',
            text: '[list-item]',
            actions: [],
            iconClass: null,
            visibleIcon: false,
            data: {},
            textDataBind: null,
            onClick: null
        };

        this.toolbar = new PMUI.toolbar.Toolbar({
            style: {
                cssClasses: ['pmui-listitem-actions']
            }
        });

        this.data = new PMUI.data.DataField();

        jQuery.extend(true, defaults, settings);

        this.setTextDataBind(defaults.textDataBind)
            .setElementTag(defaults.elementTag)
            .setText(defaults.text)
            .setIconClass(defaults.iconClass)
            .setActions(defaults.actions)
            .setData(defaults.data)
            .setOnClickHandler(defaults.onClick);

        if (defaults.visibleIcon) {
            this.showIcon();
        } else {
            this.hideIcon();
        }
    };

    ListItem.prototype.setOnClickHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnClickHandler(): The parameter must be or null.");
        }
        this.onClick = handler;
        return this;
    };

    ListItem.prototype.onClickHandler = function () {
        var that = this;

        return function () {
            if (typeof that.onClick === 'function') {
                that.onClick(that);
            }
        };
    };

    ListItem.prototype.setTextDataBind = function (textDataBind) {
        if (textDataBind !== null && typeof textDataBind !== 'string') {
            throw new Error("setTextDataBind(): the parameter must be a string or null.");
        }
        this.textDataBind = textDataBind;
        return this.bindData();
    };

    ListItem.prototype.bindData = function () {
        var data = this.data.getRecord();

        this.setText(data[this.textDataBind] || this.text || "");

        return this;
    };

    ListItem.prototype.setData = function (data) {
        var key;

        if (data instanceof PMUI.data.DataField) {
            this.data = data;
        } else if (typeof data === 'object') {
            this.data.clear();
            for (key in data) {
                this.data.addAttribute(key, data[key]);
            }
        } else {
            throw new Error("setData(): it only accepts a JSON object o an instance of PMUI.data.DataField as parameter.");
        }
        this.bindData();

        return this;
    };

    ListItem.prototype.clearActions = function () {
        this.toolbar.clearItems();
        return this;
    };

    ListItem.prototype.addAction = function (action) {
        var that = this,
            aux,
            handler;

        aux = action.handler || null;
        if (!(aux === null || typeof aux === 'function')) {
            throw new Error("addAction(): The parameter's \"'handler\" property must be a function or null or not be specified.");
        }

        handler = aux ? function () {
            if (aux) {
                aux.call(that, that, this);
            }
        } : null;

        action.action = handler;

        if (action instanceof PMUI.toolbar.ToolbarAction) {
            action.setAction(handler);
        } else if (typeof action === 'object') {
            action.action = handler;
        }

        this.toolbar.addItem(action);

        return this;
    };

    ListItem.prototype.setActions = function (actions) {
        var i;

        if (!jQuery.isArray(actions)) {
            throw new Error("setActions(): The parameter must be an array.");
        }
        this.clearActions();
        for (i = 0; i < actions.length; i += 1) {
            this.addAction(actions[i]);
        }
        return this;
    };

    ListItem.prototype.setIconClass = function (iconClass) {
        if (!(iconClass === null || typeof iconClass === 'string')) {
            throw new Error("setIconClass(): the parameter must be a string or null.");
        }
        this.iconClass = iconClass;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.className = 'pmui-listitem-icon ' + (iconClass || "");
        }
        return this;
    };

    ListItem.prototype.hideIcon = function () {
        this.visibleIcon = false;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.style.display = 'none';
        }
        return this;
    };

    ListItem.prototype.showIcon = function () {
        this.visibleIcon = true;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.style.display = '';
        }
        return this;
    }

    ListItem.prototype.setText = function (text) {
        if (typeof text !== 'string') {
            throw new Error("setText(): the parameter must be a string.");
        }
        this.text = text;
        if (this.dom.textContainer) {
            this.dom.textContainer.textContent = text;
        }
        return this;
    };

    ListItem.prototype.getData = function () {
        return this.data.getRecord();
    };

    ListItem.prototype.defineEvents = function () {
        return this;
    };

    ListItem.prototype.createHTML = function () {
        var textContainer,
            iconContainer,
            that = this;

        if (this.html) {
            return this.html;
        }
        ListItem.superclass.prototype.createHTML.call(this);
        textContainer = PMUI.createHTMLElement('div');
        textContainer.className = 'pmui-listitem-text';
        iconContainer = PMUI.createHTMLElement('i');
        iconContainer.className = 'pmui-listitem-icon';

        this.dom.textContainer = textContainer;
        this.dom.iconContainer = iconContainer;

        this.html.appendChild(iconContainer);
        this.html.appendChild(textContainer);
        this.html.appendChild(this.toolbar.getHTML());

        this.setText(this.text);

        if (this.visibleIcon) {
            this.showIcon();
        } else {
            this.hideIcon();
        }

        this.addEvent('click').listen(this.html, function (e) {
            var parent = that.getParent();
            e.stopPropagation();
            if (typeof that.onClick === 'function') {
                that.onClick(that);
            }
            if (parent) {
                parent.onItemClickHandler(that);
            }
        });

        return this.html;
    };

    PMUI.extendNamespace('PMUI.item.ListItem', ListItem);
}());