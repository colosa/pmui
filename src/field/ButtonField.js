(function () {
    /**
     * @class PMUI.field.ButtonField
     * @extends PMUI.form.Field
     * is a special field not return a value unlike other fields it is not
     * equal intanciacion a button PMUI.ui.Button, new properties tine see examples:
     *
     *      @example
     *      var b1,b2,b3;
     *      $(function() {
     *                 b1 = new PMUI.field.ButtonField({
     *                     value : "button1",
     *                     handler : function () {
     *                         alert("hi! I 'm Button 1")
     *                     },
     *                     name : 'button1'
     *                 });
     *      
     *                 document.body.appendChild(b1.getHTML());
     *                 b1.defineEvents();
     *      
     *      
     *                 b2 = new PMUI.field.ButtonField({
     *                     value : "button2",
     *                     handler : function () {
     *                         alert("hi! I 'm Button 2")
     *                     },
     *                     name : 'button2',
     *                     labelVisible : false,
     *                     style : {
     *                         cssClasses : ['customButton']
     *                     },
     *                     buttonAlign : 'center'
     *                 });
     *      
     *                 document.body.appendChild(b2.getHTML());
     *                 b2.defineEvents();
     *                 
     *                 b3 = new PMUI.field.ButtonField({
     *                     value : "button3",
     *                     handler : function () {
     *                         alert("hi! I 'm Button 3")
     *                     },
     *                     name : 'button3',
     *                     labelVisible : false,
     *                     buttonAlign : 'center'
     *                 });
     *      
     *                 document.body.appendChild(b3.getHTML());
     *                 b3.defineEvents();
     *           });
     *
     *
     * @cfg {Boolean}  [disableButtonControl = [false]] button will be disabled if
     * @cfg {String}  [value = "[button-field]"] be the value with the button as text, this value does not reflect data on a form
     * @cfg {String}  [tooltipMessage = [""]] message will have the button
     * @cfg {String}  [controlIconClass = [""]] icon wearing this button is a class
     * @cfg {Function}  [handler = function(){}] the handler of button in the field
     * @cfg {String}  [buttonAlign = [""]] the position will have the pin is reflected when the label of the field is hidden
     */
    var ButtonField = function (settings) {
        ButtonField.superclass.call(this, settings);
        this.buttonAlign = null;
        this.handler = null;
        ButtonField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', ButtonField);

    ButtonField.prototype.type = "ButtonField";

    ButtonField.prototype.init = function (settings) {
        var defaults = {
            disableButtonControl: false,
            value: '[button-field]',
            tooltipMessage: "",
            controlIconClass: '',
            handler: function () {
            },
            buttonAlign: '',
            handler: null
        }

        jQuery.extend(true, defaults, settings);

        this.setHandler(defaults.handler);
        this.setControls();
        this.setDisabled(defaults.disableButtonControl);
        this.setValue(defaults.value);
        this.setTooltipMessage(defaults.tooltipMessage);
        this.setButtonAlign(defaults.buttonAlign);
    };
    /**
     * The callback function to be executed when the the field's button is clicked.
     * @return {Function} The callback function.
     * @private
     */
    ButtonField.prototype.onButtonClickHandler = function () {
        var that = this;
        return function (button) {
            if (typeof that.handler === 'function') {
                that.handler(that, button);
            }
        };
    };
    /**
     * fix the control PMUI.control.ButtonControl
     * @chainable
     */
    ButtonField.prototype.setControls = function () {
        var that = this;
        if (this.handler === undefined || this.controls.length) {
            return this;
        }
        this.controls.push(new PMUI.control.ButtonControl({
            handler: that.onButtonClickHandler()
        }));
        return this;
    };
    /**
     * diabled the Buttonfield
     * @param {Boolean} value if true disable button, default value false
     * @chainable
     */
    ButtonField.prototype.setDisabled = function (value) {
        this.controls[0].setDisabled(value);
        return this;
    }
    /**
     * disable Button
     * @chainable
     */
    ButtonField.prototype.disable = function () {
        if (this.controls[0]) {
            this.controls[0].disable();
        }
        return this;
    };
    /**
     * ebable button
     * @chainable
     */
    ButtonField.prototype.enable = function () {
        if (this.controls[0]) {
            this.controls[0].enable();
        }
        return this;
    };

    ButtonField.prototype.click = function () {
        if (this.controls[0].click) {
            this.controls[0].click();
        }
        return this;
    };
    /**
     * this click is established with jQuery, simulate the behavior click
     * @chainable
     */
    ButtonField.prototype.setHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setHandler(): The parameter must be a function or null.");
        }
        this.handler = handler;
        return this;
    };
    /**
     * remove  one event of the events
     * @param  {String} type It is an event that is stored in an object event
     * @chainable
     */
    ButtonField.prototype.removeEvent = function (type) {
        this.controls[0].removeEvent(type);
        return this;
    };
    /**
     * removes all events
     * @return {[type]}
     */
    ButtonField.prototype.removeEvents = function () {
        this.controls[0].removeEvents();
        return this;
    };
    /**
     * fix the text for the button
     * @param {String} value
     */
    ButtonField.prototype.setValue = function (value) {
        this.value = value;
        if (this.controls[0]) {
            this.controls[0].setText(value);
        }
        return this;
    };

    ButtonField.prototype.setIcon = function (iconClass) {
        this.controls[0].setIcon(iconClass);
        return this;
    };
    ButtonField.prototype.setAliasButton = function (alias) {
        this.controls[0].setAliasButton(alias);
        return this;
    };
    ButtonField.prototype.setParent = function (parent) {
        this.controls[0].setParent(parent);
        return this;
    };
    ButtonField.prototype.defineEvents = function () {
        ButtonField.superclass.prototype.defineEvents.call(this);
        this.controls[0].defineEvents();
        return this;
    };
    /**
     * fix a message for  button
     * @param {String} messageTooltip
     */
    ButtonField.prototype.setTooltipMessage = function (messageTooltip) {
        this.controls[0].setTooltipMessage(messageTooltip);
        return this;
    };
    /**
     * the value return is forever null
     * @return {object}
     */
    ButtonField.prototype.getValue = function () {
        return null;
    }
    /**
     * hide label of the field, display none
     * @chainable
     */
    ButtonField.prototype.hideLabel = function () {
        jQuery(this.dom.labelTextContainer).hide();
        this.labelVisible = false;
        return this;
    }
    /**
     * hide label of the field, display inline-block
     * @chainable
     */
    ButtonField.prototype.showLabel = function () {
        jQuery(this.dom.labelTextContainer).show();
        this.labelVisible = true;
        return this;
    }

    ButtonField.prototype.setButtonAlign = function (position) {
        if (position == "left" || position == 'center' || position == "right" || position == '') {
            this.buttonAlign = position;
        }
        if (this.html) {
            this.style.addProperties({'textAlign': position});
        }
        return this;
    };

    ButtonField.prototype.createHTML = function () {
        ButtonField.superclass.prototype.createHTML.call(this);
        this.setButtonAlign(this.buttonAlign);
        return this.html;
    };

    PMUI.extendNamespace('PMUI.field.ButtonField', ButtonField);

    if (typeof exports !== "undefined") {
        module.exports = ButtonField;
    }
}());