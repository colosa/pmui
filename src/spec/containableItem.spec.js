//***********************other class******************************************************//
describe('PMUI.item.ContainableItem', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.item.ContainableItem();
	});
	afterEach(function () {
		a = null;
		b = null;
		c = null;
	});
	describe('class behavior', function () {
		it('Should create a ContainableItem object with and without parameters', function () {
			b = new PMUI.item.ContainableItem();
			c = new PMUI.item.ContainableItem({parent: new PMUI.core.Container() , data: new PMUI.data.DataField()});			
		});
		it('[US-44,j]should have data property equal to DataField', function () {
			expect(typeof a.data === "object").toBeTruthy();
			expect(a.data.type === "DataField").toBeTruthy();
		});
		it('[US-44,k]should have a pointer to Container', function () {
			c = new PMUI.core.Container();
			b = new PMUI.layout.Layout();
			expect(function(){a.setParent(b);}).toThrow();
			expect(function(){a.setParent(c);}).not.toThrow();
			expect(typeof a.parent === "object").toBeTruthy();
			expect(a.parent.type === "Container").toBeTruthy();
		});	
	});
	describe('method "setData"', function () {
		it('should set data, this must be an instance of DataField', function () {
			b = new PMUI.data.DataField();
			expect(function(){a.setData(b);}).not.toThrow();
			expect(a.data.type === "DataField").toBeTruthy();
		});
	});
	describe('method "getData"', function () {
		it('should get the data instance of DataField', function () {
			expect(a.getData().type === "DataField").toBeTruthy();
		});
	});
});