(function () {
    /**
     * @class PMUI.item.TabItem
     * Creates an TabItem class, this is a basic element
     * @extends {PMUI.core.Element}
     *
     *  Usage example:
     *
     *      @example
     *          var ti,p;
     *          p = new PMUI.form.FormPanel({
     *                    width: 604,
     *                    height: 130,
     *                    fieldset: true,
     *                    legend: "my fieldset panel",
     *                    items: [
     *                        {
     *                            pmType: "text",
     *                            label: "Name",
     *                            id: "123",
     *                            value: "",
     *                            placeholder: "insert your name",
     *                            name: "name"
     *                        },{
     *                            pmType: "text",
     *                            label: "Last name",
     *                            value: "",
     *                            placeholder: "your lastname here asshole!",
     *                            name: "lastname"
     *                        }, {
     *                            pmType: "panel",
     *                            layout: 'hbox',
     *                            items: [
     *                                {
     *                                    pmType: "text",
     *                                    label: "E-mail",
     *                                    value: "",
     *                                    name: "email"
     *                                },{
     *                                    pmType: "text",
     *                                    label: "Phone",
     *                                    value: "555",
     *                                    name: "phone"
     *                                }
     *                            ]
     *                        }
     *                    ],
     *                    layout: "vbox"
     *                });
     *               ti = new PMUI.item.TabItem({
     *                       title : 'titulo1',
     *                       panel : p,
     *                       icon : 'pmui-gridpanel-engranaje'
     *                   });
     *               jQuery('body').append(ti.getHTML());
     *               ti.defineEvents();
     *
     * create a new instance of the TabItem, according to the example we set the following properties
     * @cfg {String} [title='[untitled]'], Title The title for the Window
     * @cfg {PMUI.core.Panel|Object} panel, this property is to set TabItems that are sent from the JSON
     * @cfg {String} [icon = ""], this property serves to define one TabItem icon with css with putting a className for this icon
     * parameter to family {panel}
     * @cfg {Function} [onSelect= function() {}], this property is a function handler to from execute the
     * function when is callback define events
     */
    var TabItem = function (settings) {
        TabItem.superclass.call(this, settings);
        /**
         * Defines the Title of the TabItem
         * @type {String}
         */
        this.title = null;
        /**
         * Element defines the panel that concentrates tab
         * @type {PMUI.core.Panel}
         */
        this.panel = null;
        /**
         * Defines the property is a function handler to from execute the
         * function when is callback define events
         * @type {function}
         */
        this.onSelect = null;
        /**
         * The property define the TabItem is selected for triggering the
         * @type {boolean}
         */
        this.selected = null;
        /**
         * The property define the TabItem is selected for triggering
         * @type {boolean}
         */
        this.onClose = null;
        /**
         * property is defined when a TabItem is selected or event triggers a reference
         * @type {HTMLElement}
         */
        this.link = null;
        /**
         * Defines the fatory  for childrend [PMUI.core.Panel]
         * @type {PMUI.util.factory}
         */
        this.factory = null;
        /**
         * property defined Tag for icon
         * @type {HTMLElement}
         */
        this.classNameIcon = null;
        /**
         * property defined Tag for spanTitle
         * @type {HTMLElement}
         */
        this.spanTitle = null;
        /**
         * TabItem title can be hidden or viewed by the user if you only want an icon mostart
         * @type {Boolean}
         */
        this.titleVisible = null;
        /**
         * [onClick description]
         * @type {[type]}
         * @private
         */
        this.onClick = null;
        this.icon = null;
        TabItem.prototype.init.call(this, settings);
    };
    PMUI.inheritFrom('PMUI.core.Element', TabItem);

    TabItem.prototype.family = 'TabItem';
    TabItem.prototype.type = 'TabItem';

    TabItem.prototype.init = function (settings) {
        var defaults = {
            title: '[untitled]',
            panel: null,
            onSelect: function () {
            },
            elementTag: 'li',
            onClick: null
        };

        jQuery.extend(true, defaults, settings);

        this.factory = new PMUI.util.Factory({
            products: {
                'treepanel': PMUI.panel.TreePanel,
                'panel': PMUI.core.Panel,
                'grid': PMUI.grid.GridPanel
            },
            defaultProduct: 'panel'
        });
        this.setElementTag(defaults.elementTag)
            .setTitle(defaults.title)
            .setPanel(defaults.panel)
            .setOnSelectCallback(defaults.onSelect)
            .setOnClick(defaults.onClick);
    };

    TabItem.prototype.setOnClick = function (onClick) {
        if (typeof onClick !== 'function' && onClick !== null) {
            throw new Error('setOnClick(): the parameter is no valid, should be a function');
        }
        this.onClick = onClick;

        return this;
    }

    /**
     * Add an item to the panel.
     * @param {PMUI.item.TabItem} item
     * It can be a valid JSON object or an object that inherits from {@link PMUI.item.TabItem}.
     * @chainable
     */
    TabItem.prototype.setOnSelectCallback = function (callback) {
        if (typeof callback === 'function') {
            this.onSelect = callback;
        } else {
            throw new Error("setOnSelectCallback(): The parameter is not a function.");
        }
        return this;
    };
    /**
     * @event
     * Define the event listeners for the TabPanel.
     * @chainable
     */
    TabItem.prototype.defineEvents = function () {
        var that = this;

        this.removeEvents().eventsDefined = true;
        if (this.link) {
            this.addEvent('click').listen(this.link, function (e) {
                if (typeof that.onSelect === 'function') {
                    e.preventDefault();
                    e.stopPropagation();
                    if (!that.selected) {
                        that.select();
                    }
                }
                if (typeof that.onClick === 'function') {
                    that.onClick(that);
                }
            });
            that.panel.defineEvents();
        }
        return this;
    };
    /**
     * Sets the tabItem title
     * Set the proportion of the html element
     * @param {string} title
     * @chainable
     */
    TabItem.prototype.setTitle = function (title) {
        this.title = title;
        if (this.html) {
            this.spanTitle.textContent = this.title;
        }
        return this;
    };
    /**
     * Returns title the tabItem.
     * @return {string}
     */
    TabItem.prototype.getTitle = function () {
        return this.title;
    };
    /**
     * Sets the tabPanel [PMUI.core.panel] and full children except Window
     * @chainable
     */
    TabItem.prototype.setPanel = function (panel) {
        var newPanel, errorMessage = "setPanel(): the object supplied as a parameter is not a valid type.";
        if (!panel) {
            throw new Error("setPanel(): you must specified a valid panel.");
        }
        if (panel instanceof PMUI.ui.Window) {
            throw new Error(errorMessage);
        }
        if (this.factory) {
            if (this.factory.isValidClass(panel) || this.factory.isValidName(panel.pmType) || (panel instanceof PMUI.grid.GridPanel)) {
                newPanel = this.factory.make(panel);
            } else {
                throw new Error(errorMessage);
            }
        }
        if (newPanel) {
            this.panel = newPanel;
        }
        return this;
    };
    /**
     * Returns panel the tabItem.
     * @return {PMUI.core.Panel}
     */
    TabItem.prototype.getPanel = function () {
        return this.panel;
    };
    /**
     * if the tab item is selected, the selected property changes
     * @return {boolean}
     */
    TabItem.prototype.isSelected = function () {
        return this.selected;
    };
    /**
     * if the tab item is selected, the selected property changes to true and calls
     * your function onSelect
     * @chainable
     */
    TabItem.prototype.select = function () {
        this.selected = true;
        this.style.addClasses(['pmui-active']);
        if (this.html) {
            this.onSelect();
        }
        return this;
    };
    /**
     * if the tab item is selected, the selected property changes to false
     * @chainable
     */
    TabItem.prototype.deselect = function () {
        this.selected = false;
        this.style.removeClasses(['pmui-active']);
        return this;
    };
    /**
     * @method hideTitle
     * hide the title of TabItem
     * @chainable
     */
    TabItem.prototype.hideTitle = function () {
        this.titleVisible = false;
        if (this.html) {
            this.spanTitle.style.display = "none";
        }
        return this;
    };
    /**
     * @method hideTitle
     * shows the title of the TabItem
     * @chainable
     */
    TabItem.prototype.showTitle = function () {
        this.titleVisible = true;
        if (this.html) {
            this.spanTitle.style.display = "inline-block";
        }
        return this;
    };

    TabItem.prototype.createHTML = function () {
        var tab,
            ref,
            spanTitle,
            title,
            icon;

        if (this.html) {
            return this.html;
        }
        TabItem.superclass.prototype.createHTML.call(this);
        ref = PMUI.createHTMLElement('a');
        spanTitle = PMUI.createHTMLElement('span');
        icon = PMUI.createHTMLElement('i');
        icon.className = 'pmui-tab-icon';
        ref.className = 'pmui-tab-ref';
        ref.href = '#';
        spanTitle.className = 'pmui-tab_title';
        spanTitle.textContent = this.title;
        ref.appendChild(icon);
        ref.appendChild(spanTitle);
        this.icon = icon;
        this.html.appendChild(ref);
        this.spanTitle = spanTitle;
        this.link = ref;
        this.setWidth(this.width);

        if (this.eventsDefined) {
            this.defineEvents();
        }
        return this.html;
    };

    TabItem.prototype.setWidth = function (width) {
        TabItem.superclass.prototype.setWidth.call(this, width);
        if (this.html) {
            if (width == "auto") {
                this.link.setAttribute("title", "");
                this.spanTitle.style.width = "auto";
            } else {
                this.link.setAttribute("title", this.title);
                if (this.width >= 65) {
                    this.spanTitle.style.width = this.width - 46 + 'px';
                } else {
                    this.spanTitle.style.width = '0px';
                }

            }

        }
        return this;
    };

    PMUI.extendNamespace('PMUI.item.TabItem', TabItem);

    if (typeof exports !== "undefined") {
        module.exports = TabItem;
    }

}());