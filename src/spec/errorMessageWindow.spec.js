//***********************other class******************************************************//
describe('PMUI.ui.ErrorMessageWindow', function () {
	var a;
	beforeEach(function () {
		a = new PMUI.ui.ErrorMessageWindow(
											{
												title: "ERROR",
												message: "this is an error message!"
											}
				);
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});