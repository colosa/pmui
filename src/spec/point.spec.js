//***********************other class******************************************************//
describe('PMUI.util.Point', function () {
	var a, b, c, n;
	beforeEach(function () {
		a = new PMUI.util.Point(100,200);
	});
	afterEach(function () {
		a = null;
		b = null;
	});
	describe('method "getX"', function () {
		it('should return the x value', function () {
			b = a.x;
			expect(a.getX()).toEqual(b);
		});
	});
	describe('method "getY"', function () {
		it('should return the y value', function () {
			b = a.y;
			expect(a.getY()).toEqual(b);
		});
	});
	describe('method "add"', function () {
		it('should add a new point and add to the current x and y values', function () {
			b = a.x;
			c = a.y;
			expect(function () {n = a.add(new PMUI.util.Point(200,300))}).not.toThrow();
			expect(n.getX()).toEqual(200 + b);
			expect(n.getY()).toEqual(300 + c);		
		});
	});
	describe('method "subtract"', function () {
		it('should subtract the other point to the one that called the function', function () {
			b = a.x;
			c = a.y;
			expect(function () {n = a.subtract(new PMUI.util.Point(50,100))}).not.toThrow();
			expect(n.getX()).toEqual(b - 50);
			expect(n.getY()).toEqual(c - 100);		
		});
	});
	describe('method "multiply"', function () {
		it('should multiply the point with a scalar k', function () {
			b = a.x;
			c = a.y;
			expect(function () {n = a.multiply(3)}).not.toThrow();
			expect(n.getX()).toEqual(b * 3);
			expect(n.getY()).toEqual(c * 3);
		});
	});
	describe('method "equals"', function () {
		it('should return a boolean value showing if point a is equal to b', function () {
			expect(a.equals(new PMUI.util.Point(100, 200))).toBeTruthy();
			expect(a.equals(new PMUI.util.Point(-100, -200))).toBeFalsy();
			expect(a.equals(new PMUI.util.Point(300, 600))).toBeFalsy();
		});
	});
	describe('method "getDistance"', function () {
		it('should return the distance between two points', function () {
			n = new PMUI.util.Point(150, 250);
			expect(a.getDistance(n)).toEqual(Math.sqrt((100 - 150) * (100 - 150) + (200 - 250) * (200 - 250)));			
		});
	});
	describe('method "getSquaredDistance"', function () {
		it('should return the squared distance between two points', function () {
			n = new PMUI.util.Point(150, 250);
			expect(a.getSquaredDistance(n)).toEqual((100 - 150) * (100 - 150) + (200 - 250) * (200 - 250));
		});
	});
	describe('method "getManhattanDistance"', function () {
		it('should return the Manhattan distance between two points', function () {
			n = new PMUI.util.Point(150, 250);
			expect(a.getManhattanDistance(n)).toEqual(Math.abs(a.x - n.x) + Math.abs(a.y - n.y));
		});
	});
	describe('method "clone"', function () {
		it('should clone the current point', function () {
			n = a.clone();
			expect(a.equals(n)).toBeTruthy();
		});
	});
});


