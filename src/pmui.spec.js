global.dom = require('jsdom');
global.$ = null;

global.dom.env({
  html: '<body></body>',
  scripts: ['../libraries/jquery-ui/js/jquery-1.9.1.js', '../libraries/jquery-ui/js/jquery-ui-1.10.3.custom.js'],
  done: function(errors, _window) {
    if (errors) {
      console.log("errors:", errors);
    }
    global.window = _window;
    global.document = _window.document;
    global.$ = global.jQuery = window.$;

    global.PMUI = PMUI = require ('../src/pmui.js'),
    I18n = require('../src/lang/I18n.js'),
    Style = require('../src/util/Style.js'),
    ArrayList = require('../src/util/ArrayList.js'),  
                Base = require('../src/core/Base.js'),
                Element = require('../src/core/Element.js'), 
    Factory = require('../src/util/Factory.js'),
        BehaviorFactory = require('../src/behavior/BehaviorFactory.js'),
        EventFactory = require('../src/event/EventFactory.js'),
        FormItemFactory = require('../src/form/FormItemFactory.js'),
        LayoutFactory = require('../src/layout/LayoutFactory.js'),
            ValidatorFactory = require("../src/form/ValidatorFactory.js")
        ContainerItemBehaviorFactory = require ('../src/behavior/ContainerItemBehaviorFactory.js'),
    Layout = require("../src/layout/Layout.js"),
        Box = require("../src/layout/Box.js"),
        HBox = require("../src/layout/HBox.js"),
        VBox = require("../src/layout/VBox.js"),
    Color = require("../src/util/Color.js"),
    Point = require("../src/util/Point.js"),
    Proxy = require("../src/proxy/Proxy.js"),
    DataItem = require('../src/data/DataItem.js'),
        DataField = require('../src/data/DataField.js'),
    DataSet = require('../src/data/DataSet.js'),
    Base = require("../src/core/Base.js"),
                    Element = require('../src/core/Element.js'),
                    ContainableItem = require('../src/item/ContainableItem.js'),
                    Container = require('../src/core/Container.js'),
                    Item = require('../src/core/Item.js'),
                    Panel = require("../src/core/Panel.js"),
                    TooltipMessage = require('../src/ui/TooltipMessage.js'),   
                    Control = require('../src/control/Control.js'),
                    HTMLControl = require('../src/control/HTMLControl.js'),
                    TextControl = require('../src/control/TextControl.js'),
                    HiddenControl = require('../src/control/HiddenControl.js'),

                    Field = require('../src/form/Field.js'),  
                    TextField = require('../src/field/TextField.js'),
                    PasswordField = require('../src/field/PasswordField.js'),
                    ContainerItemBehavior = require("../src/behavior/ContainerItemBehavior.js"),
                    ContainerItemNoBehavior = require("../src/behavior/ContainerItemNoBehavior.js"),
                    ContainerItemDragBehavior = require("../src/behavior/ContainerItemDragBehavior.js"),
                        ContainerItemSortBehavior = require('../src/behavior/ContainerItemSortBehavior.js'),
                        ContainerItemDragSortBehavior = require('../src/behavior/ContainerItemDragSortBehavior.js'),
                    ContainerItemDropBehavior = require("../src/behavior/ContainerItemDropBehavior.js"),
                        ContainerItemDropSortBehavior = require('../src/behavior/ContainerItemDropSortBehavior'),
                    ContainerItemDragDropBehavior = require('../src/behavior/ContainerItemDragDropBehavior.js'),
                        ContainerItemDragDropSortBehavior = require('../src/behavior/ContainerItemDragDropSortBehavior.js'),

                    Form = require("../src/form/Form.js"),  
                    //SwitchButtonItem = require('../src/item/SwitchButtonItem.js'),
                    //SwitchButton = require('../src/ui/SwitchButton.js'),
        Validator = require('../src/form/Validator.js'),
            TextLengthValidator = require('../src/form/TextLengthValidator.js'),
            RegExpValidator = require('../src/form/RegExpValidator.js'),
        Element = require('../src/core/Element.js'),
            GridPanelColumn = require('../src/grid/GridPanelColumn.js'),
            GridPanelCell = require('../src/grid/GridPanelCell.js'),
            GridPanelRow = require('../src/grid/GridPanelRow.js'),
            GridPanel = require("../src/grid/GridPanel.js"),
            Button = require("../src/ui/Button.js"),
            TextLabel = require("../src/ui/TextLabel.js"),
            TooltipMessage = require('../src/ui/TooltipMessage.js'),
            Field = require('../src/form/Field.js'),
                UploadField = require('../src/field/UploadField.js'),
                TextAreaField = require('../src/field/TextAreaField.js'),
                RadioButtonGroupField = require('../src/field/RadioButtonGroupField.js'),
                CheckBoxGroupField = require('../src/field/CheckBoxGroupField.js'),
                DateTimeField = require('../src/field/DateTimeField.js'),
                OptionsSelectorField = require('../src/field/OptionsSelectorField.js'),
            Control = require('../src/control/Control.js'),
                HTMLControl = require('../src/control/HTMLControl.js'),
                    TextControl = require('../src/control/TextControl.js'),
                    PasswordControl = require('../src/control/PasswordControl.js'),
                        TextAreaControl = require('../src/control/TextAreaControl.js'),
                    UploadControl = require('../src/control/UploadControl.js'),
                    DropDownListControl = require('../src/control/DropDownListControl.js'),
                SelectableControl = require('../src/control/SelectableControl.js'),
                dateTimeControl = require('../src/control/DateTimeControl.js'),
                DropDownListField = require('../src/field/DropDownListField.js'), 
                OptionsSelectorControl = require('../src/control/OptionsSelectorControl.js'),
                    OptionsSelectorItemControl = require('../src/control/OptionsSelectorItemControl.js'),
            Container = require('../src/core/Container.js'),
                Item = require('../src/core/Item.js'),
                    TabItem = require('../src/item/TabItem.js'),
                Panel = require("../src/core/Panel.js"),
                    ButtonPanel = require("../src/panel/ButtonPanel.js"),
                    TreePanel = require("../src/panel/TreePanel.js"),
                        TreeNode = require("../src/item/TreeNode.js"),
                    Window = require('../src/ui/Window.js'),
                        MessageWindow = require("../src/ui/MessageWindow.js"),
                            ErrorMessageWindow = require("../src/ui/ErrorMessageWindow.js"),
                            InfoMessageWindow = require("../src/ui/InfoMessageWindow.js"),
                            SuccessMessageWindow = require("../src/ui/SuccessMessageWindow.js"),
                            WarningMessageWindow = require("../src/ui/WarningMessageWindow.js"),
                            ConfirmMessageWindow = require("../src/ui/ConfirmMessageWindow.js"),
                    Menu = require('../src/ui/Menu.js'),
                    Form = require('../src/form/Form.js'),
                    FormPanel = require("../src/form/FormPanel.js"),
                    LayouT = require("../src/panel/Layout.js"),
                    LayoutPanel = require("../src/panel/LayoutPanel.js"),
                    TabPanel = require("../src/panel/TabPanel.js"),
                    AccordionPanel = require('../src/panel/AccordionPanel.js'), 
                    AccordionItem = require('../src/item/AccordionItem.js'),       
    ResizeBehavior = require("../src/behavior/ResizeBehavior.js"),
        NoResizeBehavior = require("../src/behavior/NoResize.js"),
        RegularResizeBehavior = require("../src/behavior/RegularResize.js"),
    DragBehavior = require('../src/behavior/DragBehavior.js'),
    DropBehavior = require('../src/behavior/DropBehavior.js'),
        NoDropBehavior = require('../src/behavior/NoDrop.js'),
        NoDragBehavior = require('../src/behavior/NoDrag.js'),
    //GridPanel = require('../src/panel/GridPanel.js'),
    DataControlDragBehavior = require('../src/behavior/DataControlDragBehavior.js'),
    DataControlDropBehavior = require('../src/behavior/DataControlDropBehavior.js'),
    DataControlNoDragBehavior = require('../src/behavior/DataControlNoDragBehavior.js'),
    DataControlNoDropBehavior = require('../src/behavior/DataControlNoDropBehavior.js'),
    ConnectionDrag = require('../src/behavior/ConnectionDrag.js'),
    ConnectionDrop = require('../src/behavior/ConnectionDrop.js'),
    ContainerBehavior = require('../src/behavior/ContainerBehavior.js'),
        NoContainerBehavior = require("../src/behavior/NoContainer.js"),
        RegularContainerBehavior = require("../src/behavior/RegularContainer.js"),
    CustomShapeDrag = require('../src/behavior/CustomShapeDrag.js'),
    RegularDragBehavior = require('../src/behavior/RegularDrag.js'),
    
    Element = require('../src/core/Element.js'),
        MenuItemElement = require('../src/core/MenuItemElement.js'),
            MenuItem =  require('../src/item/MenuItem.js'),
            SeparatorItem = require('../src/item/SeparatorItem.js'),
            CheckboxItem = require('../src/item/CheckboxItem.js'),
        Core = require("../src/draw/Core.js"),
            //Geometry = require("../src/behavior/Geometry.js"),
            //Graphics = require("../src/behavior/Graphics.js"),
            BehavioralElement = require("../src/draw/BehavioralElement.js"),
            DragBehavior = require('../src/behavior/DragBehavior.js'),
            DropBehavior = require('../src/behavior/DropBehavior.js'),
        ConnectionContainerDrop = require("../src/behavior/ConnectionContainerDrop.js"),
        ContainerDrop = require("../src/behavior/ContainerDrop.js"),
        NoDropBehavior = require('../src/behavior/NoDrop.js'),
        NoDragBehavior = require('../src/behavior/NoDrag.js'),
                Canvas = require("../src/draw/Canvas.js"),
                Shape = require("../src/draw/Shape.js"),
                    CustomShape = require("../src/draw/CustomShape.js"),
                    Label = require("../src/draw/Label.js"),
                    RegularShape = require("../src/draw/RegularShape.js"),
                        Arc = require("../src/draw/Arc.js"),
                            Intersection = require("../src/draw/Intersection.js"),
                        Oval = require("../src/draw/Oval.js"),
                        Polygon = require("../src/draw/Polygon.js"),
                            Rectangle = require("../src/draw/Rectangle.js"),
                                MultipleSelectionContainer = require("../src/draw/MultipleSelectionContainer.js"),
            ConnectionDecorator = require("../src/draw/ConnectionDecorator.js"),
            Handler = require("../src/draw/Handler.js"),
                ResizeHandler = require("../src/draw/ResizeHandler.js"),
                SegmentMoveHandler = require("../src/draw/SegmentMoveHandler.js"),
            Layer = require("../src/draw/Layer.js"),
            Port = require("../src/draw/Port.js"),
            ReadOnlyLayer = require("../src/draw/ReadOnlyLayer.js"),
            Router = require("../src/draw/Router.js"),
                ManhattanConnectionRouter = require("../src/draw/ManhattanConnectionRouter.js"),
                Connection = require("../src/draw/Connection.js"),
            Segment = require("../src/draw/Segment.js"),
            Snapper = require("../src/draw/Snapper.js"),
        Command = require("../src/command/Command.js"),
            CommandConnect = require("../src/command/CommandConnect.js"),
            CommandCreate = require("../src/command/CommandCreate.js"),
            CommandDelete = require("../src/command/CommandDelete.js"),
            CommandEditLabel = require("../src/command/CommandEditLabel.js"),
            CommandMove = require("../src/command/CommandMove.js"),
            CommandPaste = require("../src/command/CommandPaste.js"),
            CommandReconnect = require("../src/command/CommandReconnect.js"),
            CommandResize = require("../src/command/CommandResize.js"),
            CommandSegmentMove = require("../src/command/CommandSegmentMove.js"),
            CommandSwitchContainer = require("../src/command/CommandSwitchContainer.js"),
            CommandStack = require("../src/command/CommandStack.js"),
            Geometry =require("../src/draw/Geometry.js"),
            Proxy = require("../src/proxy/Proxy.js"),
                RestProxy = require("../src/proxy/RestProxy.js"),
            Action = require('../src/event/Action.js'),
            Event = require('../src/event/Event.js'),
                KeyboardEvent = require('../src/event/KeyboardEvent.js'),
                FormEvent = require('../src/event/FormEvent.js'),
                MouseEvent = require('../src/event/MouseEvent.js');

    return global.$;
  }
});

var 
    PMUI,
    I18n,
    Style,
    ArrayList,
    /*__before factory require*/     
                Base,
                Element,
    /*__before factory require*/     
    Factory,
        BehaviorFactory,
        EventFactory,
        FormItemFactory,
        LayoutFactory,
            ValidatorFactory,
        ContainerItemBehaviorFactory,
    Layout,
        Box,
        HBox,
        VBox,
    Color,
    Point,
    Proxy,
    DataItem,
        DataField,
    DataSet,
    Base,
    /*__before validator require*/
                    Element,
                    ContainableItem,
                    Container,
                    Item,
                    Panel,
                    TooltipMessage,
                    Control,
                    HTMLControl,
                    TextControl,
                    Field,
                    TextField,
                    PasswordField,
                    ContainerItemBehavior,
                    ContainerItemNoBehavior,
                    ContainerItemDragBehavior,
                        ContainerItemDragSortBehavior,
                        ContainerItemSortBehavior,
                    ContainerItemDropBehavior,
                        ContainerItemDropSortBehavior,
                    ContainerItemDragDropBehavior,
                        ContainerItemDragDropSortBehavior,

                    Form,
    /*__before validator require*/               
        Validator,
            TextLengthValidator,
            RegExpValidator,
        Element,
            GridPanelColumn,
            GridPanelCell,
            GridPanelRow,
            GridPanel,
            Button,
            TextLabel,
            TooltipMessage,
            Field,
                //TextField,
                TextAreaField,
                RadioButtonGroupField,
                OptionsSelectorField,
            Control,
                HTMLControl,
                    TextControl,
                    PasswordControl,
                        TextAreaControl,
                    UploadControl,
                    DropDownListControl,
                    SelectableControl,
                    HiddenControl,
                DateTimeControl,
                DropDownListField,
                OptionsSelectorControl,
                    OptionsSelectorItemControl,
            Container,
                Item,
                    TreeItem,
                    TabItem,
                Panel,
                    ButtonPanel,
                    TreePanel,
                        TreeNode,
                    Window,
                        MessageWindow,
                            ErrorMessageWindow,
                            InfoMessageWindow,
                            SuccessMessageWindow,
                            WarningMessageWindow,
                            ConfirmMessageWindow,
                    Menu,
                    Form,
                    FormPanel,
                    LayouT,
                    LayoutPanel,
                    TabPanel,
                   AccordionPanel,
                    AccordionItem,
    ConnectionDrag,
    ConnectionDrop,
    ContainerBehavior,
        NoContainerBehavior,
        RegularContainerBehavior,
    CustomShapeDrag,
    RegularDragBehavior,
    Element,
        Core,
            Geometry,
            Graphics,
            BehavioralElement,
            
    ResizeBehavior,
        NoResizeBehavior,
        RegularResizeBehavior,
    DragBehavior,
    DropBehavior,
        ConnectionContainerDrop,
        ContainerDrop,
        NoDropBehavior,
        NoDragBehavior,
    GridPanel,
    DataControlDragBehavior,
    DataControlDropBehavior,
    DataControlNoDragBehavior,
    DataControlNoDropBehavior,
                Canvas,
                Shape,
                    CustomShape,
                    Label,
                    RegularShape,
                        Arc,
                            Intersection,
                        Oval,
                        Polygon,
                            Rectangle,
                                MultipleSelectionContainer,
            //Connection,
            ConnectionDecorator,
            Handler,
                ResizeHandler,
                SegmentMoveHandler,
            Layer,
            Port,
            ReadOnlyLayer,
            Router,
                ManhattanConnectionRouter,
                Connection,
            Segment,
            Snapper,
        Command,
            CommandConnect,
            CommandCreate,
            CommandDelete,
            CommandEditLabel,
            CommandMove,
            CommandPaste,
            CommandReconnect,
            CommandResize,
            CommandSegmentMove,
            CommandSwitchContainer,
            CommandStack,
            Geometry,
            Proxy,
                RestProxy,
            Action,
            Event,
                KeyboardEvent,
                FormEvent,
                MouseEvent;  
if (!$) {
  beforeEach(function() {
    return waitsFor(function() {
      return $;
    });
  });
}

//**CODE**//