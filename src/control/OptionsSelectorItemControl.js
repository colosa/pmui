(function () {
    /**
     * @class PMUI.control.OptionsSelectorItemControl
     * @extends PMUI.control.HTMLControl
     *
     * The class is the basic element for {@link PMUI.control.OptionsSelectorControl OptionsSelectorControl} class.
     *
     *  For instance the class, the sentences below show how do it.
     *
     *      @example
     *      new PMUI.control.OptionsSelectorItemControl({
     *          text: ....
     *      });
     *
     * @cfg {Boolean} selected Sets an item as selected inside of the {@link PMUI.control.OptionsSelectorControl component}
     * @cfg {String} text Defines the label for the component, the text that appear inside of the component
     *
     */
    var OptionsSelectorItemControl = function (settings) {
        OptionsSelectorItemControl.superclass.call(this, settings);
        /**
         * @property {Boolean} [selected=false]
         * Sets an item as selected inside of the {@link PMUI.control.OptionsSelectorControl component}
         */
        this.selected = null;
        /**
         * @property {String} [cls='pmui-switch-buttonitem']
         * Sets the class name for the component. It is related to the styles for the element
         */
        this.cls = null;
        /**
         * @property {String} [iconCls]
         * Sets the class name for the icon of the component.
         */
        this.iconCls = null;
        /**
         * @property {String} [clsSelected= 'pmui-switch-buttonitem-selected']
         * Sets the class name when an item is selected
         */
        this.clsSelected = null;
        /**
         * @property {Object} [dom=null]
         * Represents the DOM of the component
         */
        this.dom = null;
        /**
         * @property {Object} [disabled=false]
         * Defines whether the item is disabled or enabled
         */
        this.disabled = null;
        /**
         * @property {String} [text=null]
         * Defines the label for the component, the text that appear inside of the component
         */
        this.text = null;

        OptionsSelectorItemControl.prototype.init.call(this, settings);
    };
    PMUI.inheritFrom("PMUI.control.HTMLControl", OptionsSelectorItemControl);
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.type = "button";
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.family = "Item";

    OptionsSelectorItemControl.prototype.init = function (settings) {
        var defaults = {
            selected: false,
            disabled: false,
            cls: 'pmui-switch-buttonitem',
            iconCls: 'pmui-switch-buttonitem-icon',
            clsSelected: 'pmui-switch-buttonitem-selected',
            items: [],
            text: '',
            value: ''
        };
        jQuery.extend(true, defaults, settings);
        this.setSelected(defaults.selected)
            .setCls(defaults.cls)
            .setClsSelected(defaults.clsSelected)
            .setIconClass(defaults.iconCls)
            .setText(defaults.text)
            .setDisable(defaults.disabled)
            .setValue(defaults.value);
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.setSelected = function (status) {
        var otherElements,
            i,
            that = this;
        this.selected = (typeof status === 'boolean') ? status : false;
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.setCls = function (className) {
        this.cls = (typeof className === 'string') ? className : '';
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.setClsSelected = function (className) {
        this.clsSelected = (typeof className === 'string') ? className : '';
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.setIconClass = function (className) {
        this.iconCls = (typeof className === 'string') ? className : '';
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.setText = function (text) {
        this.text = (typeof text === 'string') ? text : '';
        return this;
    };
    OptionsSelectorItemControl.prototype.setValue = function (value) {
        value = value.toString();
        if (typeof value === 'string') {
            if (value.length === 0) {
                this.value = this.text;
            } else {
                this.value = value;
            }
        }
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.activeElement = function (that) {
        var i;

        if (typeof that === "undefined") {
            that = this;
        }
        if (!that.disabled) {
            if (!that.parent.itemsSelected.contains(that)) {
                that.parent.itemsSelected.insert(that);
            }
            that.setSelected(true);
            if (this.dom) {
                that.dom.container.className = that.clsSelected;
                for (i = 0; i < that.style.getClasses().length; i += 1) {
                    that.dom.container.className += " " + that.style.getClasses()[i];
                }
            }
        }

        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.desactiveElement = function (that) {
        var i;
        if (typeof that === "undefined") {
            that = this;
        }
        if (!that.disabled) {
            that.parent.itemsSelected.remove(that);
            that.setSelected(false);
            if (this.dom) {
                that.dom.container.className = that.cls;
                for (i = 0; i < that.style.getClasses().length; i += 1) {
                    that.dom.container.className += " " + that.style.getClasses()[i];
                }
            }
        }

        return this;
    };

    /**
     * Disables/enables the control
     * @param {Boolean} setDisable If the value is evaluated as true then the control is disabled,
     * otherwise the control is enabled.
     * @chainable
     */
    OptionsSelectorItemControl.prototype.setDisable = function (disable) {
        if (typeof disable === 'boolean') {
            this.disabled = disable;
            if (this.html) {
                if (disable) {
                    this.disable();
                } else {
                    this.enable();
                }
            }
        }

        return this;
    };
    /**
     * [disable description]
     * @return {[type]} [description]
     */
    OptionsSelectorItemControl.prototype.disable = function () {
        if (arguments[0] === undefined) {
            this.disabled = true;
            if (this.html) {
                jQuery(this.html).unbind("click");
                this.html.setAttribute("class", "pmui-switch-buttonitem-disabled");
            }
        } else {
            if (arguments[0]) {
                this.disable();
            } else {
                this.enable();
            }
        }
        return this;
    };
    /**
     * [enable description]
     * @return {[type]} [description]
     */
    OptionsSelectorItemControl.prototype.enable = function () {
        if (arguments[0] === undefined) {
            this.disabled = false;
            if (this.html) {
                this.html.setAttribute("class", this.cls);
                this.defineEvents();
            }
        } else {
            if (arguments[0]) {
                this.disable();
            } else {
                this.enable();
            }
        }
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.selectItem = function () {
        var that = this,
            otherElements;

        if (this.selected) {
            that.desactiveElement(that);
        } else {
            that.activeElement(that);
            if (this.parent.multipleSelection === false) {
                otherElements = this.parent.items.asArray();
                for (i = 0; i < otherElements.length; i += 1) {
                    if (that.id !== otherElements[i].id) {
                        that.desactiveElement(otherElements[i]);
                    }
                }
            }
        }
        return this;
    };
    /**
     *
     *
     */
    OptionsSelectorItemControl.prototype.createHTML = function () {
        var li,
            display,
            button,
            icon,
            text,
            i;

        OptionsSelectorItemControl.superclass.prototype.setElementTag.call(this, "a");
        OptionsSelectorItemControl.superclass.prototype.createHTML.call(this);

        //button = PMUI.createHTMLElement("a");
        //button.id = PMUI.generateUniqueId()
        if (this.disabled) {
            this.html.className = "pmui-switch-buttonitem-disabled";
        } else {
            this.html.className = this.cls;
            for (i = 0; i < this.style.getClasses().length; i += 1) {
                this.html.className += " " + this.style.getClasses()[i];
            }
        }

        this.html.href = '#';
        if (this.parent.orientation === 'vertical') {
            this.html.setAttribute("style", "display: block;");
        }
        icon = PMUI.createHTMLElement("span");
        icon.className = this.iconCls;
        text = PMUI.createHTMLElement("span");
        text.innerHTML = this.text;
        this.html.appendChild(icon);
        this.html.appendChild(text);

        this.dom = {
            container: this.html,
            items: [
                {
                    icon: icon
                },
                {
                    text: text
                }
            ]
        };
        if (this.selected) {
            this.activeElement(this);
        }
        this.html.id = this.id;
        return this.html;
    };
    /**
     * Defines events related to the object.
     */
    OptionsSelectorItemControl.prototype.defineEvents = function () {
        var j,
            fnSelect,
            children,
            that = this;
        this.removeEvents().eventsDefined = true;
        if (!this.disabled) {
            that.addEvent('click').listen(this.html, function (e) {
                e.preventDefault();
                that.selectItem();
                fnSelect = that.parent.listeners.select;
                fnSelect(that, e);
                e.stopPropagation();
            });
            if (e.which === PMUI.keyCodeF5) {
                this.blur();
                e.preventDefault();
                window.location.reload(true);
            }
        }
    };
    PMUI.extendNamespace("PMUI.control.OptionsSelectorItemControl", OptionsSelectorItemControl);
    if (typeof exports !== "undefined") {
        module.exports = OptionsSelectorItemControl;
    }
}());
