//***********************other class******************************************************//
describe('PMUI.field.PasswordField', function () {
	var a, b;
	beforeEach(function () {
		a = new PMUI.field.PasswordField(
											{
												label: 'pass', 
												trimOnBlur: false
											}
			);
		document.body.appendChild(a.getHTML());
		a.defineEvents();
	});
	afterEach(function () {
		$(a.html).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[]', function () {

		});
	});
	describe('method "setValue"', function () {
		it('should set the value for the field', function () {
			a.setValue("new_value");
			expect(a.value === "new_value").toBeTruthy();
			a.setValue("other_value");
			expect(a.value === "other_value").toBeTruthy();
		});
	});
	describe('method "setMaxLength"', function () {
		it('should set the max length for the control', function () {
			expect(function () {a.setMaxLength({})}).toThrow();
			expect(function () {a.setMaxLength("34")}).toThrow();
			expect(function () {a.setMaxLength(34)}).not.toThrow();
			expect(a.controls[0].maxLength === 34).toBeTruthy();
		});
	});
	describe('method "setTrimOnBlur"', function () {
		it('should set the value trimming for the field', function () {
			a.setTrimOnBlur(false);
			a.setValue("  value  ");
			expect(a.getValue() === "  value  ").toBeTruthy();
			a.setTrimOnBlur(true);
			a.setValue("  value1  ");
			expect(a.getValue() === "  value1  ").toBeFalsy();
			expect(a.getValue() === "value1").toBeTruthy();
		});
	});
	describe('method "getTrimOnBlur"', function () {
		it('should return a boolean value to show the status of trimOnBlur', function () {
			a.setTrimOnBlur(false);
			expect(a.getTrimOnBlur()).toBeFalsy();
			a.setTrimOnBlur(true);
			expect(a.getTrimOnBlur()).toBeTruthy();
		});
	});
});