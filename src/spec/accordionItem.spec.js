//***********************other class******************************************************//
describe('PMUI.panel.AccordionItem', function () {
    var a, b, c;
    beforeEach(function () {
        var formPanel1 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });
                var formPanel2 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });

                var formPanel3 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });

                   var accordion   = new PMUI.panel.AccordionPanel({
                        width: 400,
                        height: 500,
                        enableTitle: true,
                        //multipleSelection : true,
                        title: "My accordion",
                        items: [
                            {
                                iconClass: "",
                                title: "First item",
                                body: formPanel1,
                                selected: true
                            },
                            {
                                iconClass: "pmui-icon pmui-icon-warning",
                                title: "Second item",
                                body: "<a href=\"http://www.google.com\">Google Link</a>",
                                style: {
                                    cssProperties: {
                                        "background-color": "#f2eaea"
                                    }
                                }
                            },
                            {
                                title: "Third item <span class=\"classname-accordion\"></span>",
                                body: formPanel2
                            },
                            {
                                title: "Fourth item <span class=\"classname-accordion\"></span>",
                                body: formPanel3
                            }, new PMUI.item.AccordionItem({
                                title: "Five"
                             })
                        ]
                    });
        document.body.appendChild(accordion.getHTML());
        accordion.defineEvents();
        a = accordion.items.getFirst();
    });
    afterEach(function () {
            });
    describe('class behavior', function () {
        it('[US-46,b]should set collapsed, it has two values true or false, it´s only a status value', function () {
            a.setCollapsed(true);
            expect(a.collapsed).toBeTruthy();
            a.setCollapsed(false);
            expect(a.collapsed).toBeFalsy();
        });
        it('[US-46,c]should collapse or expand the item', function () {
            a.collapse();
            //expect($(a.getHTML()).find('div[style="height: 150px; display: none;"]').length).not.toEqual(0);
            expect($(a.getHTML()).find('div[style="height: 150px; display: block;"]').length).toEqual(0);
            expect(a.collapsed).toBeTruthy();
            a.expand();
            expect(a.collapsed).toBeFalsy();
            //expect($(a.getHTML()).find('div[style="height: 150px; display: block;"]').length).not.toEqual(0);
            expect($(a.getHTML()).find('div[style="height: 150px; display: none;"]').length).toEqual(0);
        });
        it('[US-46,d]should expand the item', function () {
            a.expand();
            expect(a.collapsed).toBeFalsy();
            $(a.getHTML()).find('div').first().trigger('click');
            expect(a.collapsed).toBeTruthy();
            $(a.getHTML()).find('div').first().trigger('click');
            expect(a.collapsed).toBeFalsy();
        });
    });
    describe('method "setTitle"', function () {
        it('should set the title for the AccordionItem', function () {
            var t = a.title;
            expect($(a.getHTML()).find('div').first().text() === t).toBeTruthy();
            a.setTitle("new_title");
            expect($(a.getHTML()).find('div').first().text() === "new_title").toBeTruthy();
        });
    });
    describe('method "setBody"', function () {
        it('should set the body for an AccordionItem', function () {
            var tree = new PMUI.panel.TreePanel();
            a.setBody(tree);
            expect(a.body.items[0].panel.html.className === "pmui-treepanel").toBeTruthy();
        });
    });
    describe('method "setIconClass"', function () {
        it('should set the accordion item icon class', function () {
            a.setIconClass("pmui-accordion-item-icon2");
            expect(a.iconClass === "pmui-accordion-item-icon2").toBeTruthy();
            a.setIconClass(23);
            expect(a.iconClass === "").toBeTruthy();
        });
    });
    describe('method "setHeaderClass"', function () {
        it('should set the accordion item header class', function () {
            expect(a.headerClass === "pmui-accordion-item-header").toBeTruthy();
            a.setHeaderClass("pmui-accordion-item-header2");
            expect(a.headerClass === "pmui-accordion-item-header2").toBeTruthy();
            a.setHeaderClass(3);
            expect(a.headerClass === "").toBeTruthy();
        });
    });
    describe('method "setBodyClass"', function () {
        it('should set the accordion item body class', function () {
            expect(a.bodyClass === "pmui-accordion-item-body").toBeTruthy();
            a.setBodyClass("pmui-accordion-item-body2");
            expect(a.bodyClass === "pmui-accordion-item-body2").toBeTruthy();
            a.setBodyClass(2);
            expect(a.bodyClass === "").toBeTruthy();
        });
    });
    describe('method "setFooterClass"', function () {
        it('should set the accordion item footer class', function () {
            expect(a.footerClass === "pmui-accordion-item-footer").toBeTruthy();
            a.setFooterClass("pmui-accordion-item-footer2");
            expect(a.footerClass === "pmui-accordion-item-footer2").toBeTruthy();
            a.setFooterClass(233);
            expect(a.footerClass === "").toBeTruthy();
        });
    });
    describe('method "setContainerClass"', function () {
        it('should set the accordion item container class', function () {
            expect(a.containerClass === "pmui-accordion-item-container").toBeTruthy();
            a.setContainerClass("pmui-accordion-item-container2");
            expect(a.containerClass === "pmui-accordion-item-container2").toBeTruthy();
            a.setContainerClass(234);
            expect(a.containerClass === "").toBeTruthy();
        });
    });
    describe('method "setIconExpanded"', function () {
        it('should set the accordion item container class', function () {
            expect(a.iconExpanded === "pmui-accordion-item-expanded").toBeTruthy();
            a.setIconExpanded("pmui-accordion-item-expanded2");
            expect(a.iconExpanded === "pmui-accordion-item-expanded2").toBeTruthy();
            a.setIconExpanded(234);
            expect(a.iconExpanded === "").toBeTruthy();
        });
    });
    describe('method "setIconClosed"', function () {
        it('should set the accordion item container class', function () {
            expect(a.iconClosed === "pmui-accordion-item-closed").toBeTruthy();
            a.setIconClosed("pmui-accordion-item-closed2");
            expect(a.iconClosed === "pmui-accordion-item-closed2").toBeTruthy();
            a.setIconClosed(234);
            expect(a.iconClosed === "").toBeTruthy();
        });
    });
    describe('method "toggleCollapse"', function () {
        it('should toggle the item, if it is expanded sets to collapsed', function () {
            a.toggleCollapse();
            //expect($(a.getHTML()).find('div[style="height: 150px; display: none;"]').length).not.toEqual(0);
            expect($(a.getHTML()).find('div[style="height: 150px; display: block;"]').length).toEqual(0);
            expect(a.collapsed).toBeTruthy();
            a.toggleCollapse();
            expect(a.collapsed).toBeFalsy();
            //expect($(a.getHTML()).find('div[style="height: 150px; display: block;"]').length).not.toEqual(0);
            expect($(a.getHTML()).find('div[style="height: 150px; display: none;"]').length).toEqual(0);
        });
    });
    describe('method "expand"', function () {
        it('should expand the item', function () {
            a.expand();
            expect(a.collapsed).toBeFalsy();
            //expect($(a.getHTML()).find('div[style="height: 150px; display: block;"]').length).not.toEqual(0);
            expect($(a.getHTML()).find('div[style="height: 150px; display: none;"]').length).toEqual(0);
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
});