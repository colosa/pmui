(function () {
    /**
     * @class PMUI.item.AccordionItem
     * @extends PMUI.core.Item
     *
     * Class to handle Accordion items, this is the basic element for make an {@link PMUI.panel.AccordionPanel Accordion}
     *
     * @constructor
     * For creates a new component the JSON config can have the following sentences
     * @param {Object} settings The configuration options contain:
     *
     *          {
     *          title: "The title",
     *          body: panel // This item can be any element that inherit from {@link PMUI.core.Panel Panel}
     *          iconClass: "the-class",
     *          style: {
     *              cssProperties: {
     *                  "background-color": "#f2eaea"
     *              }
     *          }
     *      } ...
     * @cfg {String} [title=""] Shows the title for the container
     * @cfg {Object} [body=null] Defines the element that will be inside the element
     * @cfg {String} [iconClass='pmui-accordion-item-icon'] Defines the class name for the header of the item
     *
     */
    AccordionItem = function (settings) {
        AccordionItem.superclass.call(this, settings);
        /**
         * @property {String} [iconClass='pmui-accordion-item-icon']
         * Defines the class name for the header of the item
         */
        this.iconClass = null;
        /**
         * @property {String} [headerClass='pmui-accordion-item-header']
         * Defines the class name for the header
         * @private
         */
        this.headerClass = null,
        /**
         * @property {String} [bodyClass='pmui-accordion-item-body']
         * Defines the class name for the body
         * @private
         */
            this.bodyClass = null,
        /**
         * @property {String} [footerClass='pmui-accordion-item-footer']
         * Defines the class name for the footer
         * @private
         */
            this.footerClass = null;
        /**
         * @property {String} [containerClass='pmui-accordion-item-container']
         * Defines the class name for the global container
         * @private
         */
        this.containerClass = null;
        /**
         * @property {String} [iconExpanded='pmui-accordion-item-closed']
         * Defines the class name that represents the icon expanded for the AccordionItem
         * @private
         */
        this.iconExpanded = null;
        /**
         * @property {String} [iconClosed='pmui-accordion-item-expanded']
         * Defines the class name that represents the icon closed for the AccordionItem
         * @private
         */
        this.iconClosed = null;

        /**
         * @property {Boolean} [collapsed=true]
         * Defines whether the object is collapsed or not
         * @private
         */
        this.collapsed = null;
        /**
         * @property {Object} [container=null]
         * container Defines the container object for the class xxxxxxx
         * @private
         */
        this.container = null;
        /**
         * @property {Object} [header=null]
         * Defines the header object for the class
         * @private
         */
        this.header = null;
        /**
         * @property {Object} [body={}]
         * Defines the body object for the item
         * @private
         */
        this.body = {};
        /**
         * @property {Object} [footer=null]
         * Defines the footer object for the class
         * @private
         */
        this.footer = null;
        /**
         * @property {Boolean} [selected=false]
         * Defines whether the item is selected by default when the component is ready to render
         * on the browser
         */
        this.selected = null;
        /**
         * @property {Boolean} [propName]
         * Represents a flag for control the rendered of the item.
         * If the property is True the item was rendered the otherwise if it
         * False the item still was not rendered
         * @private
         */
        this.hiddenBody = null;
        /**
         * @property {Object} [iconNode=null]
         * Defines the iconNode object for the class
         * @private
         */
        this.iconNode = null;
        AccordionItem.prototype.init.call(this, settings);

    };

    PMUI.inheritFrom('PMUI.core.Item', AccordionItem);
    AccordionItem.prototype.type = 'Accordion';
    AccordionItem.prototype.family = 'Item';
    AccordionItem.prototype.init = function (settings) {
        var defaults = {
            title: '',
            body: '',
            collapsed: true,
            iconClass: 'pmui-accordion-item-icon',
            headerClass: 'pmui-accordion-item-header',
            bodyClass: 'pmui-accordion-item-body',
            footerClass: 'pmui-accordion-item-footer',
            containerClass: 'pmui-accordion-item-container',
            iconClosed: "pmui-accordion-item-closed",
            iconExpanded: "pmui-accordion-item-expanded",
            selected: false,
            factory: {
                products: {
                    'panel': PMUI.core.Item
                }
            }
        };

        jQuery.extend(true, defaults, settings);

        this.setFactory(defaults.products)
            .setTitle(defaults.title)
            .setBody(defaults.body)
            .setIconClass(defaults.iconClass)
            .setHeaderClass(defaults.headerClass)
            .setBodyClass(defaults.bodyClass)
            .setFooterClass(defaults.footerClass)
            .setContainerClass(defaults.containerClass)
            .setIconExpanded(defaults.iconExpanded)
            .setIconClosed(defaults.iconClosed)
            .setCollapsed(defaults.collapsed)
            .setSelected(defaults.selected);
    };

    /**
     * Sets the string that represent the title. It could be an simple string
     * or an string that contains html tags. If the string contains html code
     * it will be rendered
     * @param {String} title
     */
    AccordionItem.prototype.setTitle = function (title) {
        if (typeof title === "string") {
            this.title = title;
            if (this.header) {
                this.header.items[2].title.html.innerHTML = title;
            }
        }

        return this;
    };
    /**
     * Sets the class name for the AccordionItem's title
     * @param {String} class name
     */
    AccordionItem.prototype.setIconClass = function (className) {
        this.iconClass = (typeof className === 'string') ? className : '';
        if (this.header) {
            this.header.items[1].iconTitle.html.className = 'pmui-accordion-item-iconbase ' + className;
        }
        return this;
    };
    /**
     * Sets the class name for the AccordionItem's header
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setHeaderClass = function (className) {
        this.headerClass = (typeof className === 'string') ? className : '';
        if (this.header) {
            this.header.html.className = className;
        }
        return this;
    };
    /**
     * Sets the class name for the AccordionItem's body
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setBodyClass = function (className) {
        this.bodyClass = (typeof className === 'string') ? className : '';
        if (this.html) {
            this.container[1].className = className;
        }
        return this;
    };
    /**
     * Sets the class name for the AccordionItem's footer
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setFooterClass = function (className) {
        this.footerClass = (typeof className === 'string') ? className : '';
        if (this.html) {
            this.container[2].className = className;
        }
        return this;
    };
    /**
     * Sets the class name for the AccordionItem's container
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setContainerClass = function (className) {
        this.containerClass = (typeof className === 'string') ? className : '';
        if (this.html) {
            this.html.className = className;
        }
        return this;
    };
    /**
     * Sets the class name that represents the icon in its expanded status
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setIconExpanded = function (className) {
        this.iconExpanded = (typeof className === 'string') ? className : '';
        if (this.header) {
            this.header.items[0].iconNode.html.className = 'pmui-accordion-item-iconbase ' + className;
        }
        return this;
    };
    /**
     * Sets the class name that represents the icon in its closed status
     * @param {String} class name
     * @private
     */
    AccordionItem.prototype.setIconClosed = function (className) {
        this.iconClosed = (typeof className === 'string') ? className : '';
        if (this.header) {
            this.header.items[0].iconNode.html.className = 'pmui-accordion-item-iconbase ' + className;
        }
        return this;
    };
    /**
     * Sets the status for the item, whether it collapsed or not
     * @param {Boolean} option
     * @private
     */
    AccordionItem.prototype.setCollapsed = function (option) {
        this.collapsed = option;
        return this;
    };
    /**
     * Sets the status collapsed. If it true the container will be opened
     * and if it false will be closed
     * @param {Boolean} status
     */
    AccordionItem.prototype.setSelected = function (status) {
        if (typeof status === 'boolean') {
            this.selected = status;
            this.setCollapsed(!status);
        }
        return this;
    };
    /**
     * Sets the body for the item as a new child for the container.
     * The parameter can be:
     *
     *  - {@link PMUI.core.Panel panel}: In this case the item can be any class that inherit from
     * {@link PMUI.core.Panel Panel}. For example {@link PMUI.form.Form Form}, {@link PMUI.panel.TreePanel TreePanel}, etc.
     *  - String: In this case the string must have contain html code o can be a simple string like to exaple below:
     *
     *      "<a href=\"http://www.colosa.com\">Colosa</a>"
     *
     * @param {PMUI.core.Panel|String} body
     */
    AccordionItem.prototype.setBody = function (body) {
        var currentPanel = this.body.html;
        if (body instanceof PMUI.core.Container) {
            body.getHTML();
            this.body.items = [
                {
                    panel: {
                        html: body.html
                    }
                }
            ];
        } else if (typeof body === 'string') {
            this.body.items = [
                {
                    panel: {
                        html: body
                    }
                }
            ];
        }
        try {
            this.body.html.removeChild(this.body.items[0].panel.html);
        } catch (e) {
        }

        this.addItem(body);
        return this;
    };
    /**
     * Adds an child item to the object.
     * @param {PMUI.core.Element|Object} item It can be one of the following data types:
     * - {PMUI.core.Element} the object to add.
     * - {Object} a JSON object with the settings for the Container to be added.
     * @param {Number} [index] An index in which the item will be added.
     * @chainable
     */
    AccordionItem.prototype.addItem = function (item, index) {
        var itemToBeAdded;
        if (this.factory) {
            itemToBeAdded = this.factory.make(item);
        }
        if (itemToBeAdded) {
            itemToBeAdded.parent = this;
            this.clearItems();
            this.items.insert(itemToBeAdded);
            if (this.body.html) {
                this.body.html.appendChild(itemToBeAdded.getHTML());

                if (this.eventsDefined) {
                    itemToBeAdded.defineEvents();
                }
            }
        }
        return this;
    };
    /**
     * Handler the collapse funtionality
     * @return {PMUI.item.AccordionItem}
     */
    AccordionItem.prototype.toggleCollapse = function () {
        if (this.collapsed) {
            this.expand();
        } else {
            this.collapse();
        }
        return this;
    };
    /**
     * Collapses the item and sets the collapsed property in false
     */
    AccordionItem.prototype.collapse = function () {
        var items;

        items = this.container;
        jQuery(items[1]).slideUp(300);
        jQuery(items[2]).slideUp(300);
        this.header.items[0].iconNode.html.className = 'pmui-accordion-item-iconbase ' + this.iconClosed;
        this.collapsed = true;

        return this;
    };
    /**
     * Expands the item selected. If the multipleSelection is enabled, It is posible select
     * one or more items the otherwise is only possible select one item.
     * @return {PMUI.item.AccordionItem}
     */
    AccordionItem.prototype.expand = function () {
        var i,
            items,
            otherElements;

        items = this.container;
        jQuery(items[1]).slideDown(300);
        jQuery(items[2]).slideDown(300);
        this.header.items[0].iconNode.html.className = 'pmui-accordion-item-iconbase ' + this.iconExpanded;
        this.collapsed = false;

        //For other items
        if (this.parent.multipleSelection === false) {
            otherElements = this.parent.getItems();
            for (i = 0; i < otherElements.length; i += 1) {
                if (this.id != otherElements[i].id) {
                    otherElements[i].collapsed = false;
                    otherElements[i].toggleCollapse();
                }
            }
        }
        return this;
    };

    AccordionItem.prototype.createHTML = function () {
        var container,
            header,
            iconNode,
            iconTitle,
            title,
            body,
            footer,
            containerIcon;

        if (this.html) {
            return this.html;
        }
        container = AccordionItem.superclass.prototype.createHTML.call(this);
        container.setAttribute('class', this.containerClass);
        /*=============================================
         Header accordion
         =============================================*/
        header = PMUI.createHTMLElement("div");
        header.setAttribute('class', this.headerClass);
        iconNode = PMUI.createHTMLElement('span');
        containerIcon = PMUI.createHTMLElement('span');
        containerIcon.className = "pmui-accordion-item-iconContainer";
        if (this.collapsed) {
            iconNode.setAttribute("class", 'pmui-accordion-item-iconbase ' + this.iconClosed);
        } else if (!this.collapsed) {
            iconNode.setAttribute("class", 'pmui-accordion-item-iconbase ' + this.iconExpanded);
        }
        iconTitle = PMUI.createHTMLElement('span');
        iconTitle.setAttribute('class', 'pmui-accordion-item-iconbase ' + this.iconClass);
        title = PMUI.createHTMLElement('span');
        title.className = 'pmui-accordion-item-title';
        title.innerHTML = this.title;
        containerIcon.appendChild(iconNode);
        header.appendChild(containerIcon);

        this.iconNode = containerIcon;
        header.appendChild(iconTitle);
        header.appendChild(title);
        this.header = {
            html: header,
            items: [
                {
                    iconNode: {
                        html: iconNode
                    }
                },
                {
                    iconTitle: {
                        html: iconTitle
                    }
                },
                {
                    title: {
                        html: title
                    }
                }
            ]
        };
        /*=============================================
         body accordion
         =============================================*/
        body = PMUI.createHTMLElement("div");
        body.setAttribute('class', this.bodyClass);

        if (typeof this.parent.heightItem === "string") {
            body.style.height = this.parent.heightItem;
        } else {
            body.style.height = this.parent.heightItem + 'px';
        }
        if (this.collapsed) {
            body.style.display = 'none';
        }

        this.body.html = body;
        jQuery(body).html(this.body.items[0].panel.html);

        /*=============================================
         footer accordion
         =============================================*/
        footer = PMUI.createHTMLElement("div");
        footer.setAttribute('class', this.footerClass);
        this.container = new Array();
        this.container.push(header, body, footer);
        /*=============================================
         Container accordion
         =============================================*/
        container.appendChild(header);
        container.appendChild(body);
        container.appendChild(footer);
        this.html = container;

        return this.html;
    };
    /**
     * Defines events related to the current panel and the calls to DefineEvents from
     * every {@link PMUI.core.Item item}
     * return {this}
     */
    AccordionItem.prototype.defineEvents = function () {
        var j,
            fnSelect,
            children,
            that = this;

        this.removeEvents().eventsDefined = true;
        if (that.container[0]) {
            that.addEvent('click').listen(this.iconNode, function (e) {
                e.preventDefault();

                that.toggleCollapse();
                fnSelect = that.parent.listeners.select;
                fnSelect(that, e);

                e.stopPropagation();
            });
        }

        if (that.items.getSize() > 0) {
            children = that.getItems();
            for (j = 0; j < children.length; j += 1) {
                children[j].defineEvents();
            }
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.item.AccordionItem', AccordionItem);
    if (typeof exports !== "undefined") {
        module.exports = AccordionItem;
    }
}());