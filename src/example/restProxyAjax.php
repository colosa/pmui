<?php
	$method = $_SERVER['REQUEST_METHOD'];
	
	$resource = $_SERVER['REQUEST_URI'];
	
	$response = array();
	switch ($method) {
		case 'GET':
			$response = array(	'name' => "Name1", 
								'lastname' => "Lastname1",
								"age" => 24
							);
			break;
		case 'POST':
			$arguments = $_POST;
			$info = json_decode(file_get_contents('php://input'));
			$response = array(	'success' => true,
			 					'message' =>  "The user with information ".
		 									$info->name." ".$info->lastname.", ".$info->age.
		 									" was saved..."
			 				 );
			break;
		case 'PUT':
			$info = json_decode(file_get_contents('php://input'));
			$response = array("success" => true,
							'message' =>  "The user with information ".
		 									$info->name." ".$info->lastname.", ".$info->age.
		 									" was updated..."
		 					 );
			break;
		case 'DELETE':
			$info = json_decode(file_get_contents('php://input'));
			$response = array("success" => true,
							'message' =>  "The user with information ".
		 									$info->name." ".$info->lastname.", ".$info->age.
		 									" was deleted..."
		 					 );
			break;
	}
	echo json_encode($response,true);
?>