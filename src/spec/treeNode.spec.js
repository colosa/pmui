//***********************other class******************************************************//
describe('PMUI.item.TreeNode', function () {
    var a, b, c, j, g, i, h;
    beforeEach(function () {
        a = new PMUI.item.TreeNode({
                    data: {
                        name: 'America'
                    },
                    labelDataBind: 'name',
                    autoBind: true,
                    childrenDefaultSettings: {
                        labelDataBind: 'name',
                        autoBind: true
                    },
                    dataItems: [
                        {
                            name: "Argentina"
                        },
                        {
                            name: "Bolivia"
                        },
                        {
                            name: "Brasil"
                        }
                    ]
                });
        b = new PMUI.item.TreeNode({
                    label: 'America',
                    childrenDefaultSettings: {
                        label: '[untitled node]'
                    },
                    items: [
                        {
                            label: "North America",
                            items: [
                                {
                                    label: 'Canada',
                                    data: {
                                        label: 'Canada',
                                        value: 'cnd'
                                    }
                                },
                                {
                                    label: 'Mexico'
                                }
                            ]
                        },
                        {
                            label: "Central America and Caribbean",
                            items: [
                                {
                                    label: 'Cuba'
                                },
                                {
                                    label: 'Guatemala'
                                } 
                            ]
                        }
                    ]
                });
        document.body.appendChild(a.getHTML());
        a.defineEvents();
        document.body.appendChild(b.getHTML());
        b.defineEvents();
    });
    afterEach(function () {
        $(a.getHTML()).remove();
        $(b.getHTML()).remove();
        a = null;
        b = null;
    });
    describe('class behavior', function () {
        it('[US-15,a]should create a new HTML element', function () {
            a = new PMUI.item.TreeNode();
            var h = a.createHTML();
            expect(h).toBeDefined();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
        });
        it('[US-15,b]should set children to a TreeNode element created using dataItems', function () {
            j = {data: {
                        name: 'root'
                    },
                    labelDataBind: 'name',
                    autoBind: true,
                    childrenDefaultSettings: {
                        labelDataBind: 'name',
                        autoBind: true
                    },
                    dataItems:[
                        {name: 'leaf1'},
                        {name: 'leaf2'},
                        {name: 'leaf3'}
                        ]
                    };
            c = new PMUI.item.TreeNode(j);
            document.body.appendChild(c.getHTML());
            c.defineEvents();
            expect(c.getItem(0)).toBeDefined();
            expect(c.label === "root").toBeTruthy();
            expect(c.data.type === "DataField").toBeTruthy();
            expect(c.items.asArray().length === 3).toBeTruthy();
            expect(c.items.asArray()[0].type === "TreeNode").toBeTruthy();
            expect(c.items.asArray()[1].type === "TreeNode").toBeTruthy();
            expect(c.items.asArray()[2].type === "TreeNode").toBeTruthy();
            expect(c.items.asArray()[0].data.type === "DataField").toBeTruthy();
            expect(c.items.asArray()[1].data.type === "DataField").toBeTruthy();
            expect(c.items.asArray()[2].data.type === "DataField").toBeTruthy();
            expect($(c.dom.nodeElement).find('li').length === 3).toBeTruthy();
            expect($(c.dom.nodeElement).find('li')[0].textContent).toContain("leaf1");
            expect($(c.dom.nodeElement).find('li')[1].textContent).toContain("leaf2");
            expect($(c.dom.nodeElement).find('li')[2].textContent).toContain("leaf3");
            $(c.getHTML()).remove();
            c = null;
            j = null;
        });
        it('[US-15,b]should set children to a TreeNode element created using just items', function () {
            j = {label: 'root',
                    childrenDefaultSettings: {
                        label: '[untitled node]'
                    },
                    items: [
                        {
                            label: "leaf1",
                            items: [
                                {
                                    label: 'sleaf1',
                                    data: {
                                        label: 'Canada',
                                        value: 'cnd'
                                    }
                                },
                                {
                                    label: 'sleaf2'
                                }
                            ]
                        }
                    ]};
            c = new PMUI.item.TreeNode(j);
            document.body.appendChild(c.getHTML());
            c.defineEvents();
            expect(a.getItem(0)).toBeDefined();
            expect(c.label === "root").toBeTruthy();
            expect(c.data.type === "DataField").toBeTruthy();
            expect(c.items.asArray().length === 1).toBeTruthy();
            expect(c.items.asArray()[0].type === "TreeNode").toBeTruthy();
            expect(c.items.asArray()[0].data.type === "DataField").toBeTruthy();
            expect($(c.dom.nodeElement).find('li').length === 3).toBeTruthy();
            expect($(c.dom.nodeElement).find('li')[0].textContent).toContain("leaf1");
            $(c.getHTML()).remove();
            c = null;
            j = null;
        });
    });
    describe('method "setLabel"', function () {
        it('should set a new text to a TreeNode element created', function () {
            expect(a.label).toBeDefined();
            expect(a.label === "America").toBeTruthy();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.setLabel("root");
            expect(a.label === "root").toBeTruthy();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("root");
        });
    });
    describe('method "collapse"', function () {
        it('should collapse an item specified', function () {
            a.collapse();
            expect(a.isCollapsed()).toBeTruthy();
            expect($(a.html)[0].className).toContain("pmui-treepanel-node-collapsed");
        });
    });
    describe('method "expand"', function () {
        it('should collapse an item specified', function () {
            a.collapse();
            expect(a.isCollapsed()).toBeTruthy();
            expect($(a.html)[0].className).toContain("pmui-treepanel-node-collapsed");
            j = $(a.html).find('ul[style="display: none;"]').length;
            a.expand();
            expect(a.isCollapsed()).toBeFalsy();
            expect($(a.html)[0].className).not.toContain("pmui-treepanel-node-collapsed");
            expect($(a.html).find('ul[style="display: none;"]').length).toBeLessThan(j);
        });
    });
    describe('method "disableAutoBind"', function () {
        it('should turn off the auto binding functionnality', function () {
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.disableAutoBind();
            expect(a.autoBind).toBeFalsy();
            a.setData({name: 'root', lastname: 'lastnameroot'});
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.setLabelDataBind("lastname");
            a.bindData();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("lastnameroot");
            a.setLabelDataBind("name");
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("lastnameroot");
            a.bindData();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("root");
        });
    });
    describe('method "enableAutoBind"', function () {
        it('should turn on the auto binding functionnality', function () {
            a.enableAutoBind();
            expect(a.autoBind).toBeTruthy();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.setData({name: 'root', lastname: 'lastnameroot'});
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("root");
            a.setLabelDataBind("lastname");
            a.bindData();//weird
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("lastnameroot");
        });
    });
    describe('method "setRecursiveChildrenDefaultSettings"', function () {
        it('should set a recursive default settings to all children, it must be a boolean value', function () {
            a.setRecursiveChildrenDefaultSettings(true);
            a.addItem({
                            name: "Colombia"
                        });
            b = a.items.asArray()[3];
            expect(b.autoBind).toBeTruthy();
            expect(b.labelDataBind === "name").toBeTruthy();
            expect(b.isRecursiveChildrenDefaultSettings()).toBeTruthy();
        });
    });
    describe('method "isRecursiveChildrenDefaultSettings"', function () {
        it('should return a boolean value to show if the treenode has this recursive property', function () {
            a.setRecursiveChildrenDefaultSettings(true);
            expect(a.isRecursiveChildrenDefaultSettings()).toBeTruthy();
            a.setRecursiveChildrenDefaultSettings(false);
            expect(a.isRecursiveChildrenDefaultSettings()).toBeFalsy();
        });
    });
    describe('method "setChildrenDefaultSettings"', function () {
        it('should set the children default settings', function () {
            var obj = {
                        labelDataBind: 'lastname',
                        autoBind: false
                    };
            a.setChildrenDefaultSettings(obj);
            expect(a.childrenDefaultSettings.labelDataBind === 'lastname').toBeTruthy();
            expect(a.childrenDefaultSettings.autoBind).toBeFalsy();
            obj = {
                        labelDataBind: 'name',
                        autoBind: true
                    };
            a.setChildrenDefaultSettings(obj);
            expect(a.childrenDefaultSettings.labelDataBind === 'name').toBeTruthy();
            expect(a.childrenDefaultSettings.autoBind).toBeTruthy();
        });
    });
    describe('method "getChildrenDefaultSettings"', function () {
        it('should return the childrenDefaultSettings expressed as an object', function () {
            expect(typeof a.getChildrenDefaultSettings() === "object").toBeTruthy();
            expect(a.getChildrenDefaultSettings().labelDataBind === "name").toBeTruthy();
            expect(a.getChildrenDefaultSettings().autoBind).toBeTruthy();
        });
    });
    describe('method "setNodeClassDataBind"', function () {
        it('should set the NodeClassDataBind, this must be a string or null', function () {
            expect(function () {a.setNodeClassDataBind("new class")}).not.toThrow();
            expect(a.nodeClassDataBind === "new class").toBeTruthy();
            expect(function () {a.setNodeClassDataBind(null)}).not.toThrow();
            expect(a.nodeClassDataBind).toBeNull();
            expect(function () {a.setNodeClassDataBind(23)}).toThrow();
            expect(function () {a.setNodeClassDataBind({name: "abc"})}).toThrow();
        });
    });
    describe('method "setItemsDataBind"', function () {
        it('should set the ItemsDataBind, this must be a string or null', function () {
            expect(function () {a.setItemsDataBind("new class")}).not.toThrow();
            expect(a.itemsDataBind === "new class").toBeTruthy();
            expect(function () {a.setItemsDataBind(null)}).not.toThrow();
            expect(a.itemsDataBind).toBeNull();
            expect(function () {a.setItemsDataBind(23)}).toThrow();
            expect(function () {a.setItemsDataBind({name: "abc"})}).toThrow();
        });
    });
    describe('method "setLabelDataBind"', function () {
        it('should set the LabelDataBind, this must be a string or null', function () {
            expect(function () {a.setLabelDataBind("new class")}).not.toThrow();
            expect(a.labelDataBind === "new class").toBeTruthy();
            expect(function () {a.setLabelDataBind(null)}).not.toThrow();
            expect(a.labelDataBind).toBeNull();
            expect(function () {a.setLabelDataBind(23)}).toThrow();
            expect(function () {a.setLabelDataBind({name: "abc"})}).toThrow();
        });
    });
    describe('method "bindData"', function () {
        it('should bind the node´s data ', function () {
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.disableAutoBind();
            expect(a.autoBind).toBeFalsy();
            a.setData({name: 'root', lastname: 'lastnameroot'});
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("America");
            a.setLabelDataBind("lastname");
            a.bindData();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("lastnameroot");
            a.setLabelDataBind("name");
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("lastnameroot");
            a.bindData();
            expect($(a.dom.nodeElement).find('span')[0].textContent).toContain("root");
        });
    });
    describe('method "setData"', function () {
        it('should set the data for the node', function () {
            a.setData({name: 'root', lastname: 'lastname root', midname: 'm root'});
            expect(typeof a.data === "object").toBeTruthy();
            expect(a.data.customKeys.name === 'root').toBeTruthy();
            expect(a.data.customKeys.lastname === 'lastname root').toBeTruthy();
            expect(a.data.customKeys.midname === 'm root').toBeTruthy();
        });
    });
    describe('method "getData"', function () {
        it('should return the node data', function () {
            a.setData({name: 'root', lastname: 'lastname root', midname: 'm root'});
            expect(a.getData().type === 'string').toBeTruthy();
            expect(a.getData().name === 'root').toBeTruthy();
            expect(a.getData().lastname === 'lastname root').toBeTruthy();
            expect(a.getData().midname === 'm root').toBeTruthy();
        });
    });
    describe('method "setParent"', function () {
        it('should set the parent node, the parent must be an instance of TreeNode, TreePanel or null', function () {
            j = a.items.asArray()[0];
            expect(j.parent === a).toBeTruthy();
            c = new PMUI.item.TreeNode();
            expect(function () {c.setParent(a)}).not.toThrow();
            expect(c.parent === a).toBeTruthy();
            g = new PMUI.panel.TreePanel();
            expect(function () {c.setParent(g)}).not.toThrow();
            expect(c.parent === g).toBeTruthy();
            expect(function () {c.setParent(null)}).not.toThrow();
            expect(function () {c.setParent(new PMUI.event.Events())}).toThrow();
        });
    });
    describe('method "setSterility"', function () {
        it('should set the sterile property to the current node', function () {
            j = a.items.asArray()[0];
            j.setSterility(true);
            expect(j.sterile).toBeTruthy();
            expect(function () {j.addItem({
                                            name: "Colombia"
                                        })}).toThrow();
            j.setSterility(false);
            expect(j.sterile).toBeFalsy();
            expect(function () {j.addItem({
                                            name: "Colombia"
                                        })}).not.toThrow();
        });
    });
    describe('method "isSterile"', function () {
        it('should return a boolean value to show if the current node is sterile or not', function () {
            j = a.items.asArray()[0];
            j.setSterility(true);
            expect(j.isSterile()).toBeTruthy();
            j.setSterility(false);
            expect(j.isSterile()).toBeFalsy();
        });
    });
    describe('method "setNodeClass"', function () {
        it('should set the node class for the HTML, this must be string', function () {
            expect(function () {a.setNodeClass("pmui-nodeClass")}).not.toThrow();
            expect(a.nodeClass === "pmui-nodeClass").toBeTruthy();
            expect($(a.html)[0].className).toContain("pmui-nodeClass");
            expect(function () {a.setNodeClass(324)}).toThrow();
            expect(function () {a.setNodeClass({name: 'pmui-nodeClass'})}).toThrow();
        });
    });
    describe('method "getNodeClass"', function () {
        it('should return the node class', function () {
            a.setNodeClass("pmui-nodeClass");
            expect(typeof a.getNodeClass() === 'string').toBeTruthy();
            expect(a.getNodeClass()).toContain("pmui-nodeClass");
        });
    });
    describe('method "isCollapsed"', function () {
        it('should return a boolean value, true if the node is collapsed, false if not', function () {
            a.collapse();
            expect(a.isCollapsed()).toBeTruthy();
            a.expand();
            expect(a.isCollapsed()).toBeFalsy();
        });
    });
    describe('method "isLeaf"', function () {
        it('should return a boolean value to show if the node is a leaf or not', function () {
            expect(a.isLeaf()).toBeFalsy();
            j = a.items.asArray()[0];
            expect(j.isLeaf()).toBeTruthy();
        });
    });
    describe('method "isRoot"', function () {
        it('should return a boolean value to show if the node is root or not', function () {
            expect(a.isRoot()).toBeTruthy();
            j = a.items.asArray()[0];
            expect(j.isRoot()).toBeFalsy();
            expect(b.isRoot()).toBeTruthy();
            g = b.items.asArray()[1];
            expect(g.isRoot()).toBeFalsy();
        });
    });
    describe('method "addItem"', function () {
        it('should add a new item to the treeNode, this must be an object or a TreeNode type', function () {
            i = a.items.asArray().length;
            j = new PMUI.item.TreeNode({data: 
                                            {
                                                name: 'Peru'
                                            },
                                        labelDataBind: 'name',
                                        autoBind: true,
                                        childrenDefaultSettings: 
                                            {
                                                labelDataBind: 'name',
                                                autoBind: true
                                            }
                                        });
            a.addItem(j);
            expect(a.items.asArray().length).toBeGreaterThan(i);
            expect(a.items.asArray().length === i+1).toBeTruthy();
            expect($(a.dom.nodeElement).find('li')[i].textContent).toContain("Peru");
            g = {name: 'Chile', label: 'Chile'};
            a.addItem(g);
            expect(a.items.asArray().length).toBeGreaterThan(i);
            expect(a.items.asArray().length === i+2).toBeTruthy();
            expect($(a.dom.nodeElement).find('li')[i+1].textContent).toContain("Chile");
        });
        it('should add a new item, with an index paremeter, this must be a number', function () {
            f = {name: 'Chile', label: 'Chile'};
            g = {name: 'Peru', label: 'Peru'};
            a.addItem(f, 0);
            a.addItem(g, 2);
            expect(a.items.asArray()[0].label === 'Chile').toBeTruthy();
            expect(a.items.asArray()[1].label === 'Argentina').toBeTruthy();
            expect(a.items.asArray()[2].label === 'Peru').toBeTruthy();
            expect(a.items.asArray()[3].label === 'Bolivia').toBeTruthy();
            expect(a.items.asArray()[4].label === 'Brasil').toBeTruthy();
            expect($(a.dom.nodeElement).find('li')[0].textContent).toContain("Chile");
            expect($(a.dom.nodeElement).find('li')[1].textContent).toContain("Argentina");
            expect($(a.dom.nodeElement).find('li')[2].textContent).toContain("Peru");
            expect($(a.dom.nodeElement).find('li')[3].textContent).toContain("Bolivia");
            expect($(a.dom.nodeElement).find('li')[4].textContent).toContain("Brasil");
        });
    });
    describe('method "removeItem"', function () {
        it('should remove an item using an index, the node id, or the TreeNode object as a parameter', function () {
            i = a.items.asArray().length;
            h = 1;
            j = a.items.asArray()[0];
            expect(j instanceof PMUI.item.TreeNode).toBeTruthy();
            g = a.items.asArray()[2].id;
            expect(typeof g === "string").toBeTruthy();
            expect(function () {a.removeItem(h)}).not.toThrow();
            expect(a.items.asArray().length).toBeLessThan(i);
            expect($(a.dom.nodeElement).find('li')[1].textContent).not.toContain("Bolivia");
            expect($(a.dom.nodeElement).find('li')[1].textContent).toContain("Brasil");
            expect(function () {a.removeItem(j)}).not.toThrow();
            expect(a.items.asArray().length).toEqual(i-2);
            expect($(a.dom.nodeElement).find('li')[0].textContent).not.toContain("Argentina");
            expect($(a.dom.nodeElement).find('li')[0].textContent).toContain("Brasil");
        });
    });
    describe('method "toggleCollapsing"', function () {
        it('should collapse if the node is expanded, or expand if the node is collapsed', function () {
            a.toggleCollapsing();
            if(a.isCollapsed()){
                expect(a.isCollapsed()).toBeTruthy();
            }
            expect(a.isCollapsed()).toBeFalsy();
        });
    });
    describe('method "defineEvents"', function () {
        it('should define events for the treeNode', function () {
            a.defineEvents();
            if(a.isCollapsed()){
                expect(a.isCollapsed()).toBeTruthy();
                $(a.dom.nodeElement).find('a').trigger('click');
                expect(a.isCollapsed()).toBeFalsy();
            }
            expect(a.isCollapsed()).toBeFalsy();
            $(a.dom.nodeElement).find('a').trigger('click');
            expect(a.isCollapsed()).toBeTruthy();
        });
    });
});