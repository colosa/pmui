(function () {
    /**
     * @class PMUI.control.SelectableControl
     * A checkbox is a graphical component that can be in either an "on" (true) or "off" (false) state.
     * Clicking on a check box changes its state from "on" to "off," or from "off" to "on."
     * @extends PMUI.control.HTMLControl
     *
     * Usage example:
     *
     *      @example
     *      var a;
     *      $(function() {
     *          a = new PMUI.control.SelectableControl({
     *              name: "music",
     *              label: "Do you like music?",
     *              value: true,
     *              mode: 'checkbox', //it also can be "radio"
     *              onSelect: function() {
     *                  console.log("checked");
     *              },
     *              onDeselect: function() {
     *                  console.log("unchecked");
     *              }
     *          });
     *          document.body.appendChild(a.getHTML());
     *          a.defineEvents();
     *      });
     *
     * @constructor
     * Creates a new instance of the SelectableControl class.
     * @param {Object} [settings=null] A JSON object with the config options.
     *
     * @cfg {Boolean} [selected=false] If the control will be selected initially.
     * @cfg {String} [label=""] The label for the control.
     * @cfg {String} [value=""] The value for the control.
     * @cfg {Function} [onSelect=null] The function to be call when the item will be selected. For info about the
     * parameters please read the {@link PMUI.control.SelectableControl#event-onSelect onSelect event} documentation.
     * @cfg {Function} [onDeselect=null] The function to be call when the item will be deselected
     * (only supported when the mode is set to "checkbox"). For info about the callback parameters please read the
     * {@link PMUI.control.SelectableControl#event-onDeselect onDeselect event} documentation.
     * @cfg {String} [mode="checkbox"] The mode for the control, it can be: "checkbox" (default)
     * for a checkbox control or "radio" for a readio button.
     */
    var SelectableControl = function (settings) {
        SelectableControl.superclass.call(this, settings);
        /**
         * If the control is selected or not.
         * @type {Boolean}
         * @readonly
         */
        this.selected = null;
        /**
         * The control's label.
         * @type {String}
         * @readonly
         */
        this.label = null;
        /**
         * @event onSelect
         * Fired when the control is selected.
         */
        this.onSelect = null;
        /**
         * @event onDeselect
         * Fired when the control is deselected.
         */
        this.onDeselect = null;
        /**
         * The control's selection mode.
         * @type {String}
         * @readonly
         */
        this.mode = null;
        /**
         * The interactive control's HTML element.
         * @type {HTMLElement}
         * @private
         */
        this.control = null;
        /**
         * The control's HTML element that will contain the label text.
         * @type {[type]}
         */
        this.textContainer = null;
        SelectableControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', SelectableControl);
    /**
     * The object's type.
     * @type {String}
     */
    SelectableControl.prototype.type = 'SelectableControl';
    /**
     * Initializes the object.
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    SelectableControl.prototype.init = function (settings) {
        var defaults = {
            selected: false,
            mode: 'checkbox',
            label: '',
            value: "",
            onSelect: null,
            onDeselect: null
        };

        jQuery.extend(true, defaults, settings);

        if (defaults.mode === 'checkbox' || defaults.mode === 'radio') {
            this.mode = defaults.mode;
        } else {
            throw new Error('SelectableControl: it only accepts "checkbox" and "radio" as value for the "mode"'
                + ' property');
        }
        this.onSelect = defaults.onSelect;
        this.onDeselect = defaults.onDeselect;

        this.setLabel(defaults.label);

        if (defaults.selected) {
            this.select();
        } else {
            this.deselect();
        }
    };
    /**
     * Sets the control's name
     * @param {String} name
     * @chainable
     */
    SelectableControl.prototype.setName = function (name) {
        SelectableControl.superclass.prototype.setName.call(this, name);
        if (this.control) {
            this.control.name = name;
        }

        return this;
    };
    /**
     * Sets the value for the control
     * @param {String|Boolean|Number} setValue This param value is evaluated as boolean.
     * @chainable
     */
    SelectableControl.prototype.setValue = function (value) {
        SelectableControl.superclass.superclass.prototype.setValue.call(this, value);
        if (this.control) {
            this.control.value = value;
        }
        return this;
    };
    /**
     * Returns a boolean that specifies if the checkbox/radio button is selected.
     * @return {Boolean}
     */
    SelectableControl.prototype.isSelected = function () {
        if (this.html) {
            this.selected = jQuery(this.control).is(':checked');
        }
        return this.selected;
    };
    /**
     * Sets deselected the checkbox/radio button.
     * @chainable
     */
    SelectableControl.prototype.deselect = function () {
        this.selected = false;

        if (this.control) {
            this.control.checked = false;
        }

        return this;
    };
    /**
     * Sets selected the checkbox/radio button.
     * @chainable
     */
    SelectableControl.prototype.select = function () {
        this.selected = true;

        if (this.control) {
            this.control.checked = true;
        }

        return this;
    };
    /**
     * A method which is called everytime the checked state from the control changes.
     *
     * This method is used internally by the object, so in most of the cases you won't need to invocated.
     * To execute instructions when the control changes, please use the
     {@link PMUI.control.Control#setOnChangeHandler setOnChangeHandler()} method.
     *
     * @chainable
     * @private
     */
    SelectableControl.prototype.onChangeHandler = function () {
        this.selected = $(this.control).is(":checked");
        if (typeof this.onChange === 'function') {
            this.onChange(this.value, this.value);
        }
        if (this.selected && typeof this.onSelect === 'function') {
            this.onSelect();
        } else if (!this.selected && typeof this.onDeselect === 'function') {
            this.onDeselect();
        }

        return this;
    };
    /**
     * Sets the label for the control.
     * @param {String} label
     * @chainable
     */
    SelectableControl.prototype.setLabel = function (label) {
        if (typeof label !== 'string') {
            throw new Error("setLabel(): it only accepts string type values.");
        }
        this.label = label;
        if (this.html) {
            this.textContainer.textContent = label;
        }

        return this;
    };
    /**
     * Disables/enables the control
     * @param {Boolean} disable If the value is evaluated as true then the control
     is disabled, otherwise the control is enabled.
     * @chainable
     */
    SelectableControl.prototype.disable = function (disable) {
        SelectableControl.superclass.superclass.prototype.disable.call(this, disable);
        if (this.html) {
            this.control.disabled = this.disabled;
        }

        return this;
    };
    /**
     * @inheritDoc
     */
    SelectableControl.prototype.defineEvents = function () {
        var that = this,
            stopPropagation = function (e) {
                e.stopPropagation();
            };
        this.removeEvents();
        this.eventsDefined = true;
        if (this.html) {
            this.addEvent('change').listen(this.html, function () {
                that.onChangeHandler();
            });
            this.addEvent('keydown').listen(this.html, function (e) {
                if (e.which === PMUI.keyCodeF5) {
                    this.blur();
                    e.preventDefault();
                    window.location.reload(true);
                }
            });
        }
        return this;
    };
    /**
     * Creates the HTML element for the control.
     * @return {HTMLElement}
     */
    SelectableControl.prototype.createHTML = function () {
        var label,
            textContainer,
            control;
        if (this.html) {
            return this.html;
        }
        label = PMUI.createHTMLElement('label');
        control = PMUI.createHTMLElement('input');
        control.type = this.mode;
        textContainer = PMUI.createHTMLElement('span');
        textContainer.contentText = this.label;

        label.appendChild(control);
        label.appendChild(textContainer);
        this.control = control;
        this.textContainer = textContainer;
        this.html = label;

        this.html.id = this.id;

        this.setName(this.name)
            .setValue(this.value)
            .disable(this.disabled)
            .setLabel(this.label);

        if (this.selected) {
            this.select();
        } else {
            this.deselect();
        }

        this.applyStyle();

        return this.html;
    };

    SelectableControl.prototype.setFocus = function () {
        if (this.html) {
            this.control.focus();
        }
    };

    PMUI.extendNamespace('PMUI.control.SelectableControl', SelectableControl);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = SelectableControl;
    }
}());