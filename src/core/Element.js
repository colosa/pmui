(function () {
    /**
     * @class PMUI.core.Element
     * Base class to handle objects that have a visual representation (HTML representation).
     * @extends PMUI.core.Base
     *
     * Usage example:
     *
     *      @example
     *      var element;
     *
     *      element = new PMUI.core.Element({
     *          x: 10,
     *          y: 20,
     *          positionMode: "absolute",
     *          width: 200,
     *          height: 200,
     *          style: {
     *              cssProperties: {
     *                  "background-color": "red"
     *              },
     *              cssClasses: [".square"]
     *          }
     *      });
     *
     *      document.body.appendChild(element.getHTML());
     *
     * @constructor
     * Create a new instace of the class 'Element'
     * @param {Object} options
     *
     * @cfg {String} [elementTag="div"] The html tag for the object's HTML element.
     * @cfg {String} [positionMode="relative"] The css position for the object's HTML element. It can take one of the
     * following values "static", "absolute", "fixed", "relative", "inherit" or "". Please read about the css "position"
     * property to know about how each value impacts in the html.
     * @cfg {Object} [style={cssProperties:{}, cssClasses:[]}] A JSON object which will contain the css properties for
     * the object's html.
     *
     * This object can have optionally two properties:
     *
     * - cssProperties: another JSON object in which each property can be a css property (if the css property has any
     * special character like "-" enclose the property name between quotes), the value can be any valid css value for
     * the property.
     * - cssClasses: An array where each element is a String, this String is the css class name.
     *
     * @cfg {Number} [x=0] The position coordinate in the x axis for the object's HTML, please note that the visual
     * position will depend on the {@link PMUI.core.Element#cfg-positionMode positionMode option}.
     * @cfg {Number} [y=0] The position coordinate in the y axis for the object's HTML, please note that the visual
     * position will depend on the {@link PMUI.core.Element#cfg-positionMode positionMode option}.
     * @cfg {Number|String} [width="auto"] The width for the object's HTML, it can be a Number or an String. In the
     * latter case the value must be "auto" or "inherit" or have one of the following formats:
     *
     * - "##px".
     * - "##%".
     * - "##em"
     *
     * Note that ## is a number.
     * @cfg {Number|String} [height="auto"] The height for the object's HTML, the values that can take are the same
     * specified in the {@link PMUI.core.Element#cfg-width}.
     * @cfg {Number|String} [zOrder="auto"] The position in the z axis for the object's HTML, it can be a Number or a
     * String, in the latter case the value only can be "auto" or "inherit".
     * @cfg {Boolean} [visible=true] If the object will be visible or not.
     * @cfg {Number} [proportion=1] A numeric value that specifies the final width of the object's HTML in case to be
     * contained by a {@link PMUI.core.Container Container} which is applying a {@link PMUI.layout.Layout Layout}.
     * @cfg {String} [display=""] The display property specifies the type of box used for an HTML element.
     * find out more about the css display property
     * @cfg {Function} [onBeforeContextMenu=null] description here.
     */
    var Element = function (settings) {
        Element.superclass.call(this, settings);
        /**
         * HTML element.
         * @type {HTMLElement}
         * @readonly
         */
        this.html = null;
        /**
         * @property {Object|PMUI.util.Style} [style={
                cssProperties: {},
                cssClasses: []
            }]
         * A {@link PMUI.util.Style Style} object or a JSON object with the settings to create a new
         * {@link PMUI.util.Style Style} object for the current's HTML element.
         * @readonly
         */
        this.style = null;
        /**
         * X coordinate for the HTML element.
         * @type {Number}
         * @readonly
         */
        this.x = null;
        /**
         * @property {Number} [y=0]
         * Y Coordinate for the HTML element.
         * @readonly
         */
        this.y = null;
        /**
         * Width for the HTML element, it can be a number or a string with the following format:
         ##px when ## is a number.
         * @type {Number|String}
         * @readonly
         */
        this.width = null;
        /**
         * Height for the HTML element, it can be a number or a string with the following format:
         ##px when ## is a number.
         * @type {Number|String}
         * @readonly
         */
        this.height = null;
        /**
         * A boolean value that indicates if the HTML element is visible or not.
         * @type {Boolean}
         * @readonly
         */
        this.visible = null;
        /**
         * A Number that indicates the HTML element's position on the Z axis.
         * @type {Number}
         * @readonly
         */
        this.zOrder = null;
        /**
         * @property {String} [elementTag="div"]
         * Tag name for the element to be created, it defaults to "div".
         * @readonly
         */
        this.elementTag = null;
        /**
         * @property {String} [positionMode="relative"]
         * Position for the object's html element, it must be a valid value for the "position" CSS property.
         * @readonly
         */
        this.positionMode = null;
        /**
         * Defines the proportion of the html.
         * @type {Number}
         */
        this.proportion = null;
        /**
         * @property {String} [display=""] The display mode for the element. It is set by the
         * {@link #cfg-display display} config option and the {@link #method-setDisplay setDisplay} method.
         * @readonly
         */
        this.display = null;
        /**
         * Defines the menu component for the element
         * @type {PMUI.ui.Menu}
         */
        this.menu = null;
        /**
         * Defines an object to handle/register events.
         * @type {Object}
         */
        this.events = {};
        /**
         * @property {Boolean} eventsDefined If the events were defined or not
         */
        this.eventsDefined = false;
        /**
         * [onContextMenu description]
         * @type {Boolean}
         */
        this.onContextMenu = false;
        /**
         * [onBeforeContextMenu description]
         * @type {Boolean}
         */
        this.onBeforeContextMenu = false;
        /**
         * The element's parent Container.
         * @type {PMUI.core.Element}
         */
        this.parent = null;
        /**
         * The HTML area in which some activities, actions can be performed (like display the context menu).
         * @type {HTMLElement}
         * @private
         */
        this.actionHTML = null;

        Element.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Base', Element);
    /**
     * The Object's type.
     * @type {String}
     */
    Element.prototype.type = 'Element';
    /**
     * Initialize the object.
     * @param  {Object} settings A JSON object with the settings to create the new class instance.
     * @private
     */
    Element.prototype.init = function (settings) {
        var defaults = {
            elementTag: "div",
            positionMode: "relative",
            menu: null,
            style: {
                cssProperties: {},
                cssClasses: []
            },
            x: 0,
            y: 0,
            width: "auto",
            height: "auto",
            zOrder: "auto",
            display: "",
            visible: true,
            proportion: 1,
            onContextMenu: null,
            onBeforeContextMenu: null
        };

        jQuery.extend(true, defaults, settings);

        this.setElementTag(defaults.elementTag)
            .setStyle(defaults.style)
            .setPositionMode(defaults.positionMode)
            .setDisplay(defaults.display)
            .setX(defaults.x)
            .setY(defaults.y)
            .setWidth(defaults.width)
            .setHeight(defaults.height)
            .setZOrder(defaults.zOrder)
            .setVisible(defaults.visible)
            .setProportion(defaults.proportion)
            .setContextMenu(defaults.menu)
            .setOnBeforeContextMenu(defaults.onBeforeContextMenu)
            .setOnContextMenuHandler(defaults.onContextMenu);
    };
    /**
     * [setOnBeforeContextMenu description]
     */
    Element.prototype.setOnBeforeContextMenu = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnBeforeContextMenu(): The parameter must be a function or null.");
        }
        this.onBeforeContextMenu = handler;
        return this;
    };
    /**
     * [setOnContextMenuHandler description]
     * @param {[type]} handler [description]
     */
    Element.prototype.setOnContextMenuHandler = function (handler) {
        if (typeof handler === 'function' || null) {
            this.onContextMenu = handler;
        }
        return this;
    };
    /**
     * Sets the display mode for the Element.
     * @param {String} display It can take one of the following values:
     *
     * - ""
     * - "block"
     * - "inline"
     * - "inline-block"
     * - "none"
     *
     * @chainable
     */
    Element.prototype.setDisplay = function (display) {
        if (display === "" || display === 'block' || display === 'inline'
            || display === 'inline-block' || display === 'none') {
            this.display = display;
            this.applyStyle();
        } else {
            throw new Error('The setDisplay() method only accepts one od the following options: ' +
                ' "", "block", "inline", "inline-block", "none"');
        }

        return this;
    };
    /**
     * Sets the position mode for the Element.
     * @param {String} position It can take one ot the following values:
     *
     * - "static"
     * - "asolute"
     * - "fixed"
     * - "relative"
     * - "inherit"
     *
     * @chainable
     */
    Element.prototype.setPositionMode = function (position) {
        if (position === 'static' || position === 'absolute' || position === 'fixed' || position === 'relative' ||
            position === 'inherit' || position === "") {
            this.positionMode = position;
            this.applyStyle();
        } else {
            throw new Error('The setPosition() method only accepts one of the following options:' +
                ' "static", "absolute", "fixed", "relative", "inherit" or an empty string.');
        }

        return this;
    };
    /**
     * Set the HTML tag for the HTML element to be created, note that it'll only work when its html property
     is still not set.
     * @param {String} tag a HTML tag
     * @chainable
     */
    Element.prototype.setElementTag = function (tag) {
        if (!this.html && typeof tag === 'string') {
            this.elementTag = tag;
        }

        return this;
    };
    /**
     * [addCSSClasses description]
     * @param {[type]} classes [description]
     */
    Element.prototype.addCSSClasses = function (classes) {
        this.style.addClasses(classes);
        return this;
    };
    /**
     * [addCSSProperties description]
     * @param {[type]} properties [description]
     */
    Element.prototype.addCSSProperties = function (properties) {
        this.style.addProperties(properties);
        return this;
    };
    /**
     * [removeCSSClasses description]
     * @param  {[type]} classes [description]
     * @return {[type]}         [description]
     */
    Element.prototype.removeCSSClasses = function (classes) {
        this.style.removeClasses(classes);
        return this;
    };
    /**
     * [removeCSSProperties description]
     * @param  {[type]} properties [description]
     * @return {[type]}            [description]
     */
    Element.prototype.removeCSSProperties = function (properties) {
        this.style.removeProperties(properties);
        return this;
    };
    /**
     * Set the style properties for the HTML element.
     * @param {Object|null} style an JSON structure with attributes cssProperties (another object) and cssClasses
     * (array). It also can be null, in this case no custom styles will be applied to the the element.
     * @chainable
     */
    Element.prototype.setStyle = function (style) {
        style = style || {};
        if (this.style && this.html) {
            this.style.unapplyStyle();
        }
        if (style instanceof PMUI.util.Style) {
            this.style = style;
            style.belongsTo = this;
        } else if (typeof style === 'object') {
            style.belongsTo = this;
            this.style = new PMUI.util.Style(style);
        }
        this.applyStyle();
        return this;
    };
    /**
     * Set the x position coordinate for the HTML element.
     * @chainable
     * @param {Number} x
     * @chainable
     */
    Element.prototype.setX = function (x) {
        if (typeof x === 'number') {
            this.x = x;
        } else if (/^\d+(\.\d+)?px$/.test(x)) {
            this.x = parseInt(x, 10);
        } else {
            throw new Error('setX: x param is not a number');
        }
        this.style.addProperties({left: this.x});

        return this;
    };
    /**
     * Returns the x position coordinate for the HTML element
     * @return {Number}
     */
    Element.prototype.getX = function () {
        return this.x;
    };
    /**
     * Set the y position coordinate for the HTML element.
     * @param {Number} y
     * @chainable
     */
    Element.prototype.setY = function (y) {
        if (typeof y === 'number') {
            this.y = y;
        } else if (/^\d+(\.\d+)?px$/.test(y)) {
            this.y = parseInt(y, 10);
        } else {
            throw new Error('setY: y param is not a number');
        }
        this.style.addProperties({top: this.y});
        return this;
    };
    /**
     * Returns the y position coordinate for the HTML element
     * @return {Number}
     */
    Element.prototype.getY = function () {
        return this.y;
    };
    /**
     * Set the width for the HTML element
     * @param {Number|String} width height it can be a number or a string.
     In case of using a String you only can use 'auto' or ##px or ##% or ##em when ## is a number.
     * @chainable
     */
    Element.prototype.setWidth = function (width) {
        if (typeof width === 'number') {
            this.width = width;
        } else if (/^\d+(\.\d+)?px$/.test(width)) {
            this.width = parseInt(width, 10);
        } else if (/^\d+(\.\d+)?%$/.test(width)) {
            this.width = width;
        } else if (/^\d+(\.\d+)?em$/.test(width)) {
            this.width = width;
        } else if (width === 'auto') {
            this.width = width;
        } else {
            throw new Error('setWidth: width param is not a number');
        }
        this.style.addProperties({width: this.width});
        return this;
    };
    /**
     * Returns the HTML element's width
     * @return {Number}
     */
    Element.prototype.getWidth = function () {
        return this.width;
    };
    /**
     * Set the height for the HTML element
     * @param {Number|String} height it can be a number or a string.
     In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number.
     * @chainable
     */
    Element.prototype.setHeight = function (height) {
        if (typeof height === 'number') {
            this.height = height;
        } else if (/^\d+(\.\d+)?px$/.test(height)) {
            this.height = parseInt(height, 10);
        } else if (/^\d+(\.\d+)?%$/.test(height)) {
            this.height = height;
        } else if (/^\d+(\.\d+)?em$/.test(height)) {
            this.height = height;
        } else if (height === 'auto' || height === 'inherit') {
            this.height = height;
        } else {
            throw new Error('setHeight: height param is not a number');
        }
        this.style.addProperties({height: this.height});
        return this;
    };
    /**
     * Returns the HTML element's height.
     * @return {Number}
     */
    Element.prototype.getHeight = function () {
        return this.height;
    };
    /**
     * Set the position index om the z axis for the object's HTML element.
     * @param {Number|String} zOrder it can be a Number or a String.
     In case of using a String you only can use 'auto' or 'inherit'.
     * @chainable
     */
    Element.prototype.setZOrder = function (zOrder) {
        if (typeof zOrder === 'number') {
            this.zOrder = parseInt(zOrder, 10);
        } else if (zOrder === 'auto' || zOrder === 'inherit') {
            this.zOrder = zOrder;
        } else {
            throw new Error('setZOrder: zOrder param is not a number');
        }
        if (this.html) {
            this.style.addProperties({"z-index": this.zOrder});
        }
        return this;
    };
    /**
     * Returns the HTML element's zOrder.
     * @return {Number}
     */
    Element.prototype.getZOrder = function () {
        return this.zOrder;
    };
    /**
     * Set if the HTML element is visible or not.
     * @param {Boolean} visible
     * @chainable
     */
    Element.prototype.setVisible = function (visible) {
        visible = !!visible;
        this.visible = visible;
        if (this.html) {
            this.style.addProperties({"display": this.visible ? this.display : "none"});
        }

        return this;
    };
    /**
     * Returns a boolean value that indicates if the HTML element is visible or not.
     * @return {Boolean} [description]
     */
    Element.prototype.isVisible = function () {
        return this.visible;
    };
    /**
     * Set the HTML element's position.
     * @param {Object} position an object with x and y properties, both of them are Numbers
     * @chainable
     */
    Element.prototype.setPosition = function (position) {
        this.setX(position.x);
        this.setY(position.y);

        return this;
    };
    /**
     * Returns and object which contains the x and y positions from the HTML element.
     * @return {Object}
     */
    Element.prototype.getPosition = function () {
        return {
            x: this.getX(),
            y: this.getY()
        };
    };
    /**
     * Set the HTML element's dimension (width and height).
     * @param {Object} dimension
     * @chainable
     */
    Element.prototype.setDimension = function (dimension) {
        this.setWidth(dimension.width);
        this.setHeight(dimension.height);

        return this;
    };
    /**
     * Returns the HTML element's dimension.
     * @return {Object} and object with width and height properties.
     */
    Element.prototype.getDimension = function () {
        return {
            width: this.getWidth(),
            height: this.getHeight()
        };
    };
    /**
     * Applies the css classes and ptoperties to the element's html which the object is related.
     * @chainable
     */
    Element.prototype.applyStyle = function () {
        if (this.html) {
            this.style.applyStyle();

            this.style.addProperties({
                display: this.visible ? this.display : "none",
                position: this.positionMode,
                left: this.x,
                top: this.y,
                width: this.width,
                height: this.height,
                zIndex: this.zOrder
            });
        }
        return this;
    };
    /**
     * Creates the object's HTML element.
     * @return {HTMLElement} returns a HTML element
     */
    Element.prototype.createHTML = function () {
        var html;
        if (this.html) {
            return this.html;
        }
        html = PMUI.createHTMLElement(this.elementTag || "div");
        html.id = this.id;
        PMUI.linkToPMUIObject(html, this);
        this.html = html;
        this.actionHTML = html;
        this.applyStyle();
        if (this.eventsDefined) {
            this.defineEvents();
        }
        return this.html;
    };
    /**
     * Returns the object's HTML element, if it doesn't exist it's created then it is returned.
     * @return {HTMLElement}
     */
    Element.prototype.getHTML = function () {
        if (!this.html) {
            this.html = this.createHTML();
        }
        return this.html;
    };

    /**
     * Set the proportion of the html element.
     * @param {Number} p Proportion value.
     * @chainable
     */
    Element.prototype.setProportion = function (p) {
        this.proportion = p;
        return this;
    };

    /**
     * Creates and Instanciate an event.
     * @param {String} type  Event type.
     * @param {String} [alias] Registered Name.
     * @return {PMUI.event.Event}
     */
    Element.prototype.addEvent = function (type, alias) {
        var factory = new PMUI.event.EventFactory(),
            newEvent = factory.make(type),
            registerName = alias || PMUI.generateUniqueId();
        this.events[registerName] = newEvent;
        return newEvent;
    };

    /**
     * @abstract
     * Defines the events associated with the element.
     * @chainable
     * @abstract
     */
    Element.prototype.defineEvents = function () {
        var that = this;
        this.removeEvents();
        this.addEvent('contextmenu').listen(this.actionHTML, function (e) {
            if (typeof that.onBeforeContextMenu === 'function') {
                that.onBeforeContextMenu(that);
            }
            if (typeof that.onContextMenu === 'function') {
                that.onContextMenu(that);
            }
            if (that.menu) {
                e.stopPropagation();
                e.preventDefault();
                that.menu.show(e.pageX, e.pageY);
            }
        });
        this.eventsDefined = true;
        return this;
    };
    /**
     * Remove a specific event attached to the object.
     * @param  {String} identifier The id or alias used in the moment of event addition.
     * @chainable
     */
    Element.prototype.removeEvent = function (identifier) {
        if (this.events[identifier]) {
            this.events[identifier].unlisten();
            delete this.events[identifier];
        }
        return this;
    };
    /**
     * Removes events attached to the object.
     * @chainable
     */
    Element.prototype.removeEvents = function () {
        var k;
        for (k in this.events) {
            this.removeEvent(k);
        }
        ;
        return this;
    };
    /**
     * Calculates the width size of the container.
     *
     * @param {String} text
     * @param {String} font
     * @chainable
     */
    Element.prototype.calculateWidth = function (text, font) {
        //TODO Improve the div creation (maybe we can use a singleton for this)
        var f = font || '12px arial',
            $o = $(PMUI.createHTMLElement('div')), w;
        $o.text(text)
            .css({
                'position': 'absolute',
                'float': 'left',
                'white-space': 'nowrap',
                'visibility': 'hidden',
                'font': f
            })
            .appendTo($('body'));
        w = $o.width();

        $o.remove();

        return w;
    };
    /**
     * Sets the context menu for the component and the menu will be contained on the {@link PMUI.core.Element#menu menu} property.
     *
     * If the settings parameter is not a valid json the {@link PMUI.core.Element#menu menu} property will be an simple empty object.
     *
     * The example below shows the appropriate way to send a basic settings parameter.
     *
     *      component = new Class();
     *      component.setContextMenu({
     *          id: "id", //optional
     *          items: [
     *              {
     *                  text: "Option 1"
     *              },
     *              {
     *                  text: "Option 2"
     *              }
     *          ]
     *      });
     *
     * For more details about the context menu, the documentation is on the {@link PMUI.ui.Menu Menu} class.
     *
     * @param {Object} settings Configuration or config options for the context {@link PMUI.ui.Menu menu}.
     * @return {Object} this Represents the component of the class.
     */
    Element.prototype.setContextMenu = function (menuItems) {
        var contextMenu;
        this.removeContextMenu();
        if (menuItems !== null) {
            if (jQuery.isArray(menuItems)) {
                contextMenu = new PMUI.menu.Menu({items: menuItems});
            } else if (menuItems instanceof PMUI.menu.Menu) {
                contextMenu = menuItems;
            } else if (typeof menuItems === 'object') {
                contextMenu = new PMUI.menu.Menu(menuItems);
            }
            if (contextMenu) {
                this.menu = contextMenu.setTargetElement(this);
            }
        }
        return this;
    };
    /**
     * [removeContextMenu description]
     * @return {[type]} [description]
     */
    Element.prototype.removeContextMenu = function () {
        if (this.menu) {
            this.menu.setTargetElement(null);
            this.menu = null;
        }
        return this;
    };
    /**
     * Returns the elemnent's parent container.
     * @return {PMUI.core.Element|null} The parent container.
     */
    Element.prototype.getParent = function () {
        return this.parent;
    };
    /**
     * Shows the context menu for the element provided that the element contains an object that represent a
     * {@link PMUI.ui.Menu Menu}.
     *
     * If the {@link PMUI.core.Element#menu menu} property is an empty object the context menu will
     * never appear on the page.
     *
     * By default the context menu will appear after to do right click on the element.
     */
    Element.prototype.showContextMenu = function () {
        if (this.menu) {
            this.menu.show();
        }
        return this;
    };
    /**
     * Return true if the object's events were defined, otherwise it returns false.
     * @return {Boolean}
     */
    Element.prototype.eventsAreDefined = function () {
        return this.eventsDefined;
    };
    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Element;
    }

    PMUI.extendNamespace('PMUI.core.Element', Element);

}());