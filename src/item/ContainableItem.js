(function () {
    var ContainableItem = function (settings) {
        ContainableItem.superclass.call(this, settings);
        /**
         * The parent container for the object.
         * @type {PMUI.core.Container}
         * @internal
         */
        this.parent = null;
        /**
         * The data abstraction for the object.
         * @type {PMUI.data.DataField}
         */
        this.data = null;
        ContainableItem.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', ContainableItem);

    ContainableItem.prototype.init = function (settings) {
        var defaults = {
            parent: null,
            data: new PMUI.data.DataField()
        };

        $.extend(true, defaults, settings);

        this.setParent(defaults.parent)
            .setData(defaults.data);
    };

    ContainableItem.prototype.setData = function (data) {
        if (data instanceof PMUI.data.DataField) {
            this.data = data;
        }

        return this;
    };

    ContainableItem.prototype.getData = function () {
        return this.data;
    };

    ContainableItem.prototype.setParent = function (parent) {
        if (parent !== undefined) {
            if (parent instanceof PMUI.core.Container || parent === null) {
                this.parent = parent;
            } else {
                throw new Error("setParent(): The parameter must be instance of PMUI.core.Container or null.");
            }
        }

        return this;
    };

    PMUI.extendNamespace('PMUI.item.ContainableItem', ContainableItem);

    if (typeof exports !== 'undefined') {
        module.exports == ContainableItem;
    }
}());