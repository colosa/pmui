//***********************other class******************************************************//
describe('PMUI.draw.Oval', function () {
	var a;
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			a = new PMUI.draw.Oval();
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});