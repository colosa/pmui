//***********************other class******************************************************//
describe('PMUI.form.Form', function () {
    var form, f;
    describe('class behavior', function () {
        it('[US-3, b] should have a Form class to generate HTML forms', function () {
           form = new PMUI.form.Form();
            expect(form.type).toEqual("PMForm");
            expect(form.family).toEqual("PMForm");
            expect(form).toBeDefined();
            expect(form instanceof PMUI.form.Form).toBeTruthy();
        }); 
         it('[US-3,c] should be able to return all files that are in the form', function () {

            f = new PMUI.form.Form({
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "1",
                            value: "",
                            placeholder: "insert your name",
                            name: "name",
                            helper: "Introduce your name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            id: "2",
                            placeholder: "your lastname here asshole!",
                            name: "lastname",
                            helper: "Introduce your lastname"
                        }, {
                            pmType: "text",
                            label: "fono",
                            value: "",
                            id: "2",
                            placeholder: "your lastname here asshole!",
                            name: "lastname",
                            helper: "Introduce your lastname"
                        }]

                });

            for (var i=0;i<f.getFields().length-1;i++){
                expect((f.getFields()[i].id)).toEqual(i+1+""); 
            }

        }); 


        it('[US-3,d] should be able to define a title for the form', function () {

            f = new PMUI.form.Form({
                    title : "FORM"
                });
            expect(f.title == 'FORM').toBeTruthy();
            f.setTitle('FORM 2');
            expect(f.title == 'FORM 2').toBeTruthy();
        }); 

        it('[US-3,e] should be able to add buttons, but these must be added in the footer of Form', function () {
             f = new PMUI.form.Form({
                    footerItems: [  
                        {
                            pmType: 'button',
                            text: 'Say Hi!',
                            handler: function() {
                                alert("hi buddy!");
                            }
                        },
                        {
                            pmType: 'button',
                            text: "Close",
                            handler : function () {
                                w.close();
                            }
                        }
                    ],
                    footerAlign: 'center'
                });
            expect(f.footer.getItems()[0].text == 'Say Hi!').toBeTruthy();
            expect(f.footer.getItems()[1].text == 'Close').toBeTruthy();
        });
        it('[US-60,b] should clear and reset all fields', function () {
            var tm, combo, rad, txt, chck;
            f = new PMUI.form.Form({
                   
                    items: [
                         
                        {
                            pmType: "datetime",
                            label:'Fecha de nacimiento',
                        helper: 'This is calendar Gregorian',
                        value: "2014-03-01", 
                        datetime : true,
                        dateFormat: 'M dd yy',
                        minDate: -90,
                        maxDate: "+1y -1m -4d",
                        firstDay: 1,
                        
                        },
                        {
                            pmType: "dropdown",
                            label: "age",
                            options: [
                                {
                                    label: "",
                                    value: ""
                                },
                                {
                                    label: "from 0 to 7 years old",
                                    value: "0-7"
                                },
                                {
                                    label: "from 31 to 45 years old",
                                    value: "31-45"
                                },
                                {
                                    label: "older than 45",
                                    value: "46+"
                                }
                            ],
                            name: "age",
                            helper: "Select one of the options",
                            required: true,
                            controlsWidth: 100
                        },
                        {
                            pmType: "radio",
                            label: "Gender",
                            value: "m",
                            name: "gender",
                            required: true,
                            options: [
                                {
                                    label: "Male",
                                    value: "m"
                                },
                                {
                                    label: "Female",
                                    value: "f"
                                }
                            ]
                        }, 
                        {
                            pmType: "text",
                            label: "E-mail",
                            value: "",
                            name: "email",
                            helper: "you email here",
                            required: true,
                            
                        }, 
                        {
                            pmType: "checkbox",
                            label: "Things you like to do",
                            name: "hobbies",
                            helper: "Check up to 3 options",
                            options: [
                                {
                                    label: "Music",
                                    value: "music"
                                },
                                {
                                    label: "Programming",
                                    value: "programming"
                                },
                                {
                                    label: "Bike ridding",
                                    value: "bike"
                                }
                            ]
                        },
                        
                    ],
                    buttons: [
                        {
                            text: "Submit",
                            handler: function() {
                                f.submit();
                                console.log("submitting form...");
                            }
                        }, {
                            text: "Reset",
                            handler: function() {
                                f.reset();
                            }
                        }
                    ]
                });
            document.body.appendChild(f.getHTML());
            tm = f.getFields()[0];
            combo = f.getFields()[1];
            rad = f.getFields()[2];
            txt = f.getFields()[3];
            chck = f.getFields()[4];    
            tm.setValue("2014-02-10");
            combo.setValue("31-45");
            rad.setValue("f");
            txt.setValue("new value");
            chck.setValue("bike");
            expect(tm.getValue()).toEqual("2014-02-10T00:00:00-04:00");
            expect(combo.getValue()).toEqual("31-45");
            expect(rad.getValue()).toEqual("f");
            expect(txt.getValue()).toEqual("new value");
            expect(chck.getValue()).toEqual('bike');
            f.reset();
            expect(tm.getValue()).not.toEqual("2014-02-10T00:00:00-04:00");
            expect(combo.getValue()).not.toEqual("31-45");
            expect(rad.getValue()).not.toEqual("f");
            expect(txt.getValue()).not.toEqual("new value");
            expect(chck.getValue()).not.toEqual('bike');
            expect(tm.getValue()).toEqual("2014-03-01T00:00:00-04:00");
            expect(combo.getValue()).toEqual("");
            expect(rad.getValue()).toEqual("m");
            expect(txt.getValue()).toEqual("");
            expect(chck.getValue()).toEqual('');
        }); 
        
    }); 
    describe('method "setHeight"', function () {
        it('should set height of a Form object', function () {
            f = new PMUI.form.Form();
            expect(f.height === "auto").toBeTruthy();
            f.setHeight(400);
            expect(f.height === 400).toBeTruthy();
        });
    });
    describe('method "setWidth"', function () {
        it('should set width of a Form object', function () {
            f = new PMUI.form.Form();
            expect(f.width === 600).toBeTruthy();
            f.setWidth(400);
            expect(f.width === 400).toBeTruthy();
        });
    });
    describe('method "setFonSize"', function () {
        it('should set Font Size of a Form object', function () {
            f = new PMUI.form.Form();
            expect(f.fontSize === 12).toBeTruthy();
            f.setFontSize(18);
            expect(f.fontSize === 18).toBeTruthy();
        });
    });
    describe('method "setName"', function () {
        it('should set a Form object name', function () {
            f = new PMUI.form.Form();
            expect(f.name).toBeDefined();
            f.setName("form1");
            expect(f.name === "form1").toBeTruthy();
        });
    });
    describe('method "setEncType"', function () {
        it('should set the Enctype of a Form object', function () {
            f = new PMUI.form.Form();
            expect(f.encType === "application/x-www-form-urlencoded").toBeTruthy();
            f.setEncType("different_encType");
            expect(f.encType === "different_encType").toBeTruthy();
        });
    });
    describe('method "showHeader"', function () {
        it('should show the header of a Form object', function () {
            f = new PMUI.form.Form();
            expect(f.header).toBeNull();
            f.createHTML();
            f.showHeader();
            expect(f.header).not.toBeNull();
        });
    });
    describe('method "createHTML"', function () {
        it('[US-15,a]should create a new HTML element', function () {
            f = new PMUI.form.Form();
            var h = f.createHTML();
            expect(h).toBeDefined();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
        });
    }); 
});