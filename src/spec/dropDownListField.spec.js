//***********************other class******************************************************//
describe('PMUI.field.DropDownListField', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.field.DropDownListField();
		a.setOptions([{
		                           label: "Letters",
		                           options: [
		                               	{
		                                   label: "option A",
		                                   value: "A"
		                               	},
		                               	{
		                                   label: "option B",
		                                   value: "B"
		                                }
		                           ]
		                        },
			                    {
		                           label: "months",
		                           disabled: true,
		                           options: [
		                               {
		                                   label: "january",
		                                   value: "january"
		                               },
		                               {
		                                   label: "february",
		                                   value: "february"
		                               }
		                           ]
	                       		}
	                   		]);
		b = new PMUI.field.DropDownListField();
        document.body.appendChild(a.getHTML());
	});
	describe('class behavior', function () {
		it('[US-25,a] should add and edit a helper', function () {
			c = new PMUI.field.DropDownListField({helper: "new_helper"});
			expect(c.helper.message === "new_helper").toBeTruthy();
			c.showHelper();
			expect(c.helper.html).toBeDefined();
			c.setHelper("other_helper");
			expect(c.helper.message === "other_helper").toBeTruthy();
		});
		it('[US-25,b] should add and edit a Label', function () {
			c = new PMUI.field.DropDownListField({label: "new_label"});
			document.body.appendChild(c.getHTML());
			expect(c.label ==="new_label").toBeTruthy();
			c.setLabel("other_label");
			c.hideColon();
			expect(c.label ==="other_label").toBeTruthy();
			expect($(c.html).find('span[class="pmui-field-label"]').text() === "other_label").toBeTruthy();
		});
		
	});
	describe('method "setOptions"', function () {
		it('should set the options in the control dropdownList ', function () {
			b.setOptions([{label:"one", value:"1"}]);
			document.body.appendChild(b.getHTML());
			expect(b.controls[0].options[0].value === "1").toBeTruthy();
			expect(b.controls[0].options[0].label === "one").toBeTruthy();
			expect($(b.html).find('option[value="1"]').text() === "one").toBeTruthy();
		});
	});
	describe('method "getOptions"', function () {
		it('should get the options from the control dropdownList', function () {
			b.setOptions([
	                       		{
			                           label: "one",
			                           value: 1
	                       		},
	                       		{
			                           label: "two",
			                           value: 2,
			                           disabled: true
			                    },
			                    {
			                           label: "three",
			                           value: 3,
			                           selected: true
			                    }, 
			                    {
		                           label: "Letters",
		                           options: [
		                               	{
		                                   value: "A"
		                               	},
		                               	{
		                                   value: "B"
		                                }
		                           ]
		                        },
			                    {
		                           label: "months",
		                           disabled: true,
		                           options: [
		                               {
		                                   value: "january"
		                               },
		                               {
		                                   value: "february"
		                               }
		                           ]
	                       		}
	                   		]);
			document.body.appendChild(b.getHTML());
			expect(b.getOptions(false).length === 7).toBeTruthy();
			expect(b.getOptions(true).length === 5).toBeTruthy();
			b.getOptions(true);
			expect(b.controls[0].options[3].options[1].value === "B").toBeTruthy();
			expect(b.controls[0].options[3].options[1].label === "B").toBeTruthy();
			expect($(b.html).find('option[value="B"]').text() === "B").toBeTruthy();
			expect(b.controls[0].options[4].options[0].disabled === false).toBeTruthy();
			expect(b.controls[0].options[4].options[0].value === "january").toBeTruthy();
			expect(b.controls[0].options[4].options[0].label === "january").toBeTruthy();
			expect($(b.html).find('option[value="january"]').text() === "january").toBeTruthy();

		});
	});
	describe('method "clearOptions"', function () {
		it('should clear all the control options', function () {
			a.setOptions([{label:"one", value:"1"}]);
			expect(a.getOptions().length === 1).toBeTruthy();
			expect($(a.html).find('option[value="1"]').text() === "one").toBeTruthy();
			a.clearOptions();
			expect(a.getOptions().length === 0).toBeTruthy();
			expect($(a.html).find('option').length === 0).toBeTruthy();
		});
	});
	describe('method "disableOption"', function () {
		it('should disable a control option' , function () {
			expect(a.controls[0].options[0].options[0].disabled).toBeFalsy();
			a.disableOption("A", "Letters");
			expect(a.controls[0].options[0].options[0].disabled).toBeTruthy();
			expect($(a.html).find('option[disabled="disabled"]').text() === "option A").toBeTruthy();
		});
	});
	describe('method "enableOption"', function () {
		it('should enable a control option', function () {
			expect(a.controls[0].options[0].options[0].disabled).toBeFalsy();
			a.disableOption("A", "Letters");
			expect(a.controls[0].options[0].options[0].disabled).toBeTruthy();
			expect($(a.html).find('option[disabled="disabled"]').text() === "option A").toBeTruthy();
			a.enableOption("A", "Letters");
			expect(a.controls[0].options[0].options[0].disabled).toBeFalsy();
			expect($(a.html).find('option[disabled="disabled"]').length === 1).toBeFalsy();
		});
	});
	describe('method "removeOption"', function () {
		it('should remove a control option', function () {
			var r = a.getOptions().length;
			expect(a.getOptions().length === r).toBeTruthy();
			expect($(a.html).find('option[value="B"]').length === 1).toBeTruthy();
			expect($(a.html).find('option[value="january"]').length === 1).toBeTruthy();
			a.removeOption("B", "Letters");
			expect(a.getOptions().length === r-1).toBeTruthy();
			expect($(a.html).find('option[value="B"]').length === 0).toBeTruthy();
			a.removeOption("january", "months");
			expect(a.getOptions().length === r-2).toBeTruthy();
			expect($(a.html).find('option[value="january"]').length === 0).toBeTruthy();
		});
	});
	describe('method "addOptionGroup"', function () {
		it('should add an option group for a dropdownList control', function () {
			a.addOptionGroup({label: "Days"});
			expect(a.controls[0].options[2].label === "Days").toBeTruthy();
			expect($(a.html).find('optgroup[label="Days"]').length === 1).toBeTruthy();
			expect(a.controls[0].options[2].options.length === 0).toBeTruthy();
		});
	});
	describe('method "addOption"', function () {
		it('should add an option and a group', function () {
			a.addOption({label:"Monday", value: "mo"}, "Days");
			expect(a.controls[0].options[2].options.length === 1).toBeTruthy();
			expect(a.controls[0].options[2].options[0].label === "Monday").toBeTruthy();
			expect($(a.html).find('optgroup[label="Days"]').find('option[value="mo"]').text() === "Monday").toBeTruthy();
		});
	});
	describe('method "getSelectedLabel"', function () {
		it('should get the selected label', function () {
			a.setValue("A");
			expect(a.getSelectedLabel() === "option A").toBeTruthy();
			a.setValue("B");
			expect(a.getSelectedLabel() === "option B").toBeTruthy();
		});
	});
	describe('method "setControls"', function () {
		it('should set the controls for the field', function () {
			expect(a.setControls()).toBeDefined();
			expect(a.setControls()).not.toBeNull();
		});
	});
	describe('method "disable"', function () {
		it('should disable the control of the field already created', function () {
			a.disable();
			expect(a.controls[0].disabled).toBeTruthy();
			expect($(a.dom.controlContainer).find('select[disabled]').length === 1).toBeTruthy();
		});
	});
	describe('method "enable"', function () {
		it('should enable the control of the field already created', function () {
			a.disable();
			expect(a.controls[0].disabled).toBeTruthy();
			expect($(a.dom.controlContainer).find('select[disabled]').length === 1).toBeTruthy();
			a.enable();
			expect(a.controls[0].disabled).toBeFalsy();
			expect($(a.dom.controlContainer).find('select[disabled]').length === 0).toBeTruthy();
		});
	});
	describe('method "setListWidth"', function () {
		it('should set the width of the list control', function () {
			expect(a.controls[0].width === "auto").toBeTruthy();
			a.setListWidth(75);
			expect(a.controls[0].width === 75).toBeTruthy();
		});
	});
	describe('method "setLabel"', function () {
		it('should set the label of the dropdownlistfield', function () {
			expect(a.label === "").toBeTruthy();
			a.setLabel("new label");
			a.hideColon();
			expect(a.label === "new label").toBeTruthy();
			expect($(a.html).find('span[class="pmui-field-label"]').text() === "new label").toBeTruthy();
		});
	});
});