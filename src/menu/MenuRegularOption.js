(function () {
    var MenuRegularOption = function (settings) {
        MenuRegularOption.superclass.call(this, settings);
        this.childMenu = null;
        MenuRegularOption.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.menu.MenuOption', MenuRegularOption);

    MenuRegularOption.prototype.init = function (settings) {
        var defaults = {
            items: []
        };

        jQuery.extend(true, defaults, settings);

        this.childMenu = new PMUI.menu.Menu({
            positionMode: 'absolute',
            parent: this
        });

        this.setItems(defaults.items);
    };

    MenuRegularOption.prototype.setItems = function (items) {
        this.childMenu.setItems(items);
        if (this.childMenu.getItems().length) {
            this.style.addClasses(['pmui-father']);
        }
        return this;
    };

    MenuRegularOption.prototype.getItems = function () {
        return this.childMenu.getItems();
    };

    MenuRegularOption.prototype.onClickHandler = function () {
        var that = this;
        return function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (!that.disabled) {
                if (typeof that.onClick === 'function') {
                    that.onClick(that);
                }
                if (that.hideOnClick && that.childMenu.getItems().length === 0) {
                    that.parent.hide();
                }
            }
        };
    };

    MenuRegularOption.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        MenuRegularOption.superclass.prototype.createHTML.call(this);
        this.html.appendChild(this.childMenu.getHTML());
        this.setItems(this.getItems());
        return this.html;
    };

    PMUI.extendNamespace('PMUI.menu.MenuRegularOption', MenuRegularOption);
}());