(function () {
    /**
     * @class PMUI.control.TextAreaControl
     * Class to handle a HTML TextArea control.
     * @extends PMUI.control.TextControl
     *
     * Usage example:
     *
     *      @example
     *          var myTextArea;
     *          $(function() {
     *              myTextArea = new PMUI.control.TextAreaControl({
     *                  name: "some_text",
     *                  value: "John Doe",
     *                  maxLength: 12,
     *                  readonly: false,
     *                  width: 300,
     *                  height: 200,
     *                  placeholder: "insert some text",
     *                  disabled: false,
     *                  onChange: function(currentValue, previousValue) {
     *                      if(previousValue !== "") {
     *                          alert("the text is not \"" + previousValue + "\" anymore.\nNow it's \"" 
     *                              + currentValue + "\"");
     *                      } else {
     *                          alert("Now your name is " + currentValue);
     *                      }
     *                  }
     *              });
     *      
     *              document.body.appendChild(myTextArea.getHTML());
     *              
     *              myTextArea.defineEvents();
     *          });
     *
     * @constructor
     * Creates a new instance
     * @param {Object} [settings] A JSON object that can contain the properties specified in the
     * config options section.
     *
     * @cfg {Boolean} [readonly=false] If the control will be readonly.
     */
    var TextAreaControl = function (settings) {
        TextAreaControl.superclass.call(this, settings);
        /**
         * @property {Boolean} readonly A Boolean that specifies if the control is enabled for read only.
         * @readonly
         */
        this.readonly = null;
        /**
         * @property {String} [elementTag='input'] The tag for the HTML element to be created.
         * @private
         */
        this.elementTag = 'textarea';
        TextAreaControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.TextControl', TextAreaControl);

    TextAreaControl.prototype.type = "TextAreaControl";
    /**
     * Initializes the object.
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    TextAreaControl.prototype.init = function (settings) {
        var defaults = {
            readonly: false
        };

        jQuery.extend(true, defaults, settings);

        this.setReadOnly(defaults.readonly);
    };
    /**
     * Creates the HTML element for the object
     * @return {HTMLElement}
     */
    TextAreaControl.prototype.createHTML = function () {
        TextAreaControl.superclass.prototype.createHTML.call(this);

        this.setReadOnly(this.readonly);

        return this.html;
    };
    /**
     * Sets the maximun character number to be accepted in the control.
     * @param {Number} maxLength The number must be an integer.
     * If the value is minor or equal to 0 then the maxLength property is set to the default (524288).
     * @chainable>
     */
    TextAreaControl.prototype.setMaxLength = function (maxLength) {
        var that = this;

        if (typeof maxLength === 'number' && maxLength % 1 === 0) {
            this.maxLength = maxLength;
            if (this.html) {
                this.html.maxLength = maxLength > 0 ? maxLength : 524288;
            }
        } else {
            throw new Error("method setMaxLength() only accepts integer values.");
        }

        return this;
    };
    PMUI.extendNamespace('PMUI.control.TextAreaControl', TextAreaControl);

    if (typeof exports !== "undefined") {
        module.exports = TextAreaControl;
    }
}());