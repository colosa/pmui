(function () {
    var NumberField = function (settings) {
        NumberField.superclass.call(this, settings);
        /**
         * @property {Boolean} [trimOnBlur=true] If the field's value must be trimmed every time it loses focus.
         */
        this.trimOnBlur = null;

        NumberField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', NumberField);

    NumberField.prototype.type = "NumberField";

    NumberField.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 0,
            trimOnBlur: true,
            readOnly: false,
            mask: "",
            value: "",
            optionDecimal: null
        };

        $.extend(true, defaults, settings);

        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setTrimOnBlur(defaults.trimOnBlur)
            .setReadOnly(defaults.readOnly)
            .setOptionDecimal(defaults.optionDecimal)
            .setMask(defaults.mask);

    };
    /**
     * Sets the value for the field.
     * @param {String} value
     * @chainable
     */
    NumberField.prototype.setValue = function (value) {
        if (this.trimOnBlur) {
            value = jQuery.trim(value);
        }
        NumberField.superclass.prototype.setValue.call(this, value);
        //this.controls[0].setValue(value);
        return this;
    };

    /**
     * Sets the value for the field.
     * @param {String} value
     * @chainable
     */
    NumberField.prototype.setOptionDecimal = function (value) {
        if (this.trimOnBlur) {
            value = jQuery.trim(value);
        }
        this.controls[0].setOptionDecimal(value);
        return this;
    };

    /**
     * Sets the value for the field.
     * @param {String} value
     * @chainable
     */
    NumberField.prototype.setMask = function (value) {
        if (this.trimOnBlur) {
            value = jQuery.trim(value);
        }
        this.controls[0].setMask(value);
        return this;
    };

    /**
     * Sets the placeholder for the control. Note that this feature is only supported
     * by browsers which support the "placeholder" for input elements.
     * @param {String} placeholder
     * @chainable
     */
    NumberField.prototype.setReadOnly = function (readonly) {
        this.controls[0].setReadOnly(readonly);
        return this;
    };
    /**
     * [setPlaceholder description]
     * @param {[type]} placeholder [description]
     */
    NumberField.prototype.setPlaceholder = function (placeholder) {
        this.controls[0].setPlaceholder(placeholder);
        return this;
    };
    /**
     * [getPlaceholder description]
     * @return {[type]} [description]
     */
    NumberField.prototype.getPlaceholder = function () {
        return this.controls[0].getPlaceholder();
    };
    /**
     * [setMaxLength description]
     * @param {[type]} maxLength [description]
     */
    NumberField.prototype.setMaxLength = function (maxLength) {
        this.controls[0].setMaxLength(maxLength);
        return this;
    };
    /**
     * [getMaxLength description]
     * @return {[type]} [description]
     */
    NumberField.prototype.getMaxLength = function () {
        return this.controls[0].getMaxLength();
    };
    /**
     * [isReadOnly description]
     * @return {Boolean} [description]
     */
    NumberField.prototype.isReadOnly = function () {
        return this.controls[0].isReadOnly();
    };
    /**
     * Switches on/off the value trimming for the field's value when it loses focus.
     * @param {Boolean} trimOnBlur Send true for switch on or false to switch off.
     * @chainable
     */
    NumberField.prototype.setTrimOnBlur = function (trimOnBlur) {
        this.trimOnBlur = !!trimOnBlur;
        return this;
    };
    /**
     * Returns a boolean value that indicates if the trimming function is enabled/disabled
     * @return {Boolean}
     */
    NumberField.prototype.getTrimOnBlur = function () {
        return this.trimOnBlur;
    };
    /**
     * Sets the control for the NumberField
     * @chainable
     * @private
     */
    NumberField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }
        this.controls.push(new PMUI.control.NumberControl());

        return this;
    };
    /**
     * Sets the control for the NumberField
     * @chainable
     * @private
     */
    NumberField.prototype.getValue = function (control) {

        return this.controls[0].getValue(control);
    };
    /**
     * Establish the handler function for the internal onChange event
     * @return {Function}
     * @private
     */
    NumberField.prototype.onChangeHandler = function () {
        var that = this;

        return function () {
            var previousValue = that.value,
                value,
                i;

            if (that.trimOnBlur) {
                value = that.controls[0].getValue();
                value = jQuery.trim(value);
                that.controls[0].setValue(value);
            }
            that.updateValueFromControls();
            if (that.validAtChange) {
                that.isValid();
            }
            if (typeof that.onChange === 'function') {
                that.onChange(that.getValue(), previousValue);
            }
            if (that.form) {
                (that.form.onChangeHandler())(that, that.getValue(), previousValue);
            }
            if (previousValue !== value) {
                for (i = 0; i < that.dependentFields.getSize(); i += 1) {
                    that.dependentFields.get(i).dependencyHandler(that.dependentFields.get(i).dependencyFields.asArray().slice(0));
                }
            }
        };
    };

    PMUI.extendNamespace('PMUI.field.NumberField', NumberField);

    if (typeof exports !== "undefined") {
        module.exports = NumberField;
    }
}());