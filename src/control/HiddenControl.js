(function () {
    /**
     * @class PMUI.control.HiddenControl
     * The HTML input tag with the type attribute set to "hidden".
     * @extends PMUI.control.HTMLControl
     *
     * Usage example:
     *
     *     var hiddenControl = new PMUI.control.HiddenControl();
     *
     *     document.body.appendChild(hiddenControl.getHTML());
     *
     * @constructor
     * Creates a new instance of the HiddenControl class.
     * @param {Object} [settings=null] A JSON object with the config options.
     */
    var HiddenControl = function (settings) {
        HiddenControl.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', HiddenControl);

    HiddenControl.prototype.type = "HiddenControl";

    /**
     * Creates the HTML element for the control.
     * @return {HTMLElement}
     */
    HiddenControl.prototype.createHTML = function () {
        HiddenControl.superclass.prototype.createHTML.call(this);
        this.html.type = 'hidden';

        return this.html;
    };

    PMUI.extendNamespace('PMUI.control.HiddenControl', HiddenControl);

    if (typeof exports !== "undefined") {
        module.exports = HiddenControl;
    }
}());