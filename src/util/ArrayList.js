(function () {
    /**
     * @class PMUI.util.ArrayList
     * Construct a List similar to Java's ArrayList that encapsulates methods for
     * making a list that supports operations like get, insert and others.
     *
     *      some examples:
     *      var item,
     *          arrayList = new ArrayList();
     *      arrayList.getSize()                 // 0
     *      arrayList.insert({                  // insert an object
     *          id: 100,
     *          width: 100,
     *          height: 100
     *      });
     *      arrayList.getSize();                // 1
     *      arrayList.asArray();                // [{id : 100, ...}]
     *      item = arrayList.find('id', 100);   // finds the first element with an id that equals 100
     *      arrayList.remove(item);             // remove item from the arrayList
     *      arrayList.getSize();                // 0
     *      arrayList.isEmpty();                // true because the arrayList has no elements
     *
     * @constructor Returns an instance of the class ArrayList
     */
    var ArrayList = function () {
        /**
         * The elements of the arrayList
         * @property {Array}
         * @private
         */
        var elements = [],
            /**
             * The size of the array
             * @property {number} [size=0]
             * @private
             */
            size = 0,
            index,
            i;
        return {

            /**
             * The ID of this ArrayList is generated using the function Math.random
             * @property {number} id
             */
            id: Math.random(),
            /**
             * Gets an element in the specified index or undefined if the index
             * is not present in the array
             * @param {number} index
             * @returns {Object / undefined}
             */
            get: function (index) {
                return elements[index];
            },
            /**
             * Inserts an element at the end of the list
             * @param {Object} item
             * @chainable
             */
            insert: function (item) {
                elements[size] = item;
                size += 1;
                return this;
            },
            /**
             * Inserts an element in a specific position
             * @param {Object} item
             * @chainable
             */
            insertAt: function (item, index) {
                elements.splice(index, 0, item);
                size = elements.length;
                return this;
            },
            /**
             * Removes an item from the list
             * @param {Object} item
             * @return {boolean}
             */
            remove: function (item) {
                index = this.indexOf(item);
                if (index === -1) {
                    return false;
                }
                //swap(elements[index], elements[size-1]);
                size -= 1;
                elements.splice(index, 1);
                return true;
            },
            /**
             * Gets the length of the list
             * @return {number}
             */
            getSize: function () {
                return size;
            },
            /**
             * Returns true if the list is empty
             * @returns {boolean}
             */
            isEmpty: function () {
                return size === 0;
            },
            /**
             * Returns the first occurrence of an element, if the element is not
             * contained in the list then returns -1
             * @param {Object} item
             * @return {number}
             */
            indexOf: function (item) {
                for (i = 0; i < size; i += 1) {
                    if (item === elements[i]) {
                        return i;
                    }
                }
                return -1;
            },
            /**
             * Returns the the first object of the list that has the
             * specified attribute with the specified value
             * if the object is not found it returns undefined
             * @param {string} attribute
             * @param {string} value
             * @return {Object / undefined}
             */
            find: function (attribute, value) {
                var i,
                    current;

                for (i = 0; i < elements.length; i += 1) {
                    current = elements[i];
                    if (current[attribute] === value) {
                        return current;
                    }
                }
                return undefined;
            },

            /**
             * Returns true if the list contains the item and false otherwise
             * @param {Object} item
             * @return {boolean}
             */
            contains: function (item) {
                if (this.indexOf(item) !== -1) {
                    return true;
                }
                return false;
            },
            /**
             * Sorts the list using compFunction if possible, if no compFunction
             * is passed as an parameter then a default sorting method will be used. This default method will sort in
             * ascending order.
             * @param {Function} [compFunction] The criteria function used to find out the position for the elements in
             * the array list. This function will receive two parameters, each one will be an element from the array
             * list, the function will compare those elements and it must return:
             *
             * - 1, if the first element must be before the second element.
             * - -1, if the second element must be before the first element.
             * - 0, if the current situation doesn't met any of the two situations above. In this case both elements
             * can be evaluated as they had the same value. For example, in an array list of numbers, when you are
             * trying to apply a lineal sorting (ascending/descending) in a array list of numbers, if the array sorting
             * function finds two elements with the value 3 they should be evaluated returning 0, since both values are
             * the same.
             *
             * IMPORTANT NOTE: for a correct performance the sent parameter must return at least two of the values
             * listed above, if it doesn't the function can produce an infinite loop and thus an error.
             * @return {boolean}
             */
//            sort : function (compFunction) {
//                var compFunction = compFunction || function(a, b) {
//                        if(a < b) {
//                            return 1;
//                        } else if(a > b) {
//                            return -1;
//                        } else {
//                            return 0;
//                        }
//                    }, swap = function (items, firstIndex, secondIndex){
//                        var temp = items[firstIndex];
//                        items[firstIndex] = items[secondIndex];
//                        items[secondIndex] = temp;
//                    }, partition = function(items, left, right) {
//                        var pivot = items[Math.floor((right + left) / 2)],
//                            i = left,
//                            j = right;
//                        while (i <= j) {
//                            while (compFunction(items[i], pivot) > 0) {
//                                i++;
//                            }
//                            while (compFunction(items[j], pivot) < 0) {
//                                j--;
//                            }
//                            if (i <= j) {
//                                swap(items, i, j);
//                                i++;
//                                j--;
//                            }
//                        }
//                        return i;
//                    }, quickSort = function (items, left, right) {
//                        var index;
//                        if (items.length > 1) {
//                            index = partition(items, left, right);
//                            if (left < index - 1) {
//                                quickSort(items, left, index - 1);
//                            }
//                            if (index < right) {
//                                quickSort(items, index, right);
//                            }
//                        }
//                        return items;
//                    };
//
//                return quickSort(elements, 0, size - 1);
//            },

            sort: function (compFunction) {
                var returnValue = false;

                if (compFunction) {
                    elements.sort(compFunction);
                    returnValue = true;
                }
                return returnValue;
            },
            /**
             * Bubble sort function in array
             * @param {*} compFunction 
             * @return {Array}
             */
            bubbleSort: function (compFunction) {
                var inputArr = elements,
                    i,
                    tmp,
                    len = inputArr.length,
                    swapped;
                do {
                    swapped = false;
                    for (i = 0; i < len; i++) {
                        if (inputArr[i] && inputArr[i + 1] && compFunction(inputArr[i], inputArr[i + 1])) {
                            tmp = inputArr[i];
                            inputArr[i] = inputArr[i + 1];
                            inputArr[i + 1] = tmp;
                            swapped = true;
                        }
                    }
                } while (swapped);
                elements = inputArr;
                return elements;
            },
            /**
             * Returns the list as an array
             * @return {Array}
             */
            asArray: function () {
                return elements.slice(0);
            },
            /**
             * Swaps the position of two elements
             * @chainable
             */
            swap: function (index1, index2) {
                var aux;

                if (index1 < size && index1 >= 0 && index2 < size && index2 >= 0) {
                    aux = elements[index1];
                    elements[index1] = elements[index2];
                    elements[index2] = aux;
                }
                return this;
            },
            /**
             * Returns the first element of the list
             * @return {Object}
             */
            getFirst: function () {
                return elements[0];
            },
            /**
             * Returns the last element of the list
             * @return {Object}
             */
            getLast: function () {
                return elements[size - 1];
            },

            /**
             * Returns the last element of the list and deletes it from the list
             * @return {Object}
             */
            popLast: function () {
                var lastElement;
                size -= 1;
                lastElement = elements[size];
                elements.splice(size, 1);
                return lastElement;
            },
            /**
             * Returns an array with the objects that determine the minimum size
             * the container should have
             * The array values are in this order TOP, RIGHT, BOTTOM AND LEFT
             * @return {Array}
             */
            getDimensionLimit: function () {
                var result = [100000, -1, -1, 100000],
                    objects = [undefined, undefined, undefined, undefined];
                //number of pixels we want the inner shapes to be
                //apart from the border

                for (i = 0; i < size; i += 1) {
                    if (result[0] > elements[i].y) {
                        result[0] = elements[i].y;
                        objects[0] = elements[i];

                    }
                    if (result[1] < elements[i].x + elements[i].width) {
                        result[1] = elements[i].x + elements[i].width;
                        objects[1] = elements[i];
                    }
                    if (result[2] < elements[i].y + elements[i].height) {
                        result[2] = elements[i].y + elements[i].height;
                        objects[2] = elements[i];
                    }
                    if (result[3] > elements[i].x) {
                        result[3] = elements[i].x;
                        objects[3] = elements[i];
                    }
                }
                return result;
            },
            /**
             * Clears the content of the arrayList
             * @chainable
             */
            clear: function () {
                if (size !== 0) {
                    elements = [];
                    size = 0;
                }
                return this;
            },
            /**
             * Sets the elements for the object.
             * @param {Array|null} items Array with the items to set.
             * @chainable
             */
            set: function (items) {
                if (!(items === null || jQuery.isArray(items))) {
                    throw new Error("set(): The parameter must be an array or null.");
                }
                elements = (items && items.slice(0)) || [];
                size = elements.length;
                return this;
            }
        };
    };

    PMUI.extendNamespace('PMUI.util.ArrayList', ArrayList);

    if (typeof exports !== "undefined") {
        module.exports = ArrayList;
    }

}());
