//***********************other class******************************************************//
describe('PMUI.form.ValidatorFactory', function () {
    var v;
    describe('method "init"', function () {
    	it('Should create a new ValidatorFactory object with all default properties', function () {
        v = new PMUI.form.ValidatorFactory();
        expect(v.family === "Factory").toBeTruthy();
        expect(v.type === "Factory").toBeTruthy();
        expect(v.defaultProduct === "textLength").toBeTruthy();
    });
    });
    
});