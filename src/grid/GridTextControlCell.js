(function () {
    var GridTextControlCell = function (settings) {
        GridTextControlCell.superclass.call(this, settings);
        GridTextControlCell.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlCell', GridTextControlCell);

    GridTextControlCell.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 0,
            trimOnBlur: true,
            readOnly: false
        };

        jQuery.extend(true, defaults, settings);

        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setTrimOnBlur(defaults.trimOnBlur)
            .setReadOnly(defaults.readOnly);
    };

    GridTextControlCell.prototype.initControls = function () {
        this.controls[0] = new PMUI.control.TextControl();
        return this;
    };

    GridTextControlCell.prototype.setValuesToControls = function (values) {
        return this;
    };

    GridTextControlCell.prototype.setContent = function (content) {
        return this.setValuesToControls(content);
    };
    /**
     * Sets the placeholder for the control. Note that this feature is only supported
     * by browsers which support the "placeholder" for input elements.
     * @param {String} placeholder
     * @chainable
     */
    GridTextControlCell.prototype.setReadOnly = function (readonly) {
        this.controls[0].setReadOnly(readonly);
        return this;
    };
    /**
     * [setPlaceholder description]
     * @param {[type]} placeholder [description]
     */
    GridTextControlCell.prototype.setPlaceholder = function (placeholder) {
        this.controls[0].setPlaceholder(placeholder);
        return this;
    };
    /**
     * [getPlaceholder description]
     * @return {[type]} [description]
     */
    GridTextControlCell.prototype.getPlaceholder = function () {
        return this.controls[0].getPlaceholder();
    };
    /**
     * [setMaxLength description]
     * @param {[type]} maxLength [description]
     */
    GridTextControlCell.prototype.setMaxLength = function (maxLength) {
        this.controls[0].setMaxLength(maxLength);
        return this;
    };
    /**
     * [getMaxLength description]
     * @return {[type]} [description]
     */
    GridTextControlCell.prototype.getMaxLength = function () {
        return this.controls[0].getMaxLength();
    };
    /**
     * [isReadOnly description]
     * @return {Boolean} [description]
     */
    GridTextControlCell.prototype.isReadOnly = function () {
        return this.controls[0].isReadOnly();
    };
    /**
     * Switches on/off the value trimming for the field's value when it loses focus.
     * @param {Boolean} trimOnBlur Send true for switch on or false to switch off.
     * @chainable
     */
    GridTextControlCell.prototype.setTrimOnBlur = function (trimOnBlur) {
        this.trimOnBlur = !!trimOnBlur;
        return this;
    };

    PMUI.extendNamespace('PMUI.grid.GridTextControlCell', GridTextControlCell);

    if (typeof exports !== 'undefined') {
        module.exports = GridTextControlCell;
    }
}());