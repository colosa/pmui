//***********************other class******************************************************//
describe('PMUI.ui.TextLabel', function () {
	var a, b, w;
	beforeEach(function () {
		w = new PMUI.ui.Window({
                    title: "FORM", 
                    width: "auto", 
                    height: "auto", 
                    footerHeight: 'auto',
                    bodyHeight: "auto",
                    modal:true,  
                    footerItems: [  
                        {
                            pmType: 'button',
                            text: 'Say Hi!',
                            handler: function() {
                                alert("hi buddy!");
                            }
                        },
                        {
                            pmType: 'label',
                            text: "and then"
                        },
                        {
                            pmType: 'button',
                            text: "Close",
                            handler : function () {
                                w.close();
                            }
                        }
                    ],
                    footerAlign: 'center'
                });
        w.open(); 
        w.defineEvents();
        a = w.footer.getItem(1);
	});
	afterEach(function () {

	});
	describe('class behavior', function () {
		it('[US-58,f] should set a label between buttons', function () {
			expect(w.footer.getItems().length === 3).toBeTruthy();
			expect(w.footer.getItem(1).type === "TextLabel").toBeTruthy();
			expect(w.footer.getItem(1).text === "and then").toBeTruthy();
			expect($(a.html).text() === "and then").toBeTruthy();
			expect(function () {a.setText(234)}).toThrow();
			expect(function () {a.setText(new PMUI.ui.Button())}).toThrow();
			expect(function () {a.setText("separator")}).not.toThrow();
			expect(w.footer.getItem(1).text === "separator").toBeTruthy();
			expect($(a.html).text() === "separator").toBeTruthy();
		});
		it('[US-58,g] should set the text type, it must be a plain or a HTML', function () {
			expect(function () {a.setTextMode("any string")}).toThrow();
			expect(function () {a.setTextMode(234)}).toThrow();
			expect(function () {a.setTextMode({})}).toThrow();
			expect(function () {a.setTextMode("plain")}).not.toThrow();
			expect(function () {a.setTextMode("html")}).not.toThrow();
			a.setElementTag('span');
			expect(function () {a.setText("separator")}).not.toThrow();
			expect(w.footer.getItem(1).text === "separator").toBeTruthy();
			expect($(a.html).text() === "separator").toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
        it('should create a new html element', function () {
            a = new PMUI.ui.TextLabel();
            var h = a.createHTML();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
});