//***********************other class******************************************************//
describe('PMUI.behavior.DataControlDropBehavior', function () {
	var g2;
	beforeEach(function () {
		g2 = new PMUI.grid.GridPanel({
                    //behavior: 'drag',
                    pageSize: 0,
                    columns: [
                        {
                            title: 'lastName',
                            columnData: 'lastName',
                            sortable: true
                        }
                    ],
                    items: [
                        {
                            name: "Dave",
                            lastName: 'Grohl'
                        }, {
                            name: "Charles",
                            lastName: "Manson"
                        }, {
                            name: "Gonzalo",
                            lastName: "Gonzales"
                        }, {
                            name: "Enrique",
                            lastName: "Enriquez"
                        }
                    ]
                });
        g2.defineEvents();
        g2.setBehavior("dragdropsort");
	});

	afterEach(function () {
        g2 = null;
	});
	describe('class behavior', function () {
		it('[US-42,c]should have the scope property in the drop behavior', function() {
		    expect(g2.behavior === "dragdropsort");
            expect(g2.behaviorObject.scope).toBeDefined();
            expect(g2.behaviorObject.scope === "pmui-containeritem-behavior").toBeTruthy();
		});
	});
});