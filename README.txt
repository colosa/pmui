pmUI
====

UI Library based on jQueryUI written for PM Michelangelo
--------------------------------------------------------


Libraries Listed
----------------

- jQuery UI 1.10.3



Rake Building Commands

rake -T                # Show all tasks available
rake                   # Default Task - Build Library
rake build[version]    # Build jCore library
rake docs              # Compile Documentation with JSDoc 3
rake jasmine           # Run Jasmine BDD tests
rake libraries         # Generate JS libraries
rake package           # Create a package for distribution
rake skeleton          # Create skeleton pages
rake sniffer           # Run JSHint Javascript sniffer
rake version[version]  # Set the library's version

Defining Developement Environtment

LINUX
1. Install Ruby and Ruby/Gems
2. Download and compile Node.js environtment from http://nodejs.org
3. Install Jasmine node
4. Install Closure-compiler node
5. Install JSHint node
6. Install JSDuck Documentator
7. Install rubyzip compressor

WINDOWS
1. Install Ruby and Ruby/Gems
    Download RubyInstaller and Developer Kit from http://rubyinstaller.org/downloads/
    Follow Step to install Developer Kit https://github.com/oneclick/rubyinstaller/wiki/development-kit
2. Download and compile Node.js environtment from http://nodejs.org
3. Install Jasmine node Ej. npm install jasmine-node -g
4. Install Closure-compiler node Ej. gem install closure-compiler
5. Install JSHint node Ej. npm install jshint
6. Install JSDuck Documentator Ej. gem install jsduck
7. Install rubyzip compressor Ej. gem install rubyzip
8. Install Rake Ej. gem install rake