(function () {
    /**
     * @class PMUI.form.Field
     * Abstract class that encapsulates the field behavior
     * @extends PMUI.core.Element
     * @abstract
     *
     * @constructor
     * While it is true that this class must not be instantiated,
     * it is useful to mention the settings parameter for the constructor function
     * (which will be used for the non abstract subclasses).
     * @param {Object} [settings] A JSON object, it can be contain the following fields:
     *
     * - {@link PMUI.form.Field#cfg-name name}.
     * - {@link PMUI.form.Field#cfg-label label}.
     * - {@link PMUI.form.Field#cfg-value value}.
     * - {@link PMUI.form.Field#cfg-helper helper}.
     * - {@link PMUI.form.Field#cfg-showHelper showHelper}.
     * - {@link PMUI.form.Field#cfg-validators validators}.
     * - {@link PMUI.form.Field#cfg-valueType valueType}.
     * - {@link PMUI.form.Field#cfg-controlPositioning controlPositioning}.
     * - {@link PMUI.form.Field#cfg-labelWidth labelWidth}.
     * - {@link PMUI.form.Field#cfg-showColon showColon}
     *
     * @cfg {String} name The name for the field.
     * @cfg {String} label The text to be shown as the field's label
     * @cfg {String} value The initial value to be set to the control.
     * @cfg {String} helper The helper text to be shown in the helper tooltip
     * @cfg {Boolean} showHelper A boolean value which determines if the helper tootltip will be shown or not.
     * @cfg {Object} validators An array where each array's item is a JSON object (with the validator setting data)
     *  or a {@link PMUI.form.Validator Validator} object.
     *      {
     *          validators: [
     *              {
     *                  type: "maxlength",
     *                  criteria: 5
     *              },
     *              new LengthValidator({
     *                  min: 0,
     *                  max: 5
     *              })
     *          ]
     *      }
     * In example above, "validators" is an array in which their first element is an JSON object
     * and the second one is a {@link PMUI.form.Validator Validator} object.
     * @cfg {String} valueType A string which specifies the data type for the Field value.
     * @cfg {String} controlPositioning A formatted string that specifies the order output for the control(s).
     * A string  which specifies the output order for the field's controls.
     * Basically this string uses a wildcard with the format "[cx]", each one is replaced by the control:
     *      "[c0]-[c1]-[c2]"
     * If you apply the string above to the controlPositioning property, it will render
     * the first three field's controls, each one separated from the other by a "-".
     *
     * Another wildcard is [c*], this represents all the controls or the ones that haven't been included yet:
     *      "[c*]-"
     * The example above will render all the controls and at the end it will add a "-".
     *
     *      "[c2]-[c*]"
     * The example above will render first the second control and then the other ones (starting from the first one).
     * @cfg {String} labelWidth The width for the label.
     * The width label should be a String with the following format "X%"
     *
     * Note that this% should not exceed 80%, because 20% is the tooltip
     * @cfg {Boolean} showColon If a colon is shown after the label text.
     * @cfg {Boolean} [labelVisible=true] If the label is visible or not.
     * @cfg {String} [labelPosition="left"] Sets the position for the label, the value can take one of the following
     * options:
     *
     * - "left" (default).
     * - "right".
     * - "top".
     * - "bottom"
     *
     * @cfg {String|Number|Array} [controlsWidth] Determines the width for the field's controls, but the width of the
     * field must be a number or a string with the format "#px", otherwise the width for the controls will be set to
     * "auto".It can be:
     *
     * - A String, in this case the string must have the following format "##px", otherwise must be "auto".
     * - A Number, in this case the value received is parsed into a px units value.
     * - An Array in which each element can be a String or a Number (following the rules above).
     *
     * This will be applied respectively to each control in the field.
     * @cfg {Function} [onClick=null] Description here.
     * @cfg {Function|null} [onBeforeChange] The callback function to be executed before the field's value changes. To
     * avoid the change the callback function must return false. For info about the parameters sent to the calback
     * function please read the {@link #event-onBeforeChange onBeforeChange event} documentation.
     */
    var Field = function (settings) {
        Field.superclass.call(this, settings);
        /**
         * @property {String} [name=[The object's id]] The field's name.
         * @readonly
         */
        this.name = null;
        /**
         * @property {String} [label = "[field]"] The field's label text.
         * @readonly
         */
        this.label = null;
        /**
         * @property {String} [value=""] The field's value.
         * @readonly
         */
        this.value = null;
        /**
         * @property {PMUI.ui.TooltipMessage} [helper] The field's help tooltip
         * @readonly
         */
        this.helper = null;
        /**
         * @property {PMUI.ui.TooltipMessage} [message] A {@link PMUI.ui.TooltipMessage TooltipMessage}
         * object to show a message related to the field (i.e. validation error messages)
         * @private
         */
        this.message = null;
        /**
         * @property {Array} controls An array, it will contain all the necessary Control objects
         * @private
         */
        this.controls = [];
        /**
         * @property {Object} validators An JSON object which will contain all the
         * {@link PMUI.form.Validator Validators} object.
         * @private
         */
        this.validators = {};
        /**
         * @property {String} [controlPositioning="[c*]"] A formatted string that specifies
         * the order output for the field's controls.
         * @readonly
         */
        this.controlPositioning = null;
        /**
         * @property {Object} [dom] A JSON object which will contain important DOM object
         * for the Field object.
         * @private
         */
        this.dom = null;
        /**
         * @property {Boolean} [helperIsVisible=false] A Boolean that let us
         * know if the help tooltip will be shown or not.
         * @readonly
         */
        this.helperIsVisible = null;
        /**
         * @property {String} [labelWidth="30%"]
         * The width for the label. This property should be a String with the following format "X%"
         * @readonly
         */
        this.labelWidth = null;
        /**
         * @property {Boolean} [visibleColon=true]
         * If a colon is shown after the label text.
         * @readonly
         */
        this.visibleColon = null;
        /**
         * @property {Boolean} labelVisible
         * If the label is visible or not.
         * @readonly
         */
        this.labelVisible = null;
        /**
         * @property {String} [labelPosition="String"] The position for the field's label.
         * @readonly
         */
        this.labelPosition = null;
        /**
         * @event onBeforeChange
         * Fired before the field's value changes.
         * @param {String} newValue The new value to be set.
         * @param {String} oldValue The old field's value.
         */
        this.onBeforeChange = null;
        /**
         * @event onChange
         * Fired when the field's value changes.
         * @param {String} newValue The field's new value.
         * @param {String} oldValue The previous field's value.
         */
        this.onChange = null;
        /**
         * @property {String} valueType The value data type for the field.
         */
        this.valueType = null;
        /**
         * @property {PMUI.form.ValidatorFactory} validatorFactory The factory object for validator production.
         */
        this.validatorFactory = null;
        /**
         * @property {Boolean} [required] If the field is required or not
         */
        this.required = null;
        /**
         * @property {String} [requiredMessage="This field is required."]
         * The message to display when the validation for required property fails.
         */
        this.requiredMessage = null;
        /**
         * @property {Boolean} [validAtChange=true]
         * If the validation must be executed every time the field's value changes.
         */
        this.validAtChange = null;
        /**
         * The width for the control(s)
         * @type {Number|String|Array}
         * @readonly
         */
        this.controlsWidth = [];
        /**
         * @property {Number|String} controlsContainerWidth
         * @type {Number|String}
         */
        this.controlsContainerWidth = null;
        /**
         * @property {PMUI.form.Form} form
         * The form the field belongs to.
         */
        this.form = null;
        /**
         * @property {Boolean} eventsDefined
         * If the events for the object have been defined.
         */
        this.eventsDefined = null;
        /**
         * @property {PMUI.data.DataSet} data
         * The field's data object.
         * @private
         */
        this.data = null;
        /**
         * The initial value the field will be set when its reset() method is called.
         * @type {String}
         * @private
         */
        this.initialValue = null;
        /**
         * [onClick description]
         * @type {[type]}
         */
        this.onClick = null;
        /**
         * If the field is disabled or not. Notice that if it is disabled the validation won't be executed.
         * @type {Boolean}
         */
        this.disabled = null;

        /**
         * @protected
         * Valid Types
         * @type {Array}
         */
        this.dependentFields = [];
        this.dependencyHandler = null;

        this.validTypes = {
            'string': true,
            'number': true,
            'boolean': true,
            'date': true,
            'object': true,
            'integer': true,
            'float': true
        };
        Field.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', Field);

    Field.prototype.type = "Field";

    Field.prototype.init = function (settings) {
        var defaults = {
            name: this.id,
            label: '[field]',
            value: '',
            helper: '',
            showHelper: !!(settings && settings.helper),
            validators: [],
            valueType: 'string',
            controlPositioning: '[c*]',
            labelWidth: '23.5%',
            width: '100%',
            showColon: true,
            validatorFactory: new PMUI.form.ValidatorFactory(),
            onBeforeChange: null,
            onChange: null,
            required: false,
            validAtChange: true,
            requiredMessage: 'This field is required.'.translate(),
            labelVisible: true,
            labelPosition: 'left',
            form: null,
            controlsWidth: "auto",
            controlsContainerWidth: "70%",
            disabled: false,
            dependentFields: [],
            dependencyHandler: function () {
            },
            onClick: null
        };

        this.dependentFields = [];
        this.dependencyFields = new PMUI.util.ArrayList();
        $.extend(true, defaults, settings);

        this.initialValue = defaults.value;

        this.helper = new PMUI.ui.TooltipMessage({
            category: 'help'
        });

        this.dom = {};
        this.data = new PMUI.data.DataField();

        this.message = new PMUI.ui.TextLabel({
            text: 'This field is required.'.translate(),
            displayMode: 'block',
            mode: 'normal',
            visible: false
        });

        this.setForm(defaults.form)
            .setValidAtChange(defaults.validAtChange)
            .setRequired(defaults.required)
            .setRequiredMessage(defaults.requiredMessage)
            .setValidatorFactory(defaults.validatorFactory)
            .setValidators(defaults.validators)
            .setName(defaults.name)
            .setLabel(defaults.label)
            .setValue(defaults.value)
            .setHelper(defaults.helper)
            .setValueType(defaults.valueType)
            .setControlPositioning(defaults.controlPositioning)
            .setOnBeforeChangeHandler(defaults.onBeforeChange)
            .setOnChangeHandler(defaults.onChange)
            .setWidth(defaults.width)
            .setLabelWidth(defaults.labelWidth)
            .setLabelPosition(defaults.labelPosition)
            .setControlsWidth(defaults.controlsWidth)
            .setControlsContainerWidth(defaults.controlsContainerWidth)
            .setOnClickHandler(defaults.onClick)
            .setControls();
        this.setDependencyHandler(defaults.dependencyHandler);
        this.setDependentFields(defaults.dependentFields);

        if (defaults.showHelper) {
            this.showHelper();
        } else {
            this.hideHelper();
        }
        if (defaults.showColon) {
            this.showColon();
        } else {
            this.hideColon();
        }
        if (defaults.labelVisible) {
            this.showLabel();
        } else {
            this.hideLabel();
        }
        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };
    Field.prototype.onBeforeChangeHandler = function (handler) {
        var that = this;
        return function (newValue, oldValue) {
            var callbackResponse,
                futureValue = "",
                i;

            if (typeof that.onBeforeChange === 'function') {
                for (i = 0; i < that.controls.length; i += 1) {
                    if (that.controls[i] === this) {
                        futureValue += ' ' + newValue;
                    } else {
                        futureValue += ' ' + that.controls[i].getValue();
                    }
                }
                futureValue = futureValue.substr(1);
                callbackResponse = that.onBeforeChange(futureValue, that.getValue());
            }
            return callbackResponse;
        };
    };
    Field.prototype.setOnBeforeChangeHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnBeforeChangeHandler(): The parameter must be a function or null.");
        }
        this.onBeforeChange = handler;
        return this;
    };
    Field.prototype.setOnClickHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnClickHandler(): The parameter must be a function or null.");
        }
        this.onClick = handler;
        return this;
    };
    Field.prototype.setDependentFields = function (dependentFields) {
        if (!jQuery.isArray(dependentFields)) {
            throw new Error("setDependentFields(): The parameter must be an array.");
        }
        this.dependentFields = dependentFields;
        if (this.form) {
            this.form.updateDependencies();
        }
        return this;
    };

    Field.prototype.removeDependentField = function (FieldName) {
        var i;

        for (i = 0; this.dependentFields.length; i += 1) {
            if (this.dependentFields[i] === FieldName) {
                this.dependentFields.splice(i, 1);
                i--;
            }
        }
        return this;
    };

    Field.prototype.setDependencyHandler = function (handler) {
        if (typeof handler == 'function' || handler == null) {
            this.dependencyHandler = handler;
        }
        return this;
    };

    /**
     * Disables the field. Notice that when a field is disabled it is not validated and it is not returned when its
     * form's getData() method is invoked.
     * @chainable
     */
    Field.prototype.disable = function () {
        var i;

        this.disabled = true;
        this.style.addClasses(["pmui-disabled"]);
        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].disable(true);
        }

        return this;
    };
    /**
     * Enables the field.
     * @chainable
     */
    Field.prototype.enable = function () {
        var i;

        this.disabled = false;
        this.style.removeClasses(["pmui-disabled"]);
        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].disable(false);
        }

        return this;
    };
    /**
     * Resets the field to its original value.
     * @chainable
     */
    Field.prototype.reset = function () {
        this.setValue(this.initialValue);
        this.hideMessage();
        if (this.eventsDefined) {
            this.onChangeHandler();
        }
        return this;
    };
    /**
     * Determines the width for the field's controls, but the width of the field must be a number or a string with
     * the format "#px", otherwise the width for the controls will be set to "auto".
     * @param {Number|String|Array} width  It can be:
     *
     * - A String, in this case the string must have the following format "##px", otherwise must be "auto".
     * - A Number, in this case the value received is parsed into a px units value.
     * - An Array in which each element can be a String or a Number (following the rules above).
     *
     * This will be applied respectively to each control in the field.
     * @chainable
     */
    Field.prototype.setControlsWidth = function (width) {
        var i,
            labelWidthPx,
            auxWidth,
            autoGerateWidthForControls = false,
            fieldWidthIsAbsolute = false;

        if (!(width === 'auto' || typeof width === 'number' || jQuery.isArray(width) || /^\d+(\.\d+)?px/.test(width))) {
            throw new Error("setControlsWidth(): The parameter must be a string \"auto\", or a number or an array.");
        }

        this.controlsWidth = width;

        if (width === 'auto') {
            autoGerateWidthForControls = true;
        }

        if (typeof this.width === 'number' || /^\d+(\.\d+)?px/.test(this.width)) {
            fieldWidthIsAbsolute = true;
        }

        if (!autoGerateWidthForControls) {
            for (i = 0; i < this.controls.length; i += 1) {
                if (jQuery.isArray(width)) {
                    this.controls[i].setWidth(width[i] || "auto");
                } else {
                    this.controls[i].setWidth(width);
                }
            }
        } else if (fieldWidthIsAbsolute) {
            auxWidth = parseInt(this.width, 10);
            labelWidthPx = auxWidth * (parseInt(this.labelWidth, 10) / 100) + 4;
            labelWidthPx = auxWidth - 83 - labelWidthPx;
            for (i = 0; i < this.controls.length; i += 1) {
                this.controls[i].setWidth(labelWidthPx);
            }
        } else {
            for (i = 0; i < this.controls.length; i += 1) {
                this.controls[i].setWidth("auto");
            }
        }

        return this;
    };
    /**
     * Sets the width for the controls container.
     * @param width
     */
    Field.prototype.setControlsContainerWidth = function (width) {
        if (!(typeof width === 'number' || jQuery.isArray(width) || /^\d+(\.\d+)?(px|\%)/.test(width))) {
            throw new Error("setControlsContainerWidth(): The parameter must be a string (with a numeric or percentage value), or a number or an array.");
        }
        this.controlsContainerWidth = width;
        if (this.dom && this.dom.controlContainer) {
            $(this.dom.controlContainer).css('width', width);
        }
        return this;
    };
    /**
     * Sets the form the field belongs to.
     * @param {PMUI.form.Form} form
     * @chainable
     */
    Field.prototype.setForm = function (form) {
        if (form instanceof PMUI.form.Form) {
            this.form = form;
        }

        return this;
    };
    /**
     * Turns on/off the validation when the field's value changes.
     * @param {Boolean} validAtChange
     * @chainable
     */
    Field.prototype.setValidAtChange = function (validAtChange) {
        this.validAtChange = !!validAtChange;
        return this;
    };
    /**
     * Sets the position for the label.
     * @param {String} position It can take one of the following values: "top", "right", "bottom", "left".
     * @chainable
     */
    Field.prototype.setLabelPosition = function (position) {
        if (position === 'top' || position === 'right' || position === 'bottom' || position === 'left') {
            this.labelPosition = position;
            if (this.html) {
                if (position === 'top' || position === 'left') {
                    this.html.insertBefore(this.dom.labelTextContainer, this.dom.controlContainer);
                    this.dom.labelTextContainer.style.display = position === 'top' ? 'block' : 'inline-block';
                    $(this.dom.controlContainer).css("float", '');
                } else {
                    this.html.insertBefore(this.dom.controlContainer, this.dom.labelTextContainer);
                    this.dom.labelTextContainer.style.display = position === 'bottom' ? 'block' : 'inline-block';
                    $(this.dom.labelTextContainer).css("float", '');
                }
            }
        } else {
            throw new Error('setLabelPosition(): it only accepts "top", "rigth", "left" or "bottom" as value for ' +
                'the parameter');
        }

        return this;
    };
    /**
     * Sets the factory which must be produce {@link PMUI.form.Validator Validator} objects.
     * @param {PMUI.util.Factory|Object} factory
     * It can be a:
     *
     * - a {@link PMUI.util.Factory} object
     * - a JSON object: in this case a new {@link PMUI.form.ValidatorFactory ValidatorFactory} will be created
     * using the JSON object as the constructor parameter.
     * @private
     * @chainable
     */
    Field.prototype.setValidatorFactory = function (factory) {
        if (factory instanceof PMUI.util.Factory) {
            this.validatorFactory = factory;
        } else {
            this.validatorFactory = new PMUI.form.ValidatorFactory(factory);
        }

        return this;
    };
    /**
     * Sets the message to show when the required field validation fails.
     * @param {String} requiredMessage
     * @chainable
     */
    Field.prototype.setRequiredMessage = function (requiredMessage) {
        if (typeof requiredMessage === 'string') {
            this.requiredMessage = requiredMessage;
        } else {
            throw new Error("the setRequiredMessage() method only accepts string values.");
        }

        return this;
    };
    /**
     * Sets if the fields is required or not.
     * @param {Boolean} required
     * @chainable
     */
    Field.prototype.setRequired = function (required) {
        this.required = !!required;
        if (this.dom.fieldRequired) {
            if (this.required) {
                this.showRequired();
            } else {
                this.hideRequired();
            }
        }
        return this;
    };

    Field.prototype.hideRequired = function () {
        this.dom.fieldRequired.style.display = 'none';
        return this;
    };

    Field.prototype.showRequired = function () {
        this.dom.fieldRequired.style.display = 'inline-block';
        return this;
    };
    /**
     * Removes all the validators from the field.
     * @chainable
     */
    Field.prototype.clearValidators = function () {
        var key;

        for (key in this.validators) {
            if (this.validators.hasOwnProperty(key)) {
                this.validators[key] = null;
                delete this.validators[key];
            }
        }

        return this;
    };
    /**
     * Adds a validator to the field.
     * @param {String|PMUI.form.Validator|Object} validator
     * It can be:
     *
     * - a String: it must be on of the supported pmTypes by the
     * {@link PMUI.form.ValidatorFactory ValidatorFactory} class.
     * - a {@link PMUI.form.Validator Validator} object.
     * - a JSON object: in this case the JSON structure must be the same as the needed one to create
     * the desired validator, additionally must have the respective pmType.
     *
     * Note. All the supported pmTypes are specified in the
     * {@link PMUI.form.ValidatorFactory ValidatorFactory documentation}.
     * @chainable
     */
    Field.prototype.addValidator = function (validator) {
        var newValidator;

        if (this.validatorFactory) {
            if (this.validatorFactory.isValidClass(validator) || this.validatorFactory.isValidName(validator.pmType)) {
                newValidator = this.validatorFactory.make(validator);
            } else {
                throw new Error('Invalid validator to add.');
            }
        }

        if (newValidator && newValidator instanceof PMUI.form.Validator) {
            newValidator.setParent(this);
            this.validators[newValidator.type] = newValidator;
        }

        return this;
    };
    /**
     * Sets the validators for the field.
     * @param {Array} validators An array in which each element can be one of the accepted types in the
     * {@link PMUI.form.Field#addValidator addValidator() method}.
     * @chainable
     */
    Field.prototype.setValidators = function (validators) {
        var i = 0;

        if (jQuery.isArray(validators)) {
            this.clearValidators();
            for (i = 0; i < validators.length; i += 1) {
                this.addValidator(validators[i]);
            }
        }

        return this;
    };
    /**
     * Shows a colon after the label text.
     * @chainable
     */
    Field.prototype.showColon = function () {
        this.visibleColon = true;
        if (this.dom.fieldColon) {
            this.dom.fieldColon.style.display = '';
        }
    };
    /**
     * Hides the colon after the label text.
     * @chainable
     */
    Field.prototype.hideColon = function () {
        this.visibleColon = false;
        if (this.dom.fieldColon) {
            this.dom.fieldColon.style.display = "none";
        }
        return this;
    }
    /**
     * Sets the width for the label.
     * @param {String} width It can be a Number or a String.
     * This parameter should be a String with the following format "X%";
     * @chainable
     */
    Field.prototype.setLabelWidth = function (width) {
        if (/^\d+(\.\d+)?(px|em|%)$/.test(width)) {
            this.labelWidth = width;
            this.setControlsWidth(this.controlsWidth);
        } else {
            throw new Error('setLabelWidth(): invalid "width" parameter');
        }
        if (this.dom.labelTextContainer) {
            this.dom.labelTextContainer.style.width = this.labelWidth;
            if (this.labelVisible && this.labelPosition != "top")
                $(this.dom.messageContainer).css({"margin-left": parseInt(this.labelWidth) + 1 + "%"});
        }
        return this;
    };
    /**
     * Sets the callback function to be called when the field's value changes.
     * @param {Function} handler
     * @chainable
     */
    Field.prototype.setOnChangeHandler = function (handler) {
        if (typeof handler === 'function') {
            this.onChange = handler;
        }

        return this;
    };
    /**
     * Sets the helper tooltip visible.
     * @chainable
     */
    Field.prototype.showHelper = function () {
        this.helperIsVisible = true;
        this.helper.setVisible(true);
        return this;
    };
    /**
     * Sets the helper tooltip non visible.
     * @chainable
     */
    Field.prototype.hideHelper = function () {
        this.helperIsVisible = false;
        this.helper.setVisible(false);
        return this;
    };
    /**
     * Returns an array with all the field's controls.
     * @return {Array}
     */
    Field.prototype.getControls = function () {
        return this.controls;
    };
    /**
     * Returns an index based field's control.
     * @param  {Number} index An integer value.
     * @return {PMUI.control.Control}
     */
    Field.prototype.getControl = function (index) {
        index = index || 0;
        return this.controls[index];
    };
    /**
     * Sets the controlPositioning property.
     * @param {String} positioning The string must have the same format that the
     * {@link PMUI.form.Field#cfg-controlPositioning controlPositioning} config option.
     */
    Field.prototype.setControlPositioning = function (positioning) {
        var pos,
            controlPos,
            i,
            j,
            k,
            controls,
            span,
            addControl,
            that = this;

        if (typeof positioning === 'string') {
            this.controlPositioning = positioning;
            if (this.html && this.controls.length) {
                for (i = 0; i < this.controls.length; i += 1) {
                    jQuery(this.controls[i].getHTML()).detach();
                }
                $(this.dom.controlContainer).empty();
                if (positioning !== "") {
                    controls = this.controls.slice();
                    pos = positioning.split(/\[c[\d|\*]\]/);
                    controlPos = positioning.match(/\[c[\d|\*]\]/g);
                    addControl = function (c) {
                        var k;
                        if (c === '[c*]') {
                            for (k = 0; k < controls.length; k += 1) {
                                if (controls[k] !== null) {
                                    that.dom.controlContainer.appendChild(controls[k].getHTML());
                                    controls[k] = null;
                                }
                            }
                        } else {
                            k = c.match(/\d+/);
                            k = parseInt(k[0], 10);
                            if (controls[k] !== null) {
                                that.dom.controlContainer.appendChild(controls[k].getHTML());
                                controls[k] = null;
                            }
                        }
                    };

                    j = 0;
                    for (i = 0; i < pos.length; i += 1) {
                        if (pos[i] === "" && j < controlPos.length) {
                            addControl(controlPos[j]);
                            j++;
                        } else {
                            span = PMUI.createHTMLElement('span');
                            span.textContent = pos[i];
                            this.dom.controlContainer.appendChild(span);
                        }
                        if (j < controlPos.length) {
                            addControl(controlPos[j]);
                            j++;
                        }
                    }
                    this.dom.labelTextContainer.setAttribute('for', this.controls[0].id);
                    this.dom.controlContainer.appendChild(this.helper.getHTML());
                }
            }
        } else {
            throw new Error("The setControlPositioning() method only accepts string values.");
        }

        return this;
    };
    /**
     * Sets the controls for the field.
     *
     * Since this is an abstract method, it must be implemented in its non-abstract subclasses
     * @abstract
     * @private
     */
    Field.prototype.setControls = function () {
    };
    /**
     * Sets the name for the Field
     * @param {String} name
     */
    Field.prototype.setName = function (name) {
        if (typeof name === 'string') {
            this.name = name;
            this.data.setKey(name);
        } else {
            throw new Error('The setName() method only accepts string values!');
        }
        return this;
    };
    /**
     * Returns the field's name
     * @return {String}
     */
    Field.prototype.getName = function () {
        return this.name;
    };
    /**
     * Sets the text for the field's label.
     * @param {String} label
     */
    Field.prototype.setLabel = function (label) {
        if (typeof label === 'string') {
            this.label = label;
        } else {
            throw new Error("The setLabel() method only accepts string values!");
        }
        if (this.dom.fieldTextLabel) {
            this.dom.fieldTextLabel.textContent = this.label;
        }
        if (this.dom.fieldRequired) {
            this.dom.fieldRequired.textContent = '*';
            this.setRequired(this.required);
        }
        if (this.dom.fieldColon) {
            this.dom.fieldColon.textContent = ":";
        }
        return this;
    };
    /**
     * Returns the text from the field's label.
     * @return {String}
     */
    Field.prototype.getLabel = function () {
        return this.label;
    };
    /**
     * Sets the value to the field's controls.
     * @protected
     * @param {String} value
     * @chainable
     */
    Field.prototype.setValueToControls = function (value) {
        var i;

        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].setValue(value);
        }
        return this;
    };
    /**
     * Returns the current value from the field's controls without affect the field's value.
     * @return {String}
     * @private
     */
    Field.prototype.getValueFromControls = function () {
        var value = '',
            i;

        for (i = 0; i < this.controls.length; i += 1) {
            value += ' ' + this.controls[i].getValue();
        }

        return value.substr(1);
    };
    /**
     * Update the field's value property from the controls
     * @protected
     * @chainable
     */
    Field.prototype.updateValueFromControls = function () {
        var value = this.getValueFromControls();

        this.value = value;
        this.data.setValue(value);

        return this;
    };
    /**
     * Sets the field's value.
     * @param {String} value
     */
    Field.prototype.setValue = function (value) {
        if (typeof value === 'number') {
            value = value.toString();
        }
        if (typeof value === 'string') {
            this.value = value;
        } else {
            throw new Error("The setValue() method only accepts string values!");
        }
        this.data.setValue(this.value);
        this.setValueToControls(this.value);
        return this;
    };
    /**
     * Returns the field's value.
     * @param [format] Defines the return format
     * @returns {*}
     */
    Field.prototype.getValue = function (format) {
        var castFormat = format || this.valueType;
        return PMUI.castValue(this.value, castFormat);
    };
    /**
     * Sets the helper text.
     * @param {String} helper
     */
    Field.prototype.setHelper = function (helper) {
        this.helper.setMessage(helper);

        return this;
    };
    /**
     * Shows a message below the control.
     * @param  {String} message  The message
     * @param  {String} [category="info"] The message category,
     * It can be one of the accepted values for the
     * {@link PMUI.ui.TooltipMessage#setCategory TooltipMessage's setCategory() method}, it defaults to "info".
     * @chainable
     */
    Field.prototype.showMessage = function () {
        this.message.setVisible(true)
        return this;
    };
    /**
     * Hides the message below the control.
     * @chainable
     */
    Field.prototype.hideMessage = function () {
        this.message.setVisible(false);

        return this;
    };
    /**
     * Sets the value type for the field.
     * @param {String} type
     */
    Field.prototype.setValueType = function (type) {
        var newType;
        if (typeof type === 'string') {
            newType = this.validTypes[type] ? type : 'object';
            this.valueType = newType;
            this.data.setType(newType);
        } else {
            throw new Error("The setValueType() method only accepts string values!");
        }

        return this;
    };
    /**
     * Returns the field's value type.
     * @return {String}
     */
    Field.prototype.getValueType = function () {
        return this.valueType;
    };
    /**
     * The onChange event handler constructor.
     * @private
     * @return {Function} The handler.
     */
    Field.prototype.onChangeHandler = function () {
        var that = this,
            i,
            dependentFields = this.dependentFields,
            form = this.form;

        return function () {
            var previousValue = that.value,
                newValue = that.getValueFromControls();

            if (newValue === previousValue) {
                return this;
            }

            that.value = newValue;
            that.data.setValue(newValue);

            if (that.validAtChange) {
                that.isValid();
            }

            if (typeof that.onChange === 'function') {
                that.onChange(that.getValue(), previousValue);
            }
            if (that.form) {
                (that.form.onChangeHandler())(that, that.value, previousValue);
                //PMUI call a event for dependency [dependencyHandler]
                for (i = 0; i < dependentFields.length; i += 1) {
                    dependentField = form.getField(dependentFields[i]);
                    if (!dependentField) {
                        that.removeDependentField(dependentFields[i]);
                        continue;
                    }
                    dependentField.fireDependencyHandler();
                }
            }
        };
    };

    Field.prototype.fireDependencyHandler = function () {
        var form = this.form,
            dependsOf,
            obj = {},
            i,
            that = this;

        if (form) {
            dependsOf = form.dependencies[that.name];
            for (i = 0; i < dependsOf.length; i += 1) {
                obj[dependsOf[i].name] = dependsOf[i];
            }
            if (typeof this.dependencyHandler === 'function') {
                this.dependencyHandler(that, obj);
            }
        }
        return this;
    };
    /**
     * Attach the event listeners to the HTML element
     * @private
     * @chainable
     */
    Field.prototype.defineEvents = function () {
        var i,
            that = this;

        Field.superclass.prototype.defineEvents.call(this);
        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].setOnChangeHandler(this.onChangeHandler())
                .setOnBeforeChangeHandler(this.onBeforeChangeHandler())
                .defineEvents();
        }
        if (this.onClick) {
            this.addEvent('click').listen(this.html, function (e) {
                if (typeof that.onClick === 'function') {
                    that.onClick(that);
                }
            });
            this.addEvent('click').listen(this.dom.labelTextContainer, function (e) {
                e.stopPropagation();
            });
        }
        this.eventsDefined = true;

        return this;
    };
    /**
     * Returns true if the label is visible, otherwise returns false.
     * @return {Boolean}
     */
    Field.prototype.isLabelVisible = function () {
        return this.labelVisible;
    };
    /**
     * Hides the field's label.
     * @chainable
     */
    Field.prototype.hideLabel = function () {
        jQuery(this.dom.labelTextContainer).css('visibility', 'hidden');
        this.labelVisible = false;
        return this;
    };
    /**
     * Shows the field's label.
     * @chainable
     */
    Field.prototype.showLabel = function () {
        jQuery(this.dom.labelTextContainer).css('visibility', '');
        this.labelVisible = true;
        return this;
    };
    /**
     * Evaluates the required validation.
     * @return {Boolean} Returns true if the validation passes otherwise returns false.
     */
    Field.prototype.evalRequired = function () {
        var valid = true;
        if (this.required) {
            if (this.valueType !== 'number') {
                if (this.getValue() === "" || this.getValue() === "[]") {
                    valid = false;
                }
            } else {
                if (this.getValue().toString() === 'NaN') {
                    valid = false;
                }
            }
            if (valid) {
                this.hideMessage();
            } else {
                this.showMessage();
            }
        }
        return valid;
    };
    /**
     * Executes the validations and returns true if all of them passes, otherwise returns false.
     * @return {Boolean}
     */
    Field.prototype.isValid = function () {
        var valid = true, validator;
        valid = valid && this.evalRequired();
        if (!valid) {
            this.controls[0].style.addClasses(['error']);
            return valid;
        }
        this.controls[0].style.removeClasses(['error']);
        for (validator in this.validators) {
            if (this.validators.hasOwnProperty(validator)) {
                valid = valid && this.validators[validator].isValid();
                if (!valid) {
                    this.message.setText(this.validators[validator].errorMessage);
                    return valid;
                }
            }
        }
        return valid;
    };
    /**
     * Create the HTML Element for the Field.
     * @protected
     * @return {HTMLElement}
     */
    Field.prototype.createHTML = function () {
        var html, label,
            labelTextContainer,
            controlContainer,
            messageContainer,
            fieldColon,
            fieldRequired,
            fieldTextLabel;

        if (!this.html) {
            this.style.addClasses(['pmui-field']);
            Field.superclass.prototype.createHTML.call(this);


            labelTextContainer = PMUI.createHTMLElement("label");

            // contenedor de todos los labels
            labelTextContainer.className = 'pmui-field-label';

            //labelText
            fieldTextLabel = PMUI.createHTMLElement('span');
            fieldTextLabel.className = "pmui-field-textLabel";
            //asterisk for required
            fieldRequired = PMUI.createHTMLElement ('span');
            fieldRequired.className = "pmui-field-required";
            //colon
            fieldColon = PMUI.createHTMLElement('span');
            fieldColon.className = "pmui-field-colon";


            controlContainer = PMUI.createHTMLElement("span");
            controlContainer.className = 'pmui-field-control';
            messageContainer = PMUI.createHTMLElement("span");
            messageContainer.className = 'pmui-field-message';
            messageContainer.style.display = "block"


            //labelTextContainer append tree childs label, * and :
            labelTextContainer.appendChild(fieldTextLabel);
            labelTextContainer.appendChild(fieldRequired);
            labelTextContainer.appendChild(fieldColon);

            //messageContainer append the helper
            messageContainer.appendChild(this.message.getHTML());

            //this html append label, control and helper tag
            this.html.appendChild(labelTextContainer);
            this.html.appendChild(controlContainer);
            this.html.appendChild(messageContainer);

            this.dom.labelTextContainer = labelTextContainer;
            this.dom.fieldTextLabel = fieldTextLabel;
            this.dom.fieldColon = fieldColon;
            this.dom.fieldRequired = fieldRequired;
            this.dom.controlContainer = controlContainer;
            this.dom.messageContainer = messageContainer;

            this.setControlPositioning(this.controlPositioning);
            this.setLabelWidth(this.labelWidth);
            this.setLabel(this.label);
            this.setValue(this.value);
            this.setLabelPosition(this.labelPosition);
            if (this.labelVisible) {
                this.showLabel();
            } else {
                this.hideLabel();
            }
            if (this.visibleColon) {
                this.showColon();
            } else {
                this.hideColon();
            }
        }

        return this.html;
    };
    /**
     * Set the width for the field.
     * @param {Number|String} width height it can be a number or a string.
     * In case of using a String you only can use 'auto' or ##px or ##% or ##em when ## is a number.
     * @chainable
     */
    Field.prototype.setWidth = function (width) {
        Field.superclass.prototype.setWidth.call(this, width);
        if (this.labelWidth) {
            this.setLabelWidth(this.labelWidth);
        }
        return this;
    };
    /**
     * @method setFocus
     * set the focus on field for to control
     * @param {Number} [index] if the field has more than one control and needs to focus on a specific Control
     * @chainable
     */
    Field.prototype.setFocus = function (index) {
        var i = index || 0,
            j,
            controls = this.getControls();

        if (this.controls[i] && this.controls[i].setFocus) {
            this.controls[i].setFocus();
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.form.Field', Field);

    if (typeof exports !== "undefined") {
        module.exports = Field;
    }
}());
