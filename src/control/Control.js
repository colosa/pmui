(function () {
    /**
     * @class PMUI.control.Control
     * Class that encapsulates the basic bahavior for a form control.
     * Since this class is abstract it shouldn't be instantiated.
     * @extends PMUI.core.Element
     * @abstract
     *
     * Usage example (only for subclasses since this is an abstract class):
     *
     *      //Remember, this is an abstract class so it shouldn't be instantiate,
     *      //anyway we are instantiating it just for this example
     *      var myControl = new PMUI.control.Control({
     *          name: "phrase",
     *          value: "Sometimes the sun goes down!",
     *          disabled: false,
     *          onChange: function(currentValue, previousValue) {
     *              console.log(this.name + " has changed!");
     *              console.log("Its previous value was: " + previousValue);
     *              console.log("Its current value is: " + currentValue);
     *          }
     *      });
     *
     * @constructor
     * While it is true that this class must not be instantiated,
     * it is useful to mention the settings parameter for the constructor
     * function (which will be used for the non abstract subclasses).
     * The parameter for the constructor will be a JSON object whose properties are specified in the
     * {@link PMUI.control.Control#cfg-disabled Config Options section}.
     *
     * @cfg {String} [name=The object's ID] The name for the control. If it is not specified then the object's id is
     * used.
     * @cfg {String} [value=""] The initial value to be set to the control.
     * @cfg {PMUI.form.Field} [field=null] The parent {@link PMUI.form.Field Field} object for the control.
     * @cfg {Boolean} [disabled=false] A boolean value which determines if the control will be enabled or not.
     * @cfg {Function} [onChange=null] A callback function to be invoked when the control changes. For info about the
     * received parameters please read the {@link PMUI.control.Control#event-onChange onChange event} section.
     * @cfg {Function} [onBeforeChange=null] A callback function to be invoked after the control's value changes. The
     * function can return false to avoid the value changing. For info about the received parameter please read the
     * {@link #event-onBeforeChange onBeforeChange event} documentation.
     */
    var Control = function (settings) {
        Control.superclass.call(this, settings);
        /**
         * @property {String} [name=id] The control's name, it defaults to null.
         * @readonly
         */
        this.name = null;
        /**
         * @property {String} [value=""] The control's value.
         * @readonly
         */
        this.value = null;
        /**
         * @property {PMUI.form.Field} [field=null] The {@link PMUI.form.Field Field}
         object the current object belongs to.
         * @readonly
         */
        this.field = null;
        /**
         * @property {Boolean} [disabled=false] If the control is disabled or not.
         * @readonly
         */
        this.disabled = null;
        /**
         * @event onChange
         * Fired when the control's value is changed.
         * @param {String} newValue The new control's value.
         * @param {String} prevValue The previous control's value.
         */
        this.onChange = null;
        /**
         * @event onBeforeChange
         * Fired after the control's value changes.
         * @param {String} newValue The new control's value.
         * @param {String} prevValue The previous vontrol's value.
         */
        this.onBeforeChange = null;
        Control.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', Control);

    Control.prototype.type = "Control";
    /**
     * Initialize the object.
     * @param  {Object} settings The config options.
     */
    Control.prototype.init = function (settings) {
        var defaults = {
            name: this.id,
            value: "",
            field: null,
            disabled: false,
            onBeforeChange: null,
            onChange: null
        };

        $.extend(true, defaults, settings);

        this.setName(defaults.name)
            .setValue(defaults.value)
            .setField(defaults.field)
            .disable(defaults.disabled)
            .setOnBeforeChangeHandler(defaults.onBeforeChange)
            .setOnChangeHandler(defaults.onChange);
    };
    /**
     * Sets the callback function to be executed when the {@link #event-onBeforeChange onBeforeChange event} fires.
     * @param {Function|null} handler
     * @chainable
     */
    Control.prototype.setOnBeforeChangeHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error('setOnBeforeChangeHandler(): The parameter must be a function or null.');
        }
        this.onBeforeChange = handler;
        return this;
    };
    /**
     * Sets the control's name.
     * @param {String} name
     * @chainable
     */
    Control.prototype.setName = function (name) {
        if (typeof name === "string" || typeof name === "number") {
            this.name = name.toString();
            if (this.html) {
                this.html.setAttribute("name", name);
            }
        } else {
            throw new Error("The setName() method only accepts string or number values");
        }

        return this;
    };
    /**
     * Returns the control's name.
     * @return {String}
     */
    Control.prototype.getName = function () {
        return this.name;
    };
    /**
     * Sets the control's value.
     * @param {String} value
     * @chainable
     */
    Control.prototype.setValue = function (value) {
        if (typeof value !== 'undefined') {
            this.value = value.toString();
        } else {
            throw new Error("setValue(): a parameter is required.");
        }

        return this;
    };
    /**
     * Returns the control's value.
     * @return {String}
     */
    Control.prototype.getValue = function () {
        return this.value;
    };
    /**
     * Sets the control's parent {@link PMUI.form.Field Field} object.
     * @param {PMUI.form.Field}
     * @chainable
     */
    Control.prototype.setField = function (field) {
        if (field instanceof PMUI.form.Field) {
            this.field = field;
        }

        return this;
    };
    /**
     * Returns the control's parent {@link PMUI.form.Field Field} object.
     * @return {PMUI.form.Field}
     */
    Control.prototype.getField = function () {
        return this.field;
    };
    /**
     * Disables/enables the control.
     * @param {Boolean} disable If the value is evaluated as true then the control is disabled,
     otherwise the control is enabled.
     * @chainable
     */
    Control.prototype.disable = function (disable) {
        this.disabled = !!disable;
        return this;
    };
    /**
     * Returns true if the control is enabled, if it don't then it returns false
     * @return {Boolean}
     */
    Control.prototype.isEnabled = function () {
        return !this.disabled;
    };
    /**
     * Sets the callback function which will be executed everytime the control changes (using the interface).
     *
     * The callback function will receive two parameters:
     *
     * - first argument: the current control's value.
     * - second argument: the previous control's value.
     *
     *
     *          //Remember, this is an abstract class so it shouldn't be instantiate,
     *          //anyway we are instantiating it just for this example
     *          var myControl = new PMUI.control.Control({
     *              name: "phrase",
     *              value: "Sometimes the sun goes down!",
     *              disabled: false
     *          });
     *
     *          myControl.setOnChangeHandler(function(currentValue, previousValue) {
     *              console.log(this.name + " has changed!");
     *              console.log("Its previous value was: " + previousValue);
     *              console.log("Its current value is: " + currentValue);
     *          });
     *
     * @chainable
     */
    Control.prototype.setOnChangeHandler = function (handler) {
        if (typeof handler === 'function') {
            this.onChange = handler;
        }

        return this;
    };
    /**
     * Returns the control's value directly from the control's HTML element.
     *
     * This method is used internally by the object, so in most of the cases you won't need to invocated.
     * To get the control's value please use the {@link PMUI.control.Control#getValue getValue()} method.
     *
     * Since this is an abstract method, it must be implemented in its non-abstract subclasses
     * @return {String}
     * @abstract
     */
    Control.prototype.getValueFromRawElement = function () {
        throw new Error("Calling getValueFromRawElement() from PMUI.control.Control: this is an abstract method!");
    };
    /**
     * A method which is called everytime the control changes.
     *
     * This method is used internally by the object, so in most of the cases you won't need to invocated.
     * To execute instructions when the control changes, please use the
     {@link PMUI.control.Control#setOnChangeHandler setOnChangeHandler()} method.
     *
     * @chainable
     * @private
     */
    Control.prototype.onChangeHandler = function () {
        var prevValue = this.value, newValue = this.getValueFromRawElement(), resCallback;
        if (typeof this.onBeforeChange === 'function' && newValue !== prevValue) {
            resCallback = this.onBeforeChange(newValue, prevValue);
            if (resCallback === false) {
                newValue = prevValue;
                this.setValue(newValue);
            }
        }
        this.value = newValue;
        if (typeof this.onChange === 'function' && this.value !== prevValue) {
            this.onChange(this.value, prevValue);
        }

        return this;
    };
    /**
     * Attach the event listeners for the control's HTML element.
     *
     * Since this is an abstract method, it must be implemented in its non-abstract subclasses
     * @abstract
     * @chainable
     */
    Control.prototype.attachListeners = function () {
        throw new Error("Calling attachListeners() from PMUI.control.Control: this is an abstract method!");
    };
    /**
     * Creates the HTML element for the control.
     *
     * Since this is an abstract method, it must be implemented in its non-abstract subclasses
     * @return {HTMLElement}
     * @abstract
     */
    Control.prototype.createHTML = function () {
        Control.superclass.prototype.createHTML.call(this);
        return this.html;
    };
    /**
     * Returns the HTML element for the control
     * @return {HTMLElement}
     */
    Control.prototype.getHTML = function () {
        if (!this.html) {
            this.html = this.createHTML();
        }

        return this.html;
    };
    /**
     * @method setFocus
     * set the focus on control
     * @chainable
     */
    Control.prototype.setFocus = function () {
        if (this.html && this.html.focus) {
            this.html.focus();
        }
    };

    PMUI.extendNamespace('PMUI.control.Control', Control);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Control;
    }
}());