//***********************other class******************************************************//
describe('PMUI.field.DateTimeField', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.field.DateTimeField();
		document.body.appendChild(a.getHTML());
		a.defineEvents();
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-39,a]should be able to create and edit a label and a helper', function () {
			expect(a.label).toBeDefined();
			expect(a.label).not.toBeNull();
			a.setLabel("Calendar");
			expect(a.label === "Calendar").toBeTruthy();
			expect(a.helper).toBeDefined();
			expect(a.helper).not.toBeNull();
			a.setHelper("help_calendar");
			expect(a.helper.message === "help_calendar").toBeTruthy();
		});
	});
	describe('method "setReturnFormat"', function () {
		it('should set the return format value of a datetime field', function () {
			expect(a.returnFormat).toBeDefined();
			expect(a.returnFormat === "UTC").toBeTruthy();
			a.setReturnFormat("newReturnFormat");
			expect(a.returnFormat === "newReturnFormat").toBeTruthy();
		});
	});
	describe('method "setValueToControls"', function () {
		it('should set the value to the controls', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValueToControls(d);
			expect(a.getValue() === "1983-07-21T01:15:00-04:00").toBeTruthy();
		});	
	});
	describe('method "setValue"', function () {
		it('should set the value for the control', function () {
			d = new Date();
			a.setValue(d);
			expect(a.value).toBeDefined();
			//expect(a.value === d).toBeTruthy();
			a.setValue(1385123462);
			expect(a.value).toBeDefined();
			a.setValue("2013-11-18");
			expect(a.value).toBeDefined();
		});
	});
	describe('method "setFirstDay"', function () {
		it('should set the first day of the week', function () {
			expect(a.controls[0].firstDay === 0).toBeTruthy();
			a.setFirstDay(1);
			expect(a.controls[0].firstDay === 1).toBeTruthy();
		});
	});
	describe('method "setDateFormat"', function () {
		it('should set the date format of the field', function () {
			expect(a.controls[0].dateFormat === 'yy-mm-dd').toBeTruthy();
			a.setDateFormat('dd-mm-yy');
			expect(a.controls[0].dateFormat === 'dd-mm-yy').toBeTruthy();
		});
	});
	describe('method "setMinDate"', function () {
		it('should set the minimun date', function () {
			d = new Date("July 21, 2009 01:15:00");
			expect(a.controls[0].minDate).toBeDefined();
			//expect(a.controls[0].minDate.toDateString() === "Sun Nov 25 2012").toBeTruthy();
			a.setMinDate(d);
			expect(a.controls[0].minDate).toBeDefined();
			expect(a.controls[0].minDate.toDateString() === "Tue Jul 21 2009").toBeTruthy();
		});
	});
	describe('method "setMaxDate"', function () {
		it('should set the maximum date', function () {
			d = new Date("July 21, 2015 01:15:00");
			expect(a.controls[0].maxDate).toBeDefined();
			//expect(a.controls[0].maxDate.toDateString() === "Tue Nov 25 2014").toBeTruthy();
			a.setMaxDate(d);
			expect(a.controls[0].maxDate).toBeDefined();
			expect(a.controls[0].maxDate.toDateString() === "Tue Jul 21 2015").toBeTruthy();
		});
	});
	describe('method "setMonths"', function () {
		it('should set the name and shortnames of the months', function () {
			var m = {
             "january": {
                 name: "Enero",
                 shortname: "Ene"
             },
             "february": {
                 name: "Febrero",
                 shortname: "Feb"
             },
             "march": {
                 name: "Marzo",
                 shortname: "Mar"
             },
             "april": {
                 name: "Abril",
                 shortname: "Abr"
             },
             "may": "May",
             "june": "Junio",
             "july": "July",
             "august": "Agosto",
             "september": "Septiembre",
             "october": "Octubre",
             "november": "Noviembre",
             "december": "Diciembre"
         	};
        	a.setMonths(m);
        	expect(a.controls[0].months.february.name === "Febrero").toBeTruthy();
        	expect(a.controls[0].months.february.shortname === "Feb").toBeTruthy();
        	expect(a.controls[0].months.march.name === "Marzo").toBeTruthy();
        	expect(a.controls[0].months.march.shortname === "Mar").toBeTruthy();
		});
	});
	describe('method "setDays"', function () {
		it('should set the name and shortnames of the days', function () {
			c = {
             "sunday": {
                 name: "Domingo",
                 shortname: "Do"
             },
             "monday": {
                 name: "Lunes",
                 shortname: "Lu"
             },
             "tuesday": {
                 name: "Martes",
                 shortname: "Ma"
             },
             "wednesday": {
                 name: "Miércoles",
                 shortname: "Mi"
             },
             "thursday": {
                 name: "Jueves",
                 shortname: "Ju"
             },
             "friday": "Viernes",
             "saturday": "Sábado"
         };
         	a.setDays(c)
         	expect(a.controls[0].days.sunday.name === "Domingo").toBeTruthy();
         	expect(a.controls[0].days.sunday.shortname === "Do").toBeTruthy();
         	expect(a.controls[0].days.tuesday.name === "Martes").toBeTruthy();
         	expect(a.controls[0].days.tuesday.shortname === "Ma").toBeTruthy();
		});
	});
	describe('method "getFormatedDate"', function () {
		it('should get the date using the format set by the programmer', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			a.setDateFormat('dd-mm-yy');
			expect(a.controls[0].getFormatedDate() === "21-07-1983").toBeTruthy();
		});
	});
	describe('method "getValue"', function () {
		it('should get the value from the datetime field', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			expect(a.controls[0].getValue()).toBeDefined();
			expect(a.controls[0].getValue() === "1983-07-21T01:15:00-04:00").toBeTruthy();
		});
	});
	describe('method "updateValueFromControls"', function () {
		it('should udpate the control´s date value', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			a.updateValueFromControls();
			expect(a.controls[0].getValue() === "1983-07-21T01:15:00-04:00").toBeTruthy();
		});
	});
});