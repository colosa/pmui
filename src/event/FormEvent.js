(function () {
    /**
     * @class PMUI.event.FormEvent
     * @extend PMUI.event.Event
     * Handles the events generated by the mouse
     *
     * @constructor
     * Create a new instance
     * @param {Object} options Constructor options
     *
     * @cfg {Object} element Defines the HTMLElement
     * @cfg {Function} handler Defines the callback Function to be executed
     * @cfg {PMUI.event.Action} action Defines an Action to be used to handle the callback
     * @cfg {String} name Event name
     */
    var FormEvent = function (options) {
        FormEvent.superclass.call(this, options);
    };

    PMUI.inheritFrom('PMUI.event.Event', FormEvent);

    /**
     * Defines the object's type
     * @type {String}
     */
    FormEvent.prototype.type = "FormEvent";

    PMUI.extendNamespace('PMUI.event.FormEvent', FormEvent);

    if (typeof exports !== 'undefined') {
        module.exports = FormEvent;
    }

}());