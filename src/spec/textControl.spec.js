//***********************other class******************************************************//
describe('PMUI.control.TextControl', function () {
	var a, b, c;
	describe('class behavior', function () {
		it('[US-18,a] should set a placeholder to a TextControl', function () {
			a = new PMUI.control.TextControl();
			expect(a.placeholder === "").toBeTruthy();
			a.setPlaceholder("new_placeholder");
			expect(a.placeholder === "new_placeholder").toBeTruthy();
		});
		it('[US-18,b] should set the maximal length to a TextControl', function () {
			a = new PMUI.control.TextControl();
			expect(a.maxLength === 524288).toBeTruthy();
			a.setMaxLength(678900);
			expect(a.maxLength === 678900).toBeTruthy();
		});	
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			a = new PMUI.control.TextControl();
			var h = a.createHTML();
            expect(h).toBeDefined();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});