(function() {
    var DataControlDropBehavior = function(settings) {
        DataControlDropBehavior.superclass.call(this, settings);
        this.targetObject = null;
        this.scope = null;
        this.hoverClass = null;
        DataControlDropBehavior.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.DropBehavior', DataControlDropBehavior);

    DataControlDropBehavior.prototype.init = function(settings) {
        var defaults = {
            scope: 'pmui-datacontrol-dragdrop-scope',
            hoverClass: 'pmui-dataControl-drag-over'
        };

        jQuery.extend(true, defaults, settings);

        if(!defaults.targetObject) {
            throw new Error("The target object wasn't specified.");
        }

        this.targetObject = defaults.targetObject;
        this.hoverClass = defaults.hoverClass;
        this.setScope(defaults.scope);
    };

    DataControlDropBehavior.prototype.setScope = function(scope) {
        if(typeof scope === 'string') {
            this.scope = scope;
        } else {
            throw new Error("setScope(): the parameter must be a string.");
        }

        return this;
    };

    DataControlDropBehavior.prototype.attachDropBehavior = function() {
        var html = this.targetObject.getHTML();

        $(html).droppable({
            greedy: true,
            scope: this.scope,
            hoverClass: this.hoverClass,
            drop: this.onDrop(),
            over: this.onDragEnter(),
            out: this.onDragLeave()
        });

        return this;
    };

    DataControlDropBehavior.prototype.onDragLeave = function() {
        return (this.targetObject.onDragLeaveHandler && this.targetObject.onDragLeaveHandler()) || null;
    };

    DataControlDropBehavior.prototype.onDragEnter = function() {
        return (this.targetObject.onDragEnterHandler && this.targetObject.onDragEnterHandler()) || null;
    };

    DataControlDropBehavior.prototype.onDrop = function() {
        var targetObject = this.targetObject;
        return function(e, ui) {
            var data = $(ui.draggable).data('pmui-datacontrol-data');
            data.currentContainer.removeItem(data.index);
            ui.helper.remove();
            targetObject.addItem(data.data);
        };
    };

    PMUI.extendNamespace('PMUI.behavior.DataControlDropBehavior', DataControlDropBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = DataControlDropBehavior;
    }
}());