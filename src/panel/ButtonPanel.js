(function () {
    /**
     * @class PMUI.panel.ButtonPanel
     * A panel that contains instances of {@link PMUI.ui.Button Button} and instances of {@link PMUI.ui.TextLabel Label}.
     * @extends {PMUI.core.Panel}
     *
     * Usage example:
     *
     *      @example
     *      var a = new PMUI.panel.ButtonPanel({
     *          items: [
     *              {
     *                  pmType: 'button',
     *                  text: 'Click Me'
     *              },
     *              {
     *                  pmType: 'label',
     *                  text: 'and then'
     *              },
     *              {
     *                  pmType: 'button',
     *                  text: 'Click Me'
     *              }
     *          ]
     *      });
     *      document.body.appendChild(a.getHTML());
     *      a.defineEvents();
     *
     * @constructor
     * Creates a new instance of the class.
     * @param {Object} [settings={}] An object literal with the config options for the class.
     *
     * @cfg {Array} [items=[]] An array in which each element can be:
     *
     * - An object literal, in this case it can have the config options for create a {@link PMUI.ui.Button Button} or a
     * {@link PMUI.ui.TextLabel Label}, additionally it must include the respective pmType ('button' for Button and 'label'
     * for Label).
     *
     * -A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of {@link
        * PMUI.ui.TextLabel Label}.
     */
    var ButtonPanel = function (settings) {
        ButtonPanel.superclass.call(this, settings);
        this.alignment = null;
        ButtonPanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Panel', ButtonPanel);
    /**
     * The object's type.
     * @type {String}
     */
    ButtonPanel.prototype.type = 'ButtonPanel';
    /**
     * Initializes the object.
     * @param  {Object} [settings={}] An object literal with the config options the new object will be initialized
     * with.
     * @chainable
     * @private
     */
    ButtonPanel.prototype.init = function (settings) {
        var defaults = {
            items: [],
            factory: {
                products: {
                    'button': PMUI.ui.Button,
                    'label': PMUI.ui.TextLabel
                },
                defaultProduct: 'button'
            },
            layout: 'box',
            alignment: 'center'
        };

        jQuery.extend(true, defaults, settings);

        this.setFactory(defaults.factory)
            .setLayout(defaults.layout)
            .setItems(defaults.items)
            .setAlignment(defaults.alignment);

        return this;
    };
    /**
     * Sets the alignment for the items in the panel.
     * @param {String} alignment A string which can take the values of 'center', 'left', 'right'.
     */
    ButtonPanel.prototype.setAlignment = function (alignment) {
        if (alignment === 'center' || alignment === 'left' || alignment === 'right') {
            this.alignment = alignment;
            this.style.addProperties({'text-align': alignment});
            return this;
        }
        throw new Error('setAlignment(): The parameter must be "center" or "left" or "right".');
    };

    PMUI.extendNamespace('PMUI.panel.ButtonPanel', ButtonPanel);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ButtonPanel;
    }
}());