(function () {
    /**
     * @class PMUI.field.TextField
     * @extends PMUI.form.Field
     * Class to handle a {@link PMUI.control.TextControl} field.
     *
     * Usage example:
     *
     *      @example
     *          var a;
     *          $(function() {
     *              a = new PMUI.field.TextField({
     *                  label: "Some Text",
     *                  placeholder: 'a text here',
     *                  required: true,
     *                  helper: "Introduce a text (6 chars. max.)",
     *                  validators: [
     *                      {
     *                          pmType: 'textLength',
     *                          criteria: {
     *                              maxLength: 6
     *                          }
     *                      }
     *                  ]
     *              });
     *              document.body.appendChild(a.getHTML());
     *              a.defineEvents();
     *          });
     *
     *
     * The code above will generate a required TextField with "Some Text" as label,
     * the control will have a placeholder with the text "a text here".
     * Also the field will contain a helper with the text "Introduce a text (6 chars. max.)"
     * and a validator that will control the maximum length for the text to be entered.
     *
     * @cfg {String} [placeholder=""] The text to show as placeholder for the field's control
     * @cfg {Number} [maxLength=0] An integer that specifies the maximum character length for the text to be entered.
     * 0 means no max length.
     *
     * #Note: This setting doesn't act as a validator, it simply set the max character length
     * for the field's control.
     * @cfg {Boolean} [trimOnBlur=true] A boolean that specifies if the value entered will be trimmed
     * when the field loses focus.
     */
    var TextField = function (settings) {
        TextField.superclass.call(this, settings);
        /**
         * @property {Boolean} [trimOnBlur=true] If the field's value must be trimmed every time it loses focus.
         */
        this.trimOnBlur = null;
        TextField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', TextField);

    TextField.prototype.type = "TextField";

    TextField.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 0,
            trimOnBlur: true,
            readOnly: false
        };

        $.extend(true, defaults, settings);

        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setTrimOnBlur(defaults.trimOnBlur)
            .setReadOnly(defaults.readOnly)
            .setDependentFields(this.dependentFields);
    };
    /**
     * Sets the value for the field.
     * @param {String} value
     * @chainable
     */
    TextField.prototype.setValue = function (value) {
        if (this.trimOnBlur) {
            value = jQuery.trim(value);
        }
        TextField.superclass.prototype.setValue.call(this, value);

        return this;
    };
    /**
     * Sets the placeholder for the control. Note that this feature is only supported
     * by browsers which support the "placeholder" for input elements.
     * @param {String} placeholder
     * @chainable
     */
    TextField.prototype.setReadOnly = function (readonly) {
        this.controls[0].setReadOnly(readonly);
        return this;
    };
    /**
     * [setPlaceholder description]
     * @param {[type]} placeholder [description]
     */
    TextField.prototype.setPlaceholder = function (placeholder) {
        this.controls[0].setPlaceholder(placeholder);
        return this;
    };
    /**
     * [getPlaceholder description]
     * @return {[type]} [description]
     */
    TextField.prototype.getPlaceholder = function () {
        return this.controls[0].getPlaceholder();
    };
    /**
     * [setMaxLength description]
     * @param {[type]} maxLength [description]
     */
    TextField.prototype.setMaxLength = function (maxLength) {
        this.controls[0].setMaxLength(maxLength);
        return this;
    };
    /**
     * Set the parameter disabled for the control.
     * @param {[disabled]} value 
     * @chainable
     */
     TextField.prototype.setDisabled = function (value) {
        if (typeof value === 'boolean') {
            this.controls[0].setDisabled(value);
        }
        return this;
    };
    /**
     * [getMaxLength description]
     * @return {[type]} [description]
     */
    TextField.prototype.getMaxLength = function () {
        return this.controls[0].getMaxLength();
    };
    /**
     * [isReadOnly description]
     * @return {Boolean} [description]
     */
    TextField.prototype.isReadOnly = function () {
        return this.controls[0].isReadOnly();
    };
    /**
     * Switches on/off the value trimming for the field's value when it loses focus.
     * @param {Boolean} trimOnBlur Send true for switch on or false to switch off.
     * @chainable
     */
    TextField.prototype.setTrimOnBlur = function (trimOnBlur) {
        this.trimOnBlur = !!trimOnBlur;
        return this;
    };
    /**
     * Returns a boolean value that indicates if the trimming function is enabled/disabled
     * @return {Boolean}
     */
    TextField.prototype.getTrimOnBlur = function () {
        return this.trimOnBlur;
    };
    /**
     * Sets the control for the TextField
     * @chainable
     * @private
     */
    TextField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }

        this.controls.push(new PMUI.control.TextControl());

        return this;
    };
    /**
     * Establish the handler function for the internal onChange event
     * @return {Function}
     * @private
     */
    TextField.prototype.onChangeHandler = function () {
        var that = this,
            i,
            dependentFields = this.dependentFields,
            form = this.form;

        return function () {
            var previousValue = that.value,
                value;

            if (that.trimOnBlur) {
                value = that.controls[0].getValue();
                value = jQuery.trim(value);
                that.controls[0].setValue(value);
            }
            that.updateValueFromControls();
            if (that.validAtChange) {
                that.isValid();
            }
            if (typeof that.onChange === 'function') {
                that.onChange(that.getValue(), previousValue);
            }
            if (that.form) {
                (that.form.onChangeHandler())(that, that.getValue(), previousValue);
            } else if (that.parent && that.parent.form) {
                (that.parent.form.onChangeHandler())(that, that.getValue(), previousValue);
            } else if (that.parent.parent && that.parent.parent.form) {
                (that.parent.parent.form.onChangeHandler())(that, that.getValue(), previousValue);
            } else if (that.parent.parent.parent && that.parent.parent.parent.form) {
                (that.parent.parent.parent.form.onChangeHandler())(that, that.getValue(), previousValue);
            } else {
                return;
            }
            if (that.form) {
                for (i = 0; i < dependentFields.length; i += 1) {
                    dependentField = form.getField(dependentFields[i]);
                    if (!dependentField) {
                        that.removeDependentField(dependentFields[i]);
                        continue;
                    }
                    dependentField.fireDependencyHandler();
                }
            }
        };
    };

    PMUI.extendNamespace('PMUI.field.TextField', TextField);

    if (typeof exports !== "undefined") {
        module.exports = TextField;
    }
}());