(function () {
    /**
     * @class PMUI.panel.List
     */
    var ListPanel = function (settings) {
        this.dom = {};
        ListPanel.superclass.call(this, settings);
        this.orientation = null;
        this.actions = null;
        this.dataItems = null;
        this.filterable = null;
        this.filterCriteria = null;
        this.filterControl = null;
        this.filterPlaceholder = null;
        this.statusBarMessage = null;
        this.visibleHeader = null;
        this.visibleStatusBar = null;
        this.listHeight = null;
        this.onItemClick = null;
        this.filteredItems = null;
        ListPanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Container', ListPanel);

    ListPanel.prototype.type = "ListPanel";

    ListPanel.prototype.init = function (settings) {
        var defaults = {
            orientation: 'vertical',
            actions: [],
            dataItems: null,
            itemTextDataBind: null,
            statusBarMessage: null,
            title: "[untitled list]",
            filterPlaceholder: "Search",
            visibleHeader: true,
            visibleStatusBar: true,
            filterable: true,
            listHeight: "auto",
            onItemClick: null
        };
        this.filteredItems = [];

        jQuery.extend(true, defaults, settings);

        this.filterControl = new PMUI.control.TextControl({
            onKeyUp: this.onFilterControlChangeHandler(),
            style: {
                cssClasses: [
                    "pmui-listpanel-search"
                ]
            }
        });

        this.setItemTextDataBind(defaults.itemTextDataBind)
            .setOrientation(defaults.orientation)
            .setElementTag(defaults.elementTag)
            .setActions(defaults.actions)
            .setStatusBarMessage(defaults.statusBarMessage)
            .setFilterPlaceholder(defaults.filterPlaceholder)
            .setTitle(defaults.title)
            .setListHeight(defaults.listHeight)
            .setOnItemClickHandler(defaults.onItemClick);

        if (defaults.filterable) {
            this.enableFiltering();
        } else {
            this.disableFiltering();
        }

        if (defaults.visibleHeader) {
            this.showHeader();
        } else {
            this.hideHeader();
        }

        if (defaults.visibleStatusBar) {
            this.showStatusBar();
        } else {
            this.hideStatusBar();
        }
    };

    ListPanel.prototype.onItemClickHandler = function (listItem) {
        if (typeof this.onItemClick === 'function') {
            this.onItemClick(this, listItem);
        }
        return this;
    };

    ListPanel.prototype.setOnItemClickHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnItemClickHandler(): The parameter must be a function or a handler.");
        }
        this.onItemClick = handler;
        return this;
    };

    ListPanel.prototype.setListHeight = function (height) {
        if (typeof height === 'number') {
            this.listHeight = height;
        } else if (/^\d+(\.\d+)?px$/.test(height)) {
            this.listHeight = parseInt(height, 10);
        } else if (/^\d+(\.\d+)?%$/.test(height)) {
            this.listHeight = height;
        } else if (/^\d+(\.\d+)?em$/.test(height)) {
            this.listHeight = height;
        } else if (height === 'auto' || height === 'inherit') {
            this.listHeight = height;
        } else {
            throw new Error('setHeight: height param is not a number');
        }
        if (this.dom.list) {
            this.dom.list.style.height = height + (typeof height === 'number' ? 'px' : '');
        }
        return this;
    };

    ListPanel.prototype.hideStatusBar = function () {
        this.visibleStatusBar = false;
        if (this.dom.statusBar) {
            this.dom.statusBar.style.display = 'none';
        }
        return this;
    };

    ListPanel.prototype.showStatusBar = function () {
        this.visibleStatusBar = true;
        if (this.dom.statusBar) {
            this.dom.statusBar.style.display = '';
        }
        return this;
    };

    ListPanel.prototype.showHeader = function () {
        this.visibleHeader = true;
        if (this.dom.header) {
            this.dom.header.style.display = '';
        }
        return this;
    };

    ListPanel.prototype.hideHeader = function () {
        this.visibleHeader = false;
        if (this.dom.header) {
            this.dom.header.style.display = 'none';
        }
        return this;
    };

    ListPanel.prototype.enableFiltering = function () {
        this.filterable = true;
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = '';
        }
        return this;
    };

    ListPanel.prototype.disableFiltering = function () {
        this.filterable = false;
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = 'none';
        }
        this.clearFilter();
        return this;
    };

    ListPanel.prototype.setTitle = function (title) {
        if (typeof title !== 'string') {
            throw new Error("setTitle(): The parameter must be a string.");
        }
        this.title = title;
        if (this.html) {
            this.dom.header.textContent = title;
        }
        return this;
    };

    ListPanel.prototype.setFilterPlaceholder = function (filterPlaceholder) {
        this.filterControl.setPlaceholder(filterPlaceholder);
        return this;
    };

    ListPanel.prototype.onFilterControlChangeHandler = function () {
        var that = this;
        return function () {
            var nextFilter = this.getHTML().value;
            if (that.filterCriteria !== nextFilter) {
                that.filter(nextFilter);
            }
        };
    };

    ListPanel.prototype.setStatusBarMessage = function (statusBarMessage) {
        if (!(statusBarMessage === null || typeof statusBarMessage === 'function')) {
            throw new Error("setStatusBarMessage(): The parameter must be a function or null.")
        }
        this.statusBarMessage = statusBarMessage;
        return this;
    };

    ListPanel.prototype.setItemTextDataBind = function (itemTextDataBind) {
        var i,
            size;

        if (!(itemTextDataBind === null || typeof itemTextDataBind === 'string')) {
            throw new Error("setItemTextDataBind(): the parameter must be a string or null.");
        }
        this.itemTextDataBind = itemTextDataBind;
        size = this.items.getSize();
        for (i = 0; i < size; i += 1) {
            this.items.get(i).setTextDataBind(itemTextDataBind);
        }
        return this;
    };

    ListPanel.prototype.setActions = function (actions) {
        if (!jQuery.isArray(actions)) {
            throw new Error("setActions(): The parameter must be an array.");
        }
        this.actions = actions;
        return this;
    };

    ListPanel.prototype.addItem = function (item) {
        if (item instanceof PMUI.item.ListItem) {
            item.setActions(this.actions).setTextDataBind(this.itemTextDataBind);
        } else if (!item.actions) {
            item.actions = this.actions;
            item.setTextDataBind = this.itemTextDataBind;
        }

        ListPanel.superclass.prototype.addItem.call(this, item);

        return this;
    };

    ListPanel.prototype.setItems = function (items) {
        if (this.actions !== null) {
            ListPanel.superclass.prototype.setItems.call(this, items);
            this.updateStatusBar();
        }
        return this;
    };

    ListPanel.prototype.updateStatusBar = function () {
        var msg,
            size;

        if (this.dom.statusBar) {
            size = this.filterCriteria ? jQuery(this.containmentArea).find('>li').length : this.items.getSize();
            if (typeof this.statusBarMessage === 'function') {
                msg = this.statusBarMessage(this, size, !!this.filterCriteria, this.filterCriteria);
            } else {
                msg = size + (this.filterCriteria ? " result(s) matching \"" + this.filterCriteria + "\"" : " item(s).");
            }
            this.dom.statusBar.textContent = msg;
        }
        return this;
    };

    ListPanel.prototype.setOrientation = function (orientation) {
        var items,
            displayBlock,
            i;

        if (orientation !== 'horizontal' && orientation !== 'vertical') {
            throw new Error("setOrientation(): The parameter must be \"horizontal\" or \"vertical\"");
        }
        this.orientation = orientation;
        displayBlock = orientation === 'vertical' ? 'block' : 'inline-block';
        items = this.items.asArray();
        for (i = 0; i < items.length; i += 1) {
            items[i].setDisplay(displayBlock);
        }
        return this;
    };

    ListPanel.prototype.setFactory = function () {
        this.factory = new PMUI.util.Factory({
            products: {
                'listitem': PMUI.item.ListItem
            },
            defaultProduct: 'listitem'
        });
        return this;
    };

    ListPanel.prototype.getData = function () {
        var i,
            items = this.items.asArray(),
            data = [];

        for (i = 0; i < items.length; i += 1) {
            data.push(items[i].getData());
        }

        return data;
    };

    ListPanel.prototype.clearFilter = function () {
        var i,
            items = this.items.asArray();

        this.filterCriteria = "";
        if (this.html) {
            for (i = 0; i < items.length; i += 1) {
                jQuery(items[i].html).detach();
            }
            for (i = 0; i < items.length; i += 1) {
                this.containmentArea.appendChild(items[i].getHTML());
            }
        }
        this.updateStatusBar();
        return this;
    };

    ListPanel.prototype.getFilteredItems = function () {
        return this.filteredItems;
    };
    ListPanel.prototype.filter = function (criteria) {
        var i,
            regExp,
            itemsCopy;

        this.filteredItems = [];
        if (typeof criteria === 'string') {
            if (criteria === "") {
                this.clearFilter();
                return this;
            }
            this.filterCriteria = criteria;
        } else if (typeof criteria === 'number') {
            this.filterCriteria = criteria.toString(10);
        } else {
            throw new Error("filter(): The parameter must be a string or number");
        }

        if (!this.containmentArea) {
            return this;
        }

        regExp = new RegExp(this.filterCriteria.replace(/([\\\.\[\]\^\$\(\)\?\*\+\|\{\}])/g, "\\\$1"), "i");
        itemsCopy = this.items.asArray();

        for (i = 0; i < itemsCopy.length; i += 1) {
            jQuery(itemsCopy[i].html).detach();
        }

        for (i = 0; i < itemsCopy.length; i += 1) {
            if (regExp.test(itemsCopy[i].text)) {
                this.containmentArea.appendChild(itemsCopy[i].getHTML());
                this.filteredItems.push(itemsCopy[i]);
            }
        }

        this.updateStatusBar();
        return this;
    };

    ListPanel.prototype.defineEvents = function () {
        this.removeEvents().eventsDefined = true;
        this.filterControl.defineEvents();
        return this;
    };

    ListPanel.prototype.createHTML = function () {
        var list,
            toolbar,
            statusBar,
            header;

        if (this.html) {
            return this.html;
        }
        ListPanel.superclass.prototype.createHTML.call(this);

        list = PMUI.createHTMLElement('ul');
        list.className = 'pmui-listpanel-list';
        toolbar = PMUI.createHTMLElement('div');
        toolbar.className = 'pmui-listpanel-toolbar';
        statusBar = PMUI.createHTMLElement('div');
        statusBar.className = 'pmui-listpanel-statusbar';
        header = PMUI.createHTMLElement('div');
        header.className = 'pmui-listpanel-title';
        this.containmentArea = list;

        this.dom.list = list;
        this.dom.toolbar = toolbar;
        this.dom.statusBar = statusBar;
        this.dom.header = header;

        toolbar.appendChild(this.filterControl.getHTML());
        this.html.appendChild(header);
        this.html.appendChild(toolbar);
        this.html.appendChild(list);
        this.html.appendChild(statusBar);

        this.setTitle(this.title)
            .setListHeight(this.listHeight);

        if (this.filterable) {
            this.enableFiltering();
            this.filter(this.filterCriteria || "");
        } else {
            this.disableFiltering();
        }

        if (this.visibleHeader) {
            this.showHeader();
        } else {
            this.hideHeader();
        }

        if (this.visibleStatusBar) {
            this.showStatusBar();
        } else {
            this.hideStatusBar();
        }

        this.defineEvents();
        return this.html;
    };

    PMUI.extendNamespace('PMUI.panel.ListPanel', ListPanel);
}());