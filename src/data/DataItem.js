(function () {
    var DataItem = function (key, value) {
        this.id = PMUI.generateUniqueId();
        this.val = value;
        this.key = key;
    };

    DataItem.prototype.type = 'DataItem';

    DataItem.prototype.family = 'DataItem';

    DataItem.prototype.getKey = function () {
        return this.key;
    };

    DataItem.prototype.getValue = function () {
        return this.val;
    };

    DataItem.prototype.setKey = function (key) {
        this.key = key;
        return this;
    };

    DataItem.prototype.setValue = function (value) {
        this.val = value;
        return this;
    };

    DataItem.prototype.set = function (key, value) {
        this.key = key;
        this.val = value;
        return this;
    };

    DataItem.prototype.get = function () {
        var aux = {};
        aux[this.key] = this.val;
        return aux;
    };

    DataItem.prototype.getRecord = function () {
        return {
            key: this.key,
            value: this.val
        };
    };

    DataItem.prototype.clear = function () {
        this.val = null;
        this.key = null;

        return this;
    };

    PMUI.extendNamespace('PMUI.data.DataItem', DataItem);

    if (typeof exports !== 'undefined') {
        module.exports = DataItem;
    }

}());