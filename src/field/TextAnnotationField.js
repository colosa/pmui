(function () {
    var TextAnnotationField = function (settings) {
        TextAnnotationField.superclass.call(this, settings);
        this.text = null;
        this._textType = null;
        this.text_Align = null;
        this._labelTag = null;
        TextAnnotationField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', TextAnnotationField);

    TextAnnotationField.prototype.type = "TextAnnotationField";

    TextAnnotationField.TEXT_TYPES = {
        Plain: 0,
        HTML: 1
    };

    TextAnnotationField.prototype.init = function (settings) {
        var defaults = {
            maxLength: 0,
            text: "",
            textType: TextAnnotationField.TEXT_TYPES.Plain,
            text_Align: "left"
        };

        $.extend(true, defaults, settings);
        this.setText(defaults.text);
        this.setTextType(defaults.textType);
        this.setText_Align(defaults.text_Align);
    };

    TextAnnotationField.prototype.setTextType = function (type) {
        switch (type) {
            case TextAnnotationField.TEXT_TYPES.Plain:
            case TextAnnotationField.TEXT_TYPES.HTML:
                this._textType = type;
                break;
            default:
                throw new Error("setTextType(): Invalid argument.");
        }
        this.setText(this.text);
        return this;
    };

    TextAnnotationField.prototype.setText = function (text) {
        if (typeof text !== "string") {
            throw new Error("setText(): The parameter is not valid");
        }
        this.text = text;
        if (this.html) {
            if (this._textType == TextAnnotationField.TEXT_TYPES.HTML) {
                jQuery(this._labelTag).html(this.text);
            } else {
                jQuery(this._labelTag).text(this.text);
            }
        }
        return this;
    };

    TextAnnotationField.prototype.setText_Align = function (value) {
        if (typeof value === "string") {
            this.text_Align = value;
        } else {
            throw new Error("the enter parameter is no valid");
        }
        return this;
    };

    TextAnnotationField.prototype.setValue = function () {
        return this;
    };

    TextAnnotationField.prototype.setControls = function () {
        return this;
    };

    TextAnnotationField.prototype.setHelper = function () {
        return this;
    };

    TextAnnotationField.prototype.createHTML = function () {
        var labelTag, textbefore, spanRequired, sp = "&nbsp;";
        if (this.html) {
            return this.html;
        }
        TextAnnotationField.superclass.superclass.prototype.createHTML.call(this);
        labelTag = PMUI.createHTMLElement('span');
        labelTag.className = 'pmui-field-label';
        labelTag.style.width = "100%";
        labelTag.style.textAlign = this.text_Align;
        this._labelTag = labelTag;
        this.html.appendChild(labelTag);

        this.setText(this.text);
        return this.html;
    };

    TextAnnotationField.prototype.isValid = function () {
        return true;
    };

    PMUI.extendNamespace('PMUI.field.TextAnnotationField', TextAnnotationField);

    if (typeof exports !== "undefined") {
        module.exports = TextAnnotationField;
    }

}());