//***********************other class******************************************************//
describe('PMUI.panel.AccordionPanel', function () {
    var a, b, c, e, j, p;
    beforeEach(function () {
        formPanel1 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });
                formPanel2 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });

                 formPanel3 = new PMUI.form.FormPanel({
                    width: 300, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here!",
                            name: "lastname"
                        }
                    ]
                });

                    accordion   = new PMUI.panel.AccordionPanel({
                        width: 400,
                        height: 500,
                        enableTitle: true,
                        //multipleSelection : true,
                        title: "My accordion",
                        items: [
                            {
                                iconClass: "",
                                title: "First item",
                                body: formPanel1,
                                selected: true
                            },
                            {
                                iconClass: "pmui-icon pmui-icon-warning",
                                title: "Second item",
                                body: "<a href=\"http://www.google.com\">Google Link</a>",
                                style: {
                                    cssProperties: {
                                        "background-color": "#f2eaea"
                                    }
                                }
                            },
                            {
                                title: "Third item <span class=\"classname-accordion\"></span>",
                                body: formPanel2
                            },
                            {
                                title: "Fourth item <span class=\"classname-accordion\"></span>",
                                body: formPanel3
                            }, new PMUI.item.AccordionItem({
                                title: "Five"
                             })
                        ]
                    });
                    document.body.appendChild(accordion.getHTML());
                    accordion.defineEvents();
    });
    afterEach(function () {
        $(accordion.getHTML()).remove();
        accordion = null;
        a = null;
        b = null;
        c = null;
        e = null;
        j = null;
    });
    describe('class behavior', function () {
        it('[US-46,a]should add an item, this must be an element object or a json', function () {
            expect($(accordion.html).find('div[class="pmui-accordion-item-container"]').size() === 5).toBeTruthy();
            p = new PMUI.form.FormPanel();
            j = {
                            item1: {
                                iconClass: "",
                                title: "First item"
                            }, item2:
                            {
                                iconClass: "",
                                title: "Second item"
                    
                            }, item3:
                            {
                                title: ""
                            }
                        };
            expect(function(){accordion.addItem(p);}).not.toThrow();
            expect($(accordion.html).find('div[class="pmui-accordion-item-container"]').size() === 6).toBeTruthy();
            expect(accordion.items.getLast().id === p.id).toBeTruthy();
            expect(function(){accordion.addItem(j);}).not.toThrow();
            expect($(accordion.html).find('div[class="pmui-accordion-item-container"]').size() === 7).toBeTruthy();
        });
    });
    describe('method "setChildren"', function () {
        it('should clear all children and add a new one', function () {
            expect($(accordion.html).find('div[class="pmui-accordion-item-container"]').size() === 5).toBeTruthy();
            p = new PMUI.form.FormPanel();
            accordion.setChildren([p]);
            expect(accordion.items.asArray().length === 1).toBeTruthy();
            expect($(accordion.html).find('div[class="pmui-accordion-item-container"]').size() === 1).toBeTruthy();
        });
    });
    describe('method "setMultipleSelection"', function () {
        it('should set the multiple selection property', function () {
            accordion.setMultipleSelection(true);
            expect(accordion.multipleSelection).toBeTruthy();
            a = accordion.items.asArray()[0];
            b = accordion.items.asArray()[1];
            c = accordion.items.asArray()[2];
            e = accordion.items.asArray()[3];
            j = accordion.items.asArray()[4];
            a.expand();
            b.expand();
            c.expand();
            j.expand();
            expect(a.collapsed).toBeFalsy();
            expect(b.collapsed).toBeFalsy();
            expect(c.collapsed).toBeFalsy();
            expect(j.collapsed).toBeFalsy();
            accordion.setMultipleSelection(false);
            expect(accordion.multipleSelection).toBeFalsy();
            a.expand();
            b.expand();
            c.expand();
            j.expand();
            expect(a.collapsed).toBeTruthy();
            expect(b.collapsed).toBeTruthy();
            expect(c.collapsed).toBeTruthy();
            expect(j.collapsed).toBeFalsy();
        });
    });
    describe('method "setEnableTitle"', function () {
        it('should set the Enable title property', function () {
            expect(accordion.enableTitle).toBeTruthy();
            expect($(accordion.getHTML()).find('b').text() === "My accordion").toBeTruthy();
            accordion.setEnableTitle(false);
            expect(accordion.enableTitle).toBeFalsy();
            expect($(accordion.getHTML()).find('b').text() === "My accordion").toBeFalsy();
        });
    });
    describe('method "setTitle"', function () {
        it('should set the accordion panel title', function () {
            expect(accordion.title === "My accordion").toBeDefined();
            expect($(accordion.getHTML()).find('b').text() === "My accordion").toBeTruthy();
            accordion.setTitle("new accordion");
            expect(accordion.title === "new accordion").toBeTruthy();
            expect($(accordion.getHTML()).find('b').text() === "new accordion").toBeTruthy();
        });
    });
    describe('method "setIconClass"', function () {
        it('should set the icon class for the accordion icon', function () {
            expect(accordion.iconClass).toBeDefined();
            expect(accordion.iconClass === "pmui-accordion-panel-icon").toBeTruthy();
            accordion.setIconClass("PMUI-accordion-icon2");
            expect(accordion.iconClass === "PMUI-accordion-icon2").toBeTruthy();
        });
    });
    describe('method "setHeaderClass"', function () {
        it('should set the header class used for css', function () {
            p = new PMUI.form.FormPanel();
            expect(accordion.headerClass === "pmui-accordion-panel-header").toBeTruthy();
            expect(function () {accordion.setHeaderClass(23);}).not.toThrow();
            expect(function () {accordion.setHeaderClass(p);}).not.toThrow();
            expect(function () {accordion.setHeaderClass("pmui-accordion-panel-header2");}).not.toThrow();
            expect(accordion.headerClass === "pmui-accordion-panel-header2").toBeTruthy();
        });
    });
    describe('method "setBodyClass"', function () {
        it('should set the body class used for css', function () {
            p = new PMUI.form.FormPanel();
            expect(accordion.bodyClass === "pmui-accordion-panel-body").toBeTruthy();
            expect(function () {accordion.setBodyClass(23);}).not.toThrow();
            expect(function () {accordion.setBodyClass(p);}).not.toThrow();
            expect(function () {accordion.setBodyClass("pmui-accordion-panel-body2");}).not.toThrow();
            expect(accordion.bodyClass === "pmui-accordion-panel-body2").toBeTruthy();
        });
    });
    describe('method "setFooterClass"', function () {
        it('should set the footer class used for css', function () {
            p = new PMUI.form.FormPanel();
            expect(accordion.footerClass === "pmui-accordion-panel-footer").toBeTruthy();
            expect(function () {accordion.setFooterClass(23);}).not.toThrow();
            expect(function () {accordion.setFooterClass(p);}).not.toThrow();
            expect(function () {accordion.setFooterClass("pmui-accordion-panel-footer2");}).not.toThrow();
            expect(accordion.footerClass === "pmui-accordion-panel-footer2").toBeTruthy();
        });
    });
    describe('method "setContainerClass"', function () {
        it('should set the container class used for css', function () {
            p = new PMUI.form.FormPanel();
            expect(accordion.containerClass === "pmui-accordion-panel-container").toBeTruthy();
            expect(function () {accordion.setContainerClass(23);}).not.toThrow();
            expect(function () {accordion.setContainerClass(p);}).not.toThrow();
            expect(function () {accordion.setContainerClass("pmui-accordion-panel-container2");}).not.toThrow();
            expect(accordion.containerClass === "pmui-accordion-panel-container2").toBeTruthy();
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = accordion.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
});