(function () {
    /**
     * @class  PMUI.control.DateTimeControl
     * A control to handle dates and times.
     * @extends {PMUI.control.HTMLControl}
     *
     *
     * Usage example:
     *
     *      @example
     *      var dateTimePicker;
     *
     *      $(function() {
     *          dateTimePicker = new PMUI.control.DateTimeControl({
     *              dateFormat: 'M dd yy',
     *              minDate: -90,
     *              maxDate: "+1y -1m -4d",
     *              firstDay: 1,
     *              months: {
     *                  "january": {
     *                      name: "Enero",
     *                      shortname: "Ene"
     *                  },
     *                  "february": {
     *                      name: "Febrero",
     *                      shortname: "Feb"
     *                  },
     *                  "march": {
     *                      name: "Marzo",
     *                      shortname: "Mar"
     *                  },
     *                  "april": {
     *                      name: "Abril",
     *                      shortname: "Abr"
     *                  },
     *                  "may": "May",
     *                  "june": "Junio",
     *                  "july": "July",
     *                  "august": "Agosto",
     *                  "september": "Septiembre",
     *                  "october": "Octubre",
     *                  "november": "Noviembre",
     *                  "december": "Diciembre"
     *              },
     *              days: {
     *                  "sunday": {
     *                      name: "Domingo",
     *                      shortname: "Do"
     *                  },
     *                  "monday": {
     *                      name: "Lunes",
     *                      shortname: "Lu"
     *                  },
     *                  "tuesday": {
     *                      name: "Martes",
     *                      shortname: "Ma"
     *                  },
     *                  "wednesday": {
     *                      name: "Miércoles",
     *                      shortname: "Mi"
     *                  },
     *                  "thursday": {
     *                      name: "Jueves",
     *                      shortname: "Ju"
     *                  },
     *                  "friday": "Viernes",
     *                  "saturday": "Sábado"
     *              }
     *          });
     *          document.body.appendChild(dateTimePicker.getHTML());
     *          dateTimePicker.defineEvents();
     *      });
     *
     * @cfg {Boolean} [datetime=false] If the control will have time supporting.
     * @cfg {String} [dateFormat="yy-mm-dd HH:ii:ss"|"yy-mm-dd"] The format for the date to show in the text box.
     * It defaults to:
     *
     * - "yy-mm-dd HH:ii:ss" if {@link PMUI.control.DateTimeControl#cfg-datetime datetime} is set to true.
     * - "yy-mm-dd" if {@link PMUI.control.DateTimeControl#cfg-datetime datetime} is set to false.
     *
     * You can set a customized date format using the following wildcards:
     *
     * - d, day of month (no leading zero).
     * - dd, day of month (two digit).
     * - o, day of the year (no leading zeros).
     * - oo, day of the year (three digit).
     * - D, day name short.
     * - DD, day name long.
     * - m, month of year (no leading zero).
     * - mm, month of year (two digit).
     * - M, month name short.
     * - MM, month name long.
     * - y, year (two digit).
     * - yy, year (four digit).
     * - P, period (AM or PM).
     * - H, hours (0-23).
     * - HH, hours (00-23).
     * - h, hours (1-12).
     * - hh, hours (01-12).
     * - i, minutes (0-59).
     * - ii, minutes (00-59).
     * - s, seconds (0-59).
     * - ss, seconds (00-59).
     * - @, Unix timestamp (ms since 01/01/1970).
     * - !, Windows ticks (100ns since 01/01/0001).
     * - '...', literal text.
     * - '', single quote.
     * - anything else, literal text.
     *
     * The wildcards can be used together in the same string.
     *
     * @cfg {Object} [months={"january": "January", "february": "February", "march": "March", "april": "April",
     * "may": "May", "june": "June", "july": "July", "august": "August", "september": "September",
     * "october": "October", "november": "November", "december": "December"}]
     * A JSON object to set the names and shortnames for every month in year. Each property of this object can be:
     *
     * - A string, in this case the name for the month is set to this string, and the shortname is set using
     * the first 3 characters of the string.
     * - A JSON object, in this case the JSON may have two properties:
     *     - "name", will be used as the name for the month.
     *     - "shortname", will be used as the shortname for the month, if it is not specified the the shortname for
     *     the month will be set using the first 3 characters of the object's "name" property.
     *
     * @cfg {Object} [days={"sunday": "Sunday","monday": "Monday","tuesday": "Tuesday","wednesday": "Wednesday",
     * "thursday": "Thursday","friday": "Friday","saturday": "Saturday"}]
     * A JSON object to set the name and shortname for every day of week. Each property of this object can be:
     *
     * - A string, in this case the name for the day is set to this string, and the shortname is set using
     * the first 3 characters of the string.
     * - A JSON object, in this case the JSON may have two properties:
     *     - "name", will be used as the name for the day.
     *     - "shortname", will be used as the shortname for the day, if it is not specified the the shortname for
     *     the day will be set using the first 3 characters of the object's "name" property.
     *
     * @cfg {String|Number} [minDate=-365] A value which sets the minimum selectable date for the calendar. It can be:
     *
     * - a Date object.
     * - a String with the following format: "([+-]\d+[dmyw]\s)?". It will represent an addition or substraction of
     * time units to the current date, for example the expression "+1y -2m +3d" means
     * "the current date plus 1 year, minus 2 months plus 3 days". The prefixes you can use are:
     *     - "d" for days.
     *     - "w" for weeks.
     *     - "m" for months.
     *     - "y" for years.
     *
     *     You can also use one single unit, i.e. "+3y", "-1m", or a combination of two: "-2y +3d", etc.
     * - a Number, in this case the number is taken as the number of days that will be sum/substracted from the
     * current date. This number can be positive (for add days) or negative (for substract days).
     * - an Object, in this case the object must have the following structure:
     *
     *      {
     *          year: 2013,
     *          month: 5,
     *          day: 4,
     *          hours: 20,
     *          minutes: 15,
     *          seconds: 3,
     *          millisenconds
     *      }
     *
     * In this case only year and month are required.
     *
     * @cfg {String|Number} [maxDate=365] A value which sets the maximum selectable date for the calendar. It can take
     * the same type of values that the {@link PMUI.contriol.DateTimeControl#cfg-minDate minDate config option}.
     *
     * - a Date object.
     * - a String with the following format: "([+-]\d+[dmyw]\s)?". It will represent an addition or substraction of
     * time units to the current date, for example the expression "+1y -2m +3d" means
     * "the current date plus 1 year, minus 2 months plus 3 days". The prefixes you can use are:
     *     - "d" for days.
     *     - "w" for weeks.
     *     - "m" for months.
     *     - "y" for years.
     *
     *     You can also use one single unit, i.e. "+3y", "-1m", or a combination of two: "-2y +3d", etc.
     * - a Number, in this case the number is taken as the number of days that will be sum/substracted from the
     * current date. This number can be positive (for add days) or negative (for substract days).
     * - an Object, in this case the object must have the following structure:
     *
     *      {
     *          year: 2013,
     *          month: 5,
     *          day: 4,
     *          hours: 20,
     *          minutes: 15,
     *          seconds: 3,
     *          millisenconds
     *      }
     *
     * In this case only year and month are required.
     *
     * @cfg {Number} [firstDay=0] Sets the first day of week. You can use numbers from 0 to 6, 0 means Sunday,
     * 1 means Monday and so on.
     */
    var DateTimeControl = function (settings) {
        DateTimeControl.superclass.call(this, settings);
        /**
         * @property {Object} dom A JSON object that contains the control's DOM elements.
         * @private
         */
        this.dom = {};
        /**
         * @property {Boolean} datetime If the calendar has time supporting.
         * @readonly
         */
        this.datetime = null;
        /**
         * @property {String} dateFormat The format for the date to be shown on the control's textbox.
         * @readonly
         */
        this.dateFormat = null;
        /**
         * @property {Date} dateObject The control's date object.
         * @private
         */
        this.dateObject = null;
        /**
         * @property {Date} minDate The minimum selectable date.
         * @private
         */
        this.minDate = null;
        /**
         * @property {Date} maxDate The maximum selectable date.
         * @private
         */
        this.maxDate = null;
        /**
         * @property {Number} firstDay The first day of the week beginning with 0: Sunday and ending with 6: Saturday.
         * @readonly
         */
        this.firstDay = null;
        DateTimeControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', DateTimeControl);

    DateTimeControl.prototype.type = "DateTimeControl";

    /**
     * An array with the name of each day of the week.
     * @type {Array}
     * @private
     */
    DateTimeControl.prototype.daysOrder = [
        "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"
    ];
    /**
     * An array with the name fo the months of year.
     * @type {Array}
     * @private
     */
    DateTimeControl.prototype.monthsOrder = [
        "january", "february", "march", "april", "may", "june", "july", "august", "september", "october",
        "november", "december"
    ];
    /**
     * An Object that contains the name, shortname and code for the days of the week.
     * @type {Object}
     * @private
     */
    DateTimeControl.prototype.days = {
        "sunday": {
            value: 0
        },
        "monday": {
            value: 1
        },
        "tuesday": {
            value: 2
        },
        "wednesday": {
            value: 3
        },
        "thursday": {
            value: 4
        },
        "friday": {
            value: 5
        },
        "saturday": {
            value: 6
        }
    };
    /**
     * An Object that contains the name, shortname and code for the months of year.
     * @type {Object}
     * @private
     */
    DateTimeControl.prototype.months = {
        "january": {
            value: 0
        },
        "february": {
            value: 1
        },
        "march": {
            value: 2
        },
        "april": {
            value: 3
        },
        "may": {
            value: 4
        },
        "june": {
            value: 5
        },
        "july": {
            value: 6
        },
        "august": {
            value: 7
        },
        "september": {
            value: 8
        },
        "october": {
            value: 9
        },
        "november": {
            value: 10
        },
        "december": {
            value: 11
        }
    };
    /**
     * Initialize the object.
     * @param  {Object} settings An object with the config options.
     * @private
     */
    DateTimeControl.prototype.init = function (settings) {
        var defaults = {
            datetime: false,
            dateFormat: settings && settings.datetime ? 'yy-mm-dd HH:ii:ss' : 'yy-mm-dd',
            months: {
                "january": "January",
                "february": "February",
                "march": "March",
                "april": "April",
                "may": "May",
                "june": "June",
                "july": "July",
                "august": "August",
                "september": "September",
                "october": "October",
                "november": "November",
                "december": "December"
            },
            days: {
                "sunday": "Sunday",
                "monday": "Monday",
                "tuesday": "Tuesday",
                "wednesday": "Wednesday",
                "thursday": "Thursday",
                "friday": "Friday",
                "saturday": "Saturday"
            },
            minDate: -365,
            maxDate: 365,
            firstDay: 0,
            height: 30

        };

        jQuery.extend(true, defaults, settings);

        this.setFirstDay(defaults.firstDay)
            .setDateFormat(defaults.dateFormat)
            .setMonths(defaults.months)
            .setDays(defaults.days)
            .setMinDate(defaults.minDate)
            .setMaxDate(defaults.maxDate)
            .visibleDateTime(defaults.datetime)
            .setHeight(defaults.height);
    };
    /**
     * @inheritdoc
     */
    DateTimeControl.prototype.setID = function (id) {
        DateTimeControl.superclass.prototype.setID.call(this, id);
        if (this.dom) {
            this.dom.calendar.id = 'pmui-datepicker-calendar-' + this.id;
        }
        return this;
    };
    /**
     * Returns the day index of the first day of week.
     * @return {Number} A number refering a day: 0 for Sunday, 1 for Monday and so on.
     */
    DateTimeControl.prototype.getFirstDay = function () {
        return this.firstDay;
    };
    /**
     * Returns the control's minimum selectable date.
     * @param {String} [format="UTC"] The format to applied to the returning date.
     * @return {String} The minimum selectable date in string format.
     */
    DateTimeControl.prototype.getMinDate = function (format) {
        return this.formatDate(this.minDate, format || "UTC");
    };
    /**
     * Returns the control's maximum selectable date.
     * @param {String} [format="UTC"] The format to applied to the returning date.
     * @return {String} The maximum selectable date in string format.
     */
    DateTimeControl.prototype.getMaxDate = function (format) {
        return this.formatDate(this.maxDate, format || "UTC");
    };
    /**
     * Enables/disabled the calendar's time supporting.
     * @param  {Boolean} visible If it's true, then the time supporting is enabled, otherwise it's disabled.
     * @chainable
     */
    DateTimeControl.prototype.visibleDateTime = function (visible) {
        visible = !!visible;
        this.datetime = visible;
        if (this.html) {
            this.dom.footer.style.display = this.datetime ? 'block' : 'none';
        }

        return this;
    };
    /**
     * @method  setValue
     * Sets the value for the Control.
     * @param {Date|Number|String} value The date to set to the control, it can be:
     *
     *  - A Date object, is used the date set in this object.
     *  - A Number, the number is used as the timestamp for the date (set in UTC).
     *  - A String, in this case the string must have one of the following formats:
     *
     *      - "" (empty string), the value is set to empty.
     *      - "\d", the value is parsed to a number to be used as the timestamp for the date.
     *      - "\d{4}\-\d{2}\-\d{2}", this date is taken as "yyyy-mm-dd" and it's parsed.
     *      - "\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}[\\+\-]\d{2}\:\d{2}", the date is taken as
     *      "yyyy-mm-ddTHH:ii:ss+HH:ii" (the timezone offset is used).
     *
     * @param {Boolean} [utc=false] If the date will be set as a UTC date.
     * (Only applicable if the first parameter is String and has the format "\d{4}\-\d{2}\-\d{2}").
     * @chainable
     */
    DateTimeControl.prototype.setValue = function (value, utc) {
        var newDate, aux, aux2, y, m, d, h, i, s, hd, id, datetime = false, error = false;
        if (value instanceof Date) {
            newDate = value;
        } else if (typeof value === 'number') {
            newDate = new Date(value);
        } else if (typeof value === 'string') {
            if (value === "") {
                this.dateObject = null;
            } else if (/^\d+$/.test(value)) {
                newDate = new Date(parseInt(value, 10));
            } else if (/^\d{4}\-\d{2}\-\d{2}$/.test(value)
                || /^\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}[\\+\-]\d{2}\:\d{2}$/.test(value)) {
                newDate = new Date();
                aux = value.split(/T|\+|\-(?=\d{2}\:\d{2})/);
                aux2 = aux[0].split("-");
                y = parseInt(aux2[0], 10);
                m = parseInt(aux2[1], 10);
                d = parseInt(aux2[2], 10);
                if (aux[1]) {
                    datetime = true;
                    aux2 = aux[1].split(":");
                    h = parseInt(aux2[0], 10);
                    i = parseInt(aux2[1], 10);
                    s = parseInt(aux2[2], 10);
                    aux2 = aux[2].split(":");
                    hd = (value[19] === '+' ? -1 : 1) * parseInt(aux2[0], 10);
                    id = (hd < 0 ? -1 : 1) * parseInt(aux2[1], 10);
                } else {
                    h = i = s = hd = id = 0;
                }

                if (!this.isValidDateTime(y, m, d, h, i, s)) {
                    throw new Error("setValue(): invalid date time.");
                }

                newDate.setFullYear(y);
                newDate.setMonth(m - 1);
                newDate.setDate(d);
                newDate.setHours(h);
                newDate.setMinutes(i);
                newDate.setSeconds(s);
                newDate.setMilliseconds(0);

                newDate.setHours(newDate.getHours() + hd);
                newDate.setMinutes(newDate.getMinutes() + id);

                if (!this.dateObject) {
                    this.dateObject = new Date();
                }

                if (datetime || utc) {
                    this.dateObject.setUTCFullYear(newDate.getFullYear());
                    this.dateObject.setUTCMonth(newDate.getMonth());
                    this.dateObject.setUTCDate(newDate.getDate());
                    this.dateObject.setUTCHours(newDate.getHours());
                    this.dateObject.setUTCMinutes(newDate.getMinutes());
                    this.dateObject.setUTCSeconds(newDate.getSeconds());
                } else {
                    this.dateObject.setFullYear(y);
                    this.dateObject.setMonth(m - 1);
                    this.dateObject.setDate(d);
                    this.dateObject.setHours(h);
                    this.dateObject.setMinutes(i);
                    this.dateObject.setSeconds(s);
                    this.dateObject.setMilliseconds(0);
                }
                newDate = null;
            } else {
                error = true;
            }
        } else {
            error = true;
        }

        if (error) {
            throw new Error("setValue(): Invalid parameter format/data type.");
        }
        if (value !== "") {
            if (!this.dateObject) {
                this.dateObject = new Date();
            }
            if (newDate) {
                this.dateObject.setFullYear(newDate.getFullYear());
                this.dateObject.setMonth(newDate.getMonth());
                this.dateObject.setDate(newDate.getDate());
                this.dateObject.setHours(newDate.getHours());
                this.dateObject.setMinutes(newDate.getMinutes());
                this.dateObject.setSeconds(newDate.getSeconds());
            }
        }

        if (this.html) {
            if (this.dateObject) {
                this.html.value = this.formatDate(this.dateObject, this.dateFormat);
                this.dom.hoursInput.value = this.dateObject.getHours();
                this.dom.minutesInput.value = this.dateObject.getMinutes();
                this.dom.secondsInput.value = this.dateObject.getSeconds();
            } else {
                this.html.value = "";
            }
            this.fillYearSelector();
            this.fillMonthSelector();
            this.buildDaysTable();
        }
        this.value = this.getValue('UTC');

        return this;
    };
    /**
     * Sets the first day of week.
     * @param {Number} day Use 0 for Sunday, 1 for Monday, 2 for Tuesday and so on!.
     * @chainable
     */
    DateTimeControl.prototype.setFirstDay = function (day) {
        if (typeof day === 'number') {
            if (day >= 0 && day < 7) {
                this.firstDay = Math.floor(day);
                if (this.html) {
                    this.setDays(this.days);
                    this.buildDaysTable();
                }
                return this;
            }
        }

        throw new Error("setFirstDay(): The parameter must be a number between 0 and 6.");
    };
    /**
     * Sets the format for the date to be displayed in the control's textbox.
     * @param {String} dateFormat A string that contains wildcards that represent a date part (day, month, etc.).
     * The valid wildcards are the same ones that are specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     * @chainable
     */
    DateTimeControl.prototype.setDateFormat = function (dateFormat) {
        if (typeof dateFormat === 'string') {
            this.dateFormat = dateFormat;
            this.updateValue();
        } else {
            throw new Error("setDateFormat(): The parameter must be a string.");
        }
        return this;
    };
    /**
     * Returns the date format to be displayed in the control's textbox.
     * @return {String}
     */
    DateTimeControl.prototype.getDateFormat = function () {
        return this.dateFormat;
    };
    /**
     * Returns the date using the format set by {@link PMUI.control.DateTimeControl#setDateFormat setDateFormat()}
     * method.
     * @return {String}
     */
    DateTimeControl.prototype.getFormatedDate = function () {
        return this.html.value;
    };
    /**
     * Returns the selected date.
     * @param  {String} format Specifies the date format that will be used for the returning date.
     * The valid values are:
     *
     * - "UTC", returns a date in the format: yyyy-mm-ddTHH:ii:ss-HH:mm (i.e. 2013-08-31T00:08:00+04:00).
     * - "@" or "timestamp", returns a date in timestamp format.
     * - [any other valid format string], will return the date using the string as the dateformat. This string can
     * contain any of the wilcards specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     * @return {String}
     */
    DateTimeControl.prototype.getValue = function (returningFormat) {
        var res;
        returningFormat = returningFormat || "UTC";
        if (this.dateObject) {
            switch (returningFormat) {
                case 'timestamp':
                case '@':
                    res = this.formatDate(this.dateObject, '@');
                    break;
                default:
                    res = this.formatDate(this.dateObject, returningFormat);
            }
        } else {
            res = "";
        }

        return res;
    };
    /**
     * Returns true if the parameter is a leap year, otherwise returns false.
     * @param  {Number}  year The year to be evaluated.
     * @return {Boolean}
     * @private
     */
    DateTimeControl.prototype.isLeapYear = function (year) {
        if (year % 400 === 0) {
            return true;
        }
        if (year % 100 === 0) {
            return false;
        }
        if (year % 4 === 0) {
            return true;
        }
        return false;
    };
    /**
     * Returns true if the bunch of arguments represents a valid date otherwise it returns false.
     * @param  {Number}  year
     * @param  {Number}  month The month number (a number from 1 to 12)
     * @param  {Number}  day
     * @param  {Number}  [hours]
     * @param  {Number}  [minutes]
     * @param  {Number}  [seconds]
     * @param  {Number}  [milliseconds]
     * @return {Boolean}
     * @private
     */
    DateTimeControl.prototype.isValidDateTime = function (year, month, day, hours, minutes, seconds, milliseconds) {
        if (!(typeof year === 'number' && typeof month === 'number' && typeof day === 'number')) {
            return false;
        }

        hours = hours || 0;
        minutes = minutes || 0;
        seconds = seconds || 0;
        milliseconds = milliseconds || 0;

        if (!(typeof hours === 'number' && typeof minutes === 'number' && typeof seconds === 'number'
            && typeof milliseconds === 'number')) {
            return false;
        }

        if (!(hours >= 0 && hours <= 23)) {
            return false;
        }

        if (!(minutes >= 0 && minutes <= 59)) {
            return false;
        }

        if (!(seconds >= 0 && seconds <= 59)) {
            return false;
        }

        if (!(milliseconds >= 0 && milliseconds <= 999)) {
            return false;
        }

        if (day < 1 || day > 31 || month < 1 || month > 12) {
            return false;
        }

        switch (month) {
            case 4:
            case 6:
            case 9:
            case 11:
                if (day > 30) {
                    return false;
                }
                break;
            case 2:
                if (this.isLeapYear(year)) {
                    return day <= 29;
                }
                return day <= 28;
        }

        return true;
    };
    /**
     * It parses the argument into a date.
     * @param  {String|Date} date It can be a String or a Date:
     *
     * - Date, in this case the argument isn't parse and it is returned.
     * - String, in this case the string must have the following format: "[-+]\d+[dwmy]". For example "+1y -4m +3d"
     * means that te returning date will be 1 year minus 4 months plus 3 days from today.
     * @return {Date}
     * @private
     */
    DateTimeControl.prototype.parseDate = function (date) {
        var startDate,
            aux,
            i,
            amount;

        if (typeof date === 'number') {
            startDate = new Date();
            startDate.setDate(startDate.getDate() + date);
        } else if (typeof date === 'string') {
            startDate = new Date();
            date = " " + jQuery.trim(date);
            if (/^(\s[\\+|\-]\d+[y|m|d|w])+$/.test(date)) {
                date = date.match(/[\\+|\-]\d+[y|m|d]/g);
                for (i = 0; i < date.length; i += 1) {
                    aux = date[i].match(/[\-|\\+]|\d+|[m|d|y]/g);
                    amount = parseInt(aux[1], 10);
                    if (aux[0] === '-') {
                        amount *= -1;
                    }
                    switch (aux[2]) {
                        case 'd':
                            startDate.setDate(startDate.getDate() + amount);
                            break;
                        case 'm':
                            startDate.setMonth(startDate.getMonth() + amount);
                            break;
                        case 'y':
                            startDate.setFullYear(startDate.getFullYear() + amount);
                            break;
                        case 'w':
                            startDate.setDate(startDate.getDate() + (amount * 7));
                            break;
                    }
                }
            }
        } else if (date instanceof Date) {
            startDate = date;
        } else if (typeof date === 'object') {
            startDate = new Date(date.year, date.month - 1, date.day || 1, date.hours || 0, date.minutes || 0, date.seconds || 0,
                date.milliseconds || 0);
        } else {
            throw new Error("parseDate(): invalid parameter.");
        }

        return startDate;
    };
    /**
     * Sets the minimum date the control can accept as a selectable one.
     * @param {String|Date|Number|Object} date It can be:
     *
     * - a Date object.
     * - a String with the following format: "([+-]\d+[dmyw]\s)?". It will represent an addition or substraction of
     * time units to the current date, for example the expression "+1y -2m +3d" means
     * "the current date plus 1 year, minus 2 months plus 3 days". The prefixes you can use are:
     *     - "d" for days.
     *     - "w" for weeks.
     *     - "m" for months.
     *     - "y" for years.
     *
     *     You can also use one single unit, i.e. "+3y", "-1m", or a combination of two: "-2y +3d", etc.
     * - a Number, in this case the number is taken as the number of days that will be sum/substracted from the
     * current date. This number can be positive (for add days) or negative (for substract days).
     * - an Object, in this case the object must have the following structure:
     *
     *      {
     *          year: 2013,
     *          month: 5,
     *          day: 4,
     *          hours: 20,
     *          minutes: 15,
     *          seconds: 3,
     *          millisenconds
     *      }
     *
     * In this case only year and month are required.
     *
     * @chainable
     */
    DateTimeControl.prototype.setMinDate = function (date) {
        var minDate = this.parseDate(date), d;

        if (this.maxDate) {
            if (minDate > this.maxDate) {
                throw new Error("setMinDate(): The min date can't be major than the max date");
            }
        }
        this.minDate = minDate;
        this.fillYearSelector();
        this.fillMonthSelector();
        this.buildDaysTable();

        if (this.html) {
            d = new Date();
            this.dom.todayButton.disabled = !(d >= minDate && (this.maxDate && d <= this.maxDate));
        }

        return this;
    };
    /**
     * Sets the maximum date the control can accept as a selectable one.
     * @param {String|Date|Number|Object} date It can be:
     *
     * - a Date object.
     * - a String with the following format: "([+-]\d+[dmyw]\s)?". It will represent an addition or substraction of
     * time units to the current date, for example the expression "+1y -2m +3d" means
     * "the current date plus 1 year, minus 2 months plus 3 days". The prefixes you can use are:
     *     - "d" for days.
     *     - "w" for weeks.
     *     - "m" for months.
     *     - "y" for years.
     *
     *     You can also use one single unit, i.e. "+3y", "-1m", or a combination of two: "-2y +3d", etc.
     * - a Number, in this case the number is taken as the number of days that will be sum/substracted from the
     * current date. This number can be positive (for add days) or negative (for substract days).
     * - an Object, in this case the object must have the following structure:
     *
     *      {
     *          year: 2013,
     *          month: 5,
     *          day: 4,
     *          hours: 20,
     *          minutes: 15,
     *          seconds: 3,
     *          millisenconds
     *      }
     *
     * In this case only year and month are required.
     * @chainable
     */
    DateTimeControl.prototype.setMaxDate = function (date) {
        var maxDate = this.parseDate(date), d;

        if (this.minDate) {
            if (maxDate < this.minDate) {
                throw new Error("setMaxDate(): The max date can't be minor than the min date");
            }
        }
        this.maxDate = maxDate;
        this.fillYearSelector();
        this.fillMonthSelector();
        this.buildDaysTable();

        if (this.html) {
            d = new Date();
            this.dom.todayButton.disabled = !(d <= maxDate && (this.minDate && d >= this.minDate));
        }

        return this;
    };
    /**
     * Sets the months names/shortnames to be used by the calendar.
     * @param {Object} months An object with the same structure that the
     * {@link PMUI.control.DateTimeControl#cfg-months months config option}.
     * @chainable
     */
    DateTimeControl.prototype.setMonths = function (months) {
        var key,
            changed;

        for (key in this.months) {
            if (this.months.hasOwnProperty(key)) {
                if (months.hasOwnProperty(key)) {
                    if (typeof months[key] === 'string') {
                        changed = true;
                        this.months[key].name = months[key];
                        this.months[key].shortname = months[key].charAt(0).toUpperCase() + months[key].slice(1, 3);
                    } else if (typeof months[key] === 'object') {
                        this.months[key].name = months[key].name;
                        this.months[key].shortname = months[key].shortname;
                        changed = true;
                    }
                }
            }
        }

        if (changed) {
            this.fillMonthSelector();
        }

        return this;
    };
    /**
     * Fills the months selector.
     * @chainable
     * @private
     */
    DateTimeControl.prototype.fillMonthSelector = function () {
        var currentMonth,
            option,
            year,
            i,
            j;

        if (this.dom.monthSelector) {
            year = this.dom.yearSelector.value;
            currentMonth = (this.dateObject && this.dateObject.getMonth()) || this.dom.monthSelector.value
                || (new Date()).getMonth().toString();
            jQuery(this.dom.monthSelector).empty();
            i = 0;
            j = this.monthsOrder.length - 1;
            if (year === this.minDate.getFullYear().toString()) {
                i = this.minDate.getMonth();
            }
            if (year === this.maxDate.getFullYear().toString()) {
                j = this.maxDate.getMonth();
            }
            for (i; i <= j; i += 1) {
                option = PMUI.createHTMLElement('option');
                option.label = this.months[this.monthsOrder[i]].name;
                option.textContent = option.label;
                option.value = this.months[this.monthsOrder[i]].value;
                option.selected = currentMonth.toString() === option.value;
                this.dom.monthSelector.appendChild(option);
            }
        }

        return this;
    };
    /**
     * Sets the name/shortnames for the days to be used in the calendar.
     * @param {Object} days A JSON object with the same structure than the
     * {@PMUI.control.DateTimeControl#cfg-days days config option}.
     * @chainable
     */
    DateTimeControl.prototype.setDays = function (days) {
        var key,
            changed,
            i,
            cell,
            daysLength = this.daysOrder.length,
            aux;

        for (key in this.days) {
            if (this.days.hasOwnProperty(key)) {
                if (days.hasOwnProperty(key)) {
                    if (typeof days[key] === 'string') {
                        this.days[key].name = days[key];
                        this.days[key].shortname = days[key].substr(0, 3);
                    } else if (typeof days[key] === 'object') {
                        this.days[key].name = days[key].name || this.days[key].name || "";
                        this.days[key].shortname = (days[key].shortname && days[key].shortname.substr(0, 3))
                            || (this.days[key].name && this.days[key].name.substr(0, 3)) || "";
                    } else {
                        throw new Error("setDays(): The argument supplied must be a string or an object.");
                    }
                    changed = true;
                }
            }
        }

        if (changed && this.html) {
            jQuery(this.dom.daysHeader).empty();
            aux = 0;
            i = this.firstDay;
            while (aux < daysLength) {
                if (i === daysLength) {
                    i = 0;
                }
                cell = PMUI.createHTMLElement('th');
                cell.textContent = this.days[this.daysOrder[i]].shortname;
                cell.setAttribute("data-value", this.days[this.daysOrder[i]].value);
                this.dom.daysHeader.appendChild(cell);
                i += 1;
                aux += 1;
            }
        }

        return this;
    };
    /**
     * Fill the year selector.
     * @chainable
     * @private
     */
    DateTimeControl.prototype.fillYearSelector = function () {
        var minYear,
            maxYear,
            option,
            selectedYear;

        if (this.dom.yearSelector) {
            selectedYear = (this.dateObject && this.dateObject.getFullYear()) || this.dom.yearSelector.value
                || (new Date()).getFullYear();
            selectedYear = selectedYear.toString();
            jQuery(this.dom.yearSelector).empty();
            minYear = this.minDate.getFullYear();
            maxYear = this.maxDate.getFullYear();
            for (minYear; minYear <= maxYear; minYear += 1) {
                option = PMUI.createHTMLElement('option');
                option.textContent = option.label = option.value = minYear;
                if (option.value.toString() === selectedYear) {
                    option.selected = true;
                }
                this.dom.yearSelector.appendChild(option);
            }
        }

        return this;
    };
    /**
     * Returns the number of days in a month
     * @param  {Number} y The year (it's necessary to determine the days in February.)
     * @param  {Number} m The month
     * @return {Number}   The number of days.
     * @private
     */
    DateTimeControl.prototype.daysInMonth = function (y, m) {
        if (m < 1 || m > 12) {
            return 0;
        }
        switch (m) {
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (this.isLeapYear(y)) {
                    return 29;
                }
                return 28;
            default:
                return 31;
        }
    };
    /**
     * Returns the day of week for a date.
     * @param  {Number} y The year.
     * @param  {Number} m The month.
     * @param  {Number} d The day of month.
     * @return {Number}   A number from 0 to 6. 0 means Sunday, 1, Monday and so on.
     * @private
     */
    DateTimeControl.prototype.whichDay = function (y, m, d) {
        var centuryCode,
            monthCode,
            lastTwoYearDigits = y % 100,
            gregorianCenturyCodes = [6, 4, 2, 0],
            i;

        if (!this.isValidDateTime(y, m, d)) {
            throw new Error("whichDay(): invalid date.");
        }

        switch (m) {
            case 3:
            case 11:
                monthCode = 3;
                break;
            case 4:
            case 7:
                monthCode = 6;
                break;
            case 5:
                monthCode = 1;
                break;
            case 6:
                monthCode = 4;
                break;
            case 8:
                monthCode = 2;
                break;
            case 9:
            case 12:
                monthCode = 5;
                break;
            case 10:
                monthCode = 0;
                break;
            case 1:
                if (this.isLeapYear(y)) {
                    monthCode = -1;
                } else {
                    monthCode = 0;
                }
                break;
            case 2:
                if (this.isLeapYear(y)) {
                    monthCode = 2;
                } else {
                    monthCode = 3;
                }
                break;
        }

        centuryCode = Math.floor(y / 100);

        i = 0;
        while (centuryCode % 4 !== 0) {
            centuryCode += 1;
            i += 1;
        }
        if (i === 0) {
            centuryCode = gregorianCenturyCodes[0];
        } else {
            centuryCode = gregorianCenturyCodes[gregorianCenturyCodes.length - i];
        }

        return (d + monthCode + lastTwoYearDigits + Math.floor(lastTwoYearDigits / 4) + centuryCode    ) % 7;
    };
    /**
     * Build the days table for the calendar
     * @chainable
     * @private
     */
    DateTimeControl.prototype.buildDaysTable = function () {
        var dayOfTheWeek,
            daysInMonth,
            y,
            m,
            i,
            day = 1,
            row,
            cell,
            limit,
            link,
            selectableDate;

        if (this.dom.yearSelector && this.dom.monthSelector) {
            jQuery(this.dom.tableBody).empty();
            y = parseInt(this.dom.yearSelector.value, 10);
            m = parseInt(this.dom.monthSelector.value, 10) + 1;
            dayOfTheWeek = this.whichDay(y, m, 1) - this.firstDay;
            if (dayOfTheWeek < 0) {
                dayOfTheWeek = 7 + dayOfTheWeek;
            }
            daysInMonth = this.daysInMonth(y, m);
            limit = Math.ceil((dayOfTheWeek + daysInMonth) / 7) * 7;
            row = PMUI.createHTMLElement('tr');

            for (i = 0; i < limit; i += 1) {
                cell = PMUI.createHTMLElement("td");
                if (i - dayOfTheWeek >= 0 && day <= daysInMonth) {
                    selectableDate = true;
                    if ((m - 1) === this.maxDate.getMonth() && y === this.maxDate.getFullYear()) {
                        if (day > this.maxDate.getDate()) {
                            selectableDate = false;
                        }
                    } else if ((m - 1) === this.minDate.getMonth() && y === this.minDate.getFullYear()) {
                        if (day < this.minDate.getDate()) {
                            selectableDate = false;
                        }
                    }
                    if (selectableDate) {
                        link = PMUI.createHTMLElement("a");
                        link.href = "#";
                        link.setAttribute("data-date", day);
                        link.textContent = day;
                        link.className = (this.dateObject && this.dateObject.getFullYear() === y
                            && this.dateObject.getMonth() + 1 === m && this.dateObject.getDate() === day) ? 'selected' : '';
                        cell.appendChild(link);
                    } else {
                        cell.className = 'pmui-datepicker-disabled-date';
                        cell.textContent = day;
                    }
                    day += 1;
                }
                row.appendChild(cell);
                if ((i + 1) % 7 === 0) {
                    this.dom.tableBody.appendChild(row);
                    row = PMUI.createHTMLElement('tr');
                }
            }
            this.dom.tableBody.appendChild(row);
        }

        return this;
    };
    /**
     * Shows the control's calendar.
     * @chainable
     * @private
     */
    DateTimeControl.prototype.showCalendar = function () {
        var position,
            inputHeight,
            maxZIndex;

        if (this.html) {
            this.fillYearSelector();
            this.fillMonthSelector();
            this.buildDaysTable();

            position = jQuery(this.html).offset();
            inputHeight = jQuery(this.html).outerHeight();

            jQuery(document.body).find(">*").each(function () {
                var n;

                n = parseInt(jQuery(this).css("z-index"));
                if (!isNaN(n)) {
                    maxZIndex = n > maxZIndex ? n : maxZIndex;
                }
            });

            document.body.appendChild(this.dom.calendar);
            this.dom.calendar.style.zIndex = maxZIndex ? maxZIndex + 1 : 999;
            this.dom.calendar.style.left = position.left + 'px';
            this.dom.calendar.style.top = (position.top + inputHeight) + 'px';
            this.dom.calendar.style.display = "";
        }

        return this;
    };
    /**
     * Hide's the control's calendar.
     * @return {HTMLElement} The object's html.
     * @private
     */
    DateTimeControl.prototype.hideCalendar = function () {
        if (this.html) {
            jQuery(this.dom.calendar).detach();
        }

        return this.html;
    };
    /**
     * Returns the day number of a date in a whole year.
     * @param  {Number} y The year.
     * @param  {Number} m The month.
     * @param  {Number} d The day of month.
     * @return {Number}   The day of year.
     * @private
     */
    DateTimeControl.prototype.getDayOfYear = function (y, m, d) {
        var day = 0,
            month = 1;

        if (!(typeof y === 'number' && typeof m === 'number' && typeof d === 'number')) {
            throw new Error("getDayOfYear(): invalid parameters.");
        }
        if (!this.isValidDateTime(y, m, d)) {
            throw new Error("getDayOfYear(): invalid date.");
        }

        while (month < m) {
            day += this.daysInMonth(y, m);
            month += 1;
        }

        day += d;

        return day;
    };
    /**
     * Formats a date using the specified format string.
     * @param  {Date} dateObject a Date object.
     * @param  {String} format     String that contains wildcards specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     * @return {String}            The formatted date as a string.
     * @private
     */
    DateTimeControl.prototype.formatDate = function (dateObject, format) {
        var finalValue,
            d = dateObject.getDate(),
            o = this.getDayOfYear(dateObject.getFullYear(), dateObject.getMonth() + 1, dateObject.getDate()),
            m = dateObject.getMonth() + 1,
            h = dateObject.getHours(),
            h12 = h > 12 ? h - 12 : h,
            i = dateObject.getMinutes(),
            s = dateObject.getSeconds(),
            yy = dateObject.getFullYear(),
            formatArray,
            timeOffset,
            timeOffsetString,
            aux;

        timeOffset = dateObject.getTimezoneOffset();

        timeOffsetString = timeOffset < 0 ? '+' : '-';
        timeOffset = Math.sqrt(timeOffset * timeOffset);
        aux = Math.floor(timeOffset / 60);
        timeOffset = timeOffset - (aux * 60);
        timeOffsetString += (aux < 10 ? '0' : '') + aux + ':' + (timeOffset < 10 ? '0' : '') + timeOffset;

        formatArray = [
            {
                regExp: /dd/g,
                value: d < 10 ? "0" + d : d
            },
            {
                regExp: /d/g,
                value: d
            },
            {
                regExp: /oo/g,
                value: o < 10 ? "00" + o : (o < 100 ? "0" + o : o)
            },
            {
                regExp: /o/g,
                value: o
            },
            {
                regExp: /mm/g,
                value: m < 10 ? "0" + m : m
            },
            {
                regExp: /m/g,
                value: m
            },
            {
                regExp: /yy/g,
                value: yy
            },
            {
                regExp: /y/g,
                value: yy % 100
            },
            {
                regExp: /\@/g,
                value: dateObject.getTime()
            },
            {
                regExp: /\!/g,
                value: (dateObject.getTime() * 10000) + 621355968000000000
            },
            {
                regExp: /MM/g,
                value: this.months[this.monthsOrder[dateObject.getMonth()]].name
            },
            {
                regExp: /M/g,
                value: this.months[this.monthsOrder[dateObject.getMonth()]].shortname
            },
            {
                regExp: /HH/g,
                value: (h < 10 ? '0' : '') + h
            },
            {
                regExp: /H/g,
                value: h
            },
            {
                regExp: /hh/g,
                value: (h12 < 10 ? '0' : '') + h12
            },
            {
                regExp: /h/g,
                value: h12
            },
            {
                regExp: /ii/g,
                value: (i < 10 ? '0' : '') + i
            },
            {
                regExp: /i/g,
                value: i
            },
            {
                regExp: /ss/g,
                value: (s < 10 ? '0' : '') + s
            },
            {
                regExp: /s/g,
                value: s
            },
            {
                regExp: /UTC/g,
                value: yy + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d + "T"
                + (h < 10 ? '0' : '') + h + ':' + (i < 10 ? '0' : '') + i + ':' + (s < 10 ? '0' : '') + s
                + timeOffsetString
            },
            {
                regExp: /P/g,
                value: h > 12 ? 'PM' : 'AM'
            },
            {
                regExp: /DD/g,
                value: this.days[this.daysOrder[dateObject.getDay()]].name
            },
            {
                regExp: /D/g,
                value: this.days[this.daysOrder[dateObject.getDay()]].shortname
            }
        ];

        finalValue = format || this.dateFormat;

        for (i = 0; i < formatArray.length; i += 1) {
            finalValue = finalValue.replace(formatArray[i].regExp, formatArray[i].value);
        }

        return finalValue;
    };
    /**
     * Updates the control's date text.
     * @chainable
     * @private
     */
    DateTimeControl.prototype.updateValue = function () {
        var finalValue;

        if (this.dateObject) {
            finalValue = this.formatDate(this.dateObject, this.dateFormat);
        } else {
            finalValue = "";
        }

        this.value = finalValue;

        if (this.html) {
            this.html.value = finalValue;
        }

        return this;
    };
    /**
     * Gets the control's value directly from the element.
     * @return {String}
     */
    DateTimeControl.prototype.getValueFromRawElement = function () {
        return this.getValue('UTC');
    };
    /**
     * The handler to be executed everytime the control's value changes.
     * @chainable
     * @private
     */
    DateTimeControl.prototype.onChangeHandler = function (hide) {
        var prevValue = this.value,
            newValue,
            resCallback,
            date,
            month,
            year,
            auxDate,
            valueChanged = false;

        auxDate = new Date();
        date = parseInt($(this.dom.tableBody).find("a.selected").text(), 10);
        if (isNaN(date)) {
            return this;
        }
        month = parseInt(this.dom.monthSelector.value, 10);
        year = parseInt(this.dom.yearSelector.value, 10);
        auxDate.setYear(year);
        auxDate.setMonth(month);
        auxDate.setDate(date);
        if (this.datetime) {
            auxDate.setMilliseconds(0);
            auxDate.setSeconds(parseInt(this.dom.secondsInput.value, 10));
            auxDate.setMinutes(parseInt(this.dom.minutesInput.value, 10));
            auxDate.setHours(parseInt(this.dom.hoursInput.value, 10));
            this.dom.doneButton.disabled = true;
        } else {
            if (!this.dateObject) {
                this.dateObject = new Date();
            }
            auxDate.setMilliseconds(0);
            auxDate.setSeconds(0);
            auxDate.setMinutes(0);
            auxDate.setHours(0);
        }
        if (!this.dateObject) {
            this.dateObject = new Date();
        }
        if (auxDate.getTime() !== this.dateObject.getTime()) {
            valueChanged = true;
        }
        if (typeof this.onBeforeChange === 'function' && valueChanged) {
            resCallback = this.onBeforeChange(this.formatDate(auxDate), prevValue);
        }
        if (resCallback !== false) {
            this.dateObject = auxDate;
        } else {
            if (this.dom.monthSelector.value == this.dateObject.getMonth()
                && this.dom.yearSelector.value == this.dateObject.getFullYear()) {

                jQuery(this.dom.tableBody).find('a.selected').removeClass('selected')
                    .find('a[data-date="' + this.dateObject.getDate() + '"]').addClass('selected');
            }
        }
        newValue = this.getValueFromRawElement();
        this.updateValue();
        if (hide) {
            this.hideCalendar();
        }
        if (typeof this.onChange === 'function' && this.value !== prevValue) {
            this.onChange(this.value, prevValue);
        }

        return this;
    };
    /**
     * Defines the events for the control
     * @chainable
     */
    DateTimeControl.prototype.defineEvents = function () {
        var that = this,
            preselectedDate,
            specialCloseHandler = function (e) {
                if ((e.target !== that.html && !$(e.target).parents('#pmui-datepicker-calendar-' + that.id).length)
                    || (e.type === 'keyup' && !jQuery(document.activeElement).parents('#pmui-datepicker-calendar-'
                        + that.id).length)) {
                    that.hideCalendar();
                }
            },
            timeInputFilter;

        if (this.dom.todayButton) {
            timeInputFilter = function () {
                var num = parseInt(this.value, 10),
                    maxValue;

                switch (this) {
                    case that.dom.hoursInput:
                        maxValue = 23;
                        break;
                    case that.dom.minutesInput:
                    case that.dom.secondsInput:
                        maxValue = 59;
                        break;
                    default:
                        throw new Error("defineEvents() at change event: No valid element.");
                }

                if (isNaN(num)) {
                    this.value = 0;
                } else {
                    this.value = num < 0 ? 0 : Math.min(num, maxValue);
                }
               that.dom.doneButton.disabled = false;
            };

            this.addEvent('change').listen(this.dom.yearSelector, function () {
                that.fillMonthSelector();
                that.buildDaysTable();
            });
            this.addEvent('change').listen(this.dom.monthSelector, function () {
                that.buildDaysTable();
            });
            this.addEvent('focusin click').listen(this.html, function () {
                that.showCalendar();
            });
            this.addEvent('change').listen(this.dom.hoursInput, timeInputFilter);
            this.addEvent('change').listen(this.dom.minutesInput, timeInputFilter);
            this.addEvent('change').listen(this.dom.secondsInput, timeInputFilter);
            this.addEvent('click').listen(this.dom.todayButton, function () {
                preselectedDate = new Date();
                that.dom.doneButton.disabled = false;
                that.dom.yearSelector.value = preselectedDate.getFullYear();
                that.fillMonthSelector();
                that.dom.monthSelector.value = preselectedDate.getMonth();
                that.buildDaysTable();
                jQuery(that.dom.tableBody).find(".selected").removeClass('selected').end()
                    .find('a[data-date=' + preselectedDate.getDate() + ']').addClass("selected");
            });
            this.addEvent('click keyup mousedown').listen(document, specialCloseHandler)
                .listen('.pmui-window', specialCloseHandler);
            //In the future, if the new Event objects model is implemented the following line should be listening to an
            //event close or windowClose. Currently a dirty implementation will fix he problem.
            this.addEvent('click').listen('.pmui-button.pmui-window-close', function () {
                that.hideCalendar();
            });
            this.addEvent('click').listen(this.dom.doneButton, function () {
                that.onChangeHandler(true);
            });
            jQuery(this.dom.tableBody).on('click', 'a', function (e) {
                e.preventDefault();
                jQuery(that.dom.tableBody).find("a").filter(".selected").removeClass("selected");
                jQuery(this).addClass("selected");
                var date, month, year;

                date = parseInt(this.textContent, 10);
                month = parseInt(that.dom.monthSelector.value, 10);
                year = parseInt(that.dom.yearSelector.value, 10);
                if (that.datetime) {
                    that.dom.doneButton.disabled = false;
                } else {
                    that.onChangeHandler(true);
                }
            });
        }
        return this;
    };
    /**
     * Creates the HTML for the control.
     * @return {HTMLElement}
     */
    DateTimeControl.prototype.createHTML = function () {
        var header,
            table,
            tableHeader,
            monthSelector,
            yearSelector,
            row,
            cell,
            tableBody,
            calendar,
            footer,
            button,
            input;
        if (this.html) {
            return this.html;
        }

        DateTimeControl.superclass.prototype.createHTML.call(this);

        this.html.type = 'text';
        this.html.readOnly = true;

        calendar = PMUI.createHTMLElement('div');
        calendar.className = 'pmui-datepicker';
        calendar.id = 'pmui-datepicker-calendar-' + this.id;
        header = PMUI.createHTMLElement('div');
        header.className = 'pmui-datepicker-header';
        table = PMUI.createHTMLElement('table');
        table.className = 'pmui-datepicker-head-table';
        monthSelector = PMUI.createHTMLElement("select");
        yearSelector = PMUI.createHTMLElement("select");
        row = PMUI.createHTMLElement("tr");
        cell = PMUI.createHTMLElement("td");
        cell.className = 'pmui-datepicker-month-selector';
        cell.appendChild(monthSelector);
        row.appendChild(cell);
        cell = PMUI.createHTMLElement("td");
        cell.className = 'pmui-datepicker-year-selector';
        cell.appendChild(yearSelector);
        row.appendChild(cell);
        table.appendChild(row);
        header.appendChild(table);

        this.dom.yearSelector = yearSelector;
        this.dom.monthSelector = monthSelector;

        table = PMUI.createHTMLElement('table');
        table.className = 'pmui-datepicker-table';
        tableHeader = PMUI.createHTMLElement('thead');
        row = PMUI.createHTMLElement('tr');
        this.dom.daysHeader = row;
        tableHeader.appendChild(row);
        table.appendChild(tableHeader);

        tableBody = PMUI.createHTMLElement('tbody');
        this.dom.tableBody = tableBody;
        table.appendChild(tableBody);

        footer = this.createFooterHTML();
        calendar.appendChild(header);
        calendar.appendChild(table);
        calendar.appendChild(footer);

        this.dom.calendar = calendar;
        this.dom.footer = footer;

        this.setMinDate(this.minDate)
            .setMaxDate(this.maxDate)
            .setDays(this.days)
            .visibleDateTime(this.datetime);

        if (this.eventsDefined) {
            this.defineEvents();
        }

        return this.html;
    };
    /**
     * Creates the Fotter HTML for the control.
     * @return {HTMLElement}
     */
    DateTimeControl.prototype.createFooterHTML = function () {
        var footertr,
            tableFooter,
            footer,
            button,
            cell1,
            cell2,
            cell3,
            cell4,
            cell5,
            input;

        footer = PMUI.createHTMLElement('div');
        tableFooter = PMUI.createHTMLElement('table');
        footertr = PMUI.createHTMLElement('tr');
        footer.className = 'pmui-datepicker-footer';

        cell1 = PMUI.createHTMLElement('td');
        cell2 = cell1.cloneNode(false);
        cell3 = cell1.cloneNode(false);
        cell4 = cell1.cloneNode(false);
        cell5 = cell1.cloneNode(false);

        button = PMUI.createHTMLElement('button');
        button.textContent = 'Today';
        button.className = 'pmui-datepicker-button';
        this.dom.todayButton = button;
        cell1.appendChild(button);
        footertr.appendChild(cell1);

        input = PMUI.createHTMLElement('input');
        input.setAttribute("type", "number");
        input.size = 2;
        input.min = 0;
        input.max = 23;
        input.value = 0;
        input.placeholder = "hrs.";
        this.dom.hoursInput = input;
        cell2.appendChild(input);
        footertr.appendChild(cell2);

        input = input.cloneNode(false);
        input.max = 59;
        input.placeholder = "min.";
        this.dom.minutesInput = input;
        cell3.appendChild(input);
        footertr.appendChild(cell3);

        input = input.cloneNode(false);
        input.max = 59;
        input.placeholder = "sec.";
        this.dom.secondsInput = input;
        cell4.appendChild(input);
        footertr.appendChild(cell4);

        button = button.cloneNode(true);
        button.textContent = "Done";
        button.disabled = true;
        this.dom.doneButton = button;
        cell5.appendChild(button);
        footertr.appendChild(cell5);
        tableFooter.appendChild(footertr);
        footer.appendChild(tableFooter);
        return footer;
    };
    PMUI.extendNamespace('PMUI.control.DateTimeControl', DateTimeControl);

    if (typeof exports !== "undefined") {
        module.exports = DateTimeControl;
    }
}());
