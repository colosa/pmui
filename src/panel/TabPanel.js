(function () {
    /**
     * @class PMUI.panel.TabPanel
     * @extends PMUI.core.Panel
     * Is the container for every {@link PMUI.item.TabItem TabItem} class
     *
     * Example:
     *
     *     @example
     var a,p,f,tree,w;

     //Form Panel*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     p = new PMUI.form.FormPanel({
                width: 604, 
                height: 130,
                fieldset: true,
                legend: "my fieldset panel",
                items: [
                    {
                        pmType: "text",
                        label: "Name",
                        id: "123",
                        value: "",
                        placeholder: "insert your name",
                        name: "name"
                    },{
                        pmType: "text",
                        label: "Last name",
                        value: "",
                        placeholder: "your lastname here asshole!",
                        name: "lastname"
                    }
                ],
                layout: "vbox"
            });
     //treePanel *-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     tree = new PMUI.panel.TreePanel({
                items: [
                    {
                        label: "America",
                        items: [
                            {
                                label: "North America",
                                items: [
                                    {
                                        label: "Canada"
                                    },
                                    {
                                        label: "USA"
                                    },
                                    {
                                        label: "Mexico"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            });
     //Layout Panel *-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     f = new PMUI.form.Form({
                onSubmit: function() {
                    console.log("submitting...");
                },
                onChange: function(field, newValue, previousValue) {
                    console.log("The field " + field.getName() + " has changed from \"" + previousValue + "\" to \"" + newValue + "\"");
                },
                items: [
                    {
                        pmType: "dropdown",
                        label: "age",
                        options: [
                            {
                                label: "from 0 to 7 years old",
                                value: "0-7"
                            },
                            {
                                label: "from 8 to 13 years old",
                                value: "8-13"
                            }
                        ],
                        name: "age",
                        helper: "Select one of the options",
                        required: true
                    },
                    {
                        pmType: "radio",
                        label: "Gender",
                        value: "m",
                        name: "gender",
                        required: true,
                        options: [
                            {
                                label: "Male",
                                value: "m"
                            },
                            {
                                label: "Female",
                                value: "f"
                            }
                        ]
                    }
                ],
                buttons: [
                    {
                        text: "Submit",
                        handler: function() {
                            f.submit();
                            console.log("submitting form...");
                        }
                    }
                ]
            });
     //Tab Item*--**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     a = new PMUI.panel.TabPanel({
            height : 400,
            width : 600,
            items:[
                {
                    icon :'pmui-gridpanel-engranaje',
                    title:'Tree Panel',
                    panel:tree
                },
                {
                    icon :'pmui-gridpanel-engranaje',
                    title:'Form Panel',
                    panel:p
                },
                {   icon :'pmui-gridpanel-engranaje',
                    title:'Form',
                    panel:f
                }
            ],
            itemsPosition : {position:"left",percentageWidth : 22}
            //itemsPosition : 'top'
    });
     //document.body.appendChild(a.getHTML());
     //a.defineEvents();
     w = new PMUI.ui.Window({
            title: "TabPanel",
            width: 650,
            height: 460,
            footerHeight: 'auto',
            bodyHeight: 'auto',
            modal:true,
            buttons: [
                {
                    text: 'Saves',
                    handler: function() {
                        alert('save');
                    }
                },
                {
                    text: 'Close',
                    handler: function() {
                        alert('close');
                    }
                },
                {
                    text: "third button"
                }
            ],
            buttonsPosition : 'center',
            spaceButtons : 30
        });
     w.open();
     w.addItem(a);
     w.defineEvents();

     * In the example, we use as a treePanel panels, FormPanel and layoutPanel, each TabItem can also use a className
     * to put an icon, see {@link PMUI.item.TabItem}, {@link PMUI.item.TabItem#cfg-icon icon}. The pocision of items [TabItem]
     * can be "top", "left", "right" and "bottom", see the configuration options.
     *
     * Note: used a window for a best visualisation.
     *
     * @cfg {Array} [items = []]
     * The items should be a Array, that Array should have Object JSON for config of the TabItems
     * for more detail see {@link PMUI.item.TabItem}
     *
     * @cfg {String|Object} [itemsPosition = 'top']
     * This option config can be of type:
     *
     *      Object, when position is "left" or "right", example:
     *          itemsPosition: {position: "left", percentageWidth: 20}
     *          itemsPosition: {position: "right", percentageWidth: 20}
     *              JSON options:
     *              position: must be a string (can only be "left" or "right")
     *              percentageWidth: must be a number between 0 and 100
     *
     *      String,  when the position is "top" or "bottom", example:
     *      itemsPosition: "top"
     *      itemsPosition: "bottom"
     *      percentageWidth is not require
     */
    var TabPanel = function (settings) {
        TabPanel.superclass.call(this, settings);
        /**
         * Defines the tabpanel panelContainer HTML Element where is fix the content of the family Panel
         * @type {HTMLElement}
         */
        this.dom = {};
        /**
         * a property to assign events to see if the tabs is selected or not
         * @type {PMUI.ite.TabItem}
         */
        this.selectedTab = null;
        /**
         * a property that can be true or false values​​, when set to true indicates
         * that the items will be collapsible
         * @type {boolean}
         */
        this.collapsible = null;
        /**
         * this posision items should be 'top', 'left', 'right' or 'bottom'
         * @type {String}
         */
        this.itemsPosition = null;
        /**
         * represents the space busy for the  containerItems HTMLElement,  default value
         * this property is 0, when itemsPosition is "right" or "left " this value is different 0
         * @type {Number}
         */
        this.percentageWidth = null;
        this.onTabClick = null;
        TabPanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom("PMUI.core.Panel", TabPanel);

    TabPanel.prototype.type = "tabpanel";
    TabPanel.prototype.family = "tabpanel";

    TabPanel.prototype.init = function (settings) {
        var defaults = {
            items: [],
            itemsPosition: 'top',
            onTabClick: null,
            collapsible: true
        };
        jQuery.extend(true, defaults, settings);
        this.setItems(defaults.items);
        this.setItemsPosition(defaults.itemsPosition);
        this.setOnTabClick(defaults.onTabClick);
        this.setCollapsible(defaults.collapsible);
    };

    TabPanel.prototype.setCollapsible = function (collapsible) {
        if (typeof collapsible !== 'boolean') {
            throw new Error('setCollapsible(): the parameter collapsible should be type "boolean"');
        }
        this.collapsible = collapsible;
        return this;
    };

    TabPanel.prototype.setOnTabClick = function (tabClick) {
        if (typeof tabClick !== 'function' && tabClick !== null) {
            throw new Error("setOnTabClick(): the parameter is not valid, should be a function.");
        }
        this.onTabClick = tabClick;
        return this;
    };

    /**
     * set in this method and call addItem to create the TabItem
     * @param {Array} The JSON array with objects with parameters for creating TabItem
     * @chainable
     */
    TabPanel.prototype.setItems = function (items) {
        var i;

        if (jQuery.isArray(items)) {
            this.clearItems();
            for (i = 0; i < items.length; i += 1) {
                this.addItem(items[i]);
            }
        } else {
            throw new Error("setTabs(): the parameter is not a valid, should to be as Array");
        }
        for (i = 0; i < this.items.getSize(); i += 1) {
            if (i == 0) {
                this.items.get(0).select();
            } else {
                this.items.get(i).deselect();
            }
        }
        return this;
    };
    /**
     * [onTabClickHandler description]
     * @return {[type]} [description]
     * @private
     */
    TabPanel.prototype.onTabClickHandler = function () {
        var that = this;

        return function (tabItem) {
            if (typeof that.onTabClick === 'function') {
                that.onTabClick(tabItem);
            }
        };
    };
    /**
     * Add an item to the panel.
     * @param {PMUI.item.TabItem} item
     * It can be a valid JSON object or an object that inherits from {@link PMUI.item.TabItem}.
     * @chainable
     */
    TabPanel.prototype.addItem = function (item) {
        var tabItem;

        item.onSelect = this.onTabSelectHandler();
        tabItem = new PMUI.item.TabItem(item);
        tabItem.deselect();
        tabItem.setOnClick(this.onTabClickHandler());
        this.items.insert(tabItem);
        if (this.html) {
            this.dom.listContainer.appendChild(tabItem.getHTML());
            this.updatePosition();
            if (this.eventsDefined) {
                tabItem.defineEvents();
            }
        }
        return this;
    };
    /**
     * @event
     * defines the function to be determined in each tabItem when it is selected, to
     * call the event set
     * @chainable
     */
    TabPanel.prototype.onTabSelectHandler = function () {
        var that = this,
            i;

        return function () {
            if (that.selectedTab) {
                jQuery(that.selectedTab.panel.getHTML()).detach();
            }
            that.selectedTab = this;
            for (i = 0; i < that.items.getSize(); i += 1) {
                if (that.items.get(i) !== that.selectedTab) {
                    that.items.get(i).deselect();
                }
            }
            jQuery(that.dom.panelContainer).append(this.getPanel().getHTML());
            if (!this.getPanel().eventsDefined) {
                this.getPanel().defineEvents();
            }
        };
    };
    /**
     * @method  setItemsPosition
     * changes the position of the elements [PM UI.item.TabItem] within the sizes assigned to TabPanel.
     * @param {String|Object}
     * Position where items are positioned, the more info about input value see {@link PMUI.panel.TabPanel}
     * config option: {@link PMUI.panel.TabPanel#cfg-itemsPosition itemsPosition}
     * @chainable
     */
    TabPanel.prototype.setItemsPosition = function (itemsPosition) {
        var validPosition,
            percentageWidth;

        if (typeof itemsPosition == "string" || typeof itemsPosition == "object") {
            if (typeof itemsPosition == "object") {
                if (itemsPosition.position == "left" || itemsPosition.position == "right") {
                    this.itemsPosition = itemsPosition.position;
                    this.percentageWidth = itemsPosition.percentageWidth || 20;
                } else {
                    throw new Error("setItemsPosition(): JSON no valid, send position : 'left' or 'right' with percentage width");
                }
            } else {
                if (itemsPosition == "top" || itemsPosition == "bottom") {
                    this.itemsPosition = itemsPosition;
                    this.percentageWidth = 0;
                } else {
                    throw new Error("setItemsPosition(): no valid, send position : 'top' or 'bottom' ");
                }
            }

            this.style.removeClasses(['pmui-tabpanel-left', 'pmui-tabpanel-right', 'pmui-tabpanel-top', 'pmui-tabpanel-bottom']);
            this.style.addClasses(['pmui-tabpanel-' + this.itemsPosition]);

            if (this.html) {
                this.updatePosition();
            }
        } else {
            throw new Error("setItemsPosition(): the parameter is not a valid, should to be as string or Object JSON");
        }
        return this;
    };
    /**
     * @private
     * update the position for the HTMLElement.
     * exists the next elements HTMLElements:
     *
     *     - containerItems
     *     - listContainer
     *     - barCollaspible
     *     - panelContainer
     *
     * this elements should update your position in the DOM, also update dimensions respect to the property itemsPosition
     * @chainable
     */
    TabPanel.prototype.updatePosition = function () {
        var i,
            itemsDisplay;

        switch (this.itemsPosition) {
            case 'top' :
            case 'left':
                $(this.dom.containerItems).prepend(this.dom.listContainer);
                $(this.html).prepend(this.dom.containerItems);


                this.dom.containerItems.style.display = this.itemsPosition === 'left' ? "inline-block" : "block";
                this.dom.listContainer.style.display = this.itemsPosition === 'left' ? "inline-block" : "block";
                this.dom.collapsibleBar.style.display = this.itemsPosition === 'left' ? "inline-block" : "none";
                this.dom.panelContainer.style.display = this.itemsPosition === 'left' ? "inline-block" : "block";
                $(this.dom.containerItems).css("float", this.itemsPosition === 'left' ? "left" : "");

                itemsDisplay = this.itemsPosition === 'left' ? "block" : "inline-block";

                for (i = 0; i < this.items.getSize(); i += 1) {
                    this.items.get(i).setDisplay(itemsDisplay);
                }

                if (this.itemsPosition == 'left') {
                    this.dom.containerItems.style.width = this.getWidth() * (this.percentageWidth / 100) + 'px';
                    this.dom.containerItems.style.height = this.getHeight() + "px";
                    this.dom.panelContainer.style.width = this.getWidth() - (this.getWidth() * (this.percentageWidth / 100)) + 'px';
                    this.dom.panelContainer.style.height = this.getHeight() + 'px';
                    this.dom.collapsibleBar.style.height = this.getHeight() + 'px';
                    jQuery(this.dom.listContainer).css("float", 'left');
                    for (i = 0; i < this.items.getSize(); i += 1) {
                        this.items.get(i).setWidth(this.getWidth() * (this.percentageWidth / 100) - 12);
                    }
                } else {
                    this.dom.containerItems.style.width = this.getWidth() + 'px';
                    this.dom.containerItems.style.height = '40px';
                    this.dom.panelContainer.style.width = this.getWidth() + 'px';
                    this.dom.panelContainer.style.height = this.getHeight() - 40 + 'px';
                    this.dom.collapsibleBar.style.height = this.getHeight() + 'px';
                    $(this.dom.listContainer).css("float", 'left');
                    for (i = 0; i < this.items.getSize(); i += 1) {
                        this.items.get(i).setWidth('auto');
                    }
                }
                break;
            case 'right' :
            case 'bottom' :
                $(this.dom.containerItems).prepend(this.dom.collapsibleBar);
                $(this.html).prepend(this.dom.panelContainer);
                this.dom.containerItems.style.display = this.itemsPosition === 'right' ? "inline-block" : "block";
                this.dom.listContainer.style.display = this.itemsPosition === 'right' ? "inline-block" : "block";
                this.dom.collapsibleBar.style.display = this.itemsPosition === 'right' ? "inline-block" : "none";
                this.dom.panelContainer.style.display = this.itemsPosition === 'right' ? "inline-block" : "block";
                $(this.dom.containerItems).css("float", this.itemsPosition === 'right' ? "right" : "");

                itemsDisplay = this.itemsPosition === 'right' ? "block" : "inline-block";

                for (i = 0; i < this.items.getSize(); i += 1) {
                    this.items.get(i).setDisplay(itemsDisplay);
                }

                if (this.itemsPosition == 'right') {
                    this.dom.containerItems.style.width = this.getWidth() * (this.percentageWidth / 100) + 'px';
                    this.dom.containerItems.style.height = this.getHeight() + "px";
                    this.dom.panelContainer.style.width = this.getWidth() - (this.getWidth() * (this.percentageWidth / 100)) + 'px';
                    this.dom.panelContainer.style.height = this.getHeight() + 'px';
                    this.dom.collapsibleBar.style.height = this.getHeight() + 'px';
                    $(this.dom.listContainer).css("float", 'right');
                    for (i = 0; i < this.items.getSize(); i += 1) {
                        this.items.get(i).setWidth(this.getWidth() * (this.percentageWidth / 100));
                    }
                } else {
                    this.dom.containerItems.style.width = this.getWidth() + 'px';
                    this.dom.containerItems.style.height = '40px';
                    this.dom.panelContainer.style.width = this.getWidth() + 'px';
                    this.dom.panelContainer.style.height = this.getHeight() + 'px';
                    this.dom.collapsibleBar.style.height = this.getHeight() + 'px';
                    $(this.dom.listContainer).css("float", 'none');
                    for (i = 0; i < this.items.getSize(); i += 1) {
                        this.items.get(i).setWidth('auto');
                    }
                }
                break;
        }
        return this;
    };
    /**
     * Create html for each item to container
     * @return {Object} the current object html
     */
    TabPanel.prototype.createHTML = function () {
        var containerItems,
            panelContainer,
            collapsibleBar,
            listContainer,
            i;

        if (this.html) {
            return html;
        }
        this.html = PMUI.createHTMLElement('div');
        this.html.className = 'pmui-tabpanel';
        this.html.id = this.id;
        containerItems = PMUI.createHTMLElement('div');
        listContainer = PMUI.createHTMLElement('ul');
        collapsibleBar = PMUI.createHTMLElement('div');
        panelContainer = PMUI.createHTMLElement('div');

        containerItems.className = "pmui-tabpanel-tabs_container";
        listContainer.className = "pmui-tabpanel-tabs";
        collapsibleBar.className = "pmui-collapsibleBar";
        panelContainer.className = "pmui-tabpanel-container";

        this.dom.containerItems = containerItems;
        this.dom.listContainer = listContainer;
        this.dom.collapsibleBar = collapsibleBar;
        this.dom.panelContainer = panelContainer;
        this.dom.panelContainer.style.overflow = 'auto';
        this.setItems(this.items.asArray().slice(0));

        this.dom.containerItems.appendChild(this.dom.listContainer);
        this.dom.containerItems.appendChild(this.dom.collapsibleBar);
        this.html.appendChild(this.dom.containerItems);
        this.html.appendChild(this.dom.panelContainer);

        if (this.itemsPosition == 'left' || this.itemsPosition == 'right') {
            this.setItemsPosition({position: this.itemsPosition, percentageWidth: this.percentageWidth});
        } else {
            this.setItemsPosition(this.itemsPosition);
        }
        this.applyStyle();
        if (this.eventsDefined) {
            this.defineEvents();
        }

        for (i = 0; i < this.items.getSize(); i += 1) {
            if (!this.getItems()[i].visible) {
                this.hideTab(i)
            }
        }
        return this.html;
    };
    /**
     * Executes children events defined
     * @chainable
     */
    TabPanel.prototype.defineEvents = function () {
        var that = this,
            i,
            auxPercentageWidth;

        this.removeEvents().eventsDefined = true;
        if (this.dom.collapsibleBar && this.collapsible) {
            this.addEvent('click').listen(this.dom.collapsibleBar, function (e) {
                e.preventDefault();
                e.stopPropagation();
                auxPercentageWidth = that.percentageWidth;
                if (that.collapsible) {
                    that.setItemsPosition({position: that.itemsPosition, percentageWidth: 9});
                    that.collapsible = false;
                    that.style.addClasses(['pmui-collipsable']);
                } else {
                    that.setItemsPosition({position: that.itemsPosition, percentageWidth: auxPercentageWidth});
                    that.style.removeClasses(['pmui-collipsable']);
                    that.collapsible = true;

                }
                that.percentageWidth = auxPercentageWidth;

                if (typeof that.onTabClick == 'function') {

                }
            });
            for (i = 0; i < that.items.getSize(); i += 1) {
                that.items.get(i).defineEvents();
            }
        }
        return this;
    };

    TabPanel.prototype.setWidth = function (width) {
        TabPanel.superclass.prototype.setWidth.call(this, width);
        this.updatePosition();
        return this;
    };

    TabPanel.prototype.setHeight = function (height) {
        TabPanel.superclass.prototype.setHeight.call(this, height);
        this.updatePosition();
        return this;
    };

    /**
     * hides a TabItem, if hidden the last TabItem passes focus to the previous,
     * otherwise the selection will move to the next always
     * @param  {Number} index  Is the selection of the TabItem that is concealed,
     * must be within the range of the length of existing items
     * @param  {[type]} current [when hidden a tabItem, can fix focus in other tabItem]
     * @chainable
     */
    TabPanel.prototype.hideTab = function (index, current) {
        var item,
            currentTab,
            i,
            len;

        for (i = 0; i < this.getItems().length; i += 1) {
            if (i === index) {
                item = this.getItems()[i];
            }
        }
        if (!current) {
            if (item.selected) {
                len = index - 1;
                while (len > -1) {
                    if (this.getItems()[len].visible === true) {
                        currentTab = this.getItems()[len];
                        len = -1;
                    } else {
                        len--;
                    }
                }
                if (!currentTab) {
                    i = index + 1;
                    while (i < this.items.getSize()) {
                        if (this.getItems()[i].visible === true) {
                            currentTab = this.getItems()[i];
                            i = this.items.getSize();
                        } else {
                            i++;
                        }
                    }
                }
            }
        } else {
            currentTab = this.getItems()[current];
        }

        item.setVisible(false);
        item.panel.setVisible(false);
        if (currentTab) {
            currentTab.select();
        }
        return this;
    }
    /**
     * tab shows a hidden item
     * @param  {Number} index Este valor es necesario para mostrar TabItem, debe estar dentro de un rango de la longitud de los elementos
     * @chainable
     */
    TabPanel.prototype.showTab = function (index) {

        if (typeof index == 'number') {
            if (!this.getItems()[index].visible) {
                this.getItems()[index].setVisible(true);
                this.getItems()[index].panel.setVisible(true);
            }
        }
        return this;
    };


    TabPanel.prototype.itemClick = function (index) {
        $($(this.dom.listContainer).find('a')[index]).trigger('click');
        return this;
    };

    if (typeof exports != 'undefined') {
        module.exports = TabPanel;
    }
    PMUI.extendNamespace("PMUI.panel.TabPanel", TabPanel);
}());