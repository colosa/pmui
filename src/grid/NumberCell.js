(function () {
    var NumberCell = function (settings) {
        NumberCell.superclass.call(this, settings);

        NumberCell.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlCell', NumberCell);

    NumberCell.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 524288,
            mask: ["###,###.###"],
            onKeyUp: null,
            content: ''
        };

        jQuery.extend(true, defaults, settings);
        this.setPlaceholder(defaults.placeholder)
            .setMaxLength(defaults.maxLength)
            .setOnKeyUpHandler(defaults.onKeyUp)
            .setContent(defaults.content)
            .setMask(defaults.mask);
    };

    NumberCell.prototype.setOnKeyUpHandler = function (handler) {
        this.controls[0].setOnKeyUpHandler(handler);
        return this;
    };

    NumberCell.prototype.setPlaceholder = function (placeholder) {
        this.controls[0].setPlaceholder(placeholder);
        return this;
    };

    NumberCell.prototype.setMask = function (mask) {
        this.controls[0].setMask(mask);
        return this;
    };

    NumberCell.prototype.setMaxLength = function (maxLength) {
        this.controls[0].setMaxLength(maxLength);
        return this;
    };

    NumberCell.prototype.setReadOnly = function (value) {
        this.controls[0].setReadOnly(value);
        return this;
    };

    NumberCell.prototype.initControls = function () {
        this.controls[0] = new PMUI.control.NumberControl();
        return this;
    };

    NumberCell.prototype.setContent = function (content) {
        return this.setValuesToControls(content);
    };

    NumberCell.prototype.setValuesToControls = function (values) {
        if (this.controls.length) {
            this.controls[0].setValue(values);
            this.content = this.controls[0].getValue();
        }
        return this;
    };

    NumberCell.prototype.getPlaceholder = function () {
        return this.controls[0].getPlaceholder();
    };
    /**
     * [isReadOnly description]
     * @return {Boolean} [description]
     */
    NumberCell.prototype.isReadOnly = function () {
        return this.controls[0].isReadOnly();
    };
    /**
     * Switches on/off the value trimming for the field's value when it loses focus.
     * @param {Boolean} trimOnBlur Send true for switch on or false to switch off.
     * @chainable
     */
    PMUI.extendNamespace('PMUI.grid.NumberCell', NumberCell);

    if (typeof exports !== 'undefined') {
        module.exports = NumberCell;
    }
}());