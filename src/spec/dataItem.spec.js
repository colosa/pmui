//***********************other class******************************************************//
describe('PMUI.data.DataItem', function () {
	var a, b;
	describe('class behavior', function () {
		it('[US-23,a]Should set the key for the DataItem object', function () {
			a = new PMUI.data.DataItem();
			expect(a.key).not.toBeDefined();
			a.setKey("key1");
			expect(a.key).toBeDefined();
			expect(a.key === "key1").toBeTruthy();
		});
		it('[US-23,a]Should set the value for the DataItem object', function () {
			a = new PMUI.data.DataItem();
			expect(a.val).not.toBeDefined();
			a.setValue("val1");
			expect(a.val).toBeDefined();
			expect(a.val === "val1").toBeTruthy();
		});
		it('[US-23,a]Should get the key for the DataItem object', function () {
			a = new PMUI.data.DataItem();
			expect(a.getKey()).not.toBeDefined();
			a.setKey("key1");
			expect(a.getKey()).toBeDefined();
			expect(a.getKey() === "key1").toBeTruthy();
		});
		it('[US-23,a]Should get the value for the DataItem object', function () {
			a = new PMUI.data.DataItem();
			expect(a.getValue()).not.toBeDefined();
			a.setValue("val1");
			expect(a.getValue()).toBeDefined();
			expect(a.getValue() === "val1").toBeTruthy();
		});
		it('[US-23,a]Should set and get the key and the value for the DataItem object', function () {
			a = new PMUI.data.DataItem();
			expect(a.getKey()).not.toBeDefined();
			expect(a.getValue()).not.toBeDefined();
			a.set("key1","val1");
			expect(a.get()).toBeDefined();
			expect(a.get().key1 === "val1").toBeTruthy();
		});
	});
});