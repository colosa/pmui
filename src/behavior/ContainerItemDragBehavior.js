(function () {
    /**
     * @class PMUI.behavior.ContainerItemDragBehavior
     * @extends PMUI.behavior.ContainerItemBehavior
     * Class that encapsulates the drag behavior for the items of a {@link PMUI.core.Container Container}. The object
     * that applies this behavior will be able to be dragged to another Containers that are applying dropping behaviors
     * ({@link PMUI.behavior.ContainerItemDropBehavior drop},
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop},
     * {@link PMUI.behavior.ContainerItemDropSortBehavior dropsort},
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}).
     *
     * Since this class is created through the
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "drag" or set it
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: 'Drop Here!'
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("drag");
     *      tree2.setBehavior("drop");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDragBehavior = function (settings) {
        ContainerItemDragBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemBehavior', ContainerItemDragBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDragBehavior.prototype.updateBehaviorAvailability = function () {
        this.targetHTML.draggable(this.disabled ? 'disable' : 'enable');
        if (this.disabled) {
            this.detachOnDraggableMouseEvents();
        } else {
            this.attachOnDraggableMouseEvents();
        }
        return this;
    };
    /**
     * Returns the items from the {@link PMUI.behavior.ContainerItemDragBehavior#property-targetObject targetObject}
     * related to the behavior.
     * @return {Object} A jQuery object.
     * @private
     */
    ContainerItemDragBehavior.prototype.getTargetObjectItems = function () {
        var $html = $(null),
            i,
            item,
            items,
            targetObject = this.targetObject;
        items = targetObject.getBehavioralItems();
        for (i = 0; i < items.length; i += 1) {
            item = items[i].getHTML();
            $html = $html.add(item);
        }
        return $html;
    };
    /**
     * @inheritdoc
     */
    ContainerItemDragBehavior.prototype.onDropOut = function (item, targetObject, index) {
        var aux;
        ContainerItemDragBehavior.superclass.prototype.onDropOut.apply(this, arguments);
        //The next lines avoid an uncontrolable error generated on the jquery ui's stop callback
        //when it try to access certain data that will be undefined because of the destruction of the
        //drag behavior when it is added to the new container in which it was dropped on.
        //What it does is add some data in the jquery data of the object's html.
        this.draggedObject = item;
        aux = jQuery(this.draggedObject.html).data('ui-draggable');
        if (!aux) {
            aux = {};
            jQuery(this.draggedObject.html).data('ui-draggable', aux);
        }
        if (!aux.sortables) {
            aux.sortables = [];
        }
        if (!aux.options) {
            aux.options = {};
        }
        return this;
    };
    /**
     * Attaches the behavior to the {@link PMUI.behavior.ContainerItemDragBehavior#property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragBehavior.prototype.attachBehavior = function (index) {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject,
            behaviorAttached;
        if (index !== undefined) {
            $html = $(this.targetObject.items.get(index).getHTML());
            behaviorAttached = this.targetObject.items.get(index).behaviorObject.behaviorAttached;
        } else {
            $html = this.getTargetObjectItems();
            behaviorAttached = this.behaviorAttached;
        }
        if (!behaviorAttached) {
            if (index !== undefined) {
                $html = $(this.targetObject.items.get(index).getHTML());
            } else {
                $html = this.getTargetObjectItems();
            }
            this.targetHTML = $html;
            $html.draggable({
                appendTo: document.body,
                connectToSortable: '.pmui-containeritembehavior-sort',
                helper: 'clone',
                revert: "invalid",
                scope: this.scope,
                drag: this.onDrag(),
                start: this.onStart(),
                stop: this.onStop(),
                items: this.draggableItems,
                handle: this.handle || false,
                zIndex: 1000
            });
            this.attachOnDraggableMouseEvents();
            if (index !== undefined) {
                this.targetObject.items.get(index).behaviorObject.behaviorAttached = true;
                this.targetObject.items.get(index).behaviorObject.updateBehaviorAvailability();
            } else {
                ContainerItemDragBehavior.superclass.prototype.attachBehavior.call(this);
            }
        }
        return this;
    };
    /**
     * Detaches the behavior from the
     * {@link PMUI.behavior.ContainerItemDragBehavior#property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragBehavior.prototype.detachBehavior = function () {
        if (this.behaviorAttached) {
            try {
                this.targetHTML.draggable('destroy');
                this.detachOnDraggableMouseEvents();
            } catch (e) {
            }
        }
        return ContainerItemDragBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDragBehavior', ContainerItemDragBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDragBehavior;
    }
}());