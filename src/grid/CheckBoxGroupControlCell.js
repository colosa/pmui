(function () {
    var CheckBoxGroupControlCell = function (settings) {
        CheckBoxGroupControlCell.superclass.call(this, settings);
        /**
         * @property {String} controlPositioning The direction for the options to be added in the field.
         * @readonly
         */
        this.controlPositioning = null;
        /**
         * @property {Number} maxDirectionOptions The max number of options that can be in the current options
         * direction.
         * @readonly
         */
        this.maxDirectionOptions = null;
        /**
         * The status if the controls to be saved when the field is switch between enabled/disabled.
         * @type {Object}
         * @private
         */
        this.auxControlStates = {};
        CheckBoxGroupControlCell.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlCell', CheckBoxGroupControlCell);

    CheckBoxGroupControlCell.prototype.init = function (settings) {
        var defaults = {
            options: [],
            controlPositioning: "vertical",
            maxDirectionOptions: 1,
            value: '[]'
        };

        jQuery.extend(true, defaults, settings);
        this.setOptions(defaults.options);
        this.setMaxDirectionOptions(defaults.maxDirectionOptions);
        this.setControlPositioning(defaults.controlPositioning);
    };


    CheckBoxGroupControlCell.prototype.enable = function () {
        var key,
            i,
            controlsLength = this.controls.length,
            controls = this.controls;

        CheckBoxGroupControlCell.superclass.prototype.enable.call(this);
        this.disabled = false;
        for (i = 0; i < controlsLength; i += 1) {
            controls[i].disable(this.auxControlStates[controls[i].id]);
        }
        return this;
    };

    CheckBoxGroupControlCell.prototype.setMaxDirectionOptions = function (max) {
        if (typeof max === 'number') {
            this.maxDirectionOptions = Math.floor(max);
            if (this.html) {
                this.setControlPositioning(this.controlPositioning);
            }
        } else {
            throw new Error("setMaxDirectionOptions(): it only accepts number values.");
        }

        return this;
    };

    CheckBoxGroupControlCell.prototype.setControlPositioning = function (positioning) {
        var errorMessage = "The setControlPositioning() method only accepts \"horizontal\" or \"vertical\" as value.",
            table,
            tbody,
            cell,
            row,
            i,
            column,
            rowIndex;

        if (typeof positioning === 'string') {
            if (!(positioning === 'horizontal' || positioning === 'vertical')) {
                return this;
            }
            this.controlPositioning = positioning;
            if (this.html && this.controls) {
                for (i = 0; i < this.controls.length; i += 1) {
                    jQuery(this.controls[i].getHTML()).detach();
                }
                $(this.contentSpan).empty();
                table = PMUI.createHTMLElement("table");
                table.className = 'pmui-field-control-table';
                tbody = PMUI.createHTMLElement("tbody");
                if (positioning === 'horizontal') {
                    row = PMUI.createHTMLElement("tr");
                    for (i = 0; i < this.controls.length; i += 1) {
                        cell = PMUI.createHTMLElement('td');
                        this.controls[i].getHTML();
                        this.controls[i].control.tabIndex = i;
                        cell.appendChild(this.controls[i].getHTML());
                        row.appendChild(cell);
                        if (this.maxDirectionOptions > 0 && (i + 1) % this.maxDirectionOptions === 0) {
                            tbody.appendChild(row);
                            row = PMUI.createHTMLElement("tr");
                        }
                    }
                    cell.style.textAlign = "start";
                    tbody.appendChild(row);
                } else {
                    column = 0;
                    for (i = 0; i < this.controls.length; i += 1) {
                        cell = PMUI.createHTMLElement('td');
                        this.controls[i].getHTML();
                        this.controls[i].control.tabIndex = i;
                        cell.appendChild(this.controls[i].getHTML());
                        rowIndex = this.maxDirectionOptions === 0 ? i : i % this.maxDirectionOptions;

                        row = jQuery(tbody).find('tr').eq(rowIndex).get(0);
                        if (!row) {
                            row = PMUI.createHTMLElement('tr');
                            tbody.appendChild(row);
                        }
                        cell.style.textAlign = "start";
                        row.appendChild(cell);
                    }
                }
                table.appendChild(tbody);
                this.contentSpan.appendChild(table);
            }
        }

        return this;
    };

    CheckBoxGroupControlCell.prototype.setOptions = function (options) {
        var i = 0;

        this.content = [];
        this.content = JSON.stringify(this.content);

        if (jQuery.isArray(options)) {
            for (i = 0; i < options.length; i += 1) {
                this.addOption(options[i]);
            }
        } else {
            throw new Error("setOptions(): the supplied argument must be an array.");
        }

        return this;
    };

    CheckBoxGroupControlCell.prototype.addOption = function (option) {
        var newOption;

        newOption = new PMUI.control.SelectableControl(jQuery.extend(true, option, {
            mode: 'checkbox',
            name: this.controls.length + 1,
            selected: option.selected
        }));

        if (newOption.isSelected()) {
            this.content = JSON.parse(this.content);
            this.content.push(newOption.getValue());
            this.content = JSON.stringify(this.content);
        }
        if (this.eventsDefined) {
            newOption.setOnChangeHandler(this.onChangeHandler()).getHTML();
            newOption.defineEvents();
        }
        this.auxControlStates[newOption.id] = newOption.disabled;
        this.controls.push(newOption);
        this.setControlPositioning(this.controlPositioning);

        return this;
    };

    CheckBoxGroupControlCell.prototype.removeOption = function (item) {
        var itemToRemove,
            i;

        if (item instanceof PMUI.control.SelectableControl) {
            for (i = 0; i < this.controls.length; i += 1) {
                if (this.controls[i] === item) {
                    itemToRemove = i;
                    break;
                }
            }
        } else if (typeof item === 'string') {
            for (i = 0; i < this.controls.length; i += 1) {
                if (this.controls[i].id === item) {
                    itemToRemove = this.controls[i];
                    break;
                }
            }
        } else {
            itemToRemove = item;
        }
        if (typeof itemToRemove === 'number') {
            itemToRemove = this.controls[itemToRemove];
            delete this.auxControlStates[itemToRemove.id];
            jQuery(itemToRemove.html).detach();
            this.controls.splice(itemToRemove, 1);
            this.setControlPositioning(this.controlPositioning);
        }
        return this;
    };

    CheckBoxGroupControlCell.prototype.clearOptions = function () {
        while (this.controls.length) {
            this.removeOption(0);
        }

        return this;
    };

    CheckBoxGroupControlCell.prototype.updateContentFromControls = function () {
        var value = [],
            i;

        for (i = 0; i < this.controls.length; i += 1) {
            if (this.controls[i].isSelected()) {
                value.push(this.controls[i].getValue());
            }
        }

        this.content = JSON.stringify(value);
        return this;
    };

    CheckBoxGroupControlCell.prototype.evalRequired = function () {
        var valid = true,
            value;

        value = JSON.parse(this.getContent());
        if (this.required && value.length === 0) {
            this.showMessage(this.requiredMessage, "error");
            valid = false;
        } else {
            this.hideMessage();
        }

        return valid;
    };


    CheckBoxGroupControlCell.prototype.disableOption = function (value) {

        var index = -1,
            i;

        if (this.html) {
            if (typeof value === 'string') {

                for (i = 0; i < this.controls.length; i += 1) {
                    if (this.controls[i].label === value) {
                        index = i;
                    }
                }
            } else {
                index = value;
            }
            if (index === -1) {
                throw new Error('the value send is not part of group CheckBox');
            } else {
                this.controls[index].disable(true);
            }
        }
        return this;
    };

    CheckBoxGroupControlCell.prototype.enableOption = function (value) {
        var index = -1,
            i;

        if (this.html) {
            if (typeof value === 'string') {

                for (i = 0; i < this.controls.length; i += 1) {
                    if (this.controls[i].label === value) {
                        index = i;
                    }
                }
            } else {
                index = value;
            }
            if (index === -1) {
                throw new Error('the value send is not part from group CheckBox');
            } else {

                this.controls[index].disable(false);
            }
        }
        return this;
    };

    CheckBoxGroupControlCell.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }

        CheckBoxGroupControlCell.superclass.prototype.createHTML.call(this)

        this.setControlPositioning(this.controlPositioning);

        return this.html;
    };

    CheckBoxGroupControlCell.prototype.setValuesToControls = function (value) {
        var i,
            j,
            values,
            controls,
            controlsLength;

        if (value) {
            try {
                values = JSON.parse(value);
            } catch (e) {
                values = '[]';
            }

            controlsLength = (controls = this.controls.slice(0)).length;
            for (i = 0; i < controlsLength; i += 1) {
                controls[i].deselect();
            }
            for (i = 0; i < values.length; i += 1) {
                for (j = 0; j < controlsLength; j += 1) {
                    if (controls[j].getValue() === values[i]) {
                        controls[j].select();
                        controls.splice(j, 1);
                        controlsLength -= 1;
                        j -= 1;
                    }
                }
            }
            this.updateContentFromControls();
        }

        return this;
    };

    PMUI.extendNamespace('PMUI.grid.CheckBoxGroupControlCell', CheckBoxGroupControlCell);
}());