(function () {
    /**
     * @class PMUI.draw.BehavioralElement
     * Class that encapsulates the behavior of all elements that have container and
     * drop behaviors attached to them.
     * since this class inherits from {@link PMUI.draw.Core}, then the common behaviors
     * and properties for all elements in the designer are also part of this class
     * The purpose of this class is to encapsulate behaviors related to drop and
     * containment of elements, so it shouldn't be instantiated, we should
     * instantiate the elements that inherit from this class instead.
     *
     *          //i.e
     *          //we will set the behaviors that are related only to this class
     *          var shape = new PMUI.draw.CustomShape({
     *          //we can set different types of containers here and the factory
     *          //will do all the work
     *              container : "regular",
     *              drop : {
     *              //type specifies the drop behavior we want, again we just need
     *              // to pass a string
     *               type : "container",
     *                //selectors are the css selectors that this element will
     *                //accept to be dropped
     *               selectors : [".firstselector",".secondselector"],
     *              //overwrite is an option to override previous and default
     *              //selectors
     *               overwrite : false
     *              }
     *          });
     *
     *
     * @extends PMUI.draw.Core
     *
     * @constructor Creates a new instance of this class
     * @param {Object} options
     * @cfg {String} [container="nocontainer"] the type of container behavior
     * we want for an object, it can be regular,or nocontainer, or any other class
     * that extends the {@link PMUI.behavior.ContainerBehavior} class, but also note that we would
     * need to override the factory for container behaviors located in this class.
     * @cfg {Object} [drop={
     *     drop : "nodrop",
     *     selectors : [],
     *     overwrite : false
     * }] Object that contains the options for the drop behavior we want an object
     * to have, we can, assign type which can be container, connection,
     * connectioncontainer, or no drop. As with the container behavior we can extend
     * the behaviors and factory for this functionality.
     * We also have selectors that specify the selectors the drop behavior will
     * accept and the overwrite feature
     */
    var BehavioralElement = function (options) {
        BehavioralElement.superclass.call(this, options);
        /**
         * Determines the Behavior object that this object has
         * @property {PMUI.behavior.ContainerBehavior}
         */
        this.containerBehavior = null;
        /**
         * Determines the Drop Behavior of the element
         * @type {PMUI.behavior.DropBehavior}
         */
        this.dropBehavior = null

        /**
         * List of the children
         * @property {*}
         */
        this.children = null;
        BehavioralElement.prototype.init.call(this, options);
    };
    PMUI.inheritFrom('PMUI.draw.Core', BehavioralElement);
    /**
     * Type of the all instances of this class
     * @property {String}
     */
    BehavioralElement.prototype.type = "BehavioralElement";
    /**
     * Instance initializer which uses options to extend the default config options.
     * The default options are container: nocontainer, and drop: nodrop
     * @param {Object} options
     */
    BehavioralElement.prototype.init = function (options) {
        var defaults = {
            drop: "nodrop",
            container: "nocontainer"
        };
        $.extend(true, defaults, options);
        var defaults = {
            drop: {
                type: "nodrop",
                selectors: [],
                overwrite: false
            },
            container: "nocontainer"
        };
        $.extend(true, defaults, options);
        this.setDropBehavior(defaults.drop.type, defaults.drop.selectors,
            defaults.drop.overwrite);
        this.setContainerBehavior(defaults.container);
        this.children = new PMUI.util.ArrayList();
    };
    BehavioralElement.prototype.setDropBehavior = function (behavior, selectors,
                                                            overwrite) {
        this.dropBehavior = this.dropBehaviorFactory(behavior, selectors);
        this.dropBehavior.setSelectors(selectors, overwrite);
        if (this.html && this.dropBehavior) {
            this.dropBehavior.attachDropBehavior(this);

            // update the saved object
            // added by mauricio to save the drop behavior of this shape
            $.extend(true, this.savedOptions.drop, {
                type: behavior,
                overwrite: overwrite
            });

            if (selectors && selectors.hasOwnProperty('length')) {
                this.dropBehavior.updateSelectors(this, selectors, overwrite);
                // update the saved object
                // added by mauricio to save the drop behavior of this shape
                $.extend(true, this.savedOptions.drop, {
                    selectors: selectors
                });
            }
        }
        return this;
    };
    /**
     * Factory of drop behaviors. It uses lazy instantiation to create instances of
     * the different drop behaviors
     * @param {String} type Type of drop behavior we want to assign to an object,
     * it can be nodrop, container, connection or connectioncontainer
     * @param {Array} selectors Array containing the css selectors that the drop
     * behavior will accept
     * @return {DropBehavior}
     */
    BehavioralElement.prototype.dropBehaviorFactory = function (type, selectors) {
        if (type === "nodrop") {
            if (!this.noDropBehavior) {
                this.noDropBehavior = new PMUI.behavior.NoDropBehavior(selectors);
            }
            return this.noDropBehavior;
        }
        if (type === "container") {
            if (!this.containerDropBehavior) {
                this.containerDropBehavior = new PMUI.behavior.ContainerDropBehavior(selectors);
            }
            return this.containerDropBehavior;
        }
        if (type === "connection") {
            if (!this.connectionDropBehavior) {
                this.connectionDropBehavior = new PMUI.behavior.ConnectionDropBehavior(selectors);
            }
            return this.connectionDropBehavior;
        }
        if (type === "connectioncontainer") {
            if (!this.connectionContainerDropBehavior) {
                this.connectionContainerDropBehavior =
                    new PMUI.behavior.ConnectionContainerDropBehavior(selectors);
            }
            return this.connectionContainerDropBehavior;
        }
    };
    /**
     * Sets the container behavior of an object, using the same options as
     * the factory
     * @param {String} behavior the container behavior we want the factory to
     * assign, it can be regular, or nocontainer
     * @chainable
     */
    BehavioralElement.prototype.setContainerBehavior = function (behavior) {
        // update the saved object
        // added by mauricio to save the container behavior of this
        $.extend(true, this.savedOptions, {container: behavior});
        this.containerBehavior = this.containerBehaviorFactory(behavior);
        return this;
    };

    /**
     * Factory of container behaviors. It uses lazy instantiation to create
     * instances of the different container behaviors
     * @param {String} type An string that specifies the container behavior we want
     * an instance to have, it can be regular or nocontainer
     * @return {ContainerBehavior}
     */
    BehavioralElement.prototype.containerBehaviorFactory = function (type) {
        if (type === "regular") {
            if (!this.regularContainerBehavior) {
                this.regularContainerBehavior = new PMUI.behavior.RegularContainerBehavior();
            }
            return this.regularContainerBehavior;

        }
        if (!this.noContainerBehavior) {
            this.noContainerBehavior = new PMUI.behavior.NoContainerBehavior();
        }
        return this.noContainerBehavior;
    };
    /**
     * Updates the children positions of a container given the x and y difference
     * @param {Number} diffX x difference
     * @param {Number} diffY y difference
     * @chainable
     * // TODO make this method recursive
     */
    BehavioralElement.prototype.updateChildrenPosition = function (diffX, diffY) {
        var children = this.getChildren(),
            child,
            i,
            updatedChildren = [],
            previousValues = [],
            newValues = [];
        for (i = 0; i < children.getSize(); i += 1) {
            child = children.get(i);
            if ((diffX !== 0 || diffY !== 0) && !this.canvas.currentSelection.contains(child)) {
                updatedChildren.push(child);
                previousValues.push({
                    x: child.x,
                    y: child.y
                });
                newValues.push({
                    x: child.x + diffX,
                    y: child.y + diffY
                });
            }
            child.setPosition(child.x + diffX, child.y + diffY);
        }
        if (updatedChildren.length > 0) {
            this.canvas.triggerPositionChangeEvent(updatedChildren, previousValues,
                newValues);
        }
        return this;
    };
    /**
     * Returns whether the instance is a container or not
     * @return {Boolean}
     */
    BehavioralElement.prototype.isContainer = function () {
        return this.containerBehavior &&
            this.containerBehavior.type !== "NoContainerBehavior";
    };

    /**
     * Encapsulates the functionality of adding an element this element according
     * to its container behavior
     * @param {PMUI.draw.Shape} shape Shape we want to add to the element
     * @param {Number} x x coordinate where the shape will be positionated relative
     * to this element
     * @param {Number} y y coordinate where the shape will be positionated relative
     * to this element
     * @param {Boolean} topLeftCorner determines if the drop position should be
     * calculated from the top left corner of the shape or, from its center
     * @chainable
     */
    BehavioralElement.prototype.addElement = function (shape, x, y,
                                                       topLeftCorner) {
        this.containerBehavior.addToContainer(this, shape, x, y, topLeftCorner);
        return this;
    };
    /**
     * Encapsulates the functionality of removing an element this element according
     * to its container behavior
     * @param {PMUI.draw.Shape} shape shape to be removed from this element
     * @chainable
     */
    BehavioralElement.prototype.removeElement = function (shape) {
        this.containerBehavior.removeFromContainer(shape);
        return this;
    };
    /**
     * Swaps a shape from this container to a different one
     * @param {PMUI.draw.Shape} shape shape to be swapped
     * @param {PMUI.draw.BehavioralElement} otherContainer the other container the shape will
     * be swapped to
     * @param {Number} x x coordinate where the shape will be positionated relative
     * to this element
     * @param {Number} y y coordinate where the shape will be positionated relative
     * to this element
     * @param {Boolean} topLeftCorner determines if the drop position should be
     * calculated from the top left corner of the shape or, from its center
     * @chainable
     */
    BehavioralElement.prototype.swapElementContainer = function (shape,
                                                                 otherContainer, x,
                                                                 y, topLeftCorner) {
        var newX = !x ? shape.getX() : x,
            newY = !y ? shape.getY() : y;
        shape.changedContainer = true;
        this.removeElement(shape);
        otherContainer.addElement(shape, newX, newY, topLeftCorner);
        return this;
    };
    /**
     * Returns the list of children belonging to this shape
     * @returns {PMUI.util.ArrayList}
     */
    BehavioralElement.prototype.getChildren = function () {
        return this.children;
    };
    /**
     * Updates the dimensions and position of this shape (note: 'this' is a shape)
     * @param {Number} margin the margin for this element to consider towards the
     * shapes near its borders
     * @chainable
     */
    BehavioralElement.prototype.updateDimensions = function (margin) {
        // update its size (if an child grew out of the shape)
        // only if it's not the canvas
        if (this.family !== 'Canvas') {
            this.updateSize(margin);
            this.refreshConnections();
            // updates JQueryUI's options (minWidth and minHeight)
            PMUI.behavior.ResizeBehavior.prototype.updateResizeMinimums(this);
            BehavioralElement.prototype.updateDimensions.call(this.parent, margin);
        }
        return this;
    };

    /**
     * Sets the selectors of the current drop behavior
     * @param {Array} selectors new css selectors for the drop behavior
     * @param {Boolean} overwrite determines whether the default selectors will
     * be erased
     * @chainable
     */
    BehavioralElement.prototype.setDropAcceptedSelectors = function (selectors,
                                                                     overwrite) {
        if (selectors && selectors.hasOwnProperty('length')) {
            this.drop.updateSelectors(this, selectors, overwrite);
        }
        return this;
    };
    /**
     * Attach the drop behavior to the element, if there is such
     * @chainable
     */
    BehavioralElement.prototype.updateBehaviors = function () {
        if (this.dropBehavior) {
            this.dropBehavior.attachDropBehavior(this);
            this.dropBehavior.updateSelectors(this);
        }
        return this;
    };
    /**
     * Stringifies the container and drop behavior of this object
     * @return {Object}
     */
    BehavioralElement.prototype.stringify = function () {
        var inheritedJSON = BehavioralElement.superclass.prototype.stringify.call(this),
            thisJSON = {
                container: this.savedOptions.container,
                drop: this.savedOptions.drop
            };
        $.extend(true, inheritedJSON, thisJSON);
        return inheritedJSON;
    };

    PMUI.extendNamespace('PMUI.draw.BehavioralElement', BehavioralElement);

    if (typeof exports !== 'undefined') {
        module.exports = BehavioralElement;
    }

}());
